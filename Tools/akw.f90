program a_k_w
!
!     This program calculates A(k,w).
!     To use it you need to have a self-energy on the real energy axis.
!
!     Written by A. Poteryaev <poter _at_ optics.imp.uran.ru>
!
  use hamiltonian_module
  use energy_module
  use parameter_module
  use impurity_module
  use iolib_module
  use units
  use lapack_module

  implicit none
  
  integer :: ierr, iuham = 2
  integer :: n1, n2
  integer :: is, isp, iom, itype, ikp, i

  character(32) :: fn
  character(128) :: ham_name

  complex(8) :: z
  complex(8), allocatable :: Vpot(:,:), g(:,:)
  
  real(8) :: deltak, dist
  real(8), allocatable :: akw(:,:,:,:)

!   Defines number of points in energy mesh

  call def_energy_params( ierr )
  if( ierr == 777 ) stop 'Wrong call in def_energy_params subroutine.'
  if( ierr  > 0 )   stop 'Cannot locate or read amulet.ini file (1).'

!   Define energy and time meshes
  call set_energy_mesh
  call use_real_mesh
  
!   Read initial data from amulet.ini file

  call read_ini( ierr )
  if( ierr == 777 ) stop 'Wrong call in read_ini subroutine.'
  if( ierr  > 0 )   stop 'Cannot locate or read amulet.ini file (2).'
  call setup_energy_units

!   Read Hamiltonian
  call hamiltonian_name( iuham, ham_name, ierr )
  if( ierr /= 0 )   stop 'Cannot locate hamiltonian file.' 
  call read_hamiltonian( iuham, ham_name, ierr )

  select case( ierr )
  case( -14001 )
    use_hmlt = .true.
    rhtm = 'TBLMTO'
  case( -14002 )
    use_hmlt = .true.
    rhtm = 'ESPRESSO'
  case( :-14003, -14000:0 )
    stop 'There is a problem with format of hamiltonian file.'
  case default
    stop 'Hamiltonian problem.'
  end select

!   Read impurity.ini files
  call read_impurity( ierr )

  if( ierr == 777 ) stop 'Wrong call in read_impurity subroutine.'
  if( ierr  > 0 )   stop 'Cannot locate or read one of impurity.ini files.'
 
!   Read self-energy
  if( use_sigma )then
    istart = 1
    call rw_sigma( ierr, 'r', 'real' )
    if( ierr /= 0 ) stop 'Cannot locate or read one of sigma files.'
  end if

!   Calculates A(k,w)

  allocate( akw(0:ndim,nkp,n_energy,nspin), Vpot(ndim,ndim), g(ndim,ndim) )
!
!    Calculation of local Green function
!
  do is = 1, nspin
    do iom = 1, n_energy
      z = zenergy(iom)
      if( nspin == 2 .and. abs(h_field) > 1.e-4 ) z = z - ( 1.5 - is )*h_field
!
!    Make "local potential": Sigma, Double counting, etc
!     
      Vpot = zero

      do itype = 1, n_imp_type
        do i = 1, impurity(itype)%n_imp
          n1 = impurity(itype)%hposition(i)
          n2 = n1 + impurity(itype)%nlm - 1

          isp = is
          if( impurity(itype)%magtype(i) == -1 ) isp = 3 - is
          Vpot(n1:n2,n1:n2) = impurity(itype)%Sigma(:,:,iom,isp)
        end do
      end do
!
!     Calculate G(k,z)
!    
      do ikp = 1, nkp
        call gk( g, h(:,:,ikp,is), Vpot, ndim, z )

        forall( i = 1:ndim )  akw(i,ikp,iom,is) = - aimag( g(i,i) ) / pi
        akw(0,ikp,iom,is) = sum( akw(1:ndim,ikp,iom,is), dim=1 )
      end do
      
    end do   ! n_energy
  end do     ! nspin
    
  deallocate( Vpot, g )
   
 ! do ikp = 1, nkp
 !   call write_function( mkfn('Akw',ikp), akw(:,ikp,:,:), prn_mesh, ierr, 'r' )
 ! end do


  do i = 1, ndim
    fn = mkfn('Akw',i)
    open( 73, file=fn, form='formatted', status='unknown', position='rewind' )
  
    deltak = 0.0
    do ikp = 1, nkp
 
      do iom = 1, n_energy
        write(73,"(2(1x,f12.7),200(1x,f10.5))") deltak, prn_mesh(iom), ( akw(i,ikp,iom,is), is=1,nspin )
      end do
      write(73,*)

      if( ikp < nkp )then
        dist   = sqrt( ( bk(1,ikp)-bk(1,ikp+1) )**2 + ( bk(2,ikp)-bk(2,ikp+1) )**2 +     &
                       ( bk(3,ikp)-bk(3,ikp+1) )**2  )
        deltak = deltak + dist
      end if
    end do
    close(73)

  end do

  open( 72, file='Akw_total.dat', form='formatted', status='unknown', position='rewind' )
  open( 74, file='arpes.gnu', form='formatted', status='unknown', position='rewind' )

  write(74,*) 'set terminal postscript color enhanced'
  write(74,*) 'set output '//achar(39)//'arpes.ps'//achar(39)
  write(74,*) 'set pm3d map'
  write(74,*) 'set nokey'
  write(74,*) 'set palette model RGB'
  write(74,*) 'set zeroaxis'
  write(74,*) 
  write(74,*) 'set palette defined (0  0.098 0.098 0.439, 6  0 0.75 1,  \'
  write(74,*) '  8  0 1 0, 10  1 1 0, 11.5   0.557 0.42 0.137, 14  1 1 1)'
  write(74,*) 
  write(74,*) 'set xtics( "{/Symbol G}" 0.0, \'


  deltak = 0.0
  do ikp = 1, nkp
 
    do iom = 1, n_energy
      write(72,"(2(1x,f12.7),200(1x,f10.5))") deltak, prn_mesh(iom), ( akw(0,ikp,iom,is), is=1,nspin )
    end do
    write(72,*)

    if( ikp < nkp )then
      dist   = dsqrt( ( bk(1,ikp)-bk(1,ikp+1) )**2 + ( bk(2,ikp)-bk(2,ikp+1) )**2 + ( bk(3,ikp)-bk(3,ikp+1) )**2  )
      deltak = deltak + dist

      if( dist < 1.e-12 )  write(74,*) ' "A1" ', deltak, ', \'

    end if

  end do

  write(74,*) ' "AN" ', deltak, ' )'
  write(74,*) 'set grid xtics'
  write(74,*) 'set xrange [0:', deltak, ']'
  write(74,*) 'set yrange [', prn_mesh(1), ':', prn_mesh(n_energy), ']'
  write(74,*)
  write(74,*) '# maxval for total (up,dn) ', maxval( akw(0,:,:,1) ), maxval( akw(0,:,:,nspin) )
  write(74,*) '# maxval for orbs  (up,dn) ', maxval( akw(1:ndim,:,:,1) ), maxval( akw(1:ndim,:,:,nspin) )
  write(74,*)
  write(74,*) 'set cbrange [0:20]'
  write(74,*) 
  write(74,*) 'splot '//achar(39)//'Akw_total.dat'//achar(39)//' u 1:2:3'

end program a_k_w
