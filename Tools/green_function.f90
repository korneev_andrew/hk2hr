program green_function
!======================================================================
!
!    Calculates Green function
!
!======================================================================
!
!   Input:
!    Hamiltonian file and(or) amulet.ini file
!   Output:
!    Gf_i,j.dat - files with (i,j) components of Green function
!
!   Remarks:    ___
!              \         C_i,n(k) C^+_n,j(k)
!   G_i,j(z) =  \  W(k)  -------------------
!              /___          z  - e_n(k)
!               n,k
!
!   W(k)   - weight of k-point
!   C      - eigenvectors
!   e      - eigenvalues
!
!======================================================================

  use hamiltonian_module
  use energy_module
  use parameter_module
  use lapack_module
  use iolib_module
  use units

  implicit none
  
  integer :: ierr, is, ikp, ie, i, j, k
  character(64) :: evry
  complex(8) :: z
  
  integer :: min_i, max_i, min_j, max_j, iuham = 2
  character(128) :: ham_name
  real(8) :: e(2)
  real(8), allocatable :: ek(:,:,:)
  complex(8), allocatable :: green(:,:,:,:)

!   Defines number of points in energy mesh

  call setup_energy_units

!   Read Hamiltonian
  call hamiltonian_name( iuham, ham_name, ierr )
  if( ierr /= 0 )   stop 'Cannot locate hamiltonian file.' 
  call read_hamiltonian( iuham, ham_name, ierr )

  select case( ierr )
  case( -14001 )
    use_hmlt = .true.
    if( rhtm == 'UNDEFINED' ) rhtm = 'TBLMTO'
  case( -14002 )
    use_hmlt = .true.
    if( rhtm == 'UNDEFINED' ) rhtm = 'ESPRESSO'
  case( :-14003, -14000:0 )
    stop 'Cannot locate or read Hamiltonian file.'
  case default
    rhtm = 'TBLMTO'
    use_hmlt = .false.
  end select

  min_i = 1
  min_j = 1
  max_i = ndim
  max_j = ndim
  write(6,200) ndim, nkp

    nspin = nsham

    write(6,"(/,' Would you like to calculate on the imaginary or real axes(default) [YES/no]',/)")
    read(5,"(a4)") evry
    evry = adjustl(evry)
  
    imenergy = .true.
    reenergy = .false.
    if( len_trim(evry) == 0 .or. scan(evry,'y') == 1 .or.  &
        scan(evry,'Y') == 1 .or. scan(evry,'/') == 1  )then
        reenergy = .true.
        imenergy = .false.
    end if

    if( reenergy )then
      emin = -20
      emax =  20
      write(6,"(/,' Enter emin and emax for Green function (default emin=-20, emax=20)',/)")
      read(5,"(a)") evry
      if( len_trim(evry) /= 0 )then
        evry = trim(adjustl(evry))
        read(evry,*,iostat=ierr) e
        if( ierr == 0 )then
          emin = e(1)
          emax = e(2)
        else
          read(evry,*,iostat=ierr) emin
        end if
      end if
      if( emin >= emax )then
        emin = -20
        emax =  20
      end if
    end if

    n_energy = 1000
    write(6,"(/,' Enter number of points (default N=1000)',/)")
    read(5,"(a)") evry
    if( len_trim(evry) /= 0 )then
      evry = trim(adjustl(evry))
      read(evry,*) n_energy
    end if
    if( n_energy <= 0 ) n_energy = 1000

    if( reenergy )then
      eta = 0.07
      write(6,"(/,' Enter small energy shift to imaginary plane (default eta=0.07)',/)")
      read(5,"(a)") evry
      if( len_trim(evry) /= 0 )then
        evry = trim(adjustl(evry))
        read(evry,*) eta
      end if
      if( eta <= 0 ) eta = 0.07
    else
      beta = 10
      write(6,"(/,' Enter inverse temperature (default beta=10)',/)")
      read(5,"(a)") evry
      if( len_trim(evry) /= 0 )then
        evry = trim(adjustl(evry))
        read(evry,*) beta
      end if
      if( beta <= 0 ) beta = 10
      write(6,"(/,' Enter Fermi energy (default mu=0)',/)")
      read(5,"(a)") evry
      if( len_trim(evry) /= 0 )then
        evry = trim(adjustl(evry))
        read(evry,*) xmu
      end if
    end if

    min_i = 1
    min_j = 1
    write(6,"(/,' Enter the coordinates of the upper left corner (default=[1,1])',/)")
    read(5,"(a)") evry
    if( len_trim(evry) /= 0 )then
      evry = trim(adjustl(evry))
      read(evry,*,iostat=ierr) e
      if( ierr == 0 )then
        min_i = nint(e(1))
        min_j = nint(e(2))
      else
        read(evry,*,iostat=ierr) min_i
      end if
      if( min_i <=0 ) min_i = 1
      if( min_j <=0 ) min_j = 1
    end if

    max_i = ndim
    max_j = ndim
    write(evry,*) max_i, max_j
    write(6,"(/,' Enter the coordinates of the lower right corner (default=[',i0,',',i0,'])',/)") ndim, ndim
    read(5,"(a)") evry
    if( len_trim(evry) /= 0 )then
      evry = trim(adjustl(evry))
      read(evry,*,iostat=ierr) e
      if( ierr == 0 )then
        max_i = nint(e(1))
        max_j = nint(e(2))
      else
        read(evry,*,iostat=ierr) e(1)
        max_i = nint(e(1))
      end if
      if( max_i < min_i .or. max_i > ndim ) max_i = ndim
      if( max_j < min_j .or. max_j > ndim ) max_j = ndim
    end if
  
!   Define energy and time meshes
  call set_energy_mesh
  if( imenergy )then
    call use_imaginary_mesh
    write(6,220) n_energy, beta, xmu
  else
    call use_real_mesh
    write(6,210) emin, emax, eta, n_energy, real(zenergy(2)-zenergy(1),8)
  end if

!  write(6,*) min_i, min_j, max_i, max_j
!
  allocate( ek(ndim,nkp,nspin) )
!  
!   Calculate eigenvalues and eigenvectors
!
  do is = 1, nspin
    do ikp = 1, nkp
      call eigenproblem( h(:,:,ikp,is), ek(:,ikp,is) )
    end do
  end do
  
  write(6,*) 'Eigenvalues and eigenvectors are found'
!
!   Calculate Green function
!
  allocate( green(ndim,ndim,n_energy,nspin) )
  green = zero

  do i = min_i, max_i
    do j = min_j, max_j

      do is = 1, nspin
        do ie = 1, n_energy
          z = zenergy(ie)

          do ikp = 1, nkp
            do k = 1, ndim
              green(i,j,ie,is) = green(i,j,ie,is) +       &
                 wtkp(ikp) * h(i,k,ikp,is) * conjg(h(j,k,ikp,is)) / ( z - ek(k,ikp,is) )
            end do
          end do

        end do
      end do

!      call write_function( mkfn( mkfn('Gf',i), j ), green(i,j,:,:), prn_mesh, ierr )

    end do 
  end do  

  call write_function( 'Gf', green, prn_mesh, ierr )

  write(6,"(/,' End of green_function program',/)")
!
!   Formats
!  
200 format( /,10x,' Green function calculation',//,                   &
            ' Hamiltonian dimension',15x,i5,/,' Number of k-points',6x,i5,/ )
210 format( ' Minimal energy for Green function',3x,f10.6,/,          &
            ' Maximal energy for Green function',3x,f10.6,/,          &
            ' Energy shift to imaginary plane',5x,f10.6,/,            &
            ' Number of points for Green function ',i5,/,             &
            ' Energy step',25x,f10.6 )
220 format( ' Number of Matsubara points',9x,i5,/,                    &
            ' Inverse temperature, beta',10x,f10.6,/,                 &
            ' Fermi energy',22x,f10.6)

end program green_function
