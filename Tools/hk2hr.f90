program hk2hr
!ssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss
!
!     The program converts H(k) (Hamiltonian in Reciprocal Dimension)
!     to H(r) (in  Direct Dimension).
!       
!
!ssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss
!
!     Input:
!     Hamiltonian file and system.am file    
!     
!     Output:
!     hoppings.out  
!
!ssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss

  use hamiltonian_module
  use parameter_module
   

  implicit none

  integer :: iuham = 2, ierr                                  !Unit specifier and error status
  character(128) :: ham_name,system_name = 'system.am', scanner
                                                              !scanner is needed for reading user's typed symbols
  integer :: i, j, k, natoms, i_central, nblocks, dim, stopmark=0, stopmark1=0, nnbrs=0, ntransl, iatom, is, m1, m2, ikp
                                        !nblocks - number of matrix blocks for the current tom in Hamiltonian matrix
  real(8) :: brav_lattice(3,3), transl_vec(3), new_pos(3)     !new_pos - position of the atom transferred to translation vector
  real(8) :: alat, radius, distance, k_r                      !distance = distance from central atom to it's neighbour
  logical :: tag_exist
  real(8), allocatable :: at_position(:,:)
  complex(8), allocatable :: H_r(:,:,:)
  character(4), allocatable :: at_name(:), bas_at_name(:), l_sym(:)     ! bas_at_name - name of the atom after %basis tag
  integer,allocatable :: atom_num(:), block_dim(:), block_start(:) 
  complex(8) :: exp_factor

!       Checking out if system.am exists

  open(iuham, file = system_name, form = 'formatted', status = 'old', iostat = ierr, action = 'read' )
  if(ierr/=0) stop 'Cannot find system.am file'

!       Reading system.am using tags

  if( tag_exist(iuham,'cell') )then
    read(iuham,*) alat
    do i = 1,3
      read(iuham,*,iostat=ierr) ( brav_lattice(i,j), j=1,3 )
    end do
  end if 


  if ( tag_exist(iuham,'atoms') )then
    read (iuham,*) natoms
    allocate ( at_position(natoms,3), at_name(natoms) )
    do i = 1,natoms
      read (iuham,*,iostat=ierr) at_name(i), ( at_position(i,j), j=1,3 )
    end do
  end if

  if ( tag_exist( iuham , 'basis' )) then
          
          read (iuham,*) dim , nblocks
          allocate ( bas_at_name (natoms) , atom_num (natoms) , l_sym (natoms) , block_dim (natoms), block_start (natoms))
     
            do i = 1,natoms
               read (iuham,*,iostat=ierr) bas_at_name(i), atom_num(i), l_sym(i), block_dim(i) , block_start(i)
            end do
          
                !checking for duplicates 
            do i=1,natoms-1
              do j = i+1,natoms
                       if(at_name(i).eq.at_name(j))stopmark = stopmark - 2
                       if(bas_at_name(i).eq.bas_at_name(j))stopmark1 = stopmark1 -2
                       end do
            end do   
                
                if(stopmark1.ne.stopmark) stop 'Some of the basis atom name does not match with names mentioned after %atoms'
                !finished checking for duplicates

                !checking for same names       
                        do i = 1 , natoms
                          do j = 1 , natoms
                             if(at_name(i).eq.bas_at_name(j)) stopmark = stopmark + 1
                          end do
                       end do
          if(stopmark/=natoms) stop 'Some of the basis atom name does not match with names mentioned after %atoms'
                !finished checking same names
                
                !checking block_dim and block_start
            do i = 1,(natoms-1)
                if( (block_dim(i)+block_start(i)).ne.(block_start(i+1))) stop 'It must be a mistake in setting block_dim or block_start'
            end do

                !finished checking block_dim and block_start
  end if    

!       Finished reading system.am

!       Writing system.am data to console

  write(6,*)
  write(6,*)
  write(6,*)'|system.am content|'
  write(6,*)
  write(6,*)'           bravais lattice' 
  write(6,*)
        do i = 1,3
          write(6,"(3f12.7)") (brav_lattice(i,j),j=1,3)
        enddo


  write(6,*)
  write(6,'(i2,A)') natoms,' atoms were found'
  write(6,*) 'with atomic positions'
  write(6,*)

        do i = 1, natoms
          write(6,"(i3,3(1x,f12.7))") i, ( at_position(i,j), j = 1,3 )
        enddo
 
  write(6,*)
  write(6,*)'           basis'  
  write(6,*)
  write(6,'(A,i3)')' dim:',dim 
  write(6,'(A,i3)')' nblocks:' ,nblocks
  write(6,*)  
  write(6,*) 'at_name  atom_num  l_sym   block_dim   block_start'
  do i = 1, natoms
  write(6,*) bas_at_name(i),' ', atom_num(i),' ', l_sym(i),' ', block_dim(i),' ', block_start(i)
  end do
  write(6,*)
  write(6,*)'|system.am content end|'
  write(6,*)

!       Finished writing system.am data

!       Reading Hamiltonian

  
  write(6,*)'           Reading Hamiltonian, please wait'
  write(6,*)
  call hamiltonian_name( iuham, ham_name, ierr )
  if( ierr /= 0 )   stop 'Cannot locate hamiltonian file.'
  call read_hamiltonian( iuham, ham_name, ierr )

  select case( ierr )
    case( -14001 )
      use_hmlt = .true.
      if( rhtm == 'UNDEFINED' ) rhtm = 'TBLMTO'
    case( -14002 )
      use_hmlt = .true.
      if( rhtm == 'UNDEFINED' ) rhtm = 'ESPRESSO'
    case( :-14003, -14000:0 )
      stop 'Cannot locate or read Hamiltonian file.'
    case default
      rhtm = 'TBLMTO'
      use_hmlt = .false.
  end select

!       Finished reading Hamiltonian

!======================================================================================
!
!                                       Body
!
!======================================================================================
  
  write(6,*)
  write(6,*) '  Enter number of a central atom to evaluate hopping integrals'
  write(6,*)
  read(5,*)  i_central
  
        do while(i_central>natoms.or.i_central<1) 
                write(6,*)
                        write(6,*)'   Reenter number of the central atom' 
                write(6,*)
                read(5,*) i_central
        end do

  write(6,*)
  write(6,*) '  Name and position of the central atom are'
  write(6,*)
  write(6,'(A,3f12.7)') at_name(i_central), at_position(i_central,:)

        
 
  !       Calculating number of neighbourghs

  stopmark = 0
  stopmark1 = 0

  do while(stopmark.eq.0)

  stopmark1 = 0
  nnbrs = 0

  write(6,*)
  write(6,*) '  Enter radius around the central atom in units of alat to evaluate hopping integrals'
  write(6,*)
  read(5,*)  radius


  do while( radius > 10*alat )
    write(6,*)
    write(6,*) ' Radius is larger than 10*alat'
    write(6,*) ' Reenter radius (or change code)'
    write(6,*)
    read(5,*) radius
  end do

  write(6,*)  

  ntransl = ceiling(radius) + 2
  
  write(6,*) ' ntransl  ', ntransl

  do i = -ntransl, ntransl
    do j = -ntransl, ntransl
      do k = -ntransl, ntransl
!        translation vector is
        transl_vec = i*brav_lattice(1,:) + j*brav_lattice(2,:) + k*brav_lattice(3,:)

        do iatom = 1, natoms
          new_pos = at_position(iatom,:) + transl_vec
          distance = sqrt( ( new_pos(1) - at_position(i_central,1) )**2  +         &
                           ( new_pos(2) - at_position(i_central,2) )**2  +         &
                           ( new_pos(3) - at_position(i_central,3) )**2 )
                                
          if( distance <= radius .and. distance > 0.001 )then
            nnbrs = nnbrs + 1
            write(6,"(' Atomic positions ',3(1x,f12.7))") new_pos - at_position(i_central,:)
          end if
                                
        end do

      end do
    end do
  end do
  
  write(6,*) '  Total number of neighbours are : ', nnbrs
  write(6,*)
  write(6,*) '  Would you like to recalculate them using a different radius? [Yes/No]'
        
  do while( stopmark1.eq.0 )
    read(5,*) scanner
    scanner = adjustl(scanner)
    if(scanner.eq.'no'.or.scanner.eq.'No'.or.scanner.eq.'n'.or.scanner.eq.'N') then 
                        
          stopmark = 1
          stopmark1 = 1
                        
    else if( scanner .eq. 'yes' .or. scanner .eq. 'yes' .or. scanner .eq. 'y' .or. scanner .eq. 'Y' .or. scanner .eq. '' ) then
                                
          stopmark1 = 1
                        
    else 
          write(6,*) '  Please type Yes/No'
    end if        
  end do   !  do while stop

  end do   !   do while
  
!     Finished calculating number of neigbours

  open( 100, file='hoppings.out', form='formatted', status='unknown', action='write' )

      
  do i = -ntransl, ntransl
    do j = -ntransl, ntransl
      do k = -ntransl, ntransl

        !translation vector is
        transl_vec = i*brav_lattice(1,:) + j*brav_lattice(2,:) + k*brav_lattice(3,:)
                                
        do iatom = 1, natoms
          new_pos = at_position(iatom,:) + transl_vec
          distance = sqrt( ( new_pos(1) - at_position(i_central,1) )**2  +         &
                           ( new_pos(2) - at_position(i_central,2) )**2  +         &
                           ( new_pos(3) - at_position(i_central,3) )**2 )
                                
          if( distance <= radius .and. distance > 0.001 )then
                  
            allocate( H_r( block_dim(i_central), block_dim(iatom), nsham) )

            new_pos = new_pos - at_position(i_central,:)

            H_r = cmplx(0,0,8)
            
            do is = 1, nsham
              do ikp = 1, nkp
                k_r = bk(1,ikp)*new_pos(1) + bk(2,ikp)*new_pos(2) + bk(3,ikp)*new_pos(3)
                exp_factor = cmplx( dcos(k_r), dsin(k_r), 8 )
  
                do m1 = block_start(i_central) + 1, block_start(i_central)+block_dim(i_central)
                  do m2 = block_start(iatom) + 1, block_start(iatom)+block_dim(iatom)
                    H_r( m1 - block_start(i_central), m2 - block_start(iatom), is ) =                                 &
                    H_r( m1 - block_start(i_central), m2 - block_start(iatom), is ) + exp_factor*h(m1-1, m2-1, ikp, is)
                  end do
                end do
              end do
            end do
            H_r = H_r / nkp

!      Print out matrix of the hopping integrals
            write(100,'(A,A,A,3f12.8)') '  Atom ', at_name(iatom), 'at position (relative to origin) ', new_pos
            write(100,'(A,f12.8)') '  distance ', distance
            write(100,*)
            do is = 1, nsham
              do m1 = 1, block_dim(i_central)
              !  write(100,"(30(1x,2f12.8))") ( H_r(m1,m2,is), m2 = 1, block_dim(iatom) )
              write(100,"(30(2f12.8))") ( H_r(m1,m2,is), m2 = 1, block_dim(iatom) )
              
                end do
            end do
            write(100,*)
            deallocate( H_r )
          end if
        end do
      end do
    end do
  end do

  
  do m1 = 1, ndim
    write(200,"(300(1x,2f12.8))") ( sum( h(m1,m2,:,1) )/nkp, m2 = 1,ndim )
  end do

end program hk2hr
