program dmft_dos
!
!     This program calculates DMFT density of states.
!     To use it you need to have a self-energy on the real energy axis.
!
!     Written by A. Poteryaev <poter _at_ optics.imp.uran.ru>
!
  use hamiltonian_module
  use hamiltonian_tools
  use energy_module
  use parameter_module
  use impurity_module
  use iolib_module
  use gf_atm_module
  use units
  use lapack_module

  implicit none

  integer :: ierr, iuham = 2
  integer :: n1, n2, nd
  integer :: is, isp, iom, itype, ikp, i

  character(128) :: ham_name

  real(8), allocatable :: ekr(:)

  complex(8) :: z
  complex(8), allocatable :: Gloc(:,:), Vpot(:,:), g(:,:)
  complex(8), allocatable :: hkz(:,:), ekz(:,:), cl(:,:,:), cr(:,:,:)
  
  real(8), allocatable :: dos(:,:,:)
  
!   Defines number of points in energy mesh

  call def_energy_params( ierr )
  if( ierr == 777 ) stop 'Wrong call in def_energy_params subroutine.'
  if( ierr  > 0 )   stop 'Cannot locate or read amulet.ini file (1).'

!   Define energy and time meshes
  call set_energy_mesh
  call use_real_mesh
  
!   Read initial data from amulet.ini file

  call read_ini( ierr )
  if( ierr == 777 ) stop 'Wrong call in read_ini subroutine.'
  if( ierr  > 0 )   stop 'Cannot locate or read amulet.ini file (2).'
  call setup_energy_units

!   Read Hamiltonian
  call hamiltonian_name( iuham, ham_name, ierr )
  if( ierr /= 0 )   stop 'Cannot locate hamiltonian file.' 
  call read_hamiltonian( iuham, ham_name, ierr )

  select case( ierr )
  case( -14001 )
    use_hmlt = .true.
    rhtm = 'TBLMTO'
  case( -14002 )
    use_hmlt = .true.
    rhtm = 'ESPRESSO'
  case( :-14003, -14000:0 )
    stop 'There is a problem with format of hamiltonian file.'
  case default
    use_hmlt = .false.
  end select

!   Read impurity.ini files

  call read_impurity( ierr )
  if( ierr == 777 ) stop 'Wrong call in read_impurity subroutine.'
  if( ierr  > 0 )   stop 'Cannot locate or read one of impurity.ini files.'
 
!   Read bare (LDA) density of states
  if( .not. use_hmlt )then
    call read_dos( ierr )
    if( ierr /= 0 )then
      stop 'Cannot locate or read one of dos.ini files.'
    else
      ndim = sum( impurity(:)%nlm )
    end if
  end if
  
!   Define tetrahedra
  if( use_atm .and. use_hmlt )then
    call def_tetra( ierr )
    if( ierr == 777 ) stop 'k-points are not defined.'
    if( ierr /= 0 )   stop 'Cannot define tetrahedra.'
  end if

!   Read self-energy
  if( use_sigma )then
    istart = 1
    call rw_sigma( ierr, 'r', 'real' )
    if( ierr /= 0 ) stop 'Cannot locate or read one of sigma files.'
  end if

  allocate( dos(0:ndim,n_energy,nspin) )
  
!   Calculates DOS

  if( use_hmlt )then
    nd = ndim

    allocate( Gloc(nd,nd), Vpot(nd,nd), g(nd,nd) )
    allocate( ekr(ndim), hkz(ndim,ndim) )
  
    if( use_atm ) allocate( ekz(ndim,nkp), cl(ndim,ndim,nkp), cr(ndim,ndim,nkp) )
!
!    Calculation of local Green function
!
    do is = 1, nspin
      do iom = 1, n_energy
        z = zenergy(iom)
        if( nspin == 2 .and. abs(h_field) > 1.e-4 ) z = z - ( 1.5 - is )*h_field

        Gloc = zero
!
!    Make "local potential": Sigma, Double counting, etc
!     
        Vpot = zero

        do itype = 1, n_imp_type
          do i = 1, impurity(itype)%n_imp
            n1 = impurity(itype)%hposition(i)
            n2 = n1 + impurity(itype)%nlm - 1
 
            isp = is
            if( impurity(itype)%magtype(i) == -1 ) isp = 3 - is
            Vpot(n1:n2,n1:n2) = impurity(itype)%Sigma(:,:,iom,isp)
          end do
        end do
!
!     Calculate G(z)
!    
        if( use_atm )then
          do ikp = 1, nkp
            hkz = h(:,:,ikp,is)
            hkz(1:nd,1:nd) = hkz(1:nd,1:nd) + Vpot

  !          call testherm( hkz, ndim, aisherm )

  !          if( aisherm )then
  !            call eigenproblem( hkz, ekr )
  !            ekz(:,ikp) = cmplx( ekr, 0, prec )
  !            cr(:,:,ikp) = hkz
  !            cl(:,:,ikp) = transpose( conjg( cr(:,:,ikp) ) )
  !          else
              call eigenproblem( hkz, ekz(:,ikp), cl(:,:,ikp), cr(:,:,ikp) )
  !          end if
          end do
 
          call green_atm( zenergy(iom), ekz, cl, cr, itt, Gloc, ndim, .true. )
 
        else
          do ikp = 1, nkp
            call gk( g, h(:,:,ikp,is), Vpot, ndim, z )
            Gloc = Gloc + g*wtkp(ikp)
          end do
        end if
      
        forall( i = 1:ndim )  dos(i,iom,is) = - aimag( Gloc(i,i) ) / pi
        dos(0,:,:) = sum( dos(1:ndim,:,:), dim=1 )

      end do   ! n_energy
    end do     ! nspin
    
    deallocate( Gloc, Vpot, g )
    deallocate( ekr, hkz )
    if( use_atm ) deallocate( ekz, cl, cr )
  else
    call glocal_dos
    
    n1 = 0
    do itype = 1, n_imp_type
      do i = 1, impurity(itype)%nlm
        n1 = n1 + 1
        dos(n1,:,:) = - aimag( impurity(1)%Green(i,i,:,:) ) / pi
      end do
    end do
    dos(0,:,:) = sum( dos(1:ndim,:,:), dim=1 )
  end if
   
  call write_function( 'total_dos', dos(0,:,:), prn_mesh, ierr, 'r' )
  call write_function( 'dos', dos(1:ndim,:,:), prn_mesh, ierr, 'r' )
  
end program dmft_dos
