#ifndef SYSTEM_INFO_H
#define SYSTEM_INFO_H

#include <boost/version.hpp>
#include <boost/config.hpp>
#if BOOST_VERSION >= 104200
#define CTHYB_HAS_UUID
#endif

#if BOOST_VERSION < 104700
#define BOOST_IS_OLD
#endif

#if !(__GLIBC__ >= 2 && __GLIBC_MINOR__ >= 15)
#define COMPILER_IS_OLD
#endif

#endif // SYSTEM_INFO_H
