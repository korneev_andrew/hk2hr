#ifndef RANDOM_H
#define RANDOM_H

#include <dSFMT.h>
#include <boost/random.hpp>
#include "system_info.h"

enum RANDOM_GENERATOR
{
    RANDOM_BOOST_MT19937 = 0,
    RANDOM_DSFMT = 1
};

class Random
{
public:
    Random(RANDOM_GENERATOR gen);
    ~Random();

    void Init(uint32_t seed);
    double Get_01();
private:
#ifndef BOOST_IS_OLD
    boost::random::mt19937 mersenne_;
    boost::random::uniform_01<> uniform_01_;
#endif
    dsfmt_t *dsfmt_;
    RANDOM_GENERATOR generator_;
};

#endif // RANDOM_H
