/*
* A.A. Dyachenko
*/
#include "interface.h"
#include "legendre_util.h"
#include <iostream>
#include <fstream>
#include <valarray>

using namespace boost::math::policies;
const std::complex<long double> long_i_c(0.0, 1.0);
const long double long_pi = boost::math::constants::pi<long double>();
const std::complex<double> i_c(0.0, 1.0);
const double pi = boost::math::constants::pi<double>();

typedef policy<domain_error<errno_on_error>, promote_double<true> > mypolicy;

extern "C" void c_green_omega_from_legendre(const double *green_legendre, double *green_omega_real, double *green_omega_imaginary, const int &num_flavors, const int &num_legendre, const int &num_matsubara)
{
    std::vector< std::vector<long double> > bessel;
    bessel.resize(num_matsubara);
    int nl = num_legendre; // >= 57 ? 56 : num_legendre;
    for(size_t w = 0; w < bessel.size(); w++) {
        bessel.at(w).resize(nl);
        for(size_t l = 0; l < bessel.at(w).size(); l++) {
            //bessel[w][l] = boost::math::cyl_bessel_j(static_cast<long double>(l) + 0.5, (static_cast<long double>(w) + 0.5) * long_pi, mypolicy()) / sqrt(2.0 * static_cast<long double>(w) + 1.0);
            bessel.at(w).at(l) = boost::math::sph_bessel(l, (static_cast<long double>(w) + 0.5) * long_pi);
        }
    }
    for(size_t w = 0; w < bessel.size(); w++) {
        bessel.at(w).resize(nl);
        for(size_t l = 0; l < bessel.at(w).size(); l++) {
            if(bessel.at(w).at(l) != bessel.at(w).at(l)) {
                bessel.at(w).at(l) = (bessel.at(w).at(l - 1) + bessel.at(w).at(l + 1)) * 0.5;
            }
        }
    }
    for(int f = 0; f < num_flavors; f++) {
        for(int wn = 0; wn < num_matsubara; wn++) {
            //bessel_dbg << "Omega: " << wn << std::endl;
            std::complex<double> gw_(0.0, 0.0);
            for(int l = 0; l < nl; ++l) {
                long double dl = static_cast<long double>(l);
                long double total0 = pow(-1.0, wn) * sqrt(2.0 * dl + 1.0) * bessel.at(wn).at(l) * green_legendre[f * num_legendre + l];
                std::complex<long double> total1 = total0 * pow(long_i_c, dl + 1.0);

                gw_ += total1;
            }
            green_omega_real[f * num_matsubara + wn] = gw_.real();
            green_omega_imaginary[f * num_matsubara + wn] = gw_.imag();
        }
    }
}

extern void c_green_legendre_restore(double *green_legendre, const int &num_flavors, const int &num_legendre)
{
    std::vector<double> green_legendre_restored;
    green_legendre_restored.resize(num_flavors * num_legendre);
    for(int flavor = 0; flavor < num_flavors; flavor++) {
        for(int legendre = 0; legendre < num_legendre; legendre++) {
            green_legendre_restored.at(flavor * num_legendre + legendre) = green_legendre[flavor * num_legendre + legendre] * sqrt(2.0 * static_cast<double>(legendre) + 1.0);
        }
    }

    for(int flavor = 0; flavor < num_flavors; flavor++) {
        for(int legendre = 0; legendre < num_legendre; legendre++) {
            green_legendre[flavor * num_legendre + legendre] = green_legendre_restored.at(flavor * num_legendre + legendre);
        }
    }
}

extern void c_get_green_legendre_moments(const double *green_legendre, const int &num_flavors, const int &num_legendre, const double &beta, double *c1, double *c2, double *c3)
{
    for(int f = 0; f < num_flavors; f++) {
        c1[f] = 0.0;
        for(int l = 0; l < num_legendre; l = l + 2) {
            c1[f] += 2.0 * sqrt(2.0 * static_cast<double>(l) + 1.0) * green_legendre[f * num_legendre + l] / beta;
        }
        c1[f] *= -1.0;
    }

    for(int f = 0; f < num_flavors; f++) {
        c2[f] = 0.0;
        for(int l = 1; l < num_legendre; l = l + 2) {
            c2[f] += 2.0 * sqrt(2.0 * static_cast<double>(l) + 1.0) * green_legendre[f * num_legendre + l] * static_cast<double>(l) * (static_cast<double>(l) + 1.0) / pow(beta, 2.0);
        }
    }

    for(int f = 0; f < num_flavors; f++) {
        c3[f] = 0.0;
        for(int l = 0; l < num_legendre; l = l + 2) {
            c3[f] += sqrt(2.0 * static_cast<double>(l) + 1.0) * green_legendre[f * num_legendre + l] * static_cast<double>(l) * (static_cast<double>(l) + 2.0) * (static_cast<double>(l) + 1.0) * (static_cast<double>(l) - 1.0) / pow(beta, 3.0);
        }
        c3[f] *= -1.0;
    }
}

extern void c_green_legendre_impose_c1(double *green_legendre, const int &num_flavors, const int &num_legendre, const double &beta)
{
    double *c1 = new double[num_flavors];
    double *c2 = new double[num_flavors];
    double *c3 = new double[num_flavors];
    c_get_green_legendre_moments(green_legendre, num_flavors, num_legendre, beta, c1, c2, c3);

    for(int f = 0; f < num_flavors; f++) {
        for(int l = 0; l < num_legendre; l++) {
            double sum2 = 0.0;
            for(int l2 = 0; l2 < num_legendre; l2++) {
                sum2 += legendre_t(l2, 1) * legendre_t(l2, 1);
            }
            green_legendre[f * num_legendre + l] += beta * (1.0 - c1[f]) * legendre_t(l, 1) / sum2;
        }
    }
}

extern void c_green_tau_from_legendre(const double *green_legendre, double *green_tau, const int &num_flavors, const int &num_legendre, const int &num_slices, const double &beta)
{
    for(int flavor = 0; flavor < num_flavors; flavor++) {
        for(int slice = 0; slice < (num_slices + 1); slice++) {
            green_tau[flavor * (num_slices + 1) + slice] = 0.0;
            const double x = 2.0 * static_cast<double>(slice) / static_cast<double>(num_slices) - 1.0;
            double pl_2 = 1.0;
            double pl_1 = x;
            double legendre_p;

            for(int l = 0; l < num_legendre; l++) {
                if(l == 0)
                    legendre_p = 1.0;
                else if (l == 1)
                    legendre_p = x;
                else {
                    legendre_p = ((2.0 * l - 1.0) * x * pl_1 - (l - 1.0) * pl_2) / static_cast<double>(l);//l
                    pl_2 = pl_1; //l-2
                    pl_1 = legendre_p; //l-1
                }
                green_tau[flavor * (num_slices + 1) + slice] += sqrt(2.0 * static_cast<double>(l) + 1.0) * legendre_p * green_legendre[flavor * num_legendre + l] / beta;
            }
        }
    }
}

//NG! redudant
extern "C" void cpp_green_omega_from_legendre(const std::valarray<double> &green_legendre, std::valarray< std::complex<double> > &green_omega)
{
    std::vector< std::vector<long double> > bessel;
    bessel.resize(green_omega.size());
    size_t nl = green_legendre.size(); // >= 57 ? 56 : green_legendre.size();
    for(size_t w = 0; w < bessel.size(); w++) {
        bessel.at(w).resize(nl);
        for(size_t l = 0; l < bessel.at(w).size(); l++) {
            //bessel[w][l] = boost::math::cyl_bessel_j(static_cast<long double>(l) + 0.5, (static_cast<long double>(w) + 0.5) * long_pi, mypolicy()) / sqrt(2.0 * static_cast<long double>(w) + 1.0);
            bessel.at(w).at(l) = boost::math::sph_bessel(l, (static_cast<long double>(w) + 0.5) * long_pi);
        }
    }
    for(size_t w = 0; w < bessel.size(); w++) {
        bessel.at(w).resize(nl);
        for(size_t l = 0; l < bessel.at(w).size(); l++) {
            if(bessel.at(w).at(l) != bessel.at(w).at(l)) {
                bessel.at(w).at(l) = (bessel.at(w).at(l - 1) + bessel.at(w).at(l + 1)) * 0.5;
            }
        }
    }
    for(size_t wn = 0; wn < green_omega.size(); wn++) {
        std::complex<double> gw_(0.0, 0.0);
        for(size_t l = 0; l < nl; ++l) {
            long double dl = static_cast<long double>(l);

            long double total0 = pow(-1.0, wn) * sqrt(2.0 * dl + 1.0) * bessel.at(wn).at(l) * green_legendre[l];
            std::complex<long double> total1 = total0 * pow(long_i_c, dl + 1.0);

            gw_ += total1;
        }
        green_omega[wn] = gw_;
    }
}

//NG!
extern "C" void cpp_green_legendre_restore(std::valarray<double> &green_legendre)
{
    for(size_t i = 0; i < green_legendre.size(); i++) {
        green_legendre[i] = green_legendre[i] * sqrt(2.0 * static_cast<double>(i) + 1.0);
    }
}

//NG!
extern "C" void cpp_get_green_legendre_moments(const std::valarray<double> &green_legendre, const double &beta, double &c1, double &c2, double &c3)
{
    c1 = 0.0;
    for(size_t l = 0; l < green_legendre.size(); l = l + 2) {
        c1 += 2.0 * sqrt(2.0 * static_cast<double>(l) + 1.0) * green_legendre[l] / beta;
    }
    c1 *= -1.0;

    c2 = 0.0;
    for(size_t l = 1; l < green_legendre.size(); l = l + 2) {
        c2 += 2.0 * sqrt(2.0 * static_cast<double>(l) + 1.0) * green_legendre[l] * static_cast<double>(l) * (static_cast<double>(l) + 1.0) / pow(beta, 2.0);
    }

    c3 = 0.0;
    for(size_t l = 0; l < green_legendre.size(); l = l + 2) {
        c3 += sqrt(2.0 * static_cast<double>(l) + 1.0) * green_legendre[l] * static_cast<double>(l) * (static_cast<double>(l) + 2.0) * (static_cast<double>(l) + 1.0) * (static_cast<double>(l) - 1.0) / pow(beta, 3.0);
    }
    c3 *= -1.0;
}

//NG!
extern "C" void cpp_green_legendre_impose_c1(std::valarray<double> &green_legendre, const double &beta)
{
    double c1;
    double c2;
    double c3;
    cpp_get_green_legendre_moments(green_legendre, beta, c1, c2, c3);

    for(size_t l = 0; l < green_legendre.size(); l++) {
        double sum2 = 0.0;
        for(size_t l2 = 0; l2 < green_legendre.size(); l2++) {
            sum2 += legendre_t(l2, 1) * legendre_t(l2, 1);
        }
        green_legendre[l] += beta * (1.0 - c1) * legendre_t(l, 1) / sum2;
    }
}

extern "C" void cpp_green_tau_from_legendre(const std::valarray<double> &green_legendre, std::valarray<double> &green_tau, const double &beta)
{
    green_tau = 0.0;
    for(size_t slice = 0; slice < green_tau.size(); slice++) {
        const double x = 2.0 * static_cast<double>(slice) / static_cast<double>(green_tau.size() - 1) - 1.0;
        double pl_2 = 1.0;
        double pl_1 = x;
        double legendre_p;

        for(size_t l = 0; l < green_legendre.size(); l++) {
            if(l == 0)
                legendre_p = 1.0;
            else if (l == 1)
                legendre_p = x;
            else {
                legendre_p = ((2.0 * l - 1.0) * x * pl_1 - (l - 1.0) * pl_2) / static_cast<double>(l);//l
                pl_2 = pl_1; //l-2
                pl_1 = legendre_p; //l-1
            }
            green_tau[slice] += sqrt(2.0 * static_cast<double>(l) + 1.0) * legendre_p * green_legendre[l] / beta;
        }
    }
}

extern "C" size_t cpp_green_legendre_analyze(const std::valarray<double> &green_legendre, const double &beta, std::valarray<double> &green_legendre_analyzed)
{
    double c1;
    double c2;
    double c3;
    std::valarray<double> test_legendre;
    test_legendre.resize(green_legendre.size());
    test_legendre = 0.0;
    for(size_t l = 2; l < green_legendre.size(); l = l + 2) {
        std::valarray<double> cropped_legendre;
        cropped_legendre.resize(l + 1);
        for(size_t i = 0; i < l + 1; i++) {
            cropped_legendre[i] = green_legendre[i];
        }
        cpp_get_green_legendre_moments(cropped_legendre, beta, c1, c2, c3);
        test_legendre[l] = c1;
    }
    green_legendre_analyzed.resize(green_legendre.size());

    for(size_t l = 0; l < green_legendre_analyzed.size(); l++) {
        green_legendre_analyzed[l] = test_legendre[l];
        test_legendre[l] = std::abs(1.0 - test_legendre[l]);
    }
    double min_value = test_legendre.min();
    for(size_t l = 0; l < test_legendre.size(); l++) {
        if(test_legendre[l] == min_value) {
            return l;
        }
    }

    return 0;
}

extern "C" void cpp_green_legendre_from_tau(const std::valarray<double> &green_tau, std::valarray<double> &green_legendre, const double &beta)
{
    green_legendre = 0.0;
    for(size_t slice = 0; slice < green_tau.size(); slice++) {
        double tau = beta * static_cast<double>(slice) / static_cast<double>(green_tau.size() - 1);
        const double x = 2.0 * tau / beta - 1.0;
        double pl_2 = 1.0;
        double pl_1 = x;
        double legendre_p;
        {
            for(size_t l = 0; l < green_legendre.size(); l++) {
                if(l == 0)
                    legendre_p = 1.0;
                else if (l == 1)
                    legendre_p = x;
                else {
                    legendre_p = ((2.0 * l - 1.0) * x * pl_1 - (l - 1.0) * pl_2) / static_cast<double>(l);//l
                    pl_2 = pl_1; //l-2
                    pl_1 = legendre_p; //l-1
                }
                green_legendre[l] -= green_tau[slice] * legendre_p * sqrt(2.0 * static_cast<double>(l) + 1.0);
            }
        }
    }
}

//fortran
extern "C" void f_green_legendre_analyze(const double *green_legendre, const int &num_flavors, const int &num_legendre, const double &beta, int *best_legendre)
{
    std::vector< std::valarray<double> > green_legendre_pp;
    std::vector< std::valarray<double> > green_legendre_analyzed;
    green_legendre_pp.resize(num_flavors);
    green_legendre_analyzed.resize(num_flavors);
    for(int f = 0; f < num_flavors; f++) {
        green_legendre_pp.at(f).resize(num_legendre);
        for(int i = 0; i < num_legendre; i++) {
            green_legendre_pp.at(f)[i] = green_legendre[f * num_legendre + i];
        }
        cpp_green_legendre_analyze(green_legendre_pp.at(f), beta, green_legendre_analyzed.at(f));
        best_legendre[f] = green_legendre_analyzed.at(f).size();
    }
}

//Fourier transorm stack
extern "C" void cpp_omega_from_legendre(const std::valarray<double> &legendre, std::valarray< std::complex<double> > &omega)
{
    std::vector< std::vector<long double> > bessel;
    bessel.resize(omega.size());
    size_t nl = legendre.size(); // >= 57 ? 56 : green_legendre.size();
    for(size_t w = 0; w < bessel.size(); w++) {
        bessel.at(w).resize(nl);
        for(size_t l = 0; l < bessel.at(w).size(); l++) {
            //bessel[w][l] = boost::math::cyl_bessel_j(static_cast<long double>(l) + 0.5, (static_cast<long double>(w) + 0.5) * long_pi, mypolicy()) / sqrt(2.0 * static_cast<long double>(w) + 1.0);
            bessel.at(w).at(l) = boost::math::sph_bessel(l, (static_cast<long double>(w) + 0.5) * long_pi);
        }
    }
    //workaround for boost bug
    //probably could be avoided with finite math, needs testing
    for(size_t w = 0; w < bessel.size(); w++) {
        bessel.at(w).resize(nl);
        for(size_t l = 0; l < bessel.at(w).size(); l++) {
            if(bessel.at(w).at(l) != bessel.at(w).at(l)) {
                bessel.at(w).at(l) = (bessel.at(w).at(l - 1) + bessel.at(w).at(l + 1)) * 0.5;
            }
        }
    }
    for(size_t wn = 0; wn < omega.size(); wn++) {
        std::complex<double> gw_(0.0, 0.0);
        for(size_t l = 0; l < nl; ++l) {
            long double dl = static_cast<long double>(l);

            long double total0 = pow(-1.0, wn) * sqrt(2.0 * dl + 1.0) * bessel.at(wn).at(l) * legendre[l];
            std::complex<long double> total1 = total0 * pow(long_i_c, dl + 1.0);

            gw_ += total1;
        }
        omega[wn] = gw_;
    }
}

