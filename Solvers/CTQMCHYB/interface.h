/*
* A.A. Dyachenko
*/
#ifndef CINTERFACE_H
#define CINTERFACE_H

#include <vector>
#include <valarray>
#include <complex>
#include <Eigen/Dense>

#define SPIN_MAX 2

struct fortran_complex
{
    double r;
    double i;
};

#define NUM_OBS 4

enum OBSERVABLE_TYPE
{
    OBS_DATA = 0,
    OBS_ERROR = 1,
    OBS_VARIANCE = 2,
    OBS_TAU = 3
};

class HybridizationSolver;

extern "C" {

extern HybridizationSolver *cpp_init_solver(const int &verbosity, const unsigned int &seed);
extern void cpp_destroy_solver(HybridizationSolver *solver);
extern std::vector< std::vector<int> > cpp_get_states(HybridizationSolver *solver);
extern void cpp_set_system(HybridizationSolver *solver, const std::vector< std::vector<double> > &hybridization_function, const std::vector<double> &mu, const Eigen::MatrixXd &u_matrix);
extern void cpp_green_omega_from_legendre(const std::valarray<double> &green_legendre, std::valarray< std::complex<double> > &green_omega);
extern void cpp_green_legendre_restore(std::valarray<double> &green_legendre);
extern void cpp_get_green_legendre_moments(const std::valarray<double> &green_legendre, const double &beta, double &c1, double &c2, double &c3);
extern void cpp_green_legendre_impose_c1(std::valarray<double> &green_legendre, const double &beta);
extern void cpp_green_tau_from_legendre(const std::valarray<double> &green_legendre, std::valarray<double> &green_tau, const double &beta);
extern size_t cpp_green_legendre_analyze(const std::valarray<double> &green_legendre, const double &beta, std::valarray<double> &green_legendre_analyzed);
extern void cpp_green_legendre_from_tau(const std::valarray<double> &green_tau, std::valarray<double> &green_legendre, const double &beta);

extern unsigned long long f_init_solver(const int &verbosity, const int &seed);
extern unsigned long long f_mc_results(const unsigned long long &solver);
extern void f_set_system(const unsigned long long &solver, const unsigned long long &hybridization_num_points, const double *hybridization_function, const double *mu, const double *u_matrix);
extern double c_fraction_done(const unsigned long long &solver);
extern bool c_is_thermalized(const unsigned long long &solver);
extern void c_make_sweep(const unsigned long long &solver);
extern void f_destroy_solver(unsigned long long &solver);
extern void f_destroy_results(unsigned long long &results);
extern void c_green_omega_from_legendre(const double *green_legendre, double *green_omega_real, double *green_omega_imaginary, const int &num_flavors, const int &num_legendre, const int &num_matsubara);
extern void c_export_data(const unsigned long long &solver);
extern void c_get_states(const unsigned long long &solver, int *states);
extern void c_green_legendre_restore(double *green_legendre, const int &num_flavors, const int &num_legendre);
extern void c_get_green_legendre_moments(const double *green_legendre, const int &num_flavors, const int &num_legendre, const double &beta, double *c1, double *c2, double *c3);
extern void c_green_legendre_impose_c1(double *green_legendre, const int &num_flavors, const int &num_legendre, const double &beta);
extern void c_green_tau_from_legendre(const double *green_legendre, double *green_tau, const int &num_flavors, const int &num_legendre, const int &num_slices, const double &beta);
//extern void c_green_omega_from_legendre_debug_v2(const double *green_legendre, double *green_omega_real, double *green_omega_imaginary, const int &num_flavors, const int &num_legendre, const int &num_matsubara, const double &beta);
//extern void c_string_test(const char *string, const int &i);
extern void c_set_parameter_int(const unsigned long long &solver, const char *string, const int &i);
extern void c_set_parameter_double(const unsigned long long &solver, const char *string, const double &d);
extern void c_set_parameter_bool(const unsigned long long &solver, const char *string, const bool &b);
extern void c_set_parameter_string(const unsigned long long &solver, const char *string, const char *data);
extern void f_get_result(const unsigned long long &solver, const unsigned long long &results, const int &type, double *data);
extern void f_get_complex_result(const unsigned long long &solver, const unsigned long long &results, const int &type, fortran_complex *data);
//extern void test_get_sweep_result_order(const unsigned long long &solver, std::valarray<double> &order);

//fortran take two
extern void f_green_legendre_analyze(const double *green_legendre, const int &num_flavors, const int &num_legendre, const double &beta, int *best_legendre);
extern void f_array_test(double *array);
extern void f_transform_legendre(const unsigned long long &solver, const double *legendre, fortran_complex *green_legendre, double *moments, int *predicted, int *legendre_cutoff);
extern void f_sigma_legendre(const unsigned long long &solver, const double *green_legendre, const double *sigmag_legendre, fortran_complex *sigma, int *legendre_cutoff);
extern void c_mem_info(int &vmpeak, int &vmsize, int &vmrss);
//Fourier transorm stack
extern void cpp_omega_from_legendre(const std::valarray<double> &legendre, std::valarray< std::complex<double> > &omega);
extern void cpp_legendre_from_tau(const std::valarray<double> &tau, std::valarray<double> &legendre, const double &beta);
extern void cpp_omega_from_tau(const std::valarray<double> &tau, std::valarray< std::complex<double> > &omega, size_t nlegendre, const double &beta);
extern void f_omega_from_tau(const unsigned long long &solver, const double *tau, fortran_complex *omega, const int &nlegendre);
//MCResults wrapper
/*extern unsigned long long f_mc_container_init(const int &bin_size);
extern void f_mc_container_push_bin(const unsigned long long &container, const char *name, const double *data, const int &array_size);
extern void f_mc_container_push_single(const unsigned long long &container, const char *name, const double *data, const int &array_size);
extern void f_mc_container_get_mean(const unsigned long long &container, const char *name, double *data);
extern void f_mc_container_get_error(const unsigned long long &container, const char *name, double *data);
extern void f_mc_container_get_variance(const unsigned long long &container, const char *name, double *data);
extern void f_mc_container_get_tau(const unsigned long long &container, const char *name, double *data);
extern void f_mc_container_sync(const unsigned long long &container);
extern void f_mc_container_destroy(const unsigned long long &container);*/
}

#endif // CINTERFACE_H
