/*
* A.A. Dyachenko
* TRIQS
*/

#ifndef LEGENDRE_UTIL_H
#define LEGENDRE_UTIL_H

#include <boost/math/special_functions/bessel.hpp>
#include <boost/math/constants/constants.hpp>

// This is t_l^p following Eq.(E8) of TRIQS-Legendre paper
double legendre_t(const int &l, const int &p) {

    // p is the 1/omega power, it can't be negative
    assert(p > 0);

    // in these two cases we can directly give back 0
    if ((l+p)%2 == 0 || p > l+1) return 0.0;

    // the factorials are done here
    double f = 1.0;
    for (int i = l+p-1; (i > l-p+1) && (i > 1); i--) f *= i;
    for (int i = p-1; i > 1; i--) f /= i;

    return pow( static_cast<double>(-1),static_cast<double>(p) ) * 2 * sqrt(2*l+1) * f;

}

int factorial(int n) {
    if(n == 1)
        return 1;

    int v = 1;
    for(int arg = n; arg > 0; arg--) {
        v *= arg;
    }

    return v;
}

double bessel_a(double n, int k)
{
    double val = 0.0;
    if(k <= n) {
        val = factorial(n + k) / (pow(2.0, static_cast<double>(k)) * factorial(k) * (factorial(n - k)));
    }

    return val;
}

//terrible at math
double j(double n, double z)
{
    double _sin = sin(z - 0.5 * n * M_PI);
    double _cos = cos(z - 0.5 * n * M_PI);
    double sum0 = 0.0;
    for(int k = 0; k <= floor(n / 2.0); k++) {
        sum0 += pow(-1.0, static_cast<double>(k)) * bessel_a(n, 2 * k) / pow(z, 2.0 * k + 1.0);
    }
    double sum1 = 0.0;
    for(int k = 0; k <= floor((n - 1.0) / 2.0); k++) {
        sum1 += pow(-1.0, static_cast<double>(k)) * bessel_a(n, (2 * k + 1)) / pow(z, 2.0 *k + 2.0);
    }

    return (sum0 * _sin + sum1 * _cos);
}

long double j2(long double l, long double z)
{
    const long double lpi = boost::math::constants::pi<long double>();
    long double _sin = sin(z - 0.5 * l * lpi);
    long double _cos = cos(z - 0.5 * l * lpi);
    long double sum0 = 0.0;
    for(int k = 0; k <= floor(l / 2.0); k++) {
        sum0 += pow(-1.0, static_cast<long double>(k)) * factorial(l + 2 * k) * pow(2.0 * z, -2 * k) / (factorial(2 * k) * factorial(l - 2 * k));
    }
    long double sum1 = 0.0;
    for(int k = 0; k <= floor((l - 1.0) / 2.0); k++) {
        sum1 += pow(-1.0, static_cast<long double>(k)) * factorial(l + 2 * k + 1) * pow(2.0 * z, -2.0 * k - 1.0) / (factorial(2 * k + 1) * factorial(l - 2 * k - 1));
    }

    return (sum0 * _sin + sum1 * _cos) / z;
}

#endif // LEGENDRE_UTIL_H
