/************************************************************************************
 *
 * ALPS DMFT Project
 *
 * Copyright (C) 2005 - 2011 by Philipp Werner <werner@itp.phys.ethz.ch>,
 *                              Emanuel Gull <gull@phys.columbia.edu>,
 *                              Hartmut Hafermann <hafermann@cpht.polytechnique.fr>
 * 2011 - 2013 minor compatibility modifications by A. A. Dyachenko <adotfive@gmail.com>
 *
 *
 * This software is part of the ALPS Applications, published under the ALPS
 * Application License; you can use, redistribute it and/or modify it under
 * the terms of the license, either version 1 or (at your option) any later
 * version.
 *
 * You should have received a copy of the ALPS Application License along with
 * the ALPS Applications; see the file LICENSE.txt. If not, the license is also
 * available from http://alps.comp-phys.org/.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
 * SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
 * FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 ************************************************************************************/
#ifndef HYBRIDIZATION_EXPANSION_COMMON_H
#define HYBRIDIZATION_EXPANSION_COMMON_H

#include <cmath>
#include <boost/math/constants/constants.hpp>
#include <map>
#include <set>
#include <vector>
#include <valarray>
#include <ctime>
#include <Eigen/Dense>
#include "times.h"

typedef  Eigen::MatrixXd  blas_matrix;
typedef std::set<times> segment_container_t;
typedef std::vector<double> hybridization_t;
typedef std::vector<hybridization_t> hybridization_container_t;
typedef std::vector<double> vector_t;
typedef segment_container_t line_t;

enum MC_FULL_LINE {
    FULL_LINE_FAIL = 0,
    FULL_LINE_INSERT = 1,
    FULL_LINE_REMOVE = 2
};

enum MC_SEGMENT {
    SEGMENT_FAIL = 0,
    SEGMENT_INSERT = 1,
    SEGMENT_REMOVE = 2
};

enum MC_ANTISEGMENT {
    ANTISEGMENT_FAIL = 0,
    ANTISEGMENT_INSERT = 1,
    ANTISEGMENT_REMOVE = 2
};

enum MC_SHIFT {
    SHIFT_FAIL = 0,
    SHIFT_SUCCESS = 1
};

enum MC_SWAP {
    SWAP_FAIL = 0,
    SWAP_SUCCESS = 1
};

inline void swap(blas_matrix &A, blas_matrix &B)
{
    /*blas_matrix tmp = A;
    A = B;
    B = tmp;
    tmp.swap()*/
    A.swap(B);
}

#endif // HYBRIDIZATION_EXPANSION_COMMON_H
