#ifndef OBSERVABLE_CONTAINER_H
#define OBSERVABLE_CONTAINER_H

#include <vector>
#include <numeric>
#include <valarray>
#include <stdexcept>
#include <string>
#include <iostream>
#include <boost/shared_array.hpp>

template <class T = std::valarray<double> > class ObservableContainer
{
public:
    ObservableContainer(size_t bin_size = 50, const char *name = 0);
    ~ObservableContainer();
    T &BinValue(size_t bin);
    T &BinValue2(size_t bin);
    size_t BinNumber();
    size_t BinSize();
    void Add(T &v);
    T &Mean();
    T &Error();
    T &Variance();
    T &Tau();
    std::string Name();
    size_t ExportValues(boost::shared_array<double> &values);
    void ImportValues(boost::shared_array<double> &values, size_t values_size = 0);
    size_t Values0Size();
    void Clear();
    void Report();

    //size_t BinSize(size_t bin);
private:
    void MakeJack();
    void PerformJack();
    void PerformBinning();
    void Analyze();
    size_t bin_number_;
    std::vector<T> values_;
    std::vector<T> values2_;
    T variance_;
    T mean_;
    T tau_;
    T error_;
    //std::vector<T> sizes_;
    size_t bin_size_;
    std::vector<T> jack_;
    size_t count_;
    size_t current_size_;
    bool valid_;

    std::string name_;
};

template <class T> ObservableContainer<T>::ObservableContainer(size_t bin_size, const char* name)
{
    bin_number_ = 0;
    bin_size_ = bin_size;
    current_size_ = 0;
    name_ = name;
    valid_ = false;
}

template <class T> ObservableContainer<T>::~ObservableContainer()
{

}

template <class T> std::string ObservableContainer<T>::Name()
{
    return name_;
}

template <class T> T &ObservableContainer<T>::BinValue(size_t bin)
{
    return values_.at(bin);
}

template <class T> T &ObservableContainer<T>::BinValue2(size_t bin)
{
    return values2_.at(bin);
}

template <class T> size_t ObservableContainer<T>::BinNumber()
{
    return bin_number_;
}

template <class T> size_t ObservableContainer<T>::BinSize()
{
    return bin_size_;
}

template <class T> void ObservableContainer<T>::MakeJack()
{
    if(BinNumber() > 0) {
        jack_.clear();
        jack_.resize(BinNumber() + 1);
        jack_[0].resize(BinValue(0).size());
        for(size_t i = 0; i < BinNumber(); i++) {
            jack_[0] += BinValue(i) / static_cast<double>(BinSize());
        }
        for(size_t i = 0; i < BinNumber(); i++) {
            jack_[i + 1].resize(jack_[0].size());
            T tmp(BinValue(i));
            tmp /= static_cast<double>(BinSize());
            jack_[i + 1] = jack_[0] - tmp;
            jack_[i + 1] /= static_cast<double>(BinNumber() - 1);
        }
        jack_[0] /= static_cast<double>(BinNumber());
    }
}

template <class T> void ObservableContainer<T>::Analyze()
{
    if(valid_)
        return;

    if(BinNumber()) {
        count_ = BinNumber() * BinSize();
#ifndef NDEBUG
        std::cout << "count_ = " << count_ << " " << __FILE__ << ":" << __LINE__ << std::endl;
#endif
#ifndef NO_RESAMPLING
        PerformJack();
#else
        PerformBinning();
#endif
        variance_.resize(BinValue2(0).size());
        variance_ = 0.0;
        for(size_t i = 0; i < values2_.size(); i++)
            variance_ += values2_.at(i);
        T mean2;
        mean2.resize(mean_.size());
        mean2 = mean_;
        mean2 *= mean_;
        mean2 *= static_cast<double>(count_);
        variance_ -= mean2;
        variance_ /= count_ - 1;
#ifndef NO_RESAMPLING
        tau_.resize(error_.size());
        tau_ = std::abs(error_);
        tau_ *= std::abs(error_) * count_;
        tau_ /= std::abs(variance_);
        tau_ -= 1.0;
        tau_ *= 0.5;
#else
        tau_.resize(variance_.size());
        error_.resize(variance_.size());
        tau_ = 0.0;
        error_ = 0.0;
#endif

        valid_ = true;
    }
}

template <class T> void ObservableContainer<T>::PerformBinning()
{
    if(BinNumber() > 0) {
        std::valarray<double> binning;
        binning.resize(BinValue(0).size());
        binning = 0.0;
        for(size_t i = 0; i < BinNumber(); i++) {
            binning += BinValue(i) / static_cast<double>(BinSize());
        }
        binning /= static_cast<double>(BinNumber());
        mean_.resize(binning.size());
        mean_ = binning;
    }
}

template <class T> void ObservableContainer<T>::PerformJack()
{
    MakeJack();

    if(jack_.size()) {
        T rav;
        mean_.resize(jack_[0].size());
        error_.resize(jack_[0].size());
        mean_ = 0;
        error_ = 0;
        rav.resize(jack_[0].size());
        size_t k = jack_.size() - 1;

        rav = 0.0;
        rav = std::accumulate(jack_.begin() + 1, jack_.end(), rav);
        rav /= static_cast<double>(k);

        T tmp(rav);
        tmp -= jack_[0];
        tmp *= k - 1;
        mean_ = jack_[0] - tmp;
        error_ = 0.0;
        for(size_t i = 1; i < jack_.size(); i++)
            error_ += (jack_[i] - rav) * (jack_[i] - rav);
        error_ /= static_cast<double>(k);
        error_ *= k - 1;
        error_ = std::sqrt(error_);
    }
}

template <class T> void ObservableContainer<T>::Add(T &v)
{
    if(values_.empty()) {
        T tmp;
        tmp.resize(v.size());
        tmp = 0.0;
        values_.push_back(tmp);
        values2_.push_back(tmp);
    }
    values_.at(BinNumber()) += v;
    values2_.at(BinNumber()) += pow(v, 2.0);
    current_size_++;
    if(current_size_ == bin_size_) {
        current_size_ = 0;
        T tmp;
        tmp.resize(v.size());
        tmp = 0.0;
        values_.push_back(tmp);
        values2_.push_back(tmp);
        bin_number_++;
    }
    valid_ = false;
}

template <class T> T &ObservableContainer<T>::Mean()
{
    Analyze();
    return mean_;
}

template <class T> T &ObservableContainer<T>::Error()
{
    Analyze();
    return error_;
}

template <class T> T &ObservableContainer<T>::Variance()
{
    Analyze();

    return variance_;
}

template <class T> T &ObservableContainer<T>::Tau()
{
    Analyze();

    return tau_;
}

template <class T> size_t ObservableContainer<T>::ExportValues(boost::shared_array<double> &values)
{
    size_t value_size = values_[0].size();
    size_t total_size = 2 * BinNumber() * value_size + 1;
    values.reset(new double[total_size]);
    values[0] = BinNumber();
    for(size_t bin = 0; bin < BinNumber(); bin++) {
        for(size_t i = 0; i < value_size; i++) {
            values[bin * value_size + i + 1] = values_[bin][i];
            values[(bin + BinNumber()) * value_size + i + 1] = values2_[bin][i];
        }
    }

    return total_size;
}

template <class T> void ObservableContainer<T>::ImportValues(boost::shared_array<double> &values, size_t values_size)
{
    size_t value_size = (values_size == 0 ? values_[0].size() : values_size);
#ifndef NDEBUG
    bool report_import = !values_.empty();
    size_t old_bin_number = bin_number_;
#endif
    for(size_t bin = 0; bin < values[0]; bin++) {
        values_.resize(values_.size() + 1);
        values2_.resize(values2_.size() + 1);
        values_.back().resize(value_size);
        values2_.back().resize(value_size);
        for(size_t i = 0; i < value_size; i++) {
            values_.back()[i] = values[bin * value_size + i + 1];
            values2_.back()[i] = values[(bin + values[0]) * value_size + i + 1];
        }
        bin_number_++;
    }
    valid_ = false;
#ifndef NDEBUG
    if(report_import) {
        std::cout << name_ << " imported " << values[0] << bin_number_ - old_bin_number << " bins." << std::endl;
        std::cout << std::flush;
    }
#endif
}

template <class T> void ObservableContainer<T>::Clear()
{
    values_.clear();
    values2_.clear();
    mean_.resize(0);
    error_.resize(0);
    tau_.resize(0);
    variance_.resize(0);
    current_size_ = 0;
    valid_ = false;
}

template <class T> size_t ObservableContainer<T>::Values0Size()
{
    return values_[0].size();
}

template <class T> void ObservableContainer<T>::Report()
{
    std::cout << "values_.size() " << values_.size() << std::endl;
    std::cout << "values2_.size() " << values2_.size() << std::endl;
    std::cout << "values_[0].size() " << values_[0].size() << std::endl;
    std::cout << "values2_[0].size() " << values2_[0].size() << std::endl;
    std::cout << "bin_number " << bin_number_ << std::endl;
}

#endif // OBSERVABLE_CONTAINER_H
