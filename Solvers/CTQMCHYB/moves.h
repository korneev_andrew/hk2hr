/************************************************************************************
 *
 * ALPS DMFT Project
 *
 * Copyright (C) 2005 - 2011 by Philipp Werner <werner@itp.phys.ethz.ch>,
 *                              Emanuel Gull <gull@phys.columbia.edu>,
 *                              Hartmut Hafermann <hafermann@cpht.polytechnique.fr>
 * 2011 - 2013 minor compatibility modifications by A. A. Dyachenko <adotfive@gmail.com>
 *
 *
 * This software is part of the ALPS Applications, published under the ALPS
 * Application License; you can use, redistribute it and/or modify it under
 * the terms of the license, either version 1 or (at your option) any later
 * version.
 *
 * You should have received a copy of the ALPS Application License along with
 * the ALPS Applications; see the file LICENSE.txt. If not, the license is also
 * available from http://alps.comp-phys.org/.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
 * SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
 * FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 ************************************************************************************/
#ifndef ___MOVES___
#define ___MOVES___

#include "hybridization_expansion_common.h"
#include <mc_random.h>

// length of inserted segment (mu>0) or anti-segment (mu<0)
inline double compute_length(double r, double l_max, double mu) {

  if (mu == 0)
    return r*l_max;  
  else
    return 1/mu*log(r*(exp(mu*l_max)-1)+1);
}

bool compare_bm(blas_matrix &m1, blas_matrix &m2);

extern "C" {
extern MC_FULL_LINE insert_remove_full_line(Random *random, double mu, double BETA, blas_matrix &u, int& full_line, std::vector<segment_container_t>& other_segments, std::vector<int>& other_full_line, int this_flavor);
extern MC_SEGMENT insert_remove_segment(Random *random, double t, double BETA, double mu, blas_matrix &u, hybridization_t& F, segment_container_t& segments, blas_matrix & M, double & sign, std::vector<segment_container_t>& other_segments, std::vector<int> other_full_line, int this_flavor);
extern MC_ANTISEGMENT insert_remove_antisegment(Random *random, double t, double BETA, double mu,blas_matrix &u, hybridization_t& F, int& full_line, segment_container_t& segments, blas_matrix & M, double & sign, std::vector<segment_container_t>& other_segments, std::vector<int> other_full_line, int this_flavor);
extern MC_SHIFT shift_segment(Random *random, segment_container_t& segments, double BETA, double mu, blas_matrix &u, hybridization_t& F, blas_matrix & M, double & sign, std::vector<segment_container_t>& other_segments, std::vector<int>& other_full_line, int this_flavor) ;
//bool swap_segments(double BETA, const int &i, const int &j, const std::vector<double> &mu, std::vector< segment_container_t> &segments, std::vector<int> &full_line, const blas_matrix &u, hybridization_container_t& F, std::vector<double> &sign, std::vector< blas_matrix > &M);
extern MC_SWAP swap_segments_spin(Random *random, double BETA, std::vector< segment_container_t > & segments, std::vector<int> & full_line, hybridization_container_t& F, std::vector<double>& sign, std::vector< blas_matrix > & M, std::vector<double> & mu_e, blas_matrix &u);
extern MC_SWAP swap_segments_spin_no_u(Random *random, double BETA, std::vector< segment_container_t > & segments, std::vector<int> & full_line, hybridization_container_t& F, std::vector<double>& sign, std::vector< blas_matrix > & M, std::vector<double> & mu_e);
}

#endif
