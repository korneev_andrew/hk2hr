#include "mc_random.h"

Random::Random(RANDOM_GENERATOR gen)
{
    generator_ = gen;
    dsfmt_ = new dsfmt_t;
}

Random::~Random()
{
    delete dsfmt_;
}

void Random::Init(uint32_t seed)
{
#ifndef BOOST_IS_OLD
    mersenne_.seed(seed);
#endif
    dsfmt_init_gen_rand(dsfmt_, seed);
}

double Random::Get_01()
{
    switch(generator_) {
    case RANDOM_BOOST_MT19937:
#ifndef BOOST_IS_OLD
    {
        return uniform_01_(mersenne_);
    }
#endif
    default:
    case RANDOM_DSFMT:
    {
        return dsfmt_genrand_close_open(dsfmt_);
    }
    }
    return 1.0;
}
