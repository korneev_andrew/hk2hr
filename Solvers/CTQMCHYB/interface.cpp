/*
* A.A. Dyachenko
*/
#include "interface.h"
#include "hybsolver/hybsolver.h"
#include "mc_results.h"
#include "array_mapper.hpp"
#include <fstream>
#include <boost/algorithm/string.hpp>
#include <boost/assign.hpp>
#include <boost/ptr_container/ptr_vector.hpp>

extern "C" HybridizationSolver *cpp_init_solver(const int &verbosity, const unsigned int &seed)
{
    HybridizationSolver *solver = new HybridizationSolver(seed);
    CommonConfig *config = solver->GetConfig();

    config->thermalization_sweeps = 100;
    config->total_sweeps = 1000;
    config->num_nn = 5;
    config->num_updates = 20;
    config->num_meas = 2000;
    config->observable_bin_size = 1;
    config->num_matsubara = 0;
    config->num_matsubara2 = 0;
    config->num_bosonic = 0;
    config->num_legendre = 0;
    config->num_slices = 0;
    config->num_flavors = 0;
    config->max_time = 17000;

    config->beta = 1.0;
    config->measure_green_omega = false;
    config->measure_green_legendre = false;
    config->measure_nnt = false;
    config->measure_sector_statistics = false;
    config->measure_nn = false;
    config->measure_sigmag = false;
    config->measure_green2_omega = false;

    config->use_arg_cache = false;
    config->arg_cache_size = 0;

    config->verbosity = std::max(verbosity, 0);

    config->global_seed = seed;
    config->slicing_method = SLICING_DUMB;
    config->random_generator = RANDOM_BOOST_MT19937;
    //add all updates here, later can be modified as needed via parameter update functions
    config->updates_.push_back(std::pair<UPDATE_TYPE, double>(UPDATE_FULL_LINE, 1.0));
    config->updates_.push_back(std::pair<UPDATE_TYPE, double>(UPDATE_SEGMENT, 1.0));
    config->updates_.push_back(std::pair<UPDATE_TYPE, double>(UPDATE_ANTISEGMENT, 1.0));
    config->updates_.push_back(std::pair<UPDATE_TYPE, double>(UPDATE_SHIFT, 0.1));
    config->updates_.push_back(std::pair<UPDATE_TYPE, double>(UPDATE_GLOBAL, 0.0001));

    time_t rawtime;
    time (&rawtime);
    std::stringstream out_stream;
    out_stream << "Solver initialized " << ctime(&rawtime) << "Solver build date: " << __DATE__ << " build time " << __TIME__;
    solver->WriteLog(out_stream);

    return solver;
}

extern "C" unsigned long long f_init_solver(const int &verbosity, const int &seed)
{
    return reinterpret_cast<unsigned long long>(cpp_init_solver(verbosity, seed));
}

extern "C" unsigned long long f_mc_results(const unsigned long long &solver)
{
    HybridizationSolver *s = reinterpret_cast<HybridizationSolver*>(solver);
    MCResults *mc_results = new MCResults;
    s->ExportResults(*mc_results);
    mc_results->SyncResults();

    return reinterpret_cast<unsigned long long>(mc_results);
}

extern "C" void f_set_system(const unsigned long long &solver, const unsigned long long &hybridization_num_points, const double *hybridization_function, const double *mu, const double *u_matrix)
{
    HybridizationSolver *s = reinterpret_cast<HybridizationSolver*>(solver);
    CommonConfig *config = s->GetConfig();

    size_t num_flavors = config->num_flavors;

    std::vector<size_t> sizes_delta = boost::assign::list_of(hybridization_num_points)(num_flavors);
    std::vector<size_t> sizes_eps = boost::assign::list_of(num_flavors);
    std::vector<size_t> sizes_umatrix = boost::assign::list_of(num_flavors)(num_flavors);
    ArrayMapper<double> delta((void*)hybridization_function, sizes_delta);
    ArrayMapper<double> eps((void*)mu, sizes_eps);
    ArrayMapper<double> um((void*)u_matrix, sizes_umatrix);

    std::vector< std::vector<double> > _hybridization_function;
    _hybridization_function.resize(config->num_flavors);

    for(size_t i = 0; i < num_flavors; i++) {
        _hybridization_function.at(i).resize(hybridization_num_points);
        for(size_t point = 0; point < hybridization_num_points; point++)
            _hybridization_function.at(i).at(point) = -delta[point](i);
    }

    std::vector<double> _chemical_potential;
    _chemical_potential.resize(num_flavors);

    for(size_t i = 0; i < num_flavors; i++)
        _chemical_potential.at(i) = eps(i);

    blas_matrix _u_matrix;
    _u_matrix.resize(num_flavors, num_flavors);

    for(size_t f1 = 0; f1 < num_flavors; f1++)
        for(size_t f2 = 0; f2 < num_flavors; f2++)
            _u_matrix(f1, f2) = um[f2](f1);

    /*ATTENTION!*/
    cpp_set_system(s, _hybridization_function, _chemical_potential, _u_matrix);
}

extern "C" void cpp_set_system(HybridizationSolver *solver, const std::vector< std::vector<double> > &hybridization_function, const std::vector<double> &mu, const Eigen::MatrixXd &u_matrix)
{
    CommonConfig *config = solver->GetConfig();

    if(config->num_slices == 0 || config->num_flavors == 0 || config->beta <= 0.0)
        solver->Exception_RuntimeError("Crucial parameter isn't set!");

    std::vector< std::vector<double> > _hybridization_function;
    _hybridization_function.resize(hybridization_function.size());

    for(size_t f = 0; f < config->num_flavors; f++) {
        _hybridization_function.at(f).resize(hybridization_function.at(f).size());
        for(size_t t = 0; t < hybridization_function.at(f).size(); t++)
            _hybridization_function.at(f).at(t) = hybridization_function.at(f)[(hybridization_function.at(f).size() - 1) - t];
    }

    std::vector<double> chemical_potential;
    chemical_potential.resize(mu.size());

    for(size_t f = 0; f < config->num_flavors; f++)
        chemical_potential.at(f) = mu.at(f);

    blas_matrix _u_matrix;
    _u_matrix.resize(u_matrix.cols(), u_matrix.rows());

    for(size_t f1 = 0; f1 < config->num_flavors; f1++)
        for(size_t f2 = 0; f2 < config->num_flavors; f2++)
            _u_matrix(f1, f2) = u_matrix(f1, f2);

    solver->Prepare();

    solver->SetSystem(_hybridization_function, chemical_potential, _u_matrix);

    if (config->verbosity >= VERBOSITY_FILE)
        solver->DumpInput();
}

extern "C" double c_fraction_done(const unsigned long long &solver)
{
    HybridizationSolver *s = reinterpret_cast<HybridizationSolver*>(solver);

    return s->FractionDone();
}

extern "C" bool c_is_thermalized(const unsigned long long &solver)
{
    HybridizationSolver *s = reinterpret_cast<HybridizationSolver*>(solver);

    return s->IsThermalized();
}

extern "C" void c_make_sweep(const unsigned long long &solver)
{
    HybridizationSolver *s = reinterpret_cast<HybridizationSolver*>(solver);
    s->MakeSweep();
}

extern "C" std::vector< std::vector<int> > cpp_get_states(HybridizationSolver *solver)
{
    CommonConfig *config = solver->GetConfig();

    if (!config->measure_sector_statistics)
        solver->Exception_RuntimeError("Sector statistics measurements are not enabled!");

    return solver->GetStates();
}

extern "C" void c_get_states(const unsigned long long &solver, int *states)
{
    HybridizationSolver *s = reinterpret_cast<HybridizationSolver*>(solver);
    CommonConfig *config = s->GetConfig();
    std::vector< std::vector<int> > conf = cpp_get_states(s);
    std::vector<size_t> sizes = boost::assign::list_of(conf.at(0).size())(conf.size());
    ArrayMapper<int> fortran_states((void*)states, sizes);
    for(size_t i = 0; i < conf.size(); i++) {
        for(size_t k = 0; k < config->num_flavors; k++)
            fortran_states[k](i) = conf.at(i).at(k);
    }
}

extern "C" void f_destroy_solver(unsigned long long &solver)
{
    HybridizationSolver *s = reinterpret_cast<HybridizationSolver*>(solver);
    cpp_destroy_solver(s);
}

extern "C" void f_destroy_results(unsigned long long &results)
{
    MCResults *r = reinterpret_cast<MCResults*>(results);
    delete r;
}

extern "C" void cpp_destroy_solver(HybridizationSolver *solver)
{
    delete solver;
}

extern "C" void c_export_data(const unsigned long long &solver)
{
    HybridizationSolver *s = reinterpret_cast<HybridizationSolver*>(solver);

    s->ExportSegments();
}

template <class T> void clear_observables(boost::ptr_vector< ArrayMapper<T> > &observables)
{
    for(size_t i = 0; i < observables.size(); i++) {
        observables.at(i).Clear();
    }
}

extern "C" void f_get_complex_result(const unsigned long long &solver, const unsigned long long &results, const int &type, fortran_complex *data)
{
    f_get_result(solver, results, type, (double*)data);
}

extern "C" void f_get_result(const unsigned long long &solver, const unsigned long long &results, const int &type, double *data)
{
    HybridizationSolver *s = reinterpret_cast<HybridizationSolver*>(solver);
    MCResults *mc_results = reinterpret_cast<MCResults*>(results);
    CommonConfig *config = s->GetConfig();

    size_t num_flavors = config->num_flavors;

    DATA_MEASUREMENTS _type = static_cast<DATA_MEASUREMENTS>(type);
    switch(_type) {
    case DATA_GREEN_TAU:
    {
        std::vector<size_t> sizes = boost::assign::list_of(NUM_OBS)(config->num_slices + 1)(num_flavors);
        ArrayMapper<double> array(data, sizes);

        for(size_t i = 0; i < num_flavors; i++)
            for(size_t point = 0; point < config->num_slices + 1; point++) {
                array[OBS_DATA][point](i) = -mc_results->GetResult("green_tau")->mean_[i * (config->num_slices + 1) + point];
                array[OBS_ERROR][point](i) = mc_results->GetResult("green_tau")->error_[i * (config->num_slices + 1) + point];
                array[OBS_VARIANCE][point](i) = mc_results->GetResult("green_tau")->variance_[i * (config->num_slices + 1) + point];
                array[OBS_TAU][point](i) = mc_results->GetResult("green_tau")->tau_[i * (config->num_slices + 1) + point];
            }

        break;
    }
    case DATA_GREEN_LEGENDRE:
    {
        std::vector<size_t> sizes = boost::assign::list_of(NUM_OBS)(config->num_legendre)(num_flavors);
        ArrayMapper<double> array(data, sizes);

        for(size_t i = 0; i < num_flavors; i++)
            for(size_t k = 0; k < config->num_legendre; k++) {
                array[OBS_DATA][k](i) = mc_results->GetResult("green_legendre")->mean_[i * config->num_legendre + k];

                array[OBS_ERROR][k](i) = mc_results->GetResult("green_legendre")->error_[i * config->num_legendre + k];
                array[OBS_VARIANCE][k](i) = mc_results->GetResult("green_legendre")->variance_[i * config->num_legendre + k];
                array[OBS_TAU][k](i) = mc_results->GetResult("green_legendre")->tau_[i * config->num_legendre + k];
            }

        break;
    }
    case DATA_GREEN_OMEGA:
    {
        std::vector<size_t> sizes = boost::assign::list_of(NUM_OBS)(config->num_matsubara)(num_flavors);
        ArrayMapper<fortran_complex> array(data, sizes);

        for(size_t i = 0; i < num_flavors; i++)
            for(size_t k = 0; k < config->num_matsubara; k++) {
                array[OBS_DATA][k](i).r = mc_results->GetResult("green_omega_real")->mean_[i * config->num_matsubara + k];
                array[OBS_DATA][k](i).i = mc_results->GetResult("green_omega_imag")->mean_[i * config->num_matsubara + k];
                array[OBS_ERROR][k](i).r = mc_results->GetResult("green_omega_real")->error_[i * config->num_matsubara + k];
                array[OBS_VARIANCE][k](i).r = mc_results->GetResult("green_omega_real")->variance_[i * config->num_matsubara + k];
                array[OBS_TAU][k](i).r = mc_results->GetResult("green_omega_real")->tau_[i * config->num_matsubara + k];

                array[OBS_ERROR][k](i).i = mc_results->GetResult("green_omega_imag")->error_[i * config->num_matsubara + k];
                array[OBS_VARIANCE][k](i).i = mc_results->GetResult("green_omega_imag")->variance_[i * config->num_matsubara + k];
                array[OBS_TAU][k](i).i = mc_results->GetResult("green_omega_imag")->tau_[i * config->num_matsubara + k];
            }

        break;
    }
    case DATA_SIGN:
    {
        std::vector<size_t> sizes = boost::assign::list_of(1);
        ArrayMapper<double> array(data, sizes);

        array(0) = mc_results->GetResult("sign")->mean_[0];
        /*array[OBS_ERROR](0) = mc_results->GetResult("sign")->error_[0];
        array[OBS_VARIANCE](0) = mc_results->GetResult("sign")->variance_[0];
        array[OBS_TAU](0) = mc_results->GetResult("sign")->tau_[0];*/

        break;
    }
    case DATA_NNT:
    {
        std::vector<size_t> sizes = boost::assign::list_of(4)(config->num_nn + 1)(num_flavors / 2)(num_flavors / 2);
        ArrayMapper<double> array(data, sizes);

        size_t num_nn = config->num_nn;
        for(size_t t0 = 0; t0 < num_nn + 1; t0++)
            for(size_t n0 = 0; n0 < num_flavors / 2; n0++)
                for(size_t n1 = 0; n1 < num_flavors / 2; n1++) {
                    array[0][t0][n1](n0) = mc_results->GetResult("nnt")->mean_[t0 * (num_flavors * num_flavors) + n0 * num_flavors + n1]; //n0,n0
                    array[1][t0][n1](n0) = mc_results->GetResult("nnt")->mean_[t0 * (num_flavors * num_flavors) + n0 * num_flavors + (n1 + num_flavors / 2)]; //n0, n1
                    array[2][t0][n1](n0) = mc_results->GetResult("nnt")->mean_[t0 * (num_flavors * num_flavors) + (n0 + num_flavors / 2) * num_flavors + n1]; //n1, n0
                    array[3][t0][n1](n0) = mc_results->GetResult("nnt")->mean_[t0 * (num_flavors * num_flavors) + (n0 + num_flavors / 2) * num_flavors + (n1 + num_flavors / 2)]; //n1, n1

                    /*array[OBS_ERROR][0][t0][n1](n0) = mc_results->GetResult("nnt")->error_[t0 * (num_flavors * num_flavors) + n0 * num_flavors + n1]; //n0,n0
                    array[OBS_ERROR][1][t0][n1](n0) = mc_results->GetResult("nnt")->error_[t0 * (num_flavors * num_flavors) + n0 * num_flavors + (n1 + num_flavors / 2)]; //n0, n1
                    array[OBS_ERROR][2][t0][n1](n0) = mc_results->GetResult("nnt")->error_[t0 * (num_flavors * num_flavors) + (n0 + num_flavors / 2) * num_flavors + n1]; //n1, n0
                    array[OBS_ERROR][3][t0][n1](n0) = mc_results->GetResult("nnt")->error_[t0 * (num_flavors * num_flavors) + (n0 + num_flavors / 2) * num_flavors + (n1 + num_flavors / 2)]; //n1, n1

                    array[OBS_VARIANCE][0][t0][n1](n0) = mc_results->GetResult("nnt")->variance_[t0 * (num_flavors * num_flavors) + n0 * num_flavors + n1]; //n0,n0
                    array[OBS_VARIANCE][1][t0][n1](n0) = mc_results->GetResult("nnt")->variance_[t0 * (num_flavors * num_flavors) + n0 * num_flavors + (n1 + num_flavors / 2)]; //n0, n1
                    array[OBS_VARIANCE][2][t0][n1](n0) = mc_results->GetResult("nnt")->variance_[t0 * (num_flavors * num_flavors) + (n0 + num_flavors / 2) * num_flavors + n1]; //n1, n0
                    array[OBS_VARIANCE][3][t0][n1](n0) = mc_results->GetResult("nnt")->variance_[t0 * (num_flavors * num_flavors) + (n0 + num_flavors / 2) * num_flavors + (n1 + num_flavors / 2)]; //n1, n1

                    array[OBS_TAU][0][t0][n1](n0) = mc_results->GetResult("nnt")->tau_[t0 * (num_flavors * num_flavors) + n0 * num_flavors + n1]; //n0,n0
                    array[OBS_TAU][1][t0][n1](n0) = mc_results->GetResult("nnt")->tau_[t0 * (num_flavors * num_flavors) + n0 * num_flavors + (n1 + num_flavors / 2)]; //n0, n1
                    array[OBS_TAU][2][t0][n1](n0) = mc_results->GetResult("nnt")->tau_[t0 * (num_flavors * num_flavors) + (n0 + num_flavors / 2) * num_flavors + n1]; //n1, n0
                    array[OBS_TAU][3][t0][n1](n0) = mc_results->GetResult("nnt")->tau_[t0 * (num_flavors * num_flavors) + (n0 + num_flavors / 2) * num_flavors + (n1 + num_flavors / 2)]; //n1, n1*/
                }

        break;
    }
    case DATA_M_SIZE:
    {
        std::vector<size_t> sizes = boost::assign::list_of(num_flavors);
        ArrayMapper<double> array(data, sizes);

        for(size_t i = 0; i < num_flavors; i++) {
            array(i) = mc_results->GetResult("matrix_size")->mean_[i];
            /*array[OBS_ERROR](i) = mc_results->GetResult("matrix_size")->error_[i];
            array[OBS_VARIANCE](i) = mc_results->GetResult("matrix_size")->variance_[i];
            array[OBS_TAU](i) = mc_results->GetResult("matrix_size")->tau_[i];*/
        }

        break;
    }
    case DATA_DENSITY:
    {
        std::vector<size_t> sizes = boost::assign::list_of(NUM_OBS)(num_flavors);
        ArrayMapper<double> array(data, sizes);

        for(size_t i = 0; i < num_flavors; i++) {
            array[OBS_DATA](i) = mc_results->GetResult("density")->mean_[i];
            array[OBS_ERROR](i) = mc_results->GetResult("density")->error_[i];
            array[OBS_VARIANCE](i) = mc_results->GetResult("density")->variance_[i];
            array[OBS_TAU](i) = mc_results->GetResult("density")->tau_[i];
        }

        break;
    }
    case DATA_SIGMAG_LEGENDRE:
    {
        std::vector<size_t> sizes = boost::assign::list_of(NUM_OBS)(config->num_legendre)(num_flavors);
        ArrayMapper<double> array(data, sizes);

        for(size_t i = 0; i < num_flavors; i++)
            for(size_t k = 0; k < config->num_legendre; k++) {
                array[OBS_DATA][k](i) = mc_results->GetResult("sigmag_legendre")->mean_[i * config->num_legendre + k];
                array[OBS_ERROR][k](i) = mc_results->GetResult("sigmag_legendre")->error_[i * config->num_legendre + k];
                array[OBS_VARIANCE][k](i) = mc_results->GetResult("sigmag_legendre")->variance_[i * config->num_legendre + k];
                array[OBS_TAU][k](i) = mc_results->GetResult("sigmag_legendre")->tau_[i * config->num_legendre + k];
            }

        break;
    }
    case DATA_STATISTICS:
    {
        std::vector<size_t> sizes = boost::assign::list_of(3)(5);
        ArrayMapper<double> array(data, sizes);

        std::vector< std::vector<int> > stat = s->GetStatistics();
        for(size_t i = 0; i < 5; i++) {
            size_t num = 3;
            if(i > 2)
                num = 2;
            for(size_t k = 0; k < num; k++) {
                array[k](i) = static_cast<double>(stat[i][k]);
            }
        }

        break;
    }
    case DATA_SIGMAG_OMEGA:
    {
        std::vector<size_t> sizes = boost::assign::list_of(NUM_OBS)(config->num_matsubara)(num_flavors);
        ArrayMapper<fortran_complex> array(data, sizes);

        for(size_t i = 0; i < num_flavors; i++)
            for(size_t k = 0; k < config->num_matsubara; k++) {
                array[OBS_DATA][k](i).r = mc_results->GetResult("sigmag_omega_real")->mean_[i * config->num_matsubara + k];
                array[OBS_DATA][k](i).i = mc_results->GetResult("sigmag_omega_imag")->mean_[i * config->num_matsubara + k];
                array[OBS_ERROR][k](i).r = mc_results->GetResult("sigmag_omega_real")->error_[i * config->num_matsubara + k];
                array[OBS_VARIANCE][k](i).r = mc_results->GetResult("sigmag_omega_real")->variance_[i * config->num_matsubara + k];
                array[OBS_TAU][k](i).r = mc_results->GetResult("sigmag_omega_real")->tau_[i * config->num_matsubara + k];

                array[OBS_ERROR][k](i).i = mc_results->GetResult("sigmag_omega_imag")->error_[i * config->num_matsubara + k];
                array[OBS_VARIANCE][k](i).i = mc_results->GetResult("sigmag_omega_imag")->variance_[i * config->num_matsubara + k];
                array[OBS_TAU][k](i).i = mc_results->GetResult("sigmag_omega_imag")->tau_[i * config->num_matsubara + k];
            }

        break;
    }
    case DATA_NN:
    {
        std::vector<size_t> sizes = boost::assign::list_of(num_flavors)(num_flavors);
        ArrayMapper<double> array(data, sizes);

        for(size_t f1 = 0; f1 < num_flavors; ++f1)
            for(size_t f2 = 0; f2 < num_flavors; ++f2) {
                array[f2](f1) = mc_results->GetResult("nn")->mean_[f1 * num_flavors + f2]; //fortran would be nn(f1,f2)
                /*array[OBS_ERROR][f2](f1) = mc_results->GetResult("nn")->error_[f1 * num_flavors + f2];
                array[OBS_VARIANCE][f2](f1) = mc_results->GetResult("nn")->variance_[f1 * num_flavors + f2];
                array[OBS_TAU][f2](f1) = mc_results->GetResult("nn")->tau_[f1 * num_flavors + f2];*/
            }

        break;
    }
    case DATA_SECTOR_STATISTICS:
    {
        size_t array_size = pow(2, num_flavors);

        std::vector<size_t> sizes = boost::assign::list_of(array_size);
        ArrayMapper<double> array(data, sizes);

        for(size_t i = 0; i < array_size; i++) {
            array(i) = mc_results->GetResult("sector_statistics")->mean_[i];
            /*array[OBS_ERROR](i) = mc_results->GetResult("atomic_weight_discrete")->error_[i];
            array[OBS_VARIANCE](i) = mc_results->GetResult("atomic_weight_discrete")->variance_[i];
            array[OBS_TAU](i) = mc_results->GetResult("atomic_weight_discrete")->tau_[i];*/
        }

        break;
    }
    case DATA_SIGMAG_TAU:
    {
        std::vector<size_t> sizes = boost::assign::list_of(config->num_slices + 1)(num_flavors);
        ArrayMapper<double> array(data, sizes);

        for(size_t i = 0; i < num_flavors; i++)
            for(size_t point = 0; point < config->num_slices + 1; point++) {
                array[point](i) = -mc_results->GetResult("sigmag_tau")->mean_[i * (config->num_slices + 1) + point];
                /*array[OBS_ERROR][point](i) = mc_results->GetResult("sigmag_tau")->error_[i * (config->num_slices + 1) + point];
                array[OBS_VARIANCE][point](i) = mc_results->GetResult("sigmag_tau")->variance_[i * (config->num_slices + 1) + point];
                array[OBS_TAU][point](i) = mc_results->GetResult("sigmag_tau")->tau_[i * (config->num_slices + 1) + point];*/
            }

        break;
    }
    case DATA_ENERGY:
    {
        std::vector<size_t> sizes = boost::assign::list_of(2);
        ArrayMapper<double> array(data, sizes);

        array(0) = mc_results->GetResult("energy")->mean_[0];
        array(1) = mc_results->GetResult("energy")->mean_[1];
        /*observables.at(OBS_ERROR)(0) = mc_results->GetResult("energy")->error_[0];
        observables.at(OBS_ERROR)(1) = mc_results->GetResult("energy")->error_[1];
        observables.at(OBS_VARIANCE)(0) = mc_results->GetResult("energy")->variance_[0];
        observables.at(OBS_VARIANCE)(1) = mc_results->GetResult("energy")->variance_[1];
        observables.at(OBS_TAU)(0) = mc_results->GetResult("energy")->tau_[0];
        observables.at(OBS_TAU)(1) = mc_results->GetResult("energy")->tau_[1];*/

        break;
    }
    default:
    {
        std::stringstream out_stream;
        out_stream << "Unknown data type: " << _type;
        s->Exception_RuntimeError(out_stream);
        break;
    }
    }
}

extern "C" void c_set_parameter_int(const unsigned long long &solver, const char *string, const int &i)
{
    std::string parameter = string;

    HybridizationSolver *s = reinterpret_cast<HybridizationSolver*>(solver);
    CommonConfig *config = s->GetConfig();

    std::stringstream out_stream;
    out_stream << "Setting parameter: " << parameter << " : " << i;

    if (parameter == CFG_THERMALIZATION_SWEEPS) {
        config->thermalization_sweeps = i;
    } else if (parameter == CFG_TOTAL_SWEEPS) {
        config->total_sweeps = i;
    } else if (parameter == CFG_NUM_NN) {
        config->num_nn = i;
    } else if (parameter == CFG_NUM_UPDATES) {
        config->num_updates = i;
    } else if (parameter == CFG_NUM_MEAS) {
        config->num_meas = i;
    } else if (parameter == CFG_OBSERVABLE_BIN_SIZE) {
#ifndef NO_RESAMPLING
        config->observable_bin_size = i;
#else
        out_stream << "Compiled with NO_RESAMPLING! " << string << " ignored." << std::endl;
#endif
    } else if (parameter == CFG_NUM_MATSUBARA) {
        config->num_matsubara = i;
    } else if (parameter == CFG_NUM_LEGENDRE) {
        config->num_legendre = i;
    } else if (parameter == CFG_NUM_SLICES) {
        config->num_slices = i;
    } else if (parameter == CFG_NUM_FLAVORS) {
        config->num_flavors = i;
    } else if (parameter == CFG_MAXIMUM_TIME) {
        config->max_time = i;
    } else if (parameter == CFG_NUM_MATSUBARA2) {
        config->num_matsubara2 = i;
    } else if (parameter == CFG_NUM_BOSONIC) {
        config->num_bosonic = i;
    } else if (parameter == CFG_ARG_CACHE_SIZE) {
        config->arg_cache_size = i;
    } else {
        out_stream << std::endl << "Unknown parameter: " << string << std::endl;
        s->Exception_RuntimeError(out_stream);
    }
    s->WriteLog(out_stream);
}

extern "C" void c_set_parameter_double(const unsigned long long &solver, const char *string, const double &d)
{
    std::string parameter = string;

    HybridizationSolver *s = reinterpret_cast<HybridizationSolver*>(solver);
    CommonConfig *config = s->GetConfig();

    std::stringstream out_stream;
    out_stream << "Setting parameter: " << parameter << " : " << d;

    if (parameter == CFG_BETA) {
        config->beta = d;
    } else {
        out_stream << std::endl << "Unknown parameter: " << string << std::endl;
        s->Exception_RuntimeError(out_stream);
    }

    s->WriteLog(out_stream);
}

extern "C" void c_set_parameter_string(const unsigned long long &solver, const char *string, const char *data)
{
    std::string parameter = string;
    std::string string_value = boost::algorithm::to_lower_copy(std::string(data));

    HybridizationSolver *s = reinterpret_cast<HybridizationSolver*>(solver);
    CommonConfig *config = s->GetConfig();

    std::stringstream out_stream;
    out_stream << "Setting parameter: " << parameter << " : " << string_value;

    if (parameter == CFG_UPDATES) {
        if(string_value.empty()) {
            out_stream << std::endl << "Specify at least one update!";
            s->Exception_RuntimeError(out_stream);
        }
        config->updates_.clear();
        std::vector<std::string> first;
        boost::algorithm::split(first, string_value, boost::algorithm::is_any_of("+"), boost::algorithm::token_compress_on);
        for(size_t i = 0; i < first.size(); i++) {
            std::vector<std::string> second;
            boost::algorithm::split(second, first.at(i), boost::algorithm::is_any_of(":"), boost::algorithm::token_compress_on);
            std::vector<std::string>::iterator it;
            for(it = second.begin(); it != second.end(); it++) {
                if(it->compare("") == 0)
                    second.erase(it);
            }
            double weight = 1.0; //assume "1.0" if none specified
            if(second.size() > 1)
                weight = boost::lexical_cast<double>(second.at(1));
            if(second.at(0).compare("full_line") == 0) {
                config->updates_.push_back(std::pair<UPDATE_TYPE, double>(UPDATE_FULL_LINE, weight));
                if(config->verbosity > 0)
                    out_stream << std::endl << "Using update UPDATE_FULL_LINE with weight " << weight;
            } else if(second.at(0).compare("segment") == 0) {
                config->updates_.push_back(std::pair<UPDATE_TYPE, double>(UPDATE_SEGMENT, weight));
                if(config->verbosity > 0)
                    out_stream << std::endl << "Using update UPDATE_SEGMENT with weight " << weight;
            } else if(second.at(0).compare("antisegment") == 0) {
                config->updates_.push_back(std::pair<UPDATE_TYPE, double>(UPDATE_ANTISEGMENT, weight));
                if(config->verbosity > 0)
                    out_stream << std::endl << "Using update UPDATE_ANTISEGMENT with weight " << weight;
            } else if(second.at(0).compare("shift") == 0) {
                config->updates_.push_back(std::pair<UPDATE_TYPE, double>(UPDATE_SHIFT, weight));
                if(config->verbosity > 0)
                    out_stream << std::endl << "Using update UPDATE_SHIFT with weight " << weight;
            } else if(second.at(0).compare("global") == 0) {
                config->updates_.push_back(std::pair<UPDATE_TYPE, double>(UPDATE_GLOBAL, weight));
                if(config->verbosity > 0)
                    out_stream << std::endl << "Using update UPDATE_GLOBAL with weight " << weight;
            } else {
                out_stream << std::endl << "Unknow update type! Please recheck input.";
                s->Exception_RuntimeError(out_stream);
            }
        }
    } else if(parameter == CFG_SLICING_METHOD) {
        if(string_value.empty()) {
            out_stream << std::endl << "String parameter should not be empty!";
            s->Exception_RuntimeError(out_stream);
        }
        if(string_value == "dumb")
            config->slicing_method = SLICING_DUMB;
        else if(string_value == "dumb_proper")
            config->slicing_method = SLICING_DUMB_PROPER;
        else if(string_value == "linear")
            config->slicing_method = SLICING_LINEAR_FIT;
        else if(string_value == "gauss")
            config->slicing_method = SLICING_GAUSSIAN_FIT;
        else {
            out_stream << std::endl << "Unknown slicing method " << string_value << ", using SLICING_DUMB!";
            config->slicing_method = SLICING_DUMB;
        }
    } else if(parameter == CFG_RANDOM_GENERATOR) {
        if(string_value.empty()) {
            out_stream << std::endl << "String parameter should not be empty!";
            s->Exception_RuntimeError(out_stream);
        }
        if(string_value == "boost_mt19937")
            config->random_generator = RANDOM_BOOST_MT19937;
        else if(string_value == "dsfmt")
            config->random_generator = RANDOM_DSFMT;
        else {
            out_stream << std::endl << "Unknown random generator " << string_value << ", using BOOST_MT19937!";
            config->random_generator = RANDOM_BOOST_MT19937;
        }
    } else {
        out_stream << std::endl << "Unknown parameter: " << string;
        s->Exception_RuntimeError(out_stream);
    }
    s->WriteLog(out_stream);
}

extern "C" void c_set_parameter_bool(const unsigned long long &solver, const char *string, const bool &b)
{
    std::string parameter = string;

    int int_value = static_cast<int>(b);
    bool bool_value = (int_value != 0 ? true : false); //fortran hack

    HybridizationSolver *s = reinterpret_cast<HybridizationSolver*>(solver);
    CommonConfig *config = s->GetConfig();

    std::stringstream out_stream;
    out_stream << "Setting parameter: " << parameter << " : ";
    if(bool_value == true)
        out_stream << "true";
    else
        out_stream << "false";

    if (parameter == CFG_MEASURE_GREEN_OMEGA) {
        config->measure_green_omega = bool_value;
    } else if (parameter == CFG_MEASURE_GREEN_LEGENDRE) {
        config->measure_green_legendre = bool_value;
    } else if (parameter == CFG_MEASURE_NNT) {
        config->measure_nnt = bool_value;
    } else if (parameter == CFG_MEASURE_SECTOR_STATISTICS) {
        config->measure_sector_statistics = bool_value;
    } else if (parameter == CFG_MEASURE_NN) {
        config->measure_nn = bool_value;
    } else if (parameter == CFG_MEASURE_SIGMAG) {
        config->measure_sigmag = bool_value;
    } else if (parameter == CFG_MEASURE_GREEN2_OMEGA) {
        config->measure_green2_omega = bool_value;
    } else if (parameter == CFG_USE_ARG_CACHE) {
        config->use_arg_cache = bool_value;
    } else {
        out_stream << std::endl << "Unknown parameter: " << string;
        s->Exception_RuntimeError(out_stream);
    }

    s->WriteLog(out_stream);
}

extern "C" void f_array_test(double *array)
{
    double array1[2][5][5];
    memcpy(array1, array, sizeof(double) * 2 * 5 * 5);
    for(size_t is = 0; is < 2; is++) {
        for(size_t i = 0; i < 5; i++) {
            std::cout << array1[is][i][i] << std::endl;
        }
    }
}

extern "C" void f_transform_legendre(const unsigned long long &solver, const double *legendre, fortran_complex *green_omega, double *moments, int *predicted, int *legendre_cutoff)
{
    HybridizationSolver *s = reinterpret_cast<HybridizationSolver*>(solver);
    CommonConfig *config = s->GetConfig();

    size_t num_flavors = config->num_flavors;

    std::vector<size_t> sizes_legendre = boost::assign::list_of(NUM_OBS)(config->num_legendre)(num_flavors);
    std::vector<size_t> sizes_predicted = boost::assign::list_of(num_flavors);
    std::vector<size_t> sizes_moments = boost::assign::list_of(config->num_legendre)(num_flavors);
    std::vector<size_t> sizes_matsubara = boost::assign::list_of(config->num_matsubara)(num_flavors);
    ArrayMapper<double> fortran_green_legendre((void*)legendre, sizes_legendre);
    ArrayMapper<int> fortran_predicted((void*)predicted, sizes_predicted);
    ArrayMapper<int> fortran_cutoff((void*)legendre_cutoff, sizes_predicted);
    ArrayMapper<double> fortran_green_moments((void*)moments, sizes_moments);
    ArrayMapper<fortran_complex> fortran_green_omega((void*)green_omega, sizes_matsubara);
    fortran_green_omega.Clear();
    fortran_green_moments.Clear();

    std::vector< std::valarray<double> > green_legendre_pp;
    green_legendre_pp.resize(num_flavors);

    for(size_t f = 0; f < num_flavors; f++) {
        green_legendre_pp[f].resize(config->num_legendre);
        for(size_t l = 0; l < config->num_legendre; l++)
            green_legendre_pp[f][l] = fortran_green_legendre[OBS_DATA][l](f);
        cpp_green_legendre_restore(green_legendre_pp[f]);
    }

    std::vector< std::valarray<double> > green_legendre;
    std::vector< std::valarray<double> > green_legendre_test;
    std::vector< std::valarray< std::complex<double> > > green_omega_legendre;
    green_legendre.resize(num_flavors);
    green_legendre_test.resize(num_flavors);
    green_omega_legendre.resize(num_flavors);

    for(size_t f = 0; f < num_flavors; f++) {
        fortran_predicted(f) = cpp_green_legendre_analyze(green_legendre_pp[f], config->beta, green_legendre_test[f]);
        int cutoff = fortran_cutoff(f);
        if(cutoff == 0) {
            cutoff = green_legendre_pp[f].size();
            fortran_cutoff(f) = cutoff;
        } else if(cutoff == -1) {
            cutoff = fortran_predicted(f);
            fortran_cutoff(f) = cutoff;
        }
        green_legendre[f].resize(cutoff);
        for(size_t i = 0; i < cutoff; i++)
            green_legendre[f][i] = green_legendre_pp[f][i];
        //std::cout << "Best predicted legendre size (" << f << ") = " << green_legendre_test[f].size() - 1 << std::endl;
        //double c1;
        ////double c2;
        //double c3;
        //cpp_get_green_legendre_moments(green_legendre[f], config->beta, c1, c2, c3);
        //std::cout << "Resulting moments are " << c1 << " " << c2 << " " << c3 << std::endl;
        cpp_green_legendre_impose_c1(green_legendre[f], config->beta);
        green_omega_legendre[f].resize(config->num_matsubara);
        cpp_green_omega_from_legendre(green_legendre[f], green_omega_legendre[f]);
    }

    for(size_t f = 0; f < num_flavors; f++) {
        for(size_t w = 0; w < config->num_matsubara; w++) {
            fortran_green_omega[w](f).r = green_omega_legendre[f][w].real();
            fortran_green_omega[w](f).i = green_omega_legendre[f][w].imag();
        }
        for(size_t l = 2; l < config->num_legendre; l += 2)
            fortran_green_moments[l](f) = green_legendre_test[f][l];
    }
}

extern "C" void f_sigma_legendre(const unsigned long long &solver, const double *green_legendre, const double *sigmag_legendre, fortran_complex *sigma, int *legendre_cutoff)
{
    HybridizationSolver *s = reinterpret_cast<HybridizationSolver*>(solver);
    CommonConfig *config = s->GetConfig();

    size_t num_flavors = config->num_flavors;

    std::vector<size_t> sizes_legendre = boost::assign::list_of(NUM_OBS)(config->num_legendre)(num_flavors);
    std::vector<size_t> sizes_matsubara = boost::assign::list_of(config->num_matsubara)(num_flavors);
    std::vector<size_t> sizes_cutoff = boost::assign::list_of(num_flavors);
    ArrayMapper<double> fortran_green_legendre((void*)green_legendre, sizes_legendre);
    ArrayMapper<double> fortran_sigmag_legendre((void*)sigmag_legendre, sizes_legendre);
    ArrayMapper<fortran_complex> fortran_sigma((void*)sigma, sizes_matsubara);
    ArrayMapper<int> fortran_cutoff((void*)legendre_cutoff, sizes_cutoff);

    fortran_sigma.Clear();

    std::vector< std::valarray<double> > green_legendre_pp;
    std::vector< std::valarray<double> > sigmag_legendre_pp;
    green_legendre_pp.resize(num_flavors);
    sigmag_legendre_pp.resize(num_flavors);

    for(size_t f = 0; f < num_flavors; f++) {
        green_legendre_pp[f].resize(config->num_legendre);
        sigmag_legendre_pp[f].resize(config->num_legendre);;
        for(size_t l = 0; l < config->num_legendre; l++) {
            green_legendre_pp[f][l] = fortran_green_legendre[OBS_DATA][l](f);
            sigmag_legendre_pp[f][l] = fortran_sigmag_legendre[OBS_DATA][l](f);
        }
        cpp_green_legendre_restore(green_legendre_pp[f]);
        cpp_green_legendre_restore(sigmag_legendre_pp[f]);
    }

    std::vector< std::valarray<double> > green_legendre0;
    std::vector< std::valarray< std::complex<double> > > green_omega_legendre;
    green_legendre0.resize(num_flavors);
    green_omega_legendre.resize(num_flavors);
    std::vector< std::valarray<double> > sigmag_legendre0;
    std::vector< std::valarray< std::complex<double> > > sigmag_omega_legendre;
    sigmag_legendre0.resize(num_flavors);
    sigmag_omega_legendre.resize(num_flavors);

    for(size_t f = 0; f < num_flavors; f++) {
        green_legendre0[f].resize(fortran_cutoff(f));
        sigmag_legendre0[f].resize(fortran_cutoff(f));
        for(size_t i = 0; i < fortran_cutoff(f); i++) {
            green_legendre0[f][i] = green_legendre_pp[f][i];
            sigmag_legendre0[f][i] = sigmag_legendre_pp[f][i];
        }
        green_omega_legendre[f].resize(config->num_matsubara);
        cpp_green_omega_from_legendre(green_legendre0[f], green_omega_legendre[f]);
        sigmag_omega_legendre[f].resize(config->num_matsubara);
        cpp_green_omega_from_legendre(sigmag_legendre0[f], sigmag_omega_legendre[f]);
    }

    std::vector< std::valarray< std::complex<double> > > sigma_omega;
    sigma_omega.resize(num_flavors);

    for(size_t f = 0; f < num_flavors; f++) {
        sigma_omega[f].resize(config->num_matsubara);
        for(size_t w = 0; w < config->num_matsubara; w++)
            sigma_omega[f][w] = sigmag_omega_legendre[f][w] / green_omega_legendre[f][w];
    }

    for(size_t f = 0; f < num_flavors; f++)
        for(size_t w = 0; w < config->num_matsubara; w++) {
            fortran_sigma[w](f).r = sigma_omega[f][w].real();
            fortran_sigma[w](f).i = sigma_omega[f][w].imag();
        }
}

inline double interpolate_F(double t, double BETA, const std::valarray<double>& F)
{
    double sign=1.0;
    double real_tau = t;
    if (real_tau < 0.0) {
        real_tau += BETA;
        sign = -1.0;
    }

    int N = F.size() - 1;
    double n = real_tau/BETA*N;
    int n_lower = n; // interpolate linearly between n_lower and n_lower+1

    if(n_lower + 1 == F.size())
        return F[n_lower];

    return sign*(F[n_lower] + (n-n_lower)*(F[n_lower+1]-F[n_lower]));
}


//Fourier transform stack
extern "C" void cpp_legendre_from_tau(const std::valarray<double> &tau, std::valarray<double> &legendre, const double &beta)
{
    int num_points = 500000; //tau.size();
    int num_legendre = legendre.size();
    legendre = 0.0;
    for(int slice = 0; slice < num_points; slice++) {
        double taup = beta * static_cast<double>(slice) / static_cast<double>(num_points - 1);
        const double x = 2.0 * taup / beta - 1.0;
        double pl_2 = 1.0;
        double pl_1 = x;
        double legendre_p;
        {
            for(int l = 0; l < num_legendre; l++) {
                if(l == 0)
                    legendre_p = 1.0;
                else if (l == 1)
                    legendre_p = x;
                else {
                    legendre_p = ((2.0 * l - 1.0) * x * pl_1 - (l - 1.0) * pl_2) / static_cast<double>(l);//l
                    pl_2 = pl_1; //l-2
                    pl_1 = legendre_p; //l-1
                }
                //legendre[l] -= tau[slice] * legendre_p * sqrt(2.0 * static_cast<double>(l) + 1.0);
                legendre[l] -= interpolate_F(taup, beta, tau) * legendre_p * sqrt(2.0 * static_cast<double>(l) + 1.0);
            }
        }
    }
    legendre *= beta / (num_points - 1);
}

extern "C" void cpp_omega_from_tau(const std::valarray<double> &tau, std::valarray< std::complex<double> > &omega, size_t nlegendre, const double &beta)
{
    std::valarray<double> legendre(nlegendre);
    legendre = 0.0;
    cpp_legendre_from_tau(tau, legendre, beta);
    cpp_omega_from_legendre(legendre, omega);
}

extern "C" void f_omega_from_tau(const unsigned long long &solver, const double *tau, fortran_complex *omega, const int &nlegendre)
{
    size_t nleg = nlegendre;
    HybridizationSolver *s = reinterpret_cast<HybridizationSolver*>(solver);
    CommonConfig *config = s->GetConfig();

    size_t num_flavors = config->num_flavors;

    std::vector<size_t> sizes_tau = boost::assign::list_of(config->num_slices + 1)(num_flavors);
    std::vector<size_t> sizes_matsubara = boost::assign::list_of(config->num_matsubara)(num_flavors);
    ArrayMapper<double> fortran_tau((void*)tau, sizes_tau);
    ArrayMapper<fortran_complex> fortran_omega((void*)omega, sizes_matsubara);

    fortran_omega.Clear();
    std::vector< std::valarray<double> > ctau(num_flavors);
    std::vector< std::valarray< std::complex<double> > > comega(num_flavors);
    for(size_t i = 0; i < num_flavors; i++) {
        ctau[i].resize(config->num_slices + 1);
        comega[i].resize(config->num_matsubara);
        for(size_t slice = 0; slice < config->num_slices + 1; slice++)
            ctau[i][slice] = fortran_tau[slice](i);
    }
    for(size_t i = 0; i < ctau.size(); i++) {
        cpp_omega_from_tau(ctau[i], comega[i], nleg, config->beta);
    }
    for(size_t i = 0; i < num_flavors; i++)
        for(size_t w = 0; w < config->num_matsubara; w++) {
            fortran_omega[w](i).r = comega[i][w].real();
            fortran_omega[w](i).i = comega[i][w].imag();
        }
}


extern "C" void c_mem_info(int &vmpeak, int &vmsize, int &vmrss)
{
    std::ifstream status("/proc/self/status");
    if(!status.is_open()) {
        std::cerr << "Can't open /proc/self/status! Memory information will be unavailable!" << std::endl;
        return;
    }
    while(!status.eof()) {
        std::string s_line;
        std::getline(status, s_line);
#ifndef NDEBUG
        std::cout << s_line.c_str() << std::endl;
#endif
        std::vector<std::string> split;
        boost::algorithm::split(split, s_line, boost::algorithm::is_any_of(" \t"), boost::algorithm::token_compress_on);
        if(split[0] == "VmPeak:") {
            vmpeak = boost::lexical_cast<int>(split[1]);
        } else if(split[0] == "VmSize:") {
            vmsize = boost::lexical_cast<int>(split[1]);
        } else if(split[0] == "VmRSS:") {
            vmrss = boost::lexical_cast<int>(split[1]);
        }
    }
}

