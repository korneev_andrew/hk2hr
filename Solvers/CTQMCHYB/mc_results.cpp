#include "mc_results.h"
#include <boost/scoped_ptr.hpp>
#include <boost/scoped_array.hpp>
#include <mpi.h>
#include "system_info.h"
#ifdef COMPILER_IS_OLD
#include <iostream>
#endif

#define MPI_TAG_IN_FLAMES_WE_TRUST 31337
#define MPI_TAG_RUST_IN_PEACE 31338
#define MPI_TAG_WHORACLE 31339
#define MPI_TAG_PEACE_SELLS_BUT_WHOS_BUYING 31340
#define MPI_TAG_BEHIND_IN_SPACE_99 31341
#define MPI_TAG_CLAD_IN_SHADOWS_99 31342

SingleResult::SingleResult(ObservableContainer<> *cont)
{
    container_ = new ObservableContainer<>(cont->BinSize(), cont->Name().c_str());
    parent_container_ = cont;
    name_ = container_->Name();
}

SingleResult::~SingleResult()
{
    if(container_)
        delete container_;
}

MCResults::MCResults()
{
}

MCResults::~MCResults()
{
    Clear();
}

void MCResults::InsertResult(ObservableContainer<> *observable)
{
    results_.push_back(std::pair<std::string, SingleResult*>(observable->Name(), new SingleResult(observable)));
}

void MCResults::Clear()
{
    for(size_t i = 0; i < results_.size(); i++)
        delete results_[i].second;
    results_.clear();
}

SingleResult *MCResults::GetResult(const char *name)
{
    std::string str_name = name;
    for(size_t i = 0; i < results_.size(); i++) {
        if(results_[i].first == str_name)
            return results_[i].second;
    }

    return NULL;
}

void MCResults::SyncResults()
{
    size_t world_size = 0;
    int flag = 0;
    MPI_Initialized(&flag);
    if(flag)
        world_size = MPI::COMM_WORLD.Get_size();
    for(size_t i = 0; i < results_.size(); i++) {
        SingleResult *sr = results_[i].second;
        if(world_size) {
            if(MPI::COMM_WORLD.Get_rank() == 0) {
#ifndef NDEBUG
                std::cout << "rank " << MPI::COMM_WORLD.Get_rank() << " entered rank 0 section of " << results_[i].first << std::endl;
                std::cout << std::flush;
#endif
                int parent_buffer_size = 0;
                boost::shared_array<double> parent_buffer;
                parent_buffer_size = sr->parent_container_->ExportValues(parent_buffer);
                size_t v0size = sr->parent_container_->Values0Size();
                sr->container_->ImportValues(parent_buffer, v0size);
                sr->parent_container_ = NULL;
                for(size_t rank = 1; rank < world_size; rank++) {
                    int buffer_size = 0;
                    MPI::COMM_WORLD.Recv(&buffer_size, 1, MPI_INT, rank, MPI_TAG_BEHIND_IN_SPACE_99);
                    boost::shared_array<double> buffer(new double[buffer_size]);
                    MPI::COMM_WORLD.Recv(buffer.get(), buffer_size, MPI_DOUBLE, rank, MPI_TAG_IN_FLAMES_WE_TRUST);
                    sr->container_->ImportValues(buffer);
                }
#ifndef NDEBUG
                sr->container_->Report();
#endif
                sr->mean_.resize(v0size);
                sr->mean_ = sr->container_->Mean();
                sr->error_.resize(v0size);
                sr->error_ = sr->container_->Error();
                sr->variance_.resize(v0size);
                sr->variance_ = sr->container_->Variance();
                sr->tau_.resize(v0size);
                sr->tau_ = sr->container_->Tau();

                boost::shared_array<double> buffer_mean(new double[v0size]);
                boost::shared_array<double> buffer_error(new double[v0size]);
                boost::shared_array<double> buffer_variance(new double[v0size]);
                boost::shared_array<double> buffer_tau(new double[v0size]);
                for(size_t i = 0; i < v0size; i++) {
                    buffer_mean[i] = sr->mean_[i];
                    buffer_error[i] = sr->error_[i];
                    buffer_variance[i] = sr->variance_[i];
                    buffer_tau[i] = sr->tau_[i];
                }
                for(size_t rank = 1; rank < world_size; rank++) {
                    MPI::COMM_WORLD.Send(buffer_mean.get(), v0size, MPI_DOUBLE, rank, MPI_TAG_RUST_IN_PEACE);
                    MPI::COMM_WORLD.Send(buffer_error.get(), v0size, MPI_DOUBLE, rank, MPI_TAG_WHORACLE);
                    MPI::COMM_WORLD.Send(buffer_variance.get(), v0size, MPI_DOUBLE, rank, MPI_TAG_PEACE_SELLS_BUT_WHOS_BUYING);
                    MPI::COMM_WORLD.Send(buffer_tau.get(), v0size, MPI_DOUBLE, rank, MPI_TAG_CLAD_IN_SHADOWS_99);
                }
            } else {
#ifndef NDEBUG
                std::cout << "rank " << MPI::COMM_WORLD.Get_rank() << " entered rank X section of " << results_[i].first << std::endl;
                std::cout << std::flush;
#endif
                boost::shared_array<double> buffer;
                int buffer_size = sr->parent_container_->ExportValues(buffer);
                size_t v0size = sr->parent_container_->Values0Size();
                delete sr->container_;
                sr->container_ = NULL;
                sr->parent_container_ = NULL;
                MPI::COMM_WORLD.Send(&buffer_size, 1, MPI_INT, 0, MPI_TAG_BEHIND_IN_SPACE_99);
                MPI::COMM_WORLD.Send(buffer.get(), buffer_size, MPI_DOUBLE, 0, MPI_TAG_IN_FLAMES_WE_TRUST);

                sr->mean_.resize(v0size);
                sr->error_.resize(v0size);
                sr->variance_.resize(v0size);
                sr->tau_.resize(v0size);

                boost::shared_array<double> buffer_mean(new double[v0size]);
                boost::shared_array<double> buffer_error(new double[v0size]);
                boost::shared_array<double> buffer_variance(new double[v0size]);
                boost::shared_array<double> buffer_tau(new double[v0size]);

                MPI::COMM_WORLD.Recv(buffer_mean.get(), v0size, MPI_DOUBLE, 0, MPI_TAG_RUST_IN_PEACE);
                MPI::COMM_WORLD.Recv(buffer_error.get(), v0size, MPI_DOUBLE, 0, MPI_TAG_WHORACLE);
                MPI::COMM_WORLD.Recv(buffer_variance.get(), v0size, MPI_DOUBLE, 0, MPI_TAG_PEACE_SELLS_BUT_WHOS_BUYING);
                MPI::COMM_WORLD.Recv(buffer_tau.get(), v0size, MPI_DOUBLE, 0, MPI_TAG_CLAD_IN_SHADOWS_99);

                for(size_t i = 0; i < v0size; i++) {
                    sr->mean_[i] = buffer_mean[i];
                    sr->error_[i] = buffer_error[i];
                    sr->variance_[i] = buffer_variance[i];
                    sr->tau_[i] = buffer_tau[i];
                }
            }
        } else {
            throw std::runtime_error("Compile me with MPI! *3*");
        }
    }
}
