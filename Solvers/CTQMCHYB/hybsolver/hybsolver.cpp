/*
* A.A. Dyachenko
*/
#include "hybsolver/hybsolver.h"
#include <moves.h>
#include <update.h>
#include "mc_results.h"
#include "argcache.h"
#ifdef CTHYB_HAS_UUID
#include <boost/uuid/random_generator.hpp>
#include <boost/uuid/uuid_io.hpp>
#endif
#include <boost/math/distributions/normal.hpp>
#include "joutai.h"

HybridizationSolver::HybridizationSolver(const uint32_t &seed)
{
    arg_cache_c_ = NULL;
    arg_cache_d_ = NULL;
    random_ = NULL;
    jg_ = NULL;
#ifdef CTHYB_HAS_UUID
    boost::mt19937 ran;
    ran.seed(seed);
    boost::uuids::basic_random_generator<boost::mt19937> gen(&ran);
    uuid_ = gen();
    uuid_string_ = to_string(uuid_);
#else
    uuid_string_ = boost::lexical_cast<std::string>(seed);
#endif
}

HybridizationSolver::~HybridizationSolver()
{
    if(random_ != NULL)
        delete random_;

    if(jg_ != NULL)
        delete jg_;

    if(arg_cache_c_ != NULL)
        delete arg_cache_c_;

    if(arg_cache_d_ != NULL)
        delete arg_cache_d_;

    delete observable_green_tau_;
    delete observable_sigmag_tau_;
    delete observable_green_legendre_;
    delete observable_sigmag_legendre_;
    delete observable_green_omega_real_;
    delete observable_green_omega_imginary_;
    delete observable_simgag_omega_real_;
    delete observable_sigmag_omega_imaginary_;
    delete observable_density_;
    delete observable_matrix_size_;
    delete observable_nnt_;
    delete observable_nn_;
    delete observable_sector_;
    delete observable_green2_omega_real_;
    delete observable_green2_omega_imaginary_;
    delete observable_h_omega_real_;
    delete observable_h_omega_imaginary_;
    delete observable_slice_coverage_;
    delete observable_sign_;
    delete observable_energy_;

    ReportStatistics();

    FlushLog();
}

void HybridizationSolver::WriteLog(std::stringstream &stream)
{
    std::stringstream tmp_stream;
    //tmp_stream <<  << stream << std::endl;
    tmp_stream << "[" << uuid_string_ << "] " << stream.str() << std::endl;
    if(config.verbosity >= VERBOSITY_STDOUT)
        std::cout << tmp_stream.str();
    log_stream_ << tmp_stream.str();
}

void HybridizationSolver::WriteLog(const char *s)
{
    std::stringstream stream;
    stream << s;
    WriteLog(stream);
}

void HybridizationSolver::WriteLog(std::string &str)
{
    std::stringstream stream;
    stream << str;
    WriteLog(stream);
}

void HybridizationSolver::FlushLog(bool severe)
{
    if(severe || config.verbosity >= VERBOSITY_FILE) {
        std::ofstream log_file_("cthyb.log", std::ios::app);
        log_file_ << log_stream_.str();
        log_file_.close();
    }
}

void HybridizationSolver::Exception_RuntimeError(std::stringstream &stream)
{
    WriteLog(stream);
    FlushLog(true);
    throw std::runtime_error(stream.str());
}

void HybridizationSolver::Exception_RuntimeError(std::string &str)
{
    std::stringstream stream;
    stream << str;
    Exception_RuntimeError(stream);
}

void HybridizationSolver::Exception_RuntimeError(const char *s)
{
    std::stringstream stream;
    stream << s;
    Exception_RuntimeError(stream);
}

const std::stringstream &HybridizationSolver::GetLog()
{
    return log_stream_;
}

std::vector< std::vector<int> > HybridizationSolver::GetStatistics()
{
    std::vector< std::vector<int> > stat;
    stat.resize(5);
    stat[0].resize(statistics_antisegment.size());
    for(size_t i = 0; i < statistics_antisegment.size(); i++) {
        stat[0][i] = statistics_antisegment[i];
    }
    stat[1].resize(statistics_full_line.size());
    for(size_t i = 0; i < statistics_full_line.size(); i++) {
        stat[1][i] = statistics_full_line[i];
    }
    stat[2].resize(statistics_segment.size());
    for(size_t i = 0; i < statistics_segment.size(); i++) {
        stat[2][i] = statistics_segment[i];
    }
    stat[3].resize(statistics_shift.size());
    for(size_t i = 0; i < statistics_shift.size(); i++) {
        stat[3][i] = statistics_shift[i];
    }
    stat[4].resize(statistics_swap.size());
    for(size_t i = 0; i < statistics_swap.size(); i++) {
        stat[4][i] = statistics_swap[i];
    }

    return stat;
}

void HybridizationSolver::ReportStatistics()
{
    std::stringstream statistics_stream;
    double total_moves = 0.000000000000001;
    double fl_moves = 0.000000000000001;
    double as_moves = 0.000000000000001;
    double s_moves = 0.000000000000001;
    double shift_moves = 0.000000000000001;
    double swap_moves = 0.000000000000001;
    for(size_t i = 0; i < statistics_full_line.size(); i++) {
        fl_moves += statistics_full_line[i];
    }
    statistics_stream << "Full line statistics (" << static_cast<int64_t>(fl_moves) << "):" << std::endl;
    statistics_stream << "failed " << 100.0 * static_cast<double>(statistics_full_line[0]) / fl_moves << "%" << std::endl;
    statistics_stream << "inserted " << 100.0 * static_cast<double>(statistics_full_line[1]) / fl_moves << "%" << std::endl;
    statistics_stream << "removed " << 100.0 * static_cast<double>(statistics_full_line[2]) / fl_moves << "%" << std::endl;
    for(size_t i = 0; i < statistics_antisegment.size(); i++) {
        as_moves += statistics_antisegment[i];
    }
    statistics_stream << "Antisegment statistics (" << static_cast<int64_t>(as_moves) << "):" << std::endl;
    statistics_stream << "failed " << 100.0 * static_cast<double>(statistics_antisegment[0]) / as_moves << "%" << std::endl;
    statistics_stream << "inserted " << 100.0 * static_cast<double>(statistics_antisegment[1]) / as_moves << "%" << std::endl;
    statistics_stream << "removed " << 100.0 * static_cast<double>(statistics_antisegment[2]) / as_moves << "%" << std::endl;
    for(size_t i = 0; i < statistics_segment.size(); i++) {
        s_moves += statistics_segment[i];
    }
    statistics_stream << "Segment statistics (" << static_cast<int64_t>(s_moves) << "):" << std::endl;
    statistics_stream << "failed " << 100.0 * static_cast<double>(statistics_segment[0]) / s_moves << "%" << std::endl;
    statistics_stream << "inserted " << 100.0 * static_cast<double>(statistics_segment[1]) / s_moves << "%" << std::endl;
    statistics_stream << "removed " << 100.0 * static_cast<double>(statistics_segment[2]) / s_moves << "%" << std::endl;
    for(size_t i = 0; i < statistics_shift.size(); i++) {
        shift_moves += statistics_shift[i];
    }
    statistics_stream << "Shift statistics (" << static_cast<int64_t>(shift_moves) << "):" << std::endl;
    statistics_stream << "failed " << 100.0 * static_cast<double>(statistics_shift[0]) / shift_moves << "%" << std::endl;
    statistics_stream << "succeeded " << 100.0 * static_cast<double>(statistics_shift[1]) / shift_moves << "%" << std::endl;

    for(size_t i = 0; i < statistics_swap.size(); i++) {
        swap_moves += statistics_swap[i];
    }
    statistics_stream << "Swap statistics (" << static_cast<int64_t>(swap_moves) << "):" << std::endl;
    statistics_stream << "failed " << 100.0 * static_cast<double>(statistics_swap[0]) / swap_moves << "%" << std::endl;
    statistics_stream << "succeeded " << 100.0 * static_cast<double>(statistics_swap[1]) / swap_moves << "%" << std::endl;

    total_moves = as_moves + fl_moves + s_moves + shift_moves + swap_moves;
    statistics_stream << "Global statistics (" << static_cast<int64_t>(total_moves) << "):" << std::endl;
    statistics_stream << "full line " << 100.0 * fl_moves / total_moves << "%" << std::endl;
    statistics_stream << "antisegment " << 100.0 * as_moves / total_moves << "%" << std::endl;
    statistics_stream << "segment " << 100.0 * s_moves / total_moves << "%" << std::endl;
    statistics_stream << "shift " << 100.0 * shift_moves / total_moves << "%" << std::endl;
    statistics_stream << "swap " << 100.0 * swap_moves / total_moves << "%" << std::endl;
    statistics_stream << "Acceptance rate " << static_cast<double>(statistics_full_line[1] + statistics_full_line[2] + statistics_antisegment[1] + statistics_antisegment[2] + statistics_segment[1] + statistics_segment[2] + statistics_shift[1] + statistics_swap[1]) / total_moves << std::endl;

    WriteLog(statistics_stream);
}

void HybridizationSolver::Prepare()
{
    start_time_ = time(0);
    sweeps_ = 0;

    config.num_matsubara_aux = config.num_matsubara2 + config.num_bosonic - 1;

    full_line_.resize(config.num_flavors);
    sign_.resize(config.num_flavors);
    segments_.resize(config.num_flavors);
    m_.resize(config.num_flavors);
    n_vectors.resize(config.num_flavors);
    for(size_t i = 0; i < config.num_flavors; i++) {
        segments_.at(i).clear();
        n_vectors.at(i).resize(config.num_nn + 1);
    }
    
    if(config.measure_sector_statistics) {
        jg_ = new JoutaiGenerator(config.num_flavors);
        jg_->Generate();
        jg_->Sort();
    }

    ResizeContainers();
#ifndef NO_RESAMPLING
    if(config.total_sweeps < config.observable_bin_size) {
        std::stringstream obs_stream;
        obs_stream << "Not enough sweeps to fill even single bin! Correcting observable_bin_size to have one bin per process (very poor resampling)";
        WriteLog(obs_stream);
        config.observable_bin_size = config.total_sweeps;
    }

    if(config.total_sweeps / config.observable_bin_size < 100) {
        std::stringstream obs_stream;
        obs_stream << "Recommended observable_bin_size is " << std::max(config.total_sweeps / 100, static_cast<size_t>(1)) << " (make sure you have enough memory)";
        WriteLog(obs_stream);
    }
#endif

    observable_green_tau_ = new ObservableContainer<>(config.observable_bin_size, "green_tau");
    observable_sigmag_tau_ = new ObservableContainer<>(config.observable_bin_size, "sigmag_tau");
    observable_green_legendre_ = new ObservableContainer<>(config.observable_bin_size, "green_legendre");
    observable_sigmag_legendre_ = new ObservableContainer<>(config.observable_bin_size, "sigmag_legendre");
    observable_green_omega_real_ = new ObservableContainer<>(config.observable_bin_size, "green_omega_real");
    observable_green_omega_imginary_ = new ObservableContainer<>(config.observable_bin_size, "green_omega_imaginary");
    observable_simgag_omega_real_ = new ObservableContainer<>(config.observable_bin_size, "sigmag_omega_real");
    observable_sigmag_omega_imaginary_ = new ObservableContainer<>(config.observable_bin_size, "sigmag_omega_imaginary");
    observable_density_ = new ObservableContainer<>(config.observable_bin_size, "density");
    observable_matrix_size_ = new ObservableContainer<>(config.observable_bin_size, "matrix_size");
    observable_nnt_ = new ObservableContainer<>(config.observable_bin_size, "nnt");
    observable_nn_ = new ObservableContainer<>(config.observable_bin_size, "nn");
    observable_sector_ = new ObservableContainer<>(config.observable_bin_size, "sector_statistics");
    observable_green2_omega_real_ = new ObservableContainer<>(config.observable_bin_size, "green2_omega_real");
    observable_green2_omega_imaginary_ = new ObservableContainer<>(config.observable_bin_size, "green2_omega_imaginary");
    observable_h_omega_real_ = new ObservableContainer<>(config.observable_bin_size, "h_omega_real");
    observable_h_omega_imaginary_ = new ObservableContainer<>(config.observable_bin_size, "h_omega_imaginary");
    observable_slice_coverage_ = new ObservableContainer<>(config.observable_bin_size, "slice_coverage");
    observable_sign_ = new ObservableContainer<>(config.observable_bin_size, "sign");
    observable_energy_ = new ObservableContainer<>(2, "energy");

    if(random_ == NULL)
        random_ = new Random(config.random_generator);
    random_->Init(config.global_seed);

    PrepareStatistics();

    if(config.use_arg_cache) {
        is_cheap_.resize(config.num_flavors);
        if(config.measure_green_legendre) {
            std::stringstream cache_stream;
            cache_stream << "Using ArgCache for legendre... cache size is " << config.arg_cache_size * config.num_legendre * sizeof(double) / (1024 * 1024) << " Mb" << std::endl;
            cache_stream << "ArgCache binning will be used only for perturbation orders of " << std::sqrt(static_cast<double>(config.arg_cache_size) / static_cast<double>(config.num_meas)) << " and higher, rest will be computed on the fly (Legendre coefficients are still precomputed on a grid).";
            WriteLog(cache_stream);
            arg_cache_d_ = new ArgCache<double>;
            arg_cache_d_->ComputeArgCache(config.arg_cache_size, config.beta, config.num_legendre);
            InitCacheFunction();
        }
        if(config.measure_green_omega) {
            std::stringstream cache_stream;
            cache_stream << "Using arg cache for omega... cache size is " << config.arg_cache_size * config.num_matsubara * sizeof(std::complex<double>) / (1024 * 1024) << " Mb";
            WriteLog(cache_stream);
            arg_cache_c_ = new ArgCache< std::complex<double> >;
            arg_cache_c_->ComputeArgCache(config.arg_cache_size, config.beta, config.num_matsubara);
        }
    }
}

void HybridizationSolver::InitCacheFunction()
{
    if(lg_cached_.size() == 0) {
        lg_cached_.resize(config.num_flavors);
        ls_cached_.resize(config.num_flavors);
        for(size_t i = 0; i < config.num_flavors; i++) {
            lg_cached_[i].resize(config.arg_cache_size);
            ls_cached_[i].resize(config.arg_cache_size);
        }
    }

    for(size_t i = 0; i < config.num_flavors; i++) {
        lg_cached_[i] = 0.0;
    }

    for(size_t i = 0; i < config.num_flavors; i++) {
        ls_cached_[i] = 0.0;
    }
}

/* Takes function accumulated into cache and puts it into legendre measurements like it would've been if it measured directly in legendre basis.
 */
void HybridizationSolver::CollectCache()
{
    for(size_t i = 0; i < config.num_flavors; i++) {
        if(is_cheap_[i] == true)
            continue;

        for(size_t j = 0; j < config.arg_cache_size; j++) {
            if(lg_cached_[i][j] != 0.0) {
                for(size_t l = 0; l < config.num_legendre; l++) {
                    double legendre_cached = arg_cache_d_->GetValue(j, l);
                    green_legendre_measurements_[i * config.num_legendre + l] -= legendre_cached * lg_cached_[i][j];
                    if(config.measure_sigmag)
                        sigmag_legendre_measurements_[i * config.num_legendre + l] -= legendre_cached * ls_cached_[i][j];
                }
            }
        }
    }
    InitCacheFunction();
}

void HybridizationSolver::EvaluateCost()
{
    for(size_t i = 0; i < config.num_flavors; i++) {
        int64_t straightforward = config.num_meas * segments_.size() * segments_.size() * config.num_legendre;
        int64_t binned = config.arg_cache_size * config.num_legendre;
        if(straightforward < binned)
            is_cheap_[i] = true;
        else
            is_cheap_[i] = false;
    }
}

void HybridizationSolver::PrepareStatistics()
{
    statistics_full_line.resize(3);
    statistics_segment.resize(3);
    statistics_antisegment.resize(3);
    statistics_shift.resize(2);
    statistics_swap.resize(2);
}

void HybridizationSolver::ResizeContainers()
{
    green_tau_measurements_.resize(config.num_flavors * (config.num_slices + 1));
    if(config.measure_sigmag)
        sigmag_tau_measurements_.resize(config.num_flavors * (config.num_slices + 1));

    occupancy_measurements_.resize(config.num_flavors);
    m_size_measurements_.resize(config.num_flavors);
    slice_coverage_measurements_.resize(config.num_flavors * (config.num_slices + 1));

    if(config.measure_nnt) {
        //nnt_measurements_.resize(config.num_flavors * (config.num_flavors + 1) / 2 * (config.num_nn + 1));
        nntm_measurements_.resize(config.num_nn + 1);
        for(size_t i = 0; i < config.num_nn + 1; i++) {
            nntm_measurements_[i] = boost::numeric::ublas::zero_matrix<double>(config.num_flavors, config.num_flavors);
        }
    }

    if(config.measure_nn) {
        //nn_measurements_.resize(config.num_flavors * (config.num_flavors + 1) / 2);
        nn_measurements_.resize(config.num_flavors * config.num_flavors);
    }

    if(config.measure_green_omega) {
        if (config.num_matsubara == 0) {
            Exception_RuntimeError("Number of matsubara frequencies is not set! Yet measure_green_omega enabled.");
        }
        green_omega_real_measurements_.resize(config.num_flavors * config.num_matsubara);
        green_omega_imaginary_measurements_.resize(config.num_flavors * config.num_matsubara);
        if (config.measure_sigmag) {
            sigmag_omega_real_measurements_.resize(config.num_flavors * config.num_matsubara);
            sigmag_omega_imaginary_measurements_.resize(config.num_flavors * config.num_matsubara);
        }
    }

    if(config.measure_green_legendre) {
        if (config.num_legendre == 0) {
            Exception_RuntimeError("Number of legendre coefficients is not set! Yet measure_green_legendre enabled.");
        }
        green_legendre_measurements_.resize(config.num_flavors * config.num_legendre);

        if(config.measure_sigmag)
            sigmag_legendre_measurements_.resize(config.num_flavors * config.num_legendre);
    }

    if(config.measure_sector_statistics)
        sector_measurements_.resize(pow(2, config.num_flavors));

    if (config.measure_green2_omega) {
        green2_omega_real_measurements_.resize(config.num_flavors * config.num_flavors * config.num_matsubara2 * config.num_matsubara2 * config.num_bosonic);
        green2_omega_imaginary_measurements_.resize(config.num_flavors * config.num_flavors * config.num_matsubara2 * config.num_matsubara2 * config.num_bosonic);
        m_omega_measurements_.resize(config.num_flavors * config.num_matsubara_aux * config.num_matsubara_aux);

        if (config.measure_sigmag) {
            h_omega_real_measurements_.resize(config.num_flavors * config.num_flavors * config.num_matsubara2 * config.num_matsubara2 * config.num_bosonic);
            h_omega_imaginary_measurements_.resize(config.num_flavors * config.num_flavors * config.num_matsubara2 * config.num_matsubara2 * config.num_bosonic);
            nm_omega_measurements_.resize(config.num_flavors * config.num_matsubara_aux * config.num_matsubara_aux);
        }
    }
    energy_measurements_.resize(2);
}

void HybridizationSolver::ResetContainers()
{
    green_tau_measurements_ = 0.0;
    if(config.measure_sigmag)
        sigmag_tau_measurements_ = 0.0;
    occupancy_measurements_ = 0.0;
    m_size_measurements_ = 0.0;
    slice_coverage_measurements_ = 0.0;

    if(config.measure_nnt) {
        //nnt_measurements_ = 0.0;
        for(size_t i = 0; i < config.num_nn + 1; i++) {
            nntm_measurements_[i] = boost::numeric::ublas::zero_matrix<double>(config.num_flavors, config.num_flavors);
        }
    }

    if(config.measure_nn)
        nn_measurements_ = 0.0;

    if(config.measure_green_omega) {
        green_omega_real_measurements_ = 0.0;
        green_omega_imaginary_measurements_ = 0.0;

        if (config.measure_sigmag) {
            sigmag_omega_real_measurements_ = 0.0;
            sigmag_omega_imaginary_measurements_ = 0.0;
        }
    }

    if(config.measure_green_legendre) {
        green_legendre_measurements_ = 0.0;

        if(config.measure_sigmag)
            sigmag_legendre_measurements_ = 0.0;
    }

    sign_measurements_ = 0.0;
    for(size_t i = 0; i < sign_.size(); i++)
        sign_[i] = 1.0;

    if(config.measure_sector_statistics)
        sector_measurements_ = 0.0;

    if (config.measure_green2_omega) {
        green2_omega_real_measurements_ = 0.0;
        green2_omega_imaginary_measurements_ = 0.0;
        m_omega_measurements_ = std::complex<double>(0.0, 0.0);

        if (config.measure_sigmag) {
            h_omega_real_measurements_ = 0.0;
            h_omega_imaginary_measurements_ = 0.0;
            nm_omega_measurements_ = std::complex<double>(0.0, 0.0);
        }
    }
    energy_measurements_ = 0.0;
}

void HybridizationSolver::SetSystem(const std::vector< std::vector<double> > &hybfunc, const std::vector<double> &mu, const blas_matrix &umat)
{
    //this->delta_original_ = hybfunc;
    delta_original_.resize(config.num_flavors);
    for(size_t f = 0; f < config.num_flavors; f++) {
        delta_original_.at(f).resize(hybfunc.at(f).size());
        delta_original_.at(f) = hybfunc.at(f);
    }
    this->mu_e_ = mu;
    mu_original_ = mu;
    this->u_matrix_ = umat;

    if(delta_original_.size() != config.num_flavors || mu_e_.size() != config.num_flavors || u_matrix_.cols() != config.num_flavors) {
        Exception_RuntimeError("System size is inconsistent!");
    }

    delta_up_down_.resize(config.num_flavors);
    for(size_t f = 0; f < config.num_flavors; f++) {
        delta_up_down_.at(f).resize(hybfunc.at(f).size());
        delta_up_down_.at(f) = hybfunc.at(f);
        for(size_t i = 0; i < delta_up_down_.at(f).size(); i++) {
            /*if(delta_up_down_.at(f).at(i) < 0.0) {
                std::stringstream errs;
                errs << "Hybridization function is negative! flavor " << f << " point " << i << " " << delta_up_down_.at(f).size() - 1 - i << " " << delta_up_down_.at(f).at(i);
                WriteLog(errs);
                delta_up_down_.at(f).at(i) = 0.0;
            }*/
        }
    }
    /* Instead of zeroing erroneous points we find two minimum points and set rest between them to minimal value.
     * Solution by K. Haule.
     */
#ifndef NO_DELTA_WORKAROUND
    for(size_t f = 0; f < config.num_flavors; f++) {
        size_t first = delta_up_down_.at(f).size();
        size_t last = delta_up_down_.at(f).size();
        for(size_t i = 0; i < delta_up_down_.at(f).size(); i++) {
            if(delta_up_down_.at(f).at(i) < 0.0) {
                if(delta_up_down_.at(f).at(i) < 1e-7) {
                    first = i;
                    break;
                }
            }
        }
        for(int i = delta_up_down_.at(f).size() - 1; i >= 0; i--) {
            if(delta_up_down_.at(f).at(i) < 0.0) {
                if(delta_up_down_.at(f).at(i) < 1e-7) {
                    last = i;
                    break;
                }
            }
        }
        if(first != delta_up_down_.at(f).size() && last != delta_up_down_.at(f).size())
            for(size_t i = first; i <= last; i++)
                delta_up_down_.at(f).at(i) = 1e-7;
    }
#endif
}

bool HybridizationSolver::IsThermalized()
{
    return (sweeps_ >= config.thermalization_sweeps);
}

double HybridizationSolver::FractionDone()
{
    if((time(NULL) - start_time_) > config.max_time)
        return 1.0;

    return (IsThermalized() ? (static_cast<double>(sweeps_ - config.thermalization_sweeps) / static_cast<double>(config.total_sweeps)) : 0.0);
}

CommonConfig *HybridizationSolver::GetConfig()
{
    return &config;
}

bool HybridizationSolver::PerformUpdate(int flavor)
{
    bool updated = false;
    bool update_selected = false;
    while(!update_selected) {
        size_t update_index = random_->Get_01() * config.updates_.size();
        UPDATE_TYPE type = config.updates_.at(update_index).first;
        if(random_->Get_01() >= config.updates_.at(update_index).second)
            continue;
        switch(type) {
        case UPDATE_FULL_LINE:
        {
            if(segments_[flavor].size() == 0) {
                // insert or remove full line
                MC_FULL_LINE fl_ret = insert_remove_full_line(random_, mu_e_[flavor], config.beta, u_matrix_, full_line_[flavor], segments_, full_line_, flavor);
                statistics_full_line[fl_ret]++;
                if(fl_ret == FULL_LINE_INSERT || fl_ret == FULL_LINE_REMOVE)
                    updated = true;
            }
            break;
        }
        case UPDATE_SEGMENT:
        {
            if (!full_line_[flavor]) {
                // local update
                MC_SEGMENT s_ret = insert_remove_segment(random_, config.beta * random_->Get_01(), config.beta, mu_e_[flavor], u_matrix_, delta_up_down_[flavor], segments_[flavor], m_[flavor], sign_[flavor], segments_, full_line_, flavor);
                statistics_segment[s_ret]++;
                if(s_ret == SEGMENT_INSERT || s_ret == SEGMENT_REMOVE)
                    updated = true;
            }
            break;
        }
        case UPDATE_ANTISEGMENT:
        {
            MC_ANTISEGMENT as_ret = insert_remove_antisegment(random_, config.beta * random_->Get_01(), config.beta, mu_e_[flavor], u_matrix_, delta_up_down_[flavor], full_line_[flavor], segments_[flavor], m_[flavor], sign_[flavor], segments_, full_line_, flavor);
            statistics_antisegment[as_ret]++;
            if(as_ret == ANTISEGMENT_INSERT || as_ret == ANTISEGMENT_REMOVE)
                updated = false;
            break;
        }
        case UPDATE_SHIFT:
        {
            if (!full_line_[flavor]) {
                // shift segments
                MC_SHIFT shift_ret = shift_segment(random_, segments_[flavor], config.beta, mu_e_[flavor], u_matrix_, delta_up_down_[flavor], m_[flavor], sign_[flavor], segments_, full_line_, flavor);
                statistics_shift[shift_ret]++;
                if(shift_ret == SHIFT_SUCCESS)
                    updated = true;
            }
            break;
        }
        case UPDATE_GLOBAL:
        {
            MC_SWAP swap_ret = swap_segments_spin_no_u(random_, config.beta, segments_, full_line_, delta_up_down_, sign_, m_, mu_e_);
            statistics_swap[swap_ret]++;
            if(swap_ret == SWAP_SUCCESS)
                updated = true;
            break;
        }
        default:
        {
            Exception_RuntimeError("Something went wrong!");
        }
        }
        update_selected = true;
    }

    return updated;
}

void HybridizationSolver::MakeSweep()
{
    ResetContainers();

    if(config.arg_cache_size > 0)
        EvaluateCost();

    sweeps_++;

    double s = 1.0;

    for(size_t measurement = 0; measurement < config.num_meas; measurement++) {

        for(size_t upd = 0; upd < config.num_updates; upd++) {
            int flavor = floor(random_->Get_01() * config.num_flavors);

            PerformUpdate(flavor);
        }
        if(IsThermalized()) {
            for(size_t flavor = 0; flavor < config.num_flavors; flavor++) {
                s *= sign_[flavor];

                m_size_measurements_[flavor] += segments_[flavor].size();

                MeasureGreens(flavor);

                //occupancy_measurements_[flavor] += compute_overlap(full_segment, segments_[flavor], full_line_[flavor], config.beta) / config.beta;
                occupancy_measurements_[flavor] += ComputeDensity(flavor) / config.beta;
            }
            if (config.measure_green2_omega && IsThermalized()) {
                for(size_t f1 = 0; f1 < config.num_flavors; f1++)
                    for(size_t f2 = 0; f2 < config.num_flavors; f2++) //measure updn and dnup for averaging
                        for(size_t w2n = 0; w2n < config.num_matsubara2; w2n++)
                            for(size_t w3n = 0; w3n < config.num_matsubara2; w3n++)
                                for(size_t Wn=0; Wn < config.num_bosonic; Wn++) {
                                    size_t w1n = w2n + Wn;
                                    size_t w4n = w3n + Wn;
                                    //              int index=(FLAVORS*f1+f2) *N_w2*N_w2*N_W + w2n *N_w2*N_W + w3n *N_W + Wn;
                                    size_t index = Wn * config.num_flavors * config.num_flavors * config.num_matsubara2 * config.num_matsubara2 + (config.num_flavors * f1 + f2) * config.num_matsubara2 * config.num_matsubara2 + w2n * config.num_matsubara2 + w3n;
                                    std::complex<double> meas = m_omega_measurements_[f1 * config.num_matsubara_aux * config.num_matsubara_aux + w1n * config.num_matsubara_aux + w2n] * m_omega_measurements_[f2 * config.num_matsubara_aux * config.num_matsubara_aux + w3n * config.num_matsubara_aux + w4n];
                                    if(f1 == f2)
                                        meas -= m_omega_measurements_[f1 * config.num_matsubara_aux * config.num_matsubara_aux + w1n * config.num_matsubara_aux + w4n] * m_omega_measurements_[f1 * config.num_matsubara_aux * config.num_matsubara_aux + w3n * config.num_matsubara_aux + w2n];
                                    green2_omega_real_measurements_[index] += meas.real();
                                    green2_omega_imaginary_measurements_[index] += meas.imag();
                                    if (config.measure_sigmag) {
                                        std::complex<double> meas_h = nm_omega_measurements_[f1 * config.num_matsubara_aux * config.num_matsubara_aux + w1n * config.num_matsubara_aux + w2n] * m_omega_measurements_[f2 * config.num_matsubara_aux * config.num_matsubara_aux + w3n * config.num_matsubara_aux + w4n];
                                        if(f1 == f2)
                                            meas_h -= nm_omega_measurements_[f1 * config.num_matsubara_aux * config.num_matsubara_aux + w1n * config.num_matsubara_aux + w4n] * m_omega_measurements_[f1 * config.num_matsubara_aux * config.num_matsubara_aux + w3n * config.num_matsubara_aux + w2n];
                                        h_omega_real_measurements_[index] += meas_h.real();
                                        h_omega_imaginary_measurements_[index] += meas_h.imag();
                                    }
                                }//Wn
            }//end::MEASURE_g2w

            sign_measurements_ += s;
        }
    } //measurement

    if(IsThermalized()) {
        if(config.measure_nn) {
            MeasureStates();
            nn_measurements_ /= config.beta;
            energy_measurements_ /= config.beta;
            if(config.measure_sector_statistics)
                sector_measurements_ /= config.beta;
        }

        if(config.measure_nnt)
            MeasureNNT_Reference();

        green_tau_measurements_ *= static_cast<double>(config.num_slices) / (config.beta * config.beta * static_cast<double>(config.num_meas));
        if(config.measure_sigmag)
            sigmag_tau_measurements_ *= static_cast<double>(config.num_slices) / (config.beta * config.beta * static_cast<double>(config.num_meas));

        occupancy_measurements_ /= static_cast<double>(config.num_meas);
        sign_measurements_ /= static_cast<double>(config.num_meas);
        m_size_measurements_ /= static_cast<double>(config.num_meas);
        slice_coverage_measurements_ /= static_cast<double>(config.num_meas);

        if(config.measure_green_omega) {
            green_omega_real_measurements_ *= 1.0 / (config.beta * static_cast<double>(config.num_meas));
            green_omega_imaginary_measurements_ *= 1.0 / (config.beta * static_cast<double>(config.num_meas));

            if (config.measure_sigmag) {
                sigmag_omega_real_measurements_ *= 1.0 / (config.beta * static_cast<double>(config.num_meas));
                sigmag_omega_imaginary_measurements_ *= 1.0 / (config.beta * static_cast<double>(config.num_meas));
            }
        }

        if(config.measure_green_legendre) {
            if(config.arg_cache_size > 0)
                CollectCache();
            green_legendre_measurements_ *= 1.0 / (config.beta * static_cast<double>(config.num_meas));
            if(config.measure_sigmag)
                sigmag_legendre_measurements_ *= 1.0 / (config.beta * static_cast<double>(config.num_meas));
        }

        if (config.measure_green2_omega) {
            green2_omega_real_measurements_ *= 1.0 / (config.beta * static_cast<double>(config.num_meas));
            green2_omega_imaginary_measurements_ *= 1.0 / (config.beta * static_cast<double>(config.num_meas));

            if (config.measure_sigmag) {
                h_omega_real_measurements_ *= 1.0 / (config.beta * static_cast<double>(config.num_meas));
                h_omega_imaginary_measurements_ *= 1.0 / (config.beta * static_cast<double>(config.num_meas));
            }
        }

        observable_green_tau_->Add(green_tau_measurements_);
        observable_sigmag_tau_->Add(sigmag_tau_measurements_);
        observable_slice_coverage_->Add(slice_coverage_measurements_);
        if(config.measure_nnt) {
            /*std::cout << "*****************" << std::endl;
            std::cout << nntm_measurements_[0](0,0) << "\t" << nntm_measurements_[0](0,5) << std::endl;
            std::cout << nntm_measurements_[0](5,0) << "\t" << nntm_measurements_[0](5,5) << std::endl;
            std::cout << occupancy_measurements_[0] << std::endl << std::endl;*/
            std::valarray<double> nnt_convert((config.num_nn + 1) * config.num_flavors * config.num_flavors);
            for(size_t t0 = 0; t0 < config.num_nn + 1; t0++) {
                for(size_t f0 = 0; f0 < config.num_flavors; f0++) {
                    for(size_t f1 = 0; f1 < config.num_flavors; f1++) {
                        nnt_convert[t0 * (config.num_flavors * config.num_flavors) + f0 * config.num_flavors + f1] = nntm_measurements_[t0](f0, f1);
                    }
                }
            }
            observable_nnt_->Add(nnt_convert);
        }
        if(config.measure_nn) {
            observable_nn_->Add(nn_measurements_);
            if(config.measure_sector_statistics)
                observable_sector_->Add(sector_measurements_);
        }

        observable_density_->Add(occupancy_measurements_);

        if(config.measure_green_omega) {
            observable_green_omega_imginary_->Add(green_omega_imaginary_measurements_);
            observable_green_omega_real_->Add(green_omega_real_measurements_);

            if (config.measure_sigmag) {
                observable_simgag_omega_real_->Add(sigmag_omega_real_measurements_);
                observable_sigmag_omega_imaginary_->Add(sigmag_omega_imaginary_measurements_);
            }
        }

        if(config.measure_green_legendre) {
            observable_green_legendre_->Add(green_legendre_measurements_);
            if(config.measure_sigmag)
                observable_sigmag_legendre_->Add(sigmag_legendre_measurements_);
        }

        if(config.measure_sector_statistics)
            observable_sector_->Add(sector_measurements_);

        if (config.measure_green2_omega) {
            observable_green2_omega_real_->Add(green2_omega_real_measurements_);
            observable_green2_omega_imaginary_->Add(green2_omega_imaginary_measurements_);

            if (config.measure_sigmag) {
                observable_h_omega_real_->Add(h_omega_real_measurements_);
                observable_h_omega_imaginary_->Add(h_omega_imaginary_measurements_);
            }
        }

        observable_matrix_size_->Add(m_size_measurements_);
        std::valarray<double> tmp_sign(1);
        tmp_sign = sign_measurements_;
        observable_sign_->Add(tmp_sign);
        observable_energy_->Add(energy_measurements_);
    }
}

void HybridizationSolver::MeasureDensityCorrelationFunction()
{
    segment_container_t::iterator it;

    for(size_t flavor = 0; flavor < config.num_flavors; ++flavor) {
        for(size_t i = 0; i < config.num_nn + 1; i++)
            n_vectors[flavor][i] = 1.0; //reset
        if(segments_[flavor].size() == 0) {
            if(full_line_[flavor] == 0) {
                for(size_t i = 0; i < n_vectors[flavor].size(); ++i)
                    n_vectors[flavor][i] = 0.0;
            }
        } else {
            it = segments_[flavor].end(); it--;
            if(it->t_end() < it->t_start())
                n_vectors[flavor][0] = 1.0; //last segment winds around the circle //n(0)=1
            else
                n_vectors[flavor][0] = 0.0; //n(0)=0
            // mark segment start and end points
            int index;
            for(it = segments_[flavor].begin(); it != segments_[flavor].end(); it++) {
                index = static_cast<int>(it->t_start() / config.beta * config.num_nn + 1);
                n_vectors[flavor][index] *= -1.0;
                index = static_cast<int>(it->t_end() / config.beta * config.num_nn + 1);
                n_vectors[flavor][index] *= -1.0;
            }
            // fill vector with occupation number
            for(size_t i = 1; i < n_vectors[flavor].size(); i++) {
                if(n_vectors[flavor][i] == -1.0)
                    n_vectors[flavor][i] = 1.0 - n_vectors[flavor][i-1]; //segment starts or ends -> occupation number changes
                else
                    n_vectors[flavor][i] = n_vectors[flavor][i-1]; //on the same segment -> occupation number identical to that of previous segment
            }
        }
    }

    //this gives the same result, but is somewhat slower
    //this is roughly 2 seconds slower

    /*for(size_t flavor = 0; flavor < config.num_flavors; flavor++) {
        for(size_t i = 0; i < config.num_nn + 1; i++){
            double tau = i * config.beta / static_cast<double>(config.num_nn);
            n_vectors[flavor][i] = get_occupation(segments_[flavor], full_line_[flavor], tau, config.beta);
        }
    }*/


    if(config.measure_nn) {
        // compute n(0)n(0)
        int pos = 0;
        for(size_t flavor1 = 0; flavor1 < config.num_flavors; ++flavor1) {
            for(size_t flavor2 = 0; flavor2 <= flavor1; ++flavor2) {
                for(size_t i = 0; i < config.num_nn + 1; ++i) {
                    nn_measurements_[pos] += n_vectors[flavor1][i] * n_vectors[flavor2][i];
                }
                pos++;
            }
        }
        nn_measurements_ /= (config.num_nn + 1);
    }

    /*if(config.measure_nnt) {
        // compute n(\tau)n(0)
        int pos = 0;
        for(size_t flavor1 = 0; flavor1 < config.num_flavors; ++flavor1) {
            for(size_t flavor2 = 0; flavor2 <= flavor1; ++flavor2) {
                for(size_t i = 0; i < config.num_nn + 1; ++i) {// COMPUTING ALL PAIRS i,j MAY BE TOO SLOW
                    //int i = 0;
                    for(size_t index = 0; index < config.num_nn + 1; ++index) {
                        size_t j = i + index;
                        if(j > config.num_nn)
                            j -= config.num_nn; //no sign change, this correlator is bosonic
                        nnt_measurements_[pos+index] += n_vectors[flavor1][j] * n_vectors[flavor2][i];
                    }
                }
                pos += (config.num_nn + 1);
            }
        }

        nnt_measurements_ /= (config.num_nn + 1);
    }*/
}

void HybridizationSolver::MeasureStates()
{
    std::set<Operator> op_set;
    for(size_t f = 0; f < config.num_flavors; f++)
        for(segment_container_t::iterator it = segments_[f].begin(); it != segments_[f].end(); it++) {
            op_set.insert(Operator(it->t_start(), 1, f));
            op_set.insert(Operator(it->t_end(), 0, f));
        }
    std::vector<TauState> tau_line;
    if(op_set.size()) {
        tau_line.push_back(TauState(0.0, op_set.begin()->tau));
        std::set<Operator>::iterator it_end = op_set.rbegin().base();
        for(std::set<Operator>::iterator it = op_set.begin(); it != it_end; it++) {
            std::set<Operator>::iterator it_next = it;
            it_next++;
            if(it_next == op_set.end())
                break;
            tau_line.push_back(TauState(it->tau, it_next->tau));
        }
        tau_line.push_back(TauState(op_set.rbegin()->tau, config.beta));
        for(size_t f = 0; f < config.num_flavors; f++)
            for(segment_container_t::iterator it = segments_[f].begin(); it != segments_[f].end(); it++) {
                if(it->t_start() < it->t_end())
                    mark_state(tau_line, it->t_start(), it->t_end(), f);
                else {
                    mark_state(tau_line, it->t_start(), config.beta, f);
                    mark_state(tau_line, 0.0, it->t_end(), f);
                }
            }
    } else
        tau_line.push_back(TauState(0.0, config.beta));

    for(size_t f = 0; f < config.num_flavors; f++)
        if(full_line_[f])
            mark_state(tau_line, 0.0, config.beta, f);

    for(size_t i = 0; i < tau_line.size(); i++) {
        TauState *ts = &tau_line[i];
        if(ts->state.size()) {
            for(int f0 = 0; f0 < ts->state.size(); f0++)
                for(int f1 = 0; f1 < ts->state.size(); f1++) {
                    int ind0 = ts->state[f0];
                    int ind1 = ts->state[f1];
                    if(ind0 == ind1)
                        nn_measurements_[ind0 * config.num_flavors + ind1] += ts->end - ts->start;
                    else if(ind0 < ind1) {
                        nn_measurements_[ind0 * config.num_flavors + ind1] += ts->end - ts->start;
                        nn_measurements_[ind1 * config.num_flavors + ind0] += ts->end - ts->start;
                        energy_measurements_[0] += u_matrix_(ind1, ind0) * (ts->end - ts->start);
                    }
                }
        }
        if(config.measure_sector_statistics) {
            Joutai jt(config.num_flavors);
            for(size_t i = 0; i < ts->state.size(); i++)
                jt.Element(ts->state[i]) = 1;
            if(jt.N() <= config.num_flavors/2) {
                for(size_t i = 0; i < jg_->InternalData().size(); i++)
                    if(jg_->InternalData()[i] == jt)
                        sector_measurements_[i] += ts->end - ts->start;
            } else {
                for(int i = jg_->InternalData().size() - 1; i >= 0; i--)
                    if(jg_->InternalData()[i] == jt)
                        sector_measurements_[i] += ts->end - ts->start;
            }
        }
    }
    for(size_t f = 0; f < config.num_flavors; f++)
        energy_measurements_[1] += segments_[f].size();

    energy_measurements_[1] /= config.num_flavors;
}

void HybridizationSolver::DumpInput()
{
    std::string suffix = "_";
    suffix.append(uuid_string_);
    suffix.append(".dat");
    std::string delta_name = "solver_food_delta";
    delta_name.append(suffix);
    std::string eps_name = "solver_food_eps";
    eps_name.append(suffix);
    std::string umatrix_name = "solver_food_umatrix";
    umatrix_name.append(suffix);
    std::ofstream delta_file(delta_name.c_str(), std::ios::trunc);
    for(size_t i = 0; i < delta_up_down_[0].size(); i++) {
        double tau = config.beta * static_cast<double>(i) / static_cast<double>(delta_up_down_[0].size() - 1);
        delta_file << tau;
        for(size_t f = 0; f < config.num_flavors; f++) {
            delta_file << "\t" << delta_up_down_[f][delta_up_down_[f].size() - i - 1];
        }
        delta_file << std::endl;
    }
    std::ofstream umatrix_file(umatrix_name.c_str(), std::ios::trunc);
    for(size_t i = 0; i < config.num_flavors; i++) {
        for(size_t j = 0; j < config.num_flavors; j++) {
            umatrix_file << u_matrix_(i, j) << "\t";
        }
        umatrix_file << std::endl;
    }
    std::ofstream chempot_file(eps_name.c_str(), std::ios::trunc);
    for(size_t i = 0; i < config.num_flavors; i++) {
        chempot_file << mu_e_[i] << std::endl;
    }
}

void HybridizationSolver::ExportSegments()
{
    std::ofstream seg("segments.save", std::ios::trunc | std::ios::binary);

    std::ofstream segtxt("segments.txt", std::ios::trunc);

    for(size_t i = 0; i < config.num_flavors; i++) {
        segtxt << "Full_line[" << i << "]: " << full_line_[i] << std::endl;
    }

    for(size_t i = 0; i < config.num_flavors; i++) {
        segtxt << "Segments[" << i << "]: " << segments_[i].size() << std::endl;
        segment_container_t::iterator it;
        for(it = segments_[i].begin(); it != segments_[i].end(); it++) {
            segtxt << "(" << it->t_start() << ", " << it->t_end() << ")" << std::endl;
        }
    }

    seg.write(reinterpret_cast<char*>(&(config.num_flavors)), sizeof(config.num_flavors));
    for(size_t i = 0; i < segments_.size(); i++) {
        int fl = full_line_[i];
        seg.write(reinterpret_cast<char*>(&fl), sizeof(fl));
        if (full_line_[i] == 1)
            continue;
        int size = segments_[i].size();
        seg.write(reinterpret_cast<char*>(&size), sizeof(size));
        segment_container_t::iterator it;
        for(it = segments_[i].begin(); it != segments_[i].end(); it++) {
            double start = it->t_start();
            double end = it->t_end();
            seg.write(reinterpret_cast<char*>(&start), sizeof(start));
            seg.write(reinterpret_cast<char*>(&end), sizeof(end));
        }
    }
}

void HybridizationSolver::MeasureGreens(int flavor)
{
    static const std::complex<double> I(0.0, 1.0);
    double tau_1, tau_2;
    segment_container_t::iterator ita1;
    segment_container_t::iterator itc1;
    if(segments_[flavor].size() > 0) {//at least one segment in the configuration
        for(size_t a1 = 0; a1 < m_[flavor].cols(); a1++){
            (a1 == 0 ? ita1 = segments_[flavor].begin() : ita1++); //1st annihilator
            tau_1 = ita1->t_end();
            double pref = 0.0;
            if(config.measure_sigmag)
                for(size_t f1 = 0; f1 < config.num_flavors; f1++)
                    pref += 0.5 * (u_matrix_(f1, flavor) + u_matrix_(flavor, f1)) * get_occupation(segments_[f1], full_line_[f1], tau_1, config.beta);

            for(size_t c1 = 0; c1 < m_[flavor].cols(); c1++) {
                (c1 == 0 ? itc1 = segments_[flavor].begin() : itc1++); //1st creator
                tau_2 = itc1->t_start();
                double m_matrix = m_[flavor](c1, a1);
                double arg = tau_1 - tau_2; //measures -<T c(tau_1) c^*(tau_2)>
                //measurement of G(tau)
                double argument = arg;
                double bubble_sign = 1.0;
                if (argument > 0)
                    bubble_sign = 1.0;
                else {
                    bubble_sign = -1.0;
                    argument += config.beta;
                }
                switch(config.slicing_method) {
                case SLICING_DUMB:
                {
                    int index = (int)(argument / config.beta * config.num_slices + 0.5);
                    double mult = (index==0 || index==config.num_slices) ? 2.0 : 1.0;
                    green_tau_measurements_[flavor * (config.num_slices + 1) + index] += m_matrix * bubble_sign * mult; //measures +<T c c^*>; need to make definitions consistent
                    if(config.measure_sigmag)
                        sigmag_tau_measurements_[flavor * (config.num_slices + 1) + index] += m_matrix * bubble_sign * pref;
                    slice_coverage_measurements_[flavor * (config.num_slices + 1) + index] += 1.0;
                    break;
                }
                case SLICING_DUMB_PROPER:
                {
                    int index = static_cast<int>(ceil(argument / config.beta * config.num_slices - 0.5));
                    double mult = (index==0 || index==config.num_slices) ? 2.0 : 1.0;
                    green_tau_measurements_[flavor * (config.num_slices + 1) + index] += m_matrix * bubble_sign * mult; //measures +<T c c^*>; need to make definitions consistent
                    if(config.measure_sigmag)
                        sigmag_tau_measurements_[flavor * (config.num_slices + 1) + index] += m_matrix * bubble_sign * pref;
                    slice_coverage_measurements_[flavor * (config.num_slices + 1) + index] += 1.0;
                    break;
                }
                case SLICING_LINEAR_FIT:
                {
                    double tau_point = argument / config.beta * static_cast<double>(config.num_slices);
                    int lower = static_cast<int>(floor(tau_point));
                    int higher = lower + 1;
                    double lower_cont = tau_point - static_cast<double>(lower);
                    double higher_cont = static_cast<double>(higher) - tau_point;
                    double weight0 = higher_cont;
                    double weight1 = lower_cont;
                    green_tau_measurements_[flavor * (config.num_slices + 1) + lower] += m_matrix * bubble_sign * weight0;
                    green_tau_measurements_[flavor * (config.num_slices + 1) + higher] += m_matrix * bubble_sign * weight1;
                    if(config.measure_sigmag) {
                        sigmag_tau_measurements_[flavor * (config.num_slices + 1) + lower] += m_matrix * bubble_sign * weight0 * pref;
                        sigmag_tau_measurements_[flavor * (config.num_slices + 1) + higher] += m_matrix * bubble_sign * weight1 * pref;
                    }
                    slice_coverage_measurements_[flavor * (config.num_slices + 1) + lower] += 1.0;
                    slice_coverage_measurements_[flavor * (config.num_slices + 1) + higher] += 1.0;
                    break;
                }
                case SLICING_GAUSSIAN_FIT:
                {
                    double sigma = 1.0 / (sqrt(boost::math::constants::pi<double>() * 2.0));
                    boost::math::normal_distribution<> norm(0.0, sigma);
                    double tau_point = argument / config.beta * static_cast<double>(config.num_slices);
                    double fixed_tau_point = ceil(tau_point - 0.5);
                    int fixed_int = static_cast<int>(fixed_tau_point);
                    green_tau_measurements_[flavor * (config.num_slices + 1) + fixed_int] += m_matrix * bubble_sign * boost::math::pdf(norm, tau_point - fixed_tau_point);
                    if(fixed_int + 1 <= config.num_slices)
                        green_tau_measurements_[flavor * (config.num_slices + 1) + fixed_int + 1] += m_matrix * bubble_sign * boost::math::pdf(norm, tau_point - (fixed_tau_point + 1.0));
                    if(fixed_int + 2 <= config.num_slices)
                        green_tau_measurements_[flavor * (config.num_slices + 1) + fixed_int + 2] += m_matrix * bubble_sign * boost::math::pdf(norm, tau_point - (fixed_tau_point + 2.0));
                    if(fixed_int - 1 >= 0)
                        green_tau_measurements_[flavor * (config.num_slices + 1) + fixed_int - 1] += m_matrix * bubble_sign * boost::math::pdf(norm, tau_point - (fixed_tau_point - 1.0));
                    if(fixed_int - 2 >= 0)
                        green_tau_measurements_[flavor * (config.num_slices + 1) + fixed_int - 2] += m_matrix * bubble_sign * boost::math::pdf(norm, tau_point - (fixed_tau_point - 2.0));

                    break;
                }
                default:
                {
                    std::string error_string = "Unknown slicing method!";
                    Exception_RuntimeError(error_string);
                }
                }

                if (config.measure_green_legendre && IsThermalized()) {
                    if(config.arg_cache_size > 0) {
                        int index = (int)(argument / config.beta * (config.arg_cache_size - 1) + 0.5);
                        if(is_cheap_[flavor] == true) {
                            for(size_t l = 0; l < config.num_legendre; l++) {
                                double leg_val = m_matrix * arg_cache_d_->GetValue(index, l) * bubble_sign;
                                green_legendre_measurements_[flavor * config.num_legendre + l] -= leg_val;
                                if(config.measure_sigmag)
                                    sigmag_legendre_measurements_[flavor * config.num_legendre + l] -= leg_val * pref;
                            }
                        } else {
                            double mult = (index==0 || index==(config.arg_cache_size - 1)) ? 2.0 : 1.0;
                            double value_cached = m_matrix * bubble_sign * mult;
                            lg_cached_[flavor][index] += value_cached;
                            if(config.measure_sigmag)
                                ls_cached_[flavor][index] += value_cached * pref;
                        }
                    } else {
                        const double x = 2.0 * argument / config.beta - 1.0;
                        double pl_2 = 1.0;
                        double pl_1 = x;
                        double legendre_p;
                        {
                            for(size_t l = 0; l < config.num_legendre; l++) {
                                /*legendre_generator L;
                            L.reset(x);
                            L.cycle_to(l);*/
                                if(l == 0)
                                    legendre_p = 1.0;
                                else if (l == 1)
                                    legendre_p = x;
                                else {
                                    legendre_p = ((2.0 * l - 1.0) * x * pl_1 - (l - 1.0) * pl_2) / static_cast<double>(l);//l
                                    pl_2 = pl_1; //l-2
                                    pl_1 = legendre_p; //l-1
                                }
                                double leg_val = -m_matrix * legendre_p * bubble_sign;
                                green_legendre_measurements_[flavor * config.num_legendre + l] += leg_val;
                                if (config.measure_sigmag)
                                    sigmag_legendre_measurements_[flavor * config.num_legendre + l] += leg_val * pref;
                            }
                        }
                    }
                }//measure gl

                if (config.measure_green_omega && IsThermalized()) {
                    int cache_index = arg_cache_c_ ? static_cast<int>(ceil((arg + config.beta) / arg_cache_c_->GetDiv() - 0.5)) : 0;
                    if(config.use_arg_cache && cache_index < arg_cache_c_->GetSize()) {
                        for(size_t wn = 0; wn < config.num_matsubara; wn++) {
                            std::complex<double> meas = -m_matrix * arg_cache_c_->GetValue(cache_index, wn);

                            green_omega_real_measurements_[flavor * config.num_matsubara + wn] += meas.real();
                            green_omega_imaginary_measurements_[flavor * config.num_matsubara + wn] += meas.imag();
                            if (config.measure_sigmag) {
                                meas *= pref;
                                sigmag_omega_real_measurements_[flavor * config.num_matsubara + wn] += meas.real();
                                sigmag_omega_imaginary_measurements_[flavor * config.num_matsubara + wn] += meas.imag();
                            }
                        }
                    } else {
                        double pi = boost::math::constants::pi<double>();
                        std::complex<double> exp_ = exp(-1.0 * I * pi * arg / config.beta);
                        std::complex<double> dexp = exp(2.0 * I * pi * arg / config.beta);

                        for(size_t wn = 0; wn < config.num_matsubara; wn++) {
                            exp_ *= dexp;
                            std::complex<double> meas = -m_matrix * exp_;//note the -
                            //              std::complex<double> meas = -m_matrix*EXP(wn,arg);//note the -
                            green_omega_real_measurements_[flavor * config.num_matsubara + wn] += meas.real();
                            green_omega_imaginary_measurements_[flavor * config.num_matsubara + wn] += meas.imag();
                            if (config.measure_sigmag) {
                                meas *= pref;
                                sigmag_omega_real_measurements_[flavor * config.num_matsubara + wn] += meas.real();
                                sigmag_omega_imaginary_measurements_[flavor * config.num_matsubara + wn] += meas.imag();
                            }
                        }
                    }
                }//measure_gw

                if (config.measure_green2_omega) {//measure <T c c^* c c^*> (a1 c1 a2 c2); factorized measurement
                    static const size_t Nwh = config.num_matsubara2 / 2;
                    double pi = boost::math::constants::pi<double>();
                    std::complex<double> dexp1 = exp(2.0 * I * pi * tau_1 / config.beta);
                    std::complex<double> dexp2 = exp(-2.0 * I * pi * tau_2 / config.beta);
                    std::complex<double> expinit1 = exp(I * ((2.0 * (-static_cast<double>(Nwh) - 1.0) + 1.0) * pi * tau_1 / config.beta));
                    std::complex<double> expinit2 = exp(-I * ((2.0 * (-static_cast<double>(Nwh) - 1.0) + 1.0) * pi * tau_2 / config.beta));
                    std::complex<double> exp1 = expinit1; //fast update of the exponential
                    for(size_t w1n = 0; w1n < config.num_matsubara_aux; w1n++) {
                        exp1 *= dexp1;
                        std::complex<double> exp2 = expinit2;
                        //std::complex<double> exp1=EXP(w1n-Nwh,tau_1);//exp(I w1 t)
                        for(size_t w2n = 0; w2n < config.num_matsubara_aux; w2n++) {
                            exp2 *= dexp2;
                            //std::complex<double> exp2=EXP(w2n-Nwh,-tau_2);//exp(-I w2 t)
                            m_omega_measurements_[flavor * config.num_matsubara_aux * config.num_matsubara_aux + w1n * config.num_matsubara_aux + w2n] += m_matrix * exp1 * exp2;
                            if (config.measure_sigmag)
                                nm_omega_measurements_[flavor * config.num_matsubara_aux * config.num_matsubara_aux + w1n * config.num_matsubara_aux + w2n] += pref * m_matrix * exp1 * exp2; //prefactor has opposite sign for h
                        }
                    }
                }//MEASURE_g2w
            }
        }
    }//segments.size()>0
}

std::vector< std::vector<int> > HybridizationSolver::GetStates()
{
    return jg_->GetData();
}

void HybridizationSolver::ExportResults(MCResults &results)
{
    results.Clear();
    results.InsertResult(observable_green_tau_);
    if(config.measure_sigmag)
        results.InsertResult(observable_sigmag_tau_);
    results.InsertResult(observable_slice_coverage_);
    if(config.measure_green_legendre) {
        results.InsertResult(observable_green_legendre_);
        if(config.measure_sigmag)
            results.InsertResult(observable_sigmag_legendre_);
    }
    if(config.measure_green_omega) {
        results.InsertResult(observable_green_omega_real_);
        results.InsertResult(observable_green_omega_imginary_);
        if(config.measure_sigmag) {
            results.InsertResult(observable_simgag_omega_real_);
            results.InsertResult(observable_sigmag_omega_imaginary_);
        }
    }
    results.InsertResult(observable_density_);
    results.InsertResult(observable_matrix_size_);
    if(config.measure_nnt)
        results.InsertResult(observable_nnt_);
    if(config.measure_nn)
        results.InsertResult(observable_nn_);
    if(config.measure_sector_statistics)
        results.InsertResult(observable_sector_);
    if(config.measure_green2_omega) {
        results.InsertResult(observable_green2_omega_real_);
        results.InsertResult(observable_green2_omega_imaginary_);
        if(config.measure_sigmag){
            results.InsertResult(observable_h_omega_real_);
            results.InsertResult(observable_h_omega_imaginary_);
        }
    }
    results.InsertResult(observable_sign_);
    results.InsertResult(observable_energy_);
}

double HybridizationSolver::ComputeDensity(int flavor)
{
    if(full_line_[flavor] == true)
        return config.beta;
    else {
        double density = 0.0;
        segment_container_t::iterator it;
        for(it = segments_[flavor].begin(); it != segments_[flavor].end(); it++) {
            if(it->t_start() < it->t_end())
                density += it->t_end() - it->t_start();
            else
                density += it->t_end() + config.beta - it->t_start();
        }

        return density;
    }
}

void HybridizationSolver::MeasureNNT_Reference()
{
    double size_multiplier = 1.0 / static_cast<double>(config.num_nn + 1);
    std::vector< std::vector<size_t> > discrete_density;
    discrete_density.resize(config.num_flavors);
    for(size_t f = 0; f < config.num_flavors; f++) {
        discrete_density[f].resize(config.num_nn + 1);
        for(size_t i = 0; i < config.num_nn + 1; i++){
            double tau = i * config.beta / static_cast<double>(config.num_nn);
            discrete_density[f][i] = get_occupation(segments_[f], full_line_[f], tau, config.beta);
        }
    }
    for(size_t f0 = 0; f0 < config.num_flavors; f0++) {
        for(size_t f1 = 0; f1 <= f0; f1++) {
            for(size_t t0 = 0; t0 < config.num_nn + 1; t0++) {
                for(size_t t1 = 0; t1 < config.num_nn + 1; t1++) {
                    size_t index = t0 + t1;
                    if(index > config.num_nn)
                        index -= config.num_nn + 1;
                    //std::cout << "b4 " << nntm_measurements_[t0](f0,f1);
                    double tmp = static_cast<double>(discrete_density[f0][index] * discrete_density[f1][t0]) * size_multiplier;
                    nntm_measurements_[t1](f0,f1) += tmp;
                    if(f0 != f1)
                        nntm_measurements_[t1](f1,f0) += tmp;
                    //std::cout << " " << tmp << " " << nntm_measurements_[t0](f0,f1) << std::endl;
                }
            }
        }
    }
}
