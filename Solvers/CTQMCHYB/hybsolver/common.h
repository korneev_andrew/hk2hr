/*
* A.A. Dyachenko
*/
#ifndef CTHYB_COMMON_HEADERS_H
#define CTHYB_COMMON_HEADERS_H

#include "system_info.h"
#include <cmath>
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/io.hpp>
#include <boost/math/constants/constants.hpp>
#include <boost/version.hpp>
#ifdef CTHYB_HAS_UUID
#include <boost/uuid/uuid.hpp>
#endif
#include <map>
#include <fstream>
#include <set>
#include <vector>
#include <valarray>
#include <ctime>
#include "mc_random.h"
#include <times.h>

#endif // CTHYB_COMMON_HEADERS_H
