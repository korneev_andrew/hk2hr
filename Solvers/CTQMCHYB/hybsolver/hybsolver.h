/*
* A.A. Dyachenko
*/
#ifndef HYBSOLVER_H
#define HYBSOLVER_H

#include "hybsolver/common.h"
#include "hybsolver/commonconfig.h"
#include "observable_container.h"
#include <boost/numeric/ublas/matrix.hpp>
#include <hybridization_expansion_common.h>

class ConfigurationSet;
class JoutaiGenerator;
class MCResults;

template <typename T> class ArgCache;

class HybridizationSolver
{
public:
    HybridizationSolver(const uint32_t &seed);
    ~HybridizationSolver();

    void MakeSweep();
    std::vector< std::vector<int> > GetStatistics();
    void SetSystem(const std::vector< std::vector<double> > &hybfunc, const std::vector<double> &mu, const blas_matrix &umat);
    void Prepare();
    bool IsThermalized();
    double FractionDone();
    CommonConfig *GetConfig();
    std::vector< std::vector<int> > GetStates();

    void DumpInput();

    void ExportSegments();

    void ExportResults(MCResults &results);
    void WriteLog(std::string &str);
    void WriteLog(std::stringstream &stream);
    void WriteLog(const char *s);
    void FlushLog(bool severe = false);
    void Exception_RuntimeError(std::string &str);
    void Exception_RuntimeError(std::stringstream &stream);
    void Exception_RuntimeError(const char *s);
    const std::stringstream &GetLog();

    //ConfigurationSet *configuration_set_;
private:
    void ResetContainers();
    void ResizeContainers();
    void PrepareStatistics();
    void ReportStatistics();

    void MeasureDensityCorrelationFunction();
    void MeasureNNT_Reference();
    void MeasureStates();
    void MeasureGreens(int flavor);

    //do one random update to random flavor per bin
    bool PerformUpdate(int flavor);
    double ComputeDensity(int flavor);

    void InitCacheFunction();
    void CollectCache();
    void EvaluateCost();

    ArgCache<double> *arg_cache_d_;
    ArgCache< std::complex<double> > *arg_cache_c_;

    CommonConfig config;

    size_t sweeps_;						// sweeps done
    std::vector<double> mu_e_;			// mu-<\epsilon>
    std::vector<double> mu_original_;
    blas_matrix u_matrix_;
    hybridization_container_t delta_original_;			// F_up(\tau) = -G_{0,down}^{-1}(-\tau) + (iw + mu) hybridization function
    hybridization_container_t delta_up_down_;
    hybridization_container_t delta_updown_downup_;
    std::vector< segment_container_t > segments_;		// stores configurations with 0,1,... segments (but not full line)

    std::vector<int> full_line_;					// if 1 means that particle occupies full time-line
    std::vector<double>	sign_;					// sign of Z_n_up
    std::vector<blas_matrix> m_;				// inverse matrix for up-spins
    std::vector< std::vector<double> > n_vectors;
    std::valarray<double> green_tau_measurements_;
    std::valarray<double> sigmag_tau_measurements_;
    std::valarray<double> green_legendre_measurements_;
    std::valarray<double> sigmag_legendre_measurements_;
    std::valarray<double> green_omega_real_measurements_;
    std::valarray<double> green_omega_imaginary_measurements_;
    std::valarray<double> occupancy_measurements_;
    double sign_measurements_;
    std::valarray<double> m_size_measurements_;
    std::valarray<double> nn_measurements_;
    std::valarray<double> sector_measurements_;
    std::valarray<double> green2_omega_real_measurements_;
    std::valarray<double> green2_omega_imaginary_measurements_;
    std::valarray<double> sigmag_omega_real_measurements_;
    std::valarray<double> sigmag_omega_imaginary_measurements_;
    std::valarray<double> h_omega_real_measurements_;
    std::valarray<double> h_omega_imaginary_measurements_;
    std::valarray< std::complex<double> > m_omega_measurements_;
    std::valarray< std::complex<double> > nm_omega_measurements_;
    std::valarray<double> slice_coverage_measurements_;
    std::valarray<double> energy_measurements_;
    std::valarray< boost::numeric::ublas::matrix<double> > nntm_measurements_;

    time_t start_time_;

    std::vector<int> statistics_full_line;
    std::vector<int> statistics_segment;
    std::vector<int> statistics_antisegment;
    std::vector<int> statistics_shift;
    std::vector<int> statistics_swap;
    JoutaiGenerator *jg_;

    Random *random_;
    ObservableContainer<> *observable_green_tau_;
    ObservableContainer<> *observable_sigmag_tau_;
    ObservableContainer<> *observable_green_legendre_;
    ObservableContainer<> *observable_sigmag_legendre_;
    ObservableContainer<> *observable_green_omega_real_;
    ObservableContainer<> *observable_green_omega_imginary_;
    ObservableContainer<> *observable_simgag_omega_real_;
    ObservableContainer<> *observable_sigmag_omega_imaginary_;
    ObservableContainer<> *observable_density_;
    ObservableContainer<> *observable_matrix_size_;
    ObservableContainer<> *observable_nnt_;
    ObservableContainer<> *observable_nn_;
    ObservableContainer<> *observable_sector_;
    ObservableContainer<> *observable_green2_omega_real_;
    ObservableContainer<> *observable_green2_omega_imaginary_;
    ObservableContainer<> *observable_h_omega_real_;
    ObservableContainer<> *observable_h_omega_imaginary_;
    ObservableContainer<> *observable_slice_coverage_;
    ObservableContainer<> *observable_sign_;
    ObservableContainer<> *observable_energy_;
    std::ofstream *log_file_;
#ifdef CTHYB_HAS_UUID
    boost::uuids::uuid uuid_;
#endif
    std::string uuid_string_;
    std::stringstream log_stream_;
    std::vector< std::valarray<double> > lg_cached_;
    std::vector< std::valarray<double> > ls_cached_;
    std::vector<bool> is_cheap_;
};

#endif // HYBSOLVER_H
