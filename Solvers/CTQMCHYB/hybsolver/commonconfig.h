/*
* A.A. Dyachenko
*/
#ifndef COMMONCONFIG_H
#define COMMONCONFIG_H

#include "hybsolver/common.h"
#include "mc_random.h"

#define CFG_THERMALIZATION_SWEEPS "thermalization_sweeps"
#define CFG_TOTAL_SWEEPS "total_sweeps"
#define CFG_NUM_NN "nnt_slices"
#define CFG_NUM_UPDATES "number_of_updates"
#define CFG_NUM_MEAS "number_of_measurements"
#define CFG_OBSERVABLE_BIN_SIZE "observable_bin_size"
#define CFG_NUM_MATSUBARA "number_of_matsubara_frequencies"
#define CFG_NUM_MATSUBARA2 "number_of_two-particle_matsubara_frequencies"
#define CFG_NUM_BOSONIC "number_of_bosonic_frequencies"
#define CFG_NUM_LEGENDRE "number_of_legendre_coefficients"
#define CFG_NUM_SLICES "number_of_tau_slices"
#define CFG_NUM_FLAVORS "number_of_flavors"
#define CFG_MAXIMUM_TIME "maximum_time"

#define CFG_BETA "beta"
#define CFG_H_FIELD "magnetic_field"

#define CFG_MEASURE_GREEN_OMEGA "measure_green_omega"
#define CFG_MEASURE_GREEN_LEGENDRE "measure_green_legendre"
#define CFG_MEASURE_NNT "measure_nnt"
#define CFG_MEASURE_NN "measure_nn"
#define CFG_MEASURE_SIGMAG "measure_sigmag"
#define CFG_MEASURE_SECTOR_STATISTICS "measure_sector_statistics"
#define CFG_MEASURE_GREEN2_OMEGA "measure_green_two-particle_omega"
#define CFG_UPDATES "updates"
#define CFG_SLICING_METHOD "slicing_method"
#define CFG_RANDOM_GENERATOR "random_generator"
#define CFG_KERNEL_TYPE "kernel_type"

#define CFG_USE_ARG_CACHE "use_arg_cache"
#define CFG_ARG_CACHE_SIZE "arg_cache_size"

enum VERBOSITY_LEVEL
{
    VERBOSITY_STDOUT = 30,
    VERBOSITY_FILE = 30
};

enum SLICING_METHOD
{
    SLICING_DUMB = 0,
    SLICING_DUMB_PROPER = 1,
    SLICING_LINEAR_FIT = 2,
    SLICING_GAUSSIAN_FIT = 3
};

enum UPDATE_TYPE
{
    UPDATE_FULL_LINE = 0,
    UPDATE_SEGMENT = 1,
    UPDATE_ANTISEGMENT = 2,
    UPDATE_SHIFT = 3,
    UPDATE_GLOBAL = 4
};

enum DATA_MEASUREMENTS {
    DATA_GREEN_TAU = 0,
    DATA_GREEN_LEGENDRE = 1,
    DATA_GREEN_OMEGA = 2,
    DATA_M_SIZE = 4,
    DATA_NNT = 5,
    DATA_DENSITY = 7,
    DATA_SIGN = 8,
    DATA_NN = 9,
    DATA_SIGMAG_LEGENDRE = 10,
    DATA_STATISTICS = 11,
    //DATA_GREEN2_OMEGA_REAL = 12,
    //DATA_GREEN2_OMEGA_IMAGINARY = 13,
    DATA_SIGMAG_OMEGA = 14,
    //DATA_SIGMAG_OMEGA_IMAGINARY = 15,
    //DATA_H_OMEGA_REAL = 16,
    //DATA_H_OMEGA_IMAGINARY = 17,
    DATA_SECTOR_STATISTICS = 18,
    DATA_SIGMAG_TAU = 19,
    DATA_ENERGY = 21
};

struct CommonConfig
{
public:
    size_t thermalization_sweeps;
    size_t total_sweeps;
    size_t num_nn;
    size_t num_updates;
    size_t num_meas;
    size_t observable_bin_size; //then we put these averaged measurements in the bins of [sic] to average them with jack-knife
    size_t num_matsubara;
    size_t num_matsubara2;
    size_t num_bosonic;
    size_t num_legendre;
    size_t num_slices;
    size_t num_flavors;
    //int minimum_occupation;
    size_t arg_cache_size;

    int max_time;

    unsigned int global_seed;

    double beta;

    bool measure_green_omega;
    bool measure_green_legendre;
    bool measure_nnt; //"measure density-density correlation function"
    bool measure_nn; //"measure density-density correlation function at equal times"
    bool measure_sigmag;
    bool measure_sector_statistics;
    bool measure_green2_omega; //two-particle

    bool use_arg_cache;

    //set by solver
    //int viable_configurations;
    size_t num_matsubara_aux;

    //set at initialization
    int verbosity;

    //very important: possible updates and their weights
    std::vector< std::pair<UPDATE_TYPE, double> > updates_;
    SLICING_METHOD slicing_method;
    RANDOM_GENERATOR random_generator;

    double spinSign(size_t flavor) {
        if(flavor < num_flavors / 2)
            return 1.0;

        return -1.0;
    }

    double invertedSpinSign(size_t flavor) {
        if(flavor < num_flavors / 2)
            return -1.0;

        return 1.0;
    }
};

#endif // COMMONCONFIG_H
