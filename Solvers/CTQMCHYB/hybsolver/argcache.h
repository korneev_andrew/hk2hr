#ifndef ARGCACHE_H
#define ARGCACHE_H

#include <vector>
#include <complex>
#include <cmath>

template <typename T> class ArgCache
{
public:
    ArgCache();
    ~ArgCache();
    void ComputeArgCache(size_t cache_size, double beta, size_t sub_size);
    const T GetValue(size_t index, size_t sub_index);
    double GetDiv();
    size_t GetSize();
private:
    T* arg_cache_;
    double arg_div_;
    size_t arg_cache_size_;
    size_t arg_cache_sub_size_;
};

template <typename T> ArgCache<T>::ArgCache()
{
    arg_cache_ = NULL;
    arg_cache_size_ = 0;
}

template <typename T> ArgCache<T>::~ArgCache()
{
    if(arg_cache_ != NULL)
        delete [] arg_cache_;
}

template <> void ArgCache<double>::ComputeArgCache(size_t cache_size, double beta, size_t sub_size)
{
    arg_cache_size_ = cache_size;
    arg_cache_sub_size_ = sub_size;
    std::cout << "Building argument cache..." << std::endl;
    //size_t cache_size = argument_mean * 2.0 / (argument_min * config.thermalization_sweeps * 0.5 * config.num_meas);
    arg_div_ = beta / (1.0 * (cache_size - 1.0));
    arg_cache_ = new double[cache_size * sub_size];
    double argument = 0.0;
    for(size_t i = 0; i < cache_size; i++) {
        double *legendre = &arg_cache_[i * sub_size];
        const double x = 2.0 * argument / beta - 1.0;
        double pl_2 = 1.0;
        double pl_1 = x;
        double legendre_p;
        {
            for(size_t l = 0; l < sub_size; l++) {
                /*legendre_generator L;
                    L.reset(x);
                    L.cycle_to(l);*/
                if(l == 0)
                    legendre_p = 1.0;
                else if (l == 1)
                    legendre_p = x;
                else {
                    legendre_p = ((2.0 * l - 1.0) * x * pl_1 - (l - 1.0) * pl_2) / static_cast<double>(l);//l
                    pl_2 = pl_1; //l-2
                    pl_1 = legendre_p; //l-1
                }
                legendre[l] = legendre_p;
            }
        }
        argument += arg_div_;
    }
    std::cout << "Built " << cache_size << " items with stride of " << arg_div_ << std::endl;
}

template <> void ArgCache< std::complex<double> >::ComputeArgCache(size_t cache_size, double beta, size_t sub_size)
{
    arg_cache_size_ = cache_size;
    arg_cache_sub_size_ = sub_size;
    static const std::complex<double> I(0.0, 1.0);
    double pi = boost::math::constants::pi<double>();
    std::cout << "Building argument cache..." << std::endl;
    //size_t cache_size = argument_mean * 2.0 / (argument_min * config.thermalization_sweeps * 0.5 * config.num_meas);
    arg_div_ = 2.0 * beta / (1.0 * (cache_size - 1.0));
    arg_cache_ = new std::complex<double>[cache_size * sub_size];
    double argument = -beta;
    for(size_t i = 0; i < cache_size; i++) {
        std::complex<double> *matsubara = &arg_cache_[i * sub_size];

        std::complex<double> exp_ = exp(-1.0 * I * pi * argument / beta);
        std::complex<double> dexp = exp(2.0 * I * pi * argument / beta);

        for(size_t wn = 0; wn < sub_size; wn++) {
            exp_ *= dexp;
            matsubara[wn] = exp_;
        }

        argument += arg_div_;
    }
    std::cout << "Built " << cache_size << " items with stride of " << arg_div_ << std::endl;
}

template <typename T> const T ArgCache<T>::GetValue(size_t index, size_t sub_index)
{
    return arg_cache_[index * arg_cache_sub_size_ + sub_index];
}

template <typename T> double ArgCache<T>::GetDiv()
{
    return arg_div_;
}

template <typename T> size_t ArgCache<T>::GetSize()
{
    return arg_cache_size_;
}

#endif // ARGCACHE_H
