/*
* A.A. Dyachenko
*/
#ifndef JOUTAI_H
#define JOUTAI_H

#include <cstdlib>
#include <vector>
#include <string>

class Operator
{
public:
    Operator(double t, int tp, int f)
    {
        tau = t;
        type = tp;
        flavor = f;
    }
    const bool operator<(const Operator &other) const
    {
        return tau < other.tau;
    }

    double tau;
    int type;
    int flavor;
};

class JoutaiBlock
{
public:
    JoutaiBlock(int n, int sz, int i)
    {
        n_ = n;
        sz_ = sz;
        i_ = i;
        size_ = 1;
        diagonal_ = false;
    }

    int i_;
    int n_;
    int sz_;
    int size_;
    bool diagonal_;
};

class Joutai
{
public:
    Joutai(size_t size);
    int &Element(size_t index);
    size_t size()
    {
        return data_.size();
    }
    std::string Print();
    const bool operator<(const Joutai &other) const
    {
        if(this->N() != other.N())
            return this->N() < other.N();
        else if(this->minusSz() != other.minusSz())
            return this->minusSz() < other.minusSz();
        else
            return this->compute_value() < other.compute_value();

        return false;
    }
    const bool operator==(const Joutai &other) const
    {
        bool equal = true;
        for(size_t i = 0; i < data_.size(); i++)
            equal *= data_.at(i) == other.data_.at(i);
        return equal;
    }
    const int N() const;
    const int minusSz() const;

private:
    std::vector<int> data_;
    const int compute_value() const;
};

class JoutaiGenerator
{
public:
    JoutaiGenerator(size_t size);
    Joutai &GetConfiguration(size_t i);
    size_t Count();
    void Generate();
    void Sort();
    void BuildBlocks();
    std::vector<JoutaiBlock> &GetBlocks();
    std::vector< std::vector<int> > GetData();
    std::vector<Joutai> &InternalData();
private:
    std::vector<Joutai> data_;
    std::vector<JoutaiBlock> blocks_;
    size_t size_;
    void _iterate(Joutai &k, size_t index);
};

class TauState
{
public:
    TauState(double s, double e) : start(s), end(e)
    {

    }

    double start;
    double end;
    std::vector<int> state;
};

void mark_state(std::vector<TauState> &tau_line, double s, double e, int f);

#endif // JOUTAI_H
