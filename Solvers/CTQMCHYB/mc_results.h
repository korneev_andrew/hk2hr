#ifndef MC_RESULTS_H
#define MC_RESULTS_H

#include <map>
#include <valarray>
#include "observable_container.h"

class SingleResult
{
public:
    //SingleResult(std::string name, std::valarray<double> &mean, std::valarray<double> &error, std::valarray<double> &variance);
    SingleResult(ObservableContainer<> *cont);
    ~SingleResult();
    std::valarray<double> mean_;
    std::valarray<double> error_;
    std::valarray<double> variance_;
    std::valarray<double> tau_;

    std::string name_;

    ObservableContainer<> *container_;
    ObservableContainer<> *parent_container_;
};

class MCResults
{
public:
    MCResults();
    ~MCResults();
    void InsertResult(ObservableContainer<> *observable);
    SingleResult *GetResult(const char *name);
    void SyncResults();
    void Clear();
private:
    std::vector< std::pair<std::string, SingleResult*> > results_;
};

#endif // MC_RESULTS_H
