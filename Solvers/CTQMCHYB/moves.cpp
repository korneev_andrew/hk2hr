/************************************************************************************
 *
 * ALPS DMFT Project
 *
 * Copyright (C) 2005 - 2011 by Philipp Werner <werner@itp.phys.ethz.ch>,
 *                              Emanuel Gull <gull@phys.columbia.edu>,
 *                              Hartmut Hafermann <hafermann@cpht.polytechnique.fr>
 * 2011 - 2013 minor compatibility modifications by A. A. Dyachenko <adotfive@gmail.com>
 *
 *
 * This software is part of the ALPS Applications, published under the ALPS
 * Application License; you can use, redistribute it and/or modify it under
 * the terms of the license, either version 1 or (at your option) any later
 * version.
 *
 * You should have received a copy of the ALPS Application License along with
 * the ALPS Applications; see the file LICENSE.txt. If not, the license is also
 * available from http://alps.comp-phys.org/.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
 * SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
 * FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 ************************************************************************************/
#include "update.h"
#include "moves.h"

bool compare_bm(blas_matrix &m1, blas_matrix &m2)
{
    bool ok = true;
    if(m1.cols() != m2.cols() || m1.rows() != m2.rows()) {
        std::cerr << "Size doesn't match!" << std::endl;
        ok = false;
    }

    for(size_t i = 0; i < m1.cols(); i++) {
        for(size_t j = 0; j < m2.rows(); j++) {
            if(std::abs(m1(i, j) - m2(i, j)) >= 0.0001) {
                std::cerr << "Elements dont match! " << m1(i, j) << " " << m2(i, j) << std::endl;
                ok = false;
            }
        }
    }

    return ok;
}

extern "C" MC_FULL_LINE insert_remove_full_line(Random *random, double mu, double BETA, blas_matrix &u, int& full_line, std::vector<segment_container_t>& other_segments, std::vector<int>& other_full_line, int this_flavor) {

    int insert = (random->Get_01() < 0.5);

    if ((insert==1 && full_line==1) || (insert==0 && full_line==0)) return FULL_LINE_FAIL; // insert=1(0) means we want to insert(remove) a full line

    int FLAVOR = other_full_line.size();

    double otherlength_u=0;
    for (int i=0; i<FLAVOR; i++) {
        if (i==this_flavor) continue;

        double other_length=0;
        for (segment_container_t::iterator it=other_segments[i].begin(); it!=other_segments[i].end(); it++)
            other_length += (it->t_end()-it->t_start()>0 ? it->t_end()-it->t_start() : it->t_end()-it->t_start()+BETA);

        if (other_full_line[i]==1)
            other_length = BETA;

        otherlength_u += other_length*u(i, this_flavor);

    }

    if (insert) { // try to insert full line
        if (log(random->Get_01()) < BETA*mu-otherlength_u) {
            full_line = 1;
            return FULL_LINE_INSERT;
        }
    }
    else { // try to remove full line
        if (log(random->Get_01()) < -BETA*mu+otherlength_u) {
            full_line = 0;
            return FULL_LINE_REMOVE;
        }
    }

    return FULL_LINE_FAIL;
}


extern "C" MC_SEGMENT insert_remove_segment(Random *random, double t, double BETA, double mu, blas_matrix &u, hybridization_t& F, segment_container_t& segments, blas_matrix & M, double & sign, std::vector<segment_container_t>& other_segments, std::vector<int> other_full_line, int this_flavor) {

    double t_up; // distance to next segment up
    double t_down; // distance to next segment down
    segment_container_t::iterator s_up; // iterator of the segment up
    segment_container_t::iterator s_down; // iterator of the segment down

    if (random->Get_01() < 0.5) { // try to insert a segment
        compute_intervals(t, BETA, t_up, t_down, segments,s_up, s_down);

        if (t_down>0) { // t does not lie on a segment -> it's possible to insert a new one starting from t

            double length = compute_length(random->Get_01(), t_up, 0);

            times segment_insert;
            segment_insert.set_t_start(t);
            double t_final = t + length;
            if (t_final > BETA)
                segment_insert.set_t_end(t_final-BETA);
            else
                segment_insert.set_t_end(t_final);

            double otherlength_u=0;
            int FLAVORS=other_full_line.size();
            for (int i=0; i<FLAVORS; i++) {
                if (i==this_flavor) continue;
                double other_length = compute_overlap(segment_insert, other_segments[i], other_full_line[i], BETA);
                otherlength_u += other_length*u(i, this_flavor);
            }
            double log_prob, overlap, det_rat, det_rat_sign;
            std::vector<double> Fs(segments.size()), Fe(segments.size());

            det_rat = det_rat_up(segment_insert, M, segments, F, Fs, Fe, BETA, det_rat_sign, overlap);

            log_prob = log(BETA*t_up/(segments.size()+1)*det_rat)+mu*length-otherlength_u;

            if (log(random->Get_01()) < log_prob) {
                int position=0;
                for (segment_container_t::iterator it=segments.begin(); it!=s_up; it++)
                    position++;
                compute_M_up(position, M, Fs, Fe, det_rat*overlap);
                sign *= det_rat_sign;
                segment_container_t::iterator sit=segments.insert(s_up, segment_insert);
                if(sit==segments.end()){std::cerr<<"segment could not be inserted! exiting."<<std::endl;}
#ifdef DEBUG_CHECK_FASTUPDATES
                /* matrix check */
                blas_matrix test_m;
                construct_inverse(test_m, segments, BETA, F);
                if(!compare_bm(test_m, M))
                    throw std::runtime_error("M matrix is wrong!");
#endif

                return SEGMENT_INSERT;
            }
        }
    }

    else if (segments.size()>0) { // try to remove a segment
        int position = static_cast<int>(random->Get_01() * segments.size());
        s_down = segments.begin();
        for (int i=0; i<position; i++)
            s_down++;
        s_up=s_down;
        s_up++;
        if (s_up==segments.end())
            s_up = segments.begin();

        double length = s_down->t_end()-s_down->t_start();
        if (length < 0) length += BETA;

        double t_total = s_up->t_start()-s_down->t_start();
        if (t_total <= 0) t_total += BETA;

        times segment_remove = *s_down;

        double otherlength_u=0;
        int FLAVORS=other_full_line.size();
        for (int i=0; i<FLAVORS; i++) {
            if (i==this_flavor) continue;
            double other_length = compute_overlap(segment_remove, other_segments[i], other_full_line[i], BETA);
            otherlength_u += other_length*u(i, this_flavor);
        }

        double log_prob, det_rat, det_rat_sign;

        det_rat = det_rat_down(position, M, segments, det_rat_sign);

        log_prob = log(BETA*t_total/segments.size()/det_rat)+length*mu-otherlength_u;

        if (log(random->Get_01()) < -log_prob) {
            compute_M_down(position, M);
            sign *= det_rat_sign;
            segments.erase(s_down);

#ifdef DEBUG_CHECK_FASTUPDATES
                /* matrix check */
                blas_matrix test_m;
                construct_inverse(test_m, segments, BETA, F);
                if(!compare_bm(test_m, M))
                    throw std::runtime_error("M matrix is wrong!");
#endif

            return SEGMENT_REMOVE;
        }
    }

    return SEGMENT_FAIL;
}


extern "C" MC_ANTISEGMENT insert_remove_antisegment(Random *random, double t, double BETA, double mu,blas_matrix &u, hybridization_t& F, int& full_line, segment_container_t& segments, blas_matrix& M, double & sign,std::vector<segment_container_t>& other_segments, std::vector<int> other_full_line, int this_flavor) {

    double t_up; // distance to next segment up (t_start)
    double t_down; // distance to next segment down (t_end)
    segment_container_t::iterator s_up; // iterator of the segment up
    segment_container_t::iterator s_down; // iterator of the segment down

    if (random->Get_01() < 0.5) { // try to insert an anti-segment

        if (full_line==1) {
            t_down = -BETA;
            double length = compute_length(random->Get_01(), BETA, 0);
            double t_end = (t+length < BETA ? t+length : t+length-BETA);
            times segment_insert(t_end, t);
            times segment_remove(t,t_end);

            double log_prob, overlap, det_rat, det_rat_sign;
            std::vector<double> Fs(segments.size()), Fe(segments.size());
            det_rat = det_rat_up(segment_insert, M, segments, F, Fs, Fe, BETA, det_rat_sign, overlap);

            double otherlength_u=0;
            int FLAVORS=other_full_line.size();
            for (int i=0; i<FLAVORS; i++) {
                if (i==this_flavor) continue;
                double other_length = compute_overlap(segment_remove, other_segments[i], other_full_line[i], BETA);
                otherlength_u += other_length*u(i, this_flavor);
            }
            log_prob = log(BETA*BETA*det_rat)-length*mu+otherlength_u;

            if (log(random->Get_01()) < log_prob) {
                compute_M_up(0, M, Fs, Fe, det_rat*overlap);
                sign *= det_rat_sign;
                //segments.push_back(segment_insert);
                segment_container_t::iterator sit;
                //std::cout<<"segment size: "<<segments.size()<<std::endl;
                sit=segments.insert(segment_insert).first;
                if(sit==segments.end()){std::cerr<<"segment could not be inserted! exiting."<<std::endl;}
                full_line = 0;
                //std::cout<<"insertion completed."<<std::endl;

#ifdef DEBUG_CHECK_FASTUPDATES
                /* matrix check */
                blas_matrix test_m;
                construct_inverse(test_m, segments, BETA, F);
                if(!compare_bm(test_m, M))
                    throw std::runtime_error("M matrix is wrong!");
#endif

                return ANTISEGMENT_INSERT;
            }
        }

        else {
            compute_intervals(t, BETA, t_up, t_down, segments, s_up, s_down);

            if (t_down<0) { // t does lie on a segment -> it's possible to insert an anti-segment starting from t

                double length = compute_length(random->Get_01(), -t_down, 0);

                times segment_shrink(s_down->t_start(),t);

                double t_start = t + length;
                if (t_start > BETA)
                    t_start-=BETA;

                times segment_insert(t_start, s_down->t_end());
                times anti_segment(t,t_start);

                double otherlength_u=0;
                int FLAVORS=other_full_line.size();
                for (int i=0; i<FLAVORS; i++) {
                    if (i==this_flavor) continue;
                    double other_length = compute_overlap(anti_segment, other_segments[i], other_full_line[i], BETA);
                    otherlength_u += other_length*u(i, this_flavor);
                }
                double log_prob, overlap, det_rat, det_rat_sign;
                std::vector<double> R(segments.size());
                det_rat = det_rat_insert_anti(anti_segment, M, segments, F, BETA, det_rat_sign, overlap, R);

                log_prob = log(BETA*(-t_down)/(segments.size()+1)*det_rat)-length*mu+otherlength_u;

                if (log(random->Get_01()) < log_prob) {

                    int s, r; // s is the segment which is shifted, r the segment which is inserted
                    s = 0;
                    for (segment_container_t::iterator it=segments.begin(); it!=s_down; it++)
                        s++;
                    if (anti_segment.t_end() > segment_shrink.t_start())
                        r = s+1;
                    else {
                        r = 0;
                        s++;
                    }

                    compute_M_insert_anti(anti_segment, s, r, M, segments, F, BETA, det_rat*overlap, R);
                    //s_down->set_t_end(t);
                    times segment_new_endpoint(*s_down);
                    segment_container_t::iterator prev_segment=s_down;
                    if(s_down !=segments.begin()) prev_segment--;
                    else prev_segment=segments.begin();
                    segment_new_endpoint.set_t_end(t);
                    segments.erase(s_down); //erase old segment (without shifted end
                    s_down=segments.insert(segment_new_endpoint).first; //in
                    //s_down=segments.insert(prev_segment, segment_new_endpoint); //in
                    if(s_down==segments.end()){std::cerr<<"segment could not be inserted! exiting."<<std::endl;}
                    segment_container_t::iterator sit=segments.insert(segment_insert).first; //insert  new segment
                    //typename S::iterator sit=segments.insert(s_down, segment_insert); //insert  new segment
                    if(sit==segments.end()){std::cerr<<"segment could not be inserted! exiting."<<std::endl;}
                    if (segment_insert.t_start()>segments.begin()->t_start()) {
                        s_down++;
                        segment_container_t::iterator sit=segments.insert(s_down, segment_insert);
                        if(sit==segments.end()){std::cerr<<"segment could not be inserted! exiting."<<std::endl;}
                    }
                    else {
                        segment_container_t::iterator sit=segments.insert(segments.begin(), segment_insert);
                        if(sit==segments.end()){std::cerr<<"segment could not be inserted! exiting."<<std::endl;}
                    }

#ifdef DEBUG_CHECK_FASTUPDATES
                /* matrix check */
                blas_matrix test_m;
                construct_inverse(test_m, segments, BETA, F);
                if(!compare_bm(test_m, M))
                    throw std::runtime_error("M matrix is wrong!");
#endif

                    return ANTISEGMENT_INSERT;
                }
            }
        }

    }
    else if (segments.size()>1) { // try to remove an anti-segment
        int r = static_cast<int>(random->Get_01() * segments.size());
        s_up = segments.begin();
        for (int i=0; i<r; i++) s_up++;

        int s = r-1;
        if (s<0) {
            s=segments.size()-1;
            s_down=segments.end();
            s_down--;
        }
        else {
            s_down = s_up;
            s_down--;
        }

        double length = s_up->t_start() - s_down->t_end();
        if (length < 0) length += BETA;

        double t_total = s_up->t_end() - s_down->t_end();
        if (t_total < 0) t_total += BETA;

        times anti_segment(s_down->t_end(),s_up->t_start());

        double otherlength_u=0;
        int FLAVORS=other_full_line.size();
        for (int i=0; i<FLAVORS; i++) {
            if (i==this_flavor) continue;
            double other_length = compute_overlap(anti_segment, other_segments[i], other_full_line[i], BETA);
            otherlength_u += other_length*u(i, this_flavor);
        }
        double log_prob, det_rat, det_rat_sign;

        det_rat = det_rat_remove_anti(anti_segment, r, s, M, segments, F, BETA, det_rat_sign);

        log_prob = log(BETA*t_total/segments.size()/det_rat)-length*mu+otherlength_u;

        if (log(random->Get_01()) < -log_prob) {

            compute_M_remove_anti(M, s, r);

            double t_end = s_up->t_end();
            segments.erase(s_up);

            if (r>0) {
                s_up=segments.begin();
                for (int k=0; k<s; k++)
                    s_up++;
            }
            else {
                s=segments.size()-1;
                s_up = segments.end();
                s_up--;
            }
            //s_up->set_t_end(t_end);
            times s_up_new(*s_up);
            s_up_new.set_t_end(t_end);
            segments.erase(s_up);
            segment_container_t::iterator sit=segments.insert(s_up_new).first;
            if(sit==segments.end()){std::cerr<<"segment could not be inserted! exiting."<<std::endl;}

#ifdef DEBUG_CHECK_FASTUPDATES
                /* matrix check */
                blas_matrix test_m;
                construct_inverse(test_m, segments, BETA, F);
                if(!compare_bm(test_m, M))
                    throw std::runtime_error("M matrix is wrong!");
#endif

            return ANTISEGMENT_REMOVE;
        }
    }

    else if (segments.size()==1) {

        s_down = segments.begin();

        double det_rat = std::abs(M(0,0));
        double length = s_down->t_start()-s_down->t_end();
        if (length<0) length += BETA;
        times anti_segment(s_down->t_end(),s_down->t_start());

        double otherlength_u=0;
        int FLAVORS=other_full_line.size();
        for (int i=0; i<FLAVORS; i++) {
            if (i==this_flavor) continue;
            double other_length = compute_overlap(anti_segment, other_segments[i], other_full_line[i], BETA);
            otherlength_u += other_length*u(i, this_flavor);
        }
        double log_prob = log(BETA*BETA/det_rat)-length*mu+otherlength_u;

        if (log(random->Get_01()) < -log_prob) {
            full_line=1;
            segments.erase(s_down);
            compute_M_down(0,M); // attention: M.clear() sets elements to zero

#ifdef DEBUG_CHECK_FASTUPDATES
                /* matrix check */
                blas_matrix test_m;
                construct_inverse(test_m, segments, BETA, F);
                if(!compare_bm(test_m, M))
                    throw std::runtime_error("M matrix is wrong!");
#endif

            return ANTISEGMENT_REMOVE;
        }
    }

    return ANTISEGMENT_FAIL;
}

// shift segment
extern "C" MC_SHIFT shift_segment(Random *random, segment_container_t& segments, double BETA, double mu, blas_matrix &u, hybridization_t& F, blas_matrix & M, double & sign, std::vector<segment_container_t>& other_segments, std::vector<int>& other_full_line, int this_flavor) {

    int size = segments.size();

    if (size < 1)
        return SHIFT_FAIL;

    int n = static_cast<int>(random->Get_01() * size);

    segment_container_t::iterator s, s_up;
    s=segments.begin();
    for (int i=0; i<n; i++) s++;
    s_up = s; s_up++;
    if (s_up == segments.end()) s_up = segments.begin();

    double interval = s_up->t_start() - s->t_start();
    if (interval <= 0) interval += BETA;

    double length = compute_length(random->Get_01(), interval, 0);
    double length_old = s->t_end()-s->t_start();
    if (length_old<0)
        length_old += BETA;

    double new_t_end = s->t_start() + length;
    if (new_t_end > BETA)
        new_t_end -= BETA;

    times segment_insert(s->t_start(), new_t_end);
    times segment_remove=*s;

    double otherlength_u=0;
    int FLAVORS=other_full_line.size();
    for (int i=0; i<FLAVORS; i++) {
        if (i==this_flavor) continue;
        double other_length = compute_overlap(segment_insert, other_segments[i], other_full_line[i], BETA)-compute_overlap(segment_remove, other_segments[i], other_full_line[i], BETA);
        otherlength_u += other_length*u(i, this_flavor);
    }
    double det_rat, det_rat_sign, overlap;

    det_rat = det_rat_shift(segment_insert, n, M, segments, F, BETA, det_rat_sign, overlap);

    if (log(random->Get_01()) < log(det_rat)+(length-length_old)*mu-otherlength_u) {

        compute_M_shift(segment_insert, n, M, segments, F, BETA, det_rat*overlap);
        sign *= det_rat_sign;
        //s->set_t_end(new_t_end);
        times s_new(*s);
        s_new.set_t_end(new_t_end);
        segments.erase(s);
        segment_container_t::iterator sit;
        sit=segments.insert(s_new).first;
        if(sit==segments.end()){std::cerr<<"segment could not be inserted! exiting."<<std::endl;}

        return SHIFT_SUCCESS;
    }

    return SHIFT_FAIL;
}

// swap segment configurations
/*void swap_segments(double BETA, hybridization_t& F_up, hybridization_t & F_down, segment_container_t& segments_up, segment_container_t& segments_down, int& full_line_up, int& full_line_down, double & sign_up, double & sign_down, blas_matrix & M_up, blas_matrix& M_down) {

    blas_matrix M_new_up, M_new_down;

    // before swap
    double det_old_up = construct_inverse(M_new_up, segments_up, BETA,  F_up); // here M_new_up is just a dummy
    double det_old_down = construct_inverse(M_new_down, segments_down, BETA,  F_down); // here M_new_down is just a dummy
    // before swap
    double det_new_up = construct_inverse(M_new_up, segments_down, BETA,  F_up);
    double det_new_down = construct_inverse(M_new_down, segments_up, BETA,  F_down);

    double det_rat = (det_new_up/det_old_up)*(det_new_down/det_old_down);

    // length of segments, overlap and phonon part are not changed
    if (genrand_real1() < fabs(det_rat)) {

        //std::cout << "success\n";
        swap(M_new_up, M_up);
        swap(M_new_down, M_down);
        swap(segments_up, segments_down);
        std::swap(full_line_up, full_line_down);
        std::swap(sign_up, sign_down);
    }
}*/
//swap_segments(config.beta, orbital1, orbital2, mu_e_, segments_, full_line_, u_matrix_, delta_, sign_, m_)
/*bool swap_segments(double BETA, const int &i, const int &j, const std::vector<double> &mu, std::vector< segment_container_t> &segments, std::vector<int> &full_line, const blas_matrix &u, hybridization_container_t& F, std::vector<double> &sign, std::vector< blas_matrix > &M)
{
    blas_matrix M_new_up, M_new_down;

    //    fprintf(outfile, "entered swap with %d %d %f %f\n", i, j, ei, ej);
    double det_old_i = construct_inverse(M_new_up, segments[i], BETA,  F[i]); // before swap
    double det_old_j = construct_inverse(M_new_down, segments[j], BETA,  F[j]);
    double det_new_i = construct_inverse(M_new_up, segments[j], BETA,  F[i]); // after swap
    double det_new_j = construct_inverse(M_new_down, segments[i], BETA,  F[j]);
    double det_rat = (det_new_i / det_old_i) * (det_new_j / det_old_j);

    times full_segment(0, BETA);
    double length_i = compute_overlap(full_segment, segments[i], full_line[i], BETA);
    double length_j = compute_overlap(full_segment, segments[j], full_line[j], BETA);
    double e_site = length_i * (mu[i] - mu[j]) + length_j*(mu[j] - mu[i]);  // site energy contribution

    double overlap_u = 0.0;
    for (int k = 0; k < segments.size(); k++) {
        if (k != i && k != j) {
            double overlap = 0.0;
            for (segment_container_t::iterator it = segments[i].begin(); it != segments[i].end(); it++) {
                overlap += compute_overlap(*it, segments[k], full_line[k], BETA);
            }
            overlap_u += (u(j,k) - u(i,k)) * overlap;
            overlap = 0.0;
            for (segment_container_t::iterator it = segments[j].begin(); it != segments[j].end(); it++) {
                overlap += compute_overlap(*it, segments[k], full_line[k], BETA);
            }
            overlap_u += (u(i,k) - u(j,k)) * overlap;
        }
    }
    //    if(rng() < fabs(det_rat)) {
    if (dsfmt_genrand_close_open(0) < std::fabs(det_rat) * exp(-overlap_u - e_site)) {   // length of segments, overlap and phonon part are not changed
        //	    fprintf(outfile, "swap accepted! overlap_u: %f e_site: %f\n", overlap_u, e_site);
        swap(M_new_up, M[i]);
        swap(M_new_down, M[j]);
        swap(segments[i], segments[j]);
        int dummy1=full_line[i];
        full_line[i]=full_line[j];
        full_line[j]=dummy1;
        double dummy2=sign[i];
        sign[i]=sign[j];
        sign[j]=dummy2;

        return true;
    } else
        return false;
}*/

extern "C" MC_SWAP swap_segments_spin(Random *random, double BETA, std::vector< segment_container_t > & segments, std::vector<int> & full_line, hybridization_container_t& F, std::vector<double>& sign, std::vector< blas_matrix > & M, std::vector<double> & mu_e, blas_matrix &u)
{
    std::vector< blas_matrix > M_new(segments.size());
    double det_rat = 1.0;
    double e_site = 0.0;
    double overlap_u = 0.0;
    size_t FLAVOR2=(segments.size()+1)/2;
    times full_segment(0,BETA);
    for (size_t k=0; k<FLAVOR2; k++){
        double det_old = construct_inverse(M_new[k+FLAVOR2], segments[k], BETA,  F[k]); // before swap
        double det_new = construct_inverse(M_new[k+FLAVOR2], segments[k], BETA,  F[k+FLAVOR2]);
        if (M_new[k+FLAVOR2].cols() > 0) det_rat *= det_new/det_old;
        e_site+=compute_overlap(full_segment,segments[k],full_line[k],BETA)*(mu_e[k + FLAVOR2] - mu_e[k]);

        for (size_t k1 = 0; k1 < segments.size(); k1++) {
            if (k1 != k) {
                double overlap = 0.0;
                for (segment_container_t::iterator it = segments[k].begin(); it != segments[k].end(); it++) {
                    overlap += compute_overlap(*it, segments[k1], full_line[k1], BETA);
                }
                overlap_u += (u(k1 - FLAVOR2, k + FLAVOR2) - u(k1, k)) * overlap * 0.5;
                std::cout << "um " << u(k1 - FLAVOR2, k + FLAVOR2) << " " << u(k1, k) << " " << overlap << std::endl;
            }
        }
    };
    std::cout << "ov " << overlap_u << std::endl;
    for (size_t k=FLAVOR2; k<segments.size(); k++){
        double det_old = construct_inverse(M_new[k-FLAVOR2], segments[k], BETA,  F[k]); // before swap
        double det_new = construct_inverse(M_new[k-FLAVOR2], segments[k], BETA,  F[k-FLAVOR2]);
        if (M_new[k-FLAVOR2].cols() > 0) det_rat *= det_new/det_old;
        e_site+=compute_overlap(full_segment,segments[k],full_line[k],BETA)*(mu_e[k - FLAVOR2] - mu_e[k]);

        for (size_t k1 = 0; k1 < segments.size(); k1++) {
            if (k1 != k) {
                double overlap = 0.0;
                for (segment_container_t::iterator it = segments[k].begin(); it != segments[k].end(); it++) {
                    overlap += compute_overlap(*it, segments[k1], full_line[k1], BETA);
                }
                overlap_u += (u(k1 + FLAVOR2, k - FLAVOR2) - u(k1, k)) * overlap * 0.5;
                std::cout << "um2 " << u(k1 + FLAVOR2, k - FLAVOR2) << " " << u(k1, k) << " " << overlap << std::endl;
            }
        }
    };

    std::cout << "ov2 " << overlap_u << std::endl;

    std::cout << "swap_segments_spin " << fabs(det_rat) * exp(-overlap_u + e_site) << " " << exp(e_site) << " " << exp(-overlap_u) << std::endl;

    if (random->Get_01() < fabs(det_rat) * exp(e_site - overlap_u)) {

        for (size_t i=0; i<FLAVOR2; i++){
            size_t j=i+FLAVOR2;

            swap(M_new[i], M[i]);
            swap(M_new[j], M[j]);
            swap(segments[i], segments[j]);
            int dummy1=full_line[i];
            full_line[i]=full_line[j];
            full_line[j]=dummy1;
            double dummy2=sign[i];
            sign[i]=sign[j];
            sign[j]=dummy2;
        }

        return SWAP_SUCCESS;
    }

    return SWAP_FAIL;
}

extern "C" MC_SWAP swap_segments_spin_no_u(Random *random, double BETA, std::vector< segment_container_t > & segments, std::vector<int> & full_line, hybridization_container_t& F, std::vector<double>& sign, std::vector< blas_matrix > & M, std::vector<double> & mu_e)
{
    std::vector< blas_matrix > M_new(segments.size());
    double det_rat = 1.0;
    double e_site = 0.0;
    int FLAVOR2=(segments.size()+1)/2;
    times full_segment(0,BETA);
    for (int k=0; k<FLAVOR2; k++){
        double det_old = construct_inverse(M_new[k+FLAVOR2], segments[k], BETA,  F[k]); // before swap
        double det_new = construct_inverse(M_new[k+FLAVOR2], segments[k], BETA,  F[k+FLAVOR2]);
        if (M_new[k+FLAVOR2].cols() > 0) det_rat *= det_new/det_old;
        e_site+=compute_overlap(full_segment,segments[k],full_line[k],BETA)*(mu_e[k]-mu_e[k+FLAVOR2]);
    };
    for (size_t k=FLAVOR2; k<segments.size(); k++){
        double det_old = construct_inverse(M_new[k-FLAVOR2], segments[k], BETA,  F[k]); // before swap
        double det_new = construct_inverse(M_new[k-FLAVOR2], segments[k], BETA,  F[k-FLAVOR2]);
        if (M_new[k-FLAVOR2].cols() > 0) det_rat *= det_new/det_old;
        e_site+=compute_overlap(full_segment,segments[k],full_line[k],BETA)*(mu_e[k]-mu_e[k-FLAVOR2]);
    };

    if (random->Get_01() < std::abs(det_rat) * exp(-e_site)) {

        for (int i=0; i<FLAVOR2; i++){
            int j=i+FLAVOR2;

            swap(M_new[i], M[i]);
            swap(M_new[j], M[j]);
            swap(segments[i], segments[j]);
            int dummy1=full_line[i];
            full_line[i]=full_line[j];
            full_line[j]=dummy1;
            double dummy2=sign[i];
            sign[i]=sign[j];
            sign[j]=dummy2;
        }

        return SWAP_SUCCESS;
    }

    return SWAP_FAIL;
}
