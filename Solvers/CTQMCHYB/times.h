/************************************************************************************
 *
 * ALPS DMFT Project
 *
 * Copyright (C) 2005 - 2011 by Philipp Werner <werner@itp.phys.ethz.ch>,
 *                              Emanuel Gull <gull@phys.columbia.edu>,
 *                              Hartmut Hafermann <hafermann@cpht.polytechnique.fr>
 * 2011 - 2013 minor compatibility modifications by A. A. Dyachenko <adotfive@gmail.com>
 *
 *
 * This software is part of the ALPS Applications, published under the ALPS
 * Application License; you can use, redistribute it and/or modify it under
 * the terms of the license, either version 1 or (at your option) any later
 * version.
 *
 * You should have received a copy of the ALPS Application License along with
 * the ALPS Applications; see the file LICENSE.txt. If not, the license is also
 * available from http://alps.comp-phys.org/.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
 * SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
 * FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 ************************************************************************************/
#ifndef TIMES_H
#define TIMES_H

class times
{
public:
    times()
    {
        t_start_=0; t_end_=0;
    }

    times(double t_start, double t_end)
    {
        t_start_=t_start; t_end_=t_end;
    }

    double t_start() const
    {
        return t_start_;
    } // begin of segment

    double t_end() const
    {
        return t_end_;
    } // end of segment

    void set_t_start(double t_start)
    {
        t_start_ = t_start;
    }

    void set_t_end(double t_end)
    {
        t_end_ = t_end;
    }

    void swap()
    {
        double dummy = t_end_;
        t_end_ = t_start_;
        t_start_ = dummy;
    }

private:
    double t_start_, t_end_;
};

inline bool operator<(const times& t1, const times& t2) {
    return t1.t_start() < t2.t_start();
}
inline bool operator<(const times& t1, const double t2) {
    return t1.t_start() < t2;
}

inline bool operator>(times t1, times t2) {
    return t1.t_start() > t2.t_start();
}

inline bool operator==(times t1, times t2) {
    return t1.t_start() == t2.t_start();
}

#endif // TIMES_H
