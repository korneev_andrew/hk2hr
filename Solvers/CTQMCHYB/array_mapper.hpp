#ifndef ARRAY_MAPPER_HPP
#define ARRAY_MAPPER_HPP

#include <vector>
#include <stdexcept>

template <typename T> class ArrayMapper
{
public:
    ArrayMapper(void *data, std::vector<size_t> &sizes);
    T &operator()(size_t i);
    ArrayMapper<T> &operator [](size_t i);
    ~ArrayMapper();
    void Clear();
private:
    std::vector<size_t> sizes_;
    T *data_;
    size_t stride_;
    std::vector< ArrayMapper<T> > sub_arrays_;
};

template <typename T> ArrayMapper<T>::~ArrayMapper()
{
    sub_arrays_.clear();
    data_ = NULL;
    stride_ = 0;
    sizes_.clear();
}

template <typename T> ArrayMapper<T>::ArrayMapper(void *data, std::vector<size_t> &sizes)
{
    data_ = static_cast<T*>(data);
    sizes_ = sizes;
    stride_ = 1;
    if(sizes_.size() > 1) {
        for(size_t i = 1; i < sizes_.size(); i++) {
            stride_ *= sizes_[i];
        }
        std::vector<size_t> new_sizes = sizes_;
        new_sizes.erase(new_sizes.begin());
        for(size_t i = 0; i < sizes_.at(0); i++) {
            sub_arrays_.push_back(ArrayMapper<T>(static_cast<void*>(data_ + i * stride_), new_sizes));
        }
    }
}

template <typename T> T &ArrayMapper<T>::operator ()(size_t i)
{
    return *(data_ + i * stride_);
}

template <typename T> ArrayMapper<T> &ArrayMapper<T>::operator [](size_t i)
{
    if(sizes_.size() == 1)
        throw std::runtime_error("you shouldn't call this! )=");

    return sub_arrays_.at(i);
}


template <typename T> void ArrayMapper<T>::Clear()
{
    size_t total = 1;
    for(size_t i = 0; i < sizes_.size(); i++) {
        total *= sizes_[i];
    }
    memset(data_, 0, sizeof(T) * total);
}

#endif // ARRAY_MAPPER_HPP
