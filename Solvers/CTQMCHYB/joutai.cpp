/*
* A.A. Dyachenko
*/
#include <cmath>
#include <set>
#include <stdexcept>
#include <boost/lexical_cast.hpp>
#include <iostream>
#include "joutai.h"

void mark_state(std::vector<TauState> &tau_line, double s, double e, int f)
{
    for(size_t i = 0; i < tau_line.size(); i++)
        if(tau_line[i].start >= s && tau_line[i].end <= e)
            tau_line[i].state.push_back(f);
}

Joutai::Joutai(size_t size)
{
    data_.resize(size);
    for(size_t i = 0; i < data_.size(); i++)
        data_[i] = 0;
}

int &Joutai::Element(size_t index)
{
    return data_.at(index);
}

std::string Joutai::Print()
{
    std::string str;
    str.append(" |");
    for(size_t i = 0; i < data_.size(); i++) {
        if(data_.at(i) == 1)
            str.append("1");
        else
            str.append("0");
    }
    str.append("| N=");
    str.append(boost::lexical_cast<std::string>(this->N()));
    str.append(" Sz=");
    str.append(boost::lexical_cast<std::string>(-0.5 * this->minusSz()));

    return str;
}

JoutaiGenerator::JoutaiGenerator(size_t size)
{
    size_ = size;
}

void JoutaiGenerator::Generate()
{
    data_.clear();
    //data_.reserve(pow(2, size_));
    Joutai k(size_);
    _iterate(k, 0);
}

Joutai &JoutaiGenerator::GetConfiguration(size_t i)
{
    return data_.at(i);
}

size_t JoutaiGenerator::Count()
{
    return data_.size();
}

void JoutaiGenerator::_iterate(Joutai &k, size_t index) //i am still terrible but much better now
{
    for(int val = 0; val < 2; val++) {
        k.Element(index) = val;
        if(index == size_ - 1)
            data_.push_back(k);
        if(index < size_ - 1)
            _iterate(k, index + 1);
    }
}

std::vector< std::vector<int> > JoutaiGenerator::GetData()
{
    std::vector< std::vector<int> > ret;
    ret.resize(data_.size());
    for(size_t i = 0; i < data_.size(); i++) {
        std::vector<int> current;
        current.resize(size_);
        for(size_t k = 0; k < size_; k++) {
            current[k] = data_[i].Element(k);
        }
        ret[i].resize(size_);
        ret[i] = current;
    }

    return ret;
}

std::vector<Joutai> &JoutaiGenerator::InternalData()
{
    return data_;
}

const int Joutai::compute_value() const
{
    int value = 0;
    for(size_t i = 0; i < data_.size(); i++) {
        value += data_.at(i) * std::pow(2.0, i);
    }

    return value;
}

void JoutaiGenerator::Sort()
{
    size_t old_size = data_.size();
    std::set<Joutai> sort;
    for(size_t i = 0; i < data_.size(); i++)
        sort.insert(data_.at(i));
    data_.clear();
    for(std::set<Joutai>::iterator it = sort.begin(); it != sort.end(); it++)
        data_.push_back(*it);

    if(old_size != data_.size())
        throw std::runtime_error("sorting gone bad");
}

const int Joutai::N() const
{
    int nz = 0;
    for(size_t i = 0; i < data_.size(); i++)
        nz += data_.at(i);

    return nz;
}

//compute Sz for given configuration
//assume that second half is spin down
//it returns inverted value for the purpose of sorting
//returns doubled value to ease comparison
const int Joutai::minusSz() const
{
    int sz = 0.0;
    for(size_t i = 0; i < data_.size(); i++) {
        if(i < data_.size() / 2)
            sz += -data_.at(i);
        else
            sz += data_.at(i);
    }
    return sz;
}

void JoutaiGenerator::BuildBlocks()
{
    for(size_t i = 0; i < data_.size(); i++) {
        int n = data_.at(i).N();
        int sz = data_.at(i).minusSz();
        if(blocks_.size() == 0)
            blocks_.push_back(JoutaiBlock(n, sz, i));
        else {
            if(blocks_.rbegin()->n_ == n && blocks_.rbegin()->sz_ == sz)
                blocks_.rbegin()->size_++;
            else
                blocks_.push_back(JoutaiBlock(n, sz, i));
        }
    }
    for(size_t i = 0; i < blocks_.size(); i++) {
        std::cout << "Block " << i << " at " << blocks_.at(i).i_ << " with size " << blocks_.at(i).size_ << " N=" << blocks_.at(i).n_ << " Sz=" << blocks_.at(i).sz_ * -0.5 << std::endl;
    }
}

std::vector<JoutaiBlock> &JoutaiGenerator::GetBlocks()
{
    return blocks_;
}

