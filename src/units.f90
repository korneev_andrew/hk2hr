module units

  implicit none
  public

  real(8), parameter :: pi = 3.1415926535897932384626433832795028841971693993751d0
  real(8), parameter :: twopi  = 2 * pi

  complex(8), parameter :: xi   = ( 0, 1 )
  complex(8), parameter :: xone = ( 1, 0 )
  complex(8), parameter :: zero = ( 0, 0 )

!    Physics constants
!    Rev. Mod. Phys. 77, 1 (2005)

  real(8), parameter :: Ry2eV = 13.60569225d0         !     Ry = 13.60569225 eV
  real(8), parameter :: eV2K = 11604.51918d0          !     eV = 11604.51918 K

!   Energy units
  logical, save :: in_Ry  = .false.   ! calculations will be carried out in Rydberg
  logical, save :: in_au  = .false.   !                                  in Hartree
  
  real(8), save :: energy_factor = -1
  
  contains
  
  subroutine setup_energy_units
    implicit none
    
    if( energy_factor < 0 )then
       if( in_Ry .and. .not.( in_au ) )then
         energy_factor = 1.d0
         return
       elseif( in_au .and. .not.( in_Ry )  )then
         energy_factor = 0.5d0
         return
       else
         energy_factor = Ry2eV
      end if
    end if
  
  end subroutine setup_energy_units
  
end module units
