subroutine read_dos( ierr )
!    
!     Read bare (LDA) density of states from dos_x.ini file
!
  use impurity_module
  use parameter_module
  
  implicit none
  
  integer      :: ierr         !   Error status value
  
  integer :: iunit, itype, ie, i, is, n_dos, n
  real(8) :: sum, de, si
  character(16) :: fname

  iunit = 22

  do itype = 1, n_imp_type

    n = impurity(itype)%nlm
    
    write(fname,*) itype
    fname = 'bare_dos_'//trim(adjustl(fname))//'.ini'
    
    open( iunit, file=fname, form='formatted', status='old', iostat=ierr, action='read', position='rewind' )

    if( ierr /= 0 )   return

!     Counts number of point in bare DOS
    n_dos = 0
    do
      n_dos = n_dos + 1
      read(iunit,*,iostat=ierr) sum
      if( ierr /= 0 ) exit
    end do
    n_dos = n_dos - 1
    ierr = 0
    rewind(iunit)

    impurity(itype)%ndos = n_dos
    allocate( impurity(itype)%dos(n,n_dos,nspin), impurity(itype)%edos(n_dos) )
    
!     Read bare DOS
    do ie = 1, n_dos
      read(iunit,*,iostat=ierr) impurity(itype)%edos(ie), (( impurity(itype)%dos(i,ie,is), i=1,n ), is=1,nspin )
    end do
    
    close(iunit)
    
!     Normalize DOS
    de = impurity(itype)%edos(2) - impurity(itype)%edos(1)
    do is = 1, nspin
      do i = 1, n
        sum = 0.d0
        do ie = 1, n_dos
          si = 2.d0 * de / 3.d0
          if( mod(ie,2) == 0 ) si = 2.d0 * si
          sum = sum + si*impurity(itype)%dos(i,ie,is)
        end do
        impurity(itype)%dos(i,:,is) = impurity(itype)%dos(i,:,is) / sum
      end do
    end do
    
  end do
    
end subroutine read_dos
