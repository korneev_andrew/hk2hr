subroutine make_umtrx( ierr )
!======================================================================
!
!   Make <ij|V|kl> matrix of Coulomb interaction
!
!======================================================================
  use impurity_module
  use parameter_module

  implicit none 

  integer, intent(  out) :: ierr

  integer :: iunit, n_orb
  integer :: itype, i, j, k, l, m
  real(8) :: ddum
  complex(8), allocatable :: u(:,:)
  real(8), allocatable :: umtrx_jmj(:,:,:,:)
  character(16) :: fname

  ierr  = 0
  iunit = 15

  if( so == 0 )then
    allocate( u(7,7) )
  else
    allocate( u(14,14) )
    allocate( umtrx_jmj(14,14,14,14) )
  endif

  do itype = 1, n_imp_type
    n_orb = impurity(itype)%nlm
    if( so == 1 )  n_orb = n_orb / 2

    impurity(itype)%umtrx = 0.d0
!
!    Read  <ij|V|kl> matrix from umtrx.ini file
!
    write(fname,*) itype
    fname = 'umtrx_'//trim(adjustl(fname))//'.ini'

    open( iunit, file=fname, form='formatted', status='old', iostat=ierr, action='read' )

    if( ierr == 0 )then
!    Read full Coulomb interaction matrix from umtrx_?.ini file
      do m = 1, n_orb**4
        read(iunit,*,iostat=ierr) i, j, k, l, ddum
        if( ierr /= 0 ) exit
        impurity(itype)%umtrx(i,j,k,l) = ddum
      end do
      ierr = 0

!    Extract density-density part of U matrix
      call rotinv_u2udd( impurity(itype)%umtrx, impurity(itype)%ummss, n_orb )

!    Value of screened Coulomb interaction and Hund exchange
      impurity(itype)%u_value = sum( impurity(itype)%ummss(n_orb+1:2*n_orb,1:n_orb) ) / n_orb / n_orb
      if( n_orb /= 1 )then
        impurity(itype)%j_value = sum( impurity(itype)%ummss(1:n_orb,1:n_orb) ) / n_orb / (n_orb-1)
        impurity(itype)%j_value = impurity(itype)%u_value - impurity(itype)%j_value
      else
        impurity(itype)%j_value = 0.d0
      end if

!    Define Slater integrals
      call UandJ2F( impurity(itype)%u_value, impurity(itype)%j_value, impurity(itype)%F(0:n_orb-1), n_orb )

      cycle
    end if
!
!     Define <ij|V|kl> matrix via density-density one from impurity_X.ini file
!
    if( sum( impurity(itype)%ummss ) > 1.d-4 )then
!    Value of screened Coulomb interaction and Hund exchange
      impurity(itype)%u_value = sum( impurity(itype)%ummss(n_orb+1:2*n_orb,1:n_orb) ) / n_orb / n_orb
      if( n_orb /= 1 )then
        impurity(itype)%j_value = sum( impurity(itype)%ummss(1:n_orb,1:n_orb) ) / n_orb / (n_orb-1)
        impurity(itype)%j_value = impurity(itype)%u_value - impurity(itype)%j_value
      else
        impurity(itype)%j_value = 0.d0
      end if

!    Define Slater integrals
      call UandJ2F( impurity(itype)%u_value, impurity(itype)%j_value, impurity(itype)%F(0:n_orb-1), n_orb )

!    Calculate full matrix using eq. (10) from JPCM 9, 767 (1997)
      if( mod(n_orb-1,2) == 0 )then
        l = ( n_orb - 1 ) / 2
        call umatrix( impurity(itype)%umtrx, l, impurity(itype)%F )
      else
        do i = 1, n_orb
         do j = 1, n_orb
           impurity(itype)%umtrx(i,j,j,i) = impurity(itype)%ummss(i,j+n_orb) - impurity(itype)%ummss(i,j)
           impurity(itype)%umtrx(i,j,i,j) = impurity(itype)%ummss(i,j+n_orb)
         end do
        end do
      end if

      ierr = 0
      cycle
    end if
!
!     Define <ij|V|kl> matrix via Slater integrals
!
    if( sum( impurity(itype)%F ) > 1.d-4 )then
!    Calculate full matrix using eq. (10) from JPCM 9, 767 (1997)
      if( mod(n_orb,2) == 1 )then
        l = ( n_orb - 1 ) / 2
        call umatrix( impurity(itype)%umtrx, l, impurity(itype)%F )
      else
        ierr = 777
        return
      end if

!    Define U and J via Slater integrals
      impurity(itype)%u_value = impurity(itype)%F(0)
      select case (n_orb)
       case (3)
         impurity(itype)%j_value = impurity(itype)%F(2) / 5
       case (5)
         impurity(itype)%j_value = ( impurity(itype)%F(2) + impurity(itype)%F(4) ) / 14
       case (7)
         impurity(itype)%j_value = ( 286*impurity(itype)%F(2)  +              &
                                     195*impurity(itype)%F(4)  +              &
                                     250*impurity(itype)%F(6) )   / 6435
       case default
         impurity(itype)%j_value = 0.d0
      end select

      if( so == 0 )then
!    Transformation matrix to real(cubic) harmonics
        call transformation_matrix( u, n_orb, rhtm )

!    Rotate <ij|V|kl> matrix to real harmonics
        call rotate_u_matrix( impurity(itype)%umtrx, u(1:n_orb,1:n_orb), n_orb )

!    Rotate <ij|V|kl> matrix to local coordinate system
        if( sum( abs(impurity(itype)%lcs) ) > 1.d-4 )then
          u(1:n_orb,1:n_orb) = cmplx( impurity(itype)%lcs, 0.d0, 8 )
          call rotate_u_matrix( impurity(itype)%umtrx, u(1:n_orb,1:n_orb), n_orb )
        end if
      endif !so ==0

!    Extract density-density part of U matrix
      call rotinv_u2udd( impurity(itype)%umtrx, impurity(itype)%ummss, n_orb )

      ierr = 0
      cycle
    end if
!
!     Define <ij|V|kl> matrix via screened Coulomb and exchange parameters
!
    if( impurity(itype)%u_value > 0 )then
!    Use Kanamori parametrization (U, U-2J, U-3J)
      if( kanamori .or. mod(n_orb,2) == 0 )then
        do i = 1, n_orb
          do j = 1, n_orb
            impurity(itype)%umtrx(i,j,i,j) = impurity(itype)%u_value - 2*impurity(itype)%j_value
            impurity(itype)%umtrx(i,j,j,i) = impurity(itype)%j_value
          end do
          impurity(itype)%umtrx(i,i,i,i) = impurity(itype)%u_value
        end do

!    Extract density-density part of U matrix
        call rotinv_u2udd( impurity(itype)%umtrx, impurity(itype)%ummss, n_orb )
      else
!    Define Slater integrals
        call UandJ2F( impurity(itype)%u_value, impurity(itype)%j_value, impurity(itype)%F(0:n_orb-1), n_orb )

!    Calculate full matrix using eq. (10) from JPCM 9, 767 (1997)
        l = ( n_orb - 1 ) / 2
        call umatrix( impurity(itype)%umtrx, l, impurity(itype)%F )
    
        if( so == 0 )then 
!    Transformation matrix to real(cubic) harmonics
          call transformation_matrix( u, n_orb, rhtm )

!    Rotate <ij|V|kl> matrix to real harmonics
          call rotate_u_matrix( impurity(itype)%umtrx, u(1:n_orb,1:n_orb), n_orb )

!    Rotate <ij|V|kl> matrix to local coordinate system
          if( sum( abs(impurity(itype)%lcs) ) > 1.d-4 )then
            u(1:n_orb,1:n_orb) = cmplx( impurity(itype)%lcs, 0.d0, 8 )
            call rotate_u_matrix( impurity(itype)%umtrx, u(1:n_orb,1:n_orb), n_orb )
          end if        
          call rotinv_u2udd( impurity(itype)%umtrx, impurity(itype)%ummss, n_orb )
       else   ! so == 0
!  Transform rot_inf U matrix into jmj basis
         write(14,*)'Note, U(i,j,k,l) is transformed into jmj-basis'
         write(14,*)
         write(14,*)'U4IND:  u4ind(i1,i2,i3,i4)'
         write(14,*)
     
         do i = 1,n_orb
          do k = 1,n_orb
            write(14,*)'i1, i2 : ',i,k
            do l = 1,n_orb
              write(14,"(30f8.4)") ( impurity(itype)%umtrx(i,k,l,j), j = 1,n_orb )
            end do
          end do
        end do

         call rotinv_u2rotinv_u_jmj(impurity(itype)%umtrx,umtrx_jmj,n_orb)

  !   Rotate <ij|V|kl> matrix to local coordinate system      
          if( sum( abs(impurity(itype)%lcs) ) > 1.d-4 )then
            u(1:2*n_orb,1:2*n_orb) = cmplx( impurity(itype)%lcs, 0.d0, 8 )
            call rotate_u_matrix( umtrx_jmj, u(1:2*n_orb,1:2*n_orb), 2*n_orb )
!    Extract density-density part of U matrix
!        
            do i = 1, 2*n_orb
              do j = 1, 2*n_orb
                impurity(itype)%ummss(i,j) = umtrx_jmj(i,j,i,j) - umtrx_jmj(i,j,j,i)
              end do
            end do
          end if
       endif ! so == 0
       call rotinv_u2udd( impurity(itype)%umtrx, impurity(itype)%ummss, n_orb )
      end if !kanamori

      ierr = 0
      cycle
    end if  !U_value

    close(iunit) 

    if( ierr /= 0 )  return

  end do     !     n_imp_type

end subroutine make_umtrx


subroutine rotate_u_matrix( umtrx, a_rot, n )
!   Rotate <ij|V|kl> matrix to new basis
  implicit none
  integer,    intent(in   ) :: n
  complex(8), intent(in   ) :: a_rot(n,n)
  real(8),    intent(inout) :: umtrx(n,n,n,n)

  integer :: i, j, k, l, m1, m2, m3, m4
  complex(8) :: uu(7,7,7,7)

  uu = cmplx(0.d0,0.d0,8)

  do i = 1, n
   do j = 1, n
    do k = 1, n
     do l = 1, n

       do m1 = 1, n
        do m2 = 1, n
         do m3 = 1, n
          do m4 = 1, n
            uu(i,j,k,l) = uu(i,j,k,l) +                                       &
                          conjg( a_rot(i,m1) ) * conjg( a_rot(j,m2) )  *      &
                   umtrx(m1,m2,m3,m4)  *  a_rot(k,m3) * a_rot(l,m4)
          end do
         end do
        end do
       end do

     end do
    end do
   end do
  end do

  umtrx = real( uu(1:n,1:n,1:n,1:n), 8 )

end subroutine rotate_u_matrix

subroutine UandJ2F( U, J, F, n )
!   Extract Slater integrals F0, F2, etc. out of U and J parameters
  implicit none
  integer, intent(in   ) :: n
  real(8), intent(in   ) :: U, J
  real(8), intent(  out) :: F(0:n-1)

  F = 0
  F(0) = U
!  F(2) = 5 * J

  select case (n)
   case (3)
    F(2) = 5 * J 
   case (5) 
    F(2) = 14 * J / 1.63
    F(4) = 0.63 * F(2)
   case (7)
    F(2) = 6435 * J / ( 286 + 195 * 451 / 675 + 250 * 1001 / 2025 )
    F(4) = 451  * F(2) / 675
    F(6) = 1001 * F(2) / 2025
  end select

end subroutine UandJ2F

subroutine rotinv_u2udd( ufull, udd, n )
!   Extract density-density part of full rotationally 
!   invariant <ij|V|kl> matrix
  implicit none
  integer, intent(in   ) :: n
  real(8), intent(in   ) :: ufull(n,n,n,n)
  real(8), intent(  out) :: udd(2*n,2*n)
  
  integer :: i, j

  do i = 1, n
    do j = 1, n
     udd(i,  j+n) = ufull(i,j,i,j)
     udd(i+n,j)   = ufull(i,j,i,j)
     udd(i,j)     = ufull(i,j,i,j) - ufull(i,j,j,i)
     udd(i+n,j+n) = ufull(i,j,i,j) - ufull(i,j,j,i)
   end do
  end do

end subroutine rotinv_u2udd

subroutine transformation_matrix( u, n, packs )
!
!   Tra
  implicit none
  
  integer,       intent(in   ) :: n
  character(16), intent(in   ) :: packs
  complex(8), intent(  out) :: u(7,7)
  
  integer    :: i
  real(8) :: sqrt2
  complex(8), parameter :: xi = (0.d0,1.d0)
  
  sqrt2 = 1 / sqrt(2.d0)
  
  u = cmplx(0.d0,0.d0,8)

!   Define basis transformation

  if( packs == 'TBLMTO' )then
    select case (n)
      case(1)
        u(1,1) = 1
      case(3)      !  y  z  x
        u(1,1) = sqrt2*xi
        u(1,3) = sqrt2*xi
        u(2,2) = 1
        u(3,1) = sqrt2
        u(3,3) =-sqrt2
      case(5)      !  xy  yz  z2  xz  x2-y2
        u(1,1) = sqrt2*xi
        u(1,5) =-sqrt2*xi
        u(2,2) = sqrt2*xi
        u(2,4) = sqrt2*xi
        u(3,3) = 1
        u(4,2) = sqrt2
        u(4,4) =-sqrt2
        u(5,1) = sqrt2
        u(5,5) = sqrt2
      case(7)
        u(1,1) = sqrt2*xi
        u(1,7) = sqrt2*xi
        u(2,2) = sqrt2*xi
        u(2,6) =-sqrt2*xi
        u(3,3) = sqrt2*xi
        u(3,5) = sqrt2*xi
        u(4,4) = 1
        u(5,3) = sqrt2
        u(5,5) =-sqrt2
        u(6,2) = sqrt2
        u(6,6) = sqrt2
        u(7,1) = sqrt2
        u(7,7) =-sqrt2
    end select
  elseif( packs == 'ESPRESSO' )then
    select case (n)
      case(1)
        u(1,1) = 1
      case(3)
        u(1,2) = 1
        u(2,1) = sqrt2
        u(2,3) =-sqrt2
        u(3,1) = sqrt2*xi
        u(3,3) = sqrt2*xi
      case(5)      !  z2  xz  yz  x2-y2  xy
        u(1,3) = 1
        u(2,2) = sqrt2
        u(2,4) =-sqrt2
        u(3,2) = sqrt2*xi
        u(3,4) = sqrt2*xi
        u(4,1) = sqrt2
        u(4,5) = sqrt2
        u(5,1) = sqrt2*xi
        u(5,5) =-sqrt2*xi
    end select
  else
    forall( i = 1:7 ) u(i,i) = cmplx(1.d0,0.d0,8)
  end if
  end subroutine transformation_matrix
  
  subroutine rotinv_u2rotinv_u_jmj(ufull,ujmj4,n)
  !   transform full rotationally invariant <ij|V|kl> matrix
  !   into jmj basis
  implicit none
  integer, intent(in   ) :: n
  real(8), intent(in   ) :: ufull(n,n,n,n)
  real(8), intent(  out) :: ujmj4(2*n,2*n,2*n,2*n)
  
  integer ::  is1, is2, j1, j2, j3, j4, m1, m2, m3, m4
  real(8) :: csm7,csm5,mu,nu,tmp1,kappa,lambda
  
  ujmj4(:,:,:,:) = 0.d0

! 7/2 by 7/2 block      
    do j1 = 1, 8
        mu = -7.d0/2.d0 + j1 - 1.d0      
        do j2 = 1, 8
        nu = -7.d0/2.d0 + j2 - 1.d0
        do j3 = 1, 8
            kappa = -7.d0/2.d0 + j3 - 1.d0      
            do j4 = 1, 8
            lambda = -7.d0/2.d0 + j4 - 1.d0
            tmp1 = 0.d0
            do is1 = -1,1,2
                do is2 = -1,1,2
                    m1 = 4 + int(mu - is1/2.d0)
                    m2 = 4 + int(nu - is2/2.d0)
                    m3 = 4 + int(kappa - is1/2.d0)
                    m4=  4 + int(lambda - is2/2.d0)             
                    if(m1.ne.0.and.m1.ne.8.and.m2.ne.0.and.m2.ne.8.and.m3.ne.0.and.m3.ne.8.and.m4.ne.0.and.m4.ne.8) then
                        tmp1 = tmp1 + csm7(mu,is1)*csm7(nu,is2)*csm7(kappa,is1)*csm7(lambda,is2)*ufull(m1,m2,m3,m4)
                    endif  
                enddo
            enddo
            ujmj4(j1,j2,j3,j4) = tmp1
            enddo
        enddo
        enddo
    enddo




! 5/2 by 5/2 block           
    do j1 = 9, 14
        mu = -5.d0/2.d0 + j1 - 9.d0      
        do j2 = 9, 14
        nu = -5.d0/2.d0 + j2 - 9.d0
        do j3 = 9, 14
            kappa = -5.d0/2.d0 + j3 - 9.d0      
            do j4 = 9, 14
            lambda = -5.d0/2.d0 + j4 - 9.d0   
            tmp1 = 0.d0
            do is1 = -1,1,2
                do is2 = -1,1,2
                    m1 = 4 + int(mu - is1/2.d0)
                    m2 = 4 + int(nu - is2/2.d0)
                    m3 = 4 + int(kappa - is1/2.d0)
                    m4 = 4 + int(lambda - is2/2.d0) 
                    tmp1 = tmp1 + csm5(mu,is1)*csm5(nu,is2)*csm5(kappa,is1)*csm5(lambda,is2)*ufull(m1,m2,m3,m4)
                enddo
            enddo
            ujmj4(j1,j2,j3,j4) = tmp1 
            enddo
        enddo
        enddo
    enddo
    

            
! 7/2 by 5/2 block           
    do j1 = 1, 8
        mu = -7.d0/2.d0 + j1 - 1.d0      
        do j2 = 9, 14
        nu = -5.d0/2.d0 + j2 - 9.d0
        do j3 = 1, 8
            kappa = -7.d0/2.d0 + j3 - 1.d0      
            do j4 = 9, 14
            lambda = -5.d0/2.d0 + j4 - 9.d0
            tmp1 = 0.d0
            do is1 = -1,1,2
                do is2 = -1,1,2
                    m1 = 4 + int(mu - is1/2.d0)
                    m2 = 4 + int(nu - is2/2.d0)
                    m3 = 4 + int(kappa - is1/2.d0)
                    m4 = 4 + int(lambda - is2/2.d0) 
                    if(m1.ne.0.and.m1.ne.8.and.m3.ne.0.and.m3.ne.8.) then
                        tmp1 = tmp1 + csm7(mu,is1)*csm5(nu,is2)*csm7(kappa,is1)*csm5(lambda,is2)*ufull(m1,m2,m3,m4)
                    endif
                enddo
            enddo
            ujmj4(j1,j2,j3,j4) = tmp1
            ujmj4(j2,j1,j4,j3) = tmp1
            enddo
        enddo
        enddo
    enddo
    
    do j1 = 1, 8
        mu = -7.d0/2.d0 + j1 - 1.d0      
        do j2 = 9, 14
        nu = -5.d0/2.d0 + j2 - 9.d0
        do j3 = 9, 14
            lambda = -5.d0/2.d0 + j3 - 9.d0          
            do j4 = 1, 8
            kappa = -7.d0/2.d0 + j4 - 1.d0
            tmp1 = 0.d0
            do is1 = -1,1,2
                do is2 = -1,1,2
                m1 = 4 + int(mu - is1/2.d0)
                m2 = 4 + int(nu - is2/2.d0)
                m3 = 4 + int(lambda - is1/2.d0)
                m4 = 4 + int(kappa  - is2/2.d0) 
                if(m1.ne.0.and.m1.ne.8.and.m4.ne.0.and.m4.ne.8.) then
                    tmp1 = tmp1 + csm7(mu,is1)*csm5(nu,is2)*csm5(lambda,is1)*csm7(kappa,is2)*ufull(m1,m2,m3,m4)
                endif
                enddo
            enddo
            ujmj4(j1,j2,j3,j4) = tmp1
            ujmj4(j2,j1,j4,j3) = tmp1
            enddo
        enddo
        enddo
    enddo

    end subroutine rotinv_u2rotinv_u_jmj
  
  real(8) function  csm7(mj,s)
  implicit none
  
  integer,    intent  (in    )  :: s
  real(8), intent (in     ) :: mj
  csm7 = sqrt((7.d0/2.d0 + mj*s)/7.d0)
  end function csm7
       
  real(8) function csm5(mj,s)
  implicit none
  integer,     intent  (in    )   :: s
  real(8), intent  (in     )  :: mj
  csm5 = -1.d0*s*sqrt((7.d0/2.d0 - mj*s)/7.d0)
  end  function csm5
  

