subroutine find_fermi_level
!
!   This subroutine calculates total Green function and
!   adjusts Fermi level to desired number of electrons.
!
  use parameter_module
  use impurity_module
  use hamiltonian_module
  use energy_module
  use iolib_module
  use lapack_module
  use qlmp_module
  use moments_module
  use gf_atm_module
  use mpi_module
  
#ifdef MPI
  use mpi
#endif
 
  implicit none
  
  integer :: is, iom, itype, i, j, n1, n2, ikp, it, isp, nom_proc, iom_glob, ierr
  
  real(8) :: x1, x1_loc, x2, x2_loc, f1, f2, a, b, de, dee, si
  real(8) :: qtotal(3), moms(0:3,2)
  
  complex(8) :: z
  
  complex(8), allocatable :: Vpot(:,:)
  complex(8), allocatable :: hkz(:,:)
  complex(8), allocatable :: ekz(:,:,:,:)
  complex(8), allocatable :: green_total_local(:,:)
  complex(8), allocatable :: green_total(:,:)
  
  logical :: lmom(0:3) = (/ .false., .false., .true., .true. /)
  
  x1 = -1000
  x2 =  1000
  x1_loc = -1000
  x2_loc =  1000
  f1 = 0
  f2 = 2*ndim
  de = ( emax - emin ) / ( n_energy - 1 )

  allocate( green_total_local(n_energy,nspin), green_total(n_energy,nspin), stat=i )

  nom_proc = n_energy / numprocs
  if( proc_id < mod(n_energy,numprocs) )  nom_proc = nom_proc + 1


  if( use_hmlt )then
    allocate( Vpot(ndim,ndim) )
    allocate( hkz(ndim,ndim) )
    allocate( ekz(ndim,nom_proc,nkp,nspin) )
!  
!   Find eigenvalues of H(k)+Sigma(iw)
!
    ekz = zero
    do is = 1, nspin
      do iom = 1, nom_proc
        iom_glob = proc_id+1+(iom-1)*numprocs
       
!    Make "local potential": Sigma, Double counting, etc
        Vpot = zero
        do itype = 1, n_imp_type
          do i = 1, impurity(itype)%n_imp
            n1 = impurity(itype)%hposition(i)
            n2 = n1 + impurity(itype)%nlm - 1
          
            isp = is
            if( impurity(itype)%magtype(i) == -1 ) isp = 3 - is
            Vpot(n1:n2,n1:n2) = impurity(itype)%Sigma(:,:,iom_glob,isp)

            forall( j = n1:n2 ) Vpot(j,j) = Vpot(j,j) + ( 1.5 - is )*impurity(itype)%h_field_local*impurity(itype)%magtype(i)
          end do
        end do

        do ikp = 1, nkp
          hkz = h(:,:,ikp,is) + Vpot
          call eigenvalue( hkz, ekz(:,iom,ikp,is) )
        end do ! ikp
      
      end do   ! iom
    end do     ! is
    
    if( nspin == 2 .and. abs(h_field) > 1.e-4 )then
      ekz(:,:,:,1) = ekz(:,:,:,1) + 0.5*h_field
      ekz(:,:,:,2) = ekz(:,:,:,2) - 0.5*h_field 
    end if
!   Initial energy window 
    x1_loc = minval( real(ekz) ) - 10
    x2_loc = maxval( real(ekz) ) + 10
  end if
!
!    Cycle to adjust Fermi level for required number of electrons
!
#ifdef MPI
  call mpi_allreduce( x1_loc, x1, 1, mpi_double_precision, mpi_min, mpi_comm_world, ierr )
  call mpi_allreduce( x2_loc, x2, 1, mpi_double_precision, mpi_max, mpi_comm_world, ierr )
#else
  x1 = x1_loc
  x2 = x2_loc
#endif

  it = 0
  do 
!    Reset Fermi level 
    call complex_energy_mesh

!    Calculate total Green function
    green_total_local = zero
    green_total = zero
    do is = 1, nspin
      do iom = 1, nom_proc
        iom_glob = proc_id+1+(iom-1)*numprocs
        z = zenergy(iom_glob)
      
!    Use hamiltonian
        if( use_hmlt )then      
          if( use_atm .and. fermionic(iom_glob) < atm_cutoff )then
            call green_atm( z, ekz(:,iom,:,is), itt, green_total_local(iom_glob,is) )
          else
            do ikp = 1, nkp
             green_total_local(iom_glob,is) = green_total_local(iom_glob,is) +           &
                                sum( wtkp(ikp) / ( z - ekz(:,iom,ikp,is) ) )
            end do
          end if
        else
!    Use DOS 
          if( nspin == 2 .and. abs(h_field) > 1.e-4 ) z = z - ( 1.5 - is )*h_field

          do itype = 1, n_imp_type
            do i = 1, impurity(itype)%nlm
              dee = impurity(itype)%edos(2) - impurity(itype)%edos(1)
              do it = 1, impurity(itype)%ndos
                si = 2 * dee / 3
                if( mod(it,2) == 0 ) si = 2 * si
                
                green_total_local(iom_glob,is) = green_total_local(iom_glob,is) +         &
                                                    si*impurity(itype)%dos(i,it,is)   /   &
                       ( z - impurity(itype)%edos(it) - impurity(itype)%Sigma(i,i,iom_glob,is) )
              end do
            end do  ! i (orbitals)
          end do    !  itype
        end if
        
      end do   ! iom
    end do     ! nspin
    
#ifdef MPI
    call mpi_barrier( mpi_comm_world, ierr )
    call mpi_allreduce( green_total_local, green_total, n_energy*nspin, mpi_double_complex, mpi_sum, mpi_comm_world, ierr )
#else
    green_total = green_total_local
#endif

!    Calculate total number of particles on imaginary or real axis

    do is = 1, nspin
      if( imenergy )then
!    Find moments of Green function from high-energy expansion
        moms(0,is) = 0
        moms(1,is) = ndim
        call def_moments_w( green_total(n_energy-ntail:n_energy,is), fermionic(n_energy-ntail:n_energy), moms(:,is), lmom )

!    Calculate number of electrons
        qtotal(is) = qlmp( green_total(:,is), fermionic, moms(1:3,is) )
       else
        qtotal(is) = 0
        do iom = 1, n_energy
          qtotal(is) = qtotal(is) - aimag(green_total(iom,is))*de / pi
          if( ener(iom) >= xmu ) exit
        end do
      end if
    end do  !   nspin

    if( so == 0 )then
      if( nspin == 1 )  qtotal(2) = qtotal(1)
      qtotal(3) = qtotal(1) + qtotal(2)
    elseif ( so == 1 )then
      qtotal(3) = qtotal(1)
    endif

    if( verbos >= 20 ) write(14,*) ' mu ', xmu, ' qt ', qtotal(3), ' x1, x2 ', x1, x2, ' f1, f2 ', f1, f2

!    Fixed Fermi level    
    if( ntotal < 0 ) exit

    if( it == 100 )  exit

    if( abs(x2-x1) <= fentoler ) exit

    if( abs( ntotal - qtotal(3) ) <= fpatoler ) exit

!    Find new guess for the Fermi level    
    if( qtotal(3) > ntotal )then
      x2 = xmu
      f2 = qtotal(3)
    else
      x1 = xmu
      f1 = qtotal(3)
    end if
    
    a = ( f2 - f1 ) / ( x2 - x1 )
    b = f1 - a*x1
    
    xmu = ( ntotal - b ) / a
        
    it = it + 1
  end do
  
  if( so == 0 .and.  nspin == 1 ) moms(:,2) = moms(:,1)
!
!    Print out
!
  if( verbos > 0 )then
    if( so == 0 )then
      if( verbos >= 15 .and. imenergy )                                                          &
        write(14,"(' Moments of the total Green function for spin 1 :',/,4(1x,f10.6),/,          &
                  &' Moments of the total Green function for spin 2 :',/,4(1x,f10.6))") moms

      write(14,"(' Number of particles for spin 1 : ',f10.6,/,                                   &
                &' Number of particles for spin 2 : ',f10.6,/,                                   &
                &' Total magnetization              ',f10.6,/,                                   &
                &' Total number of particles        ',f10.6)") qtotal(1), qtotal(2),             &
                                                     qtotal(2)-qtotal(1), qtotal(3)
    elseif( so == 1 )then
      if( verbos >= 15 .and. imenergy )                                                          &
        write(14,"(' Moments of the total Green function  :',/,4(1x,f12.6))") moms(:,1)

      write(14,"(' Total number of particles        ',f10.6)") qtotal(3)
    end if

    write(14,"(' Fermi level  ',20x,f10.6)") xmu

    call write_function( 'Green_total', green_total, prn_mesh, iom, 'r' )
  end if

  if( use_hmlt ) deallocate( Vpot, hkz, ekz )
  deallocate( green_total_local, green_total )

end subroutine find_fermi_level
