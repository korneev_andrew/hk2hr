subroutine read_impurity( ierr )
!======================================================================
!
!   Read data from impurity_?.ini file.
!   Allocate and initialize impurity quatities.
!
!======================================================================
  use impurity_module
  use parameter_module
  use energy_module, only : n_energy, Ltau

  implicit none 

  interface
    subroutine free_format_reader( iunit, ios, vname, n, ivalue, rvalue, zvalue, lvalue, cvalue )
      implicit none 
      integer,      intent(in   ) :: iunit
      character(*), intent(in   ) :: vname
      integer,      intent(  out) :: ios
      integer,      intent(in   ) :: n
      integer,      intent(inout), optional :: ivalue(*)
      real(8),      intent(inout), optional :: rvalue(*)
      complex(8),   intent(inout), optional :: zvalue(*)
      logical,      intent(inout), optional :: lvalue(*)
      character(*), intent(inout), optional :: cvalue(*)
    end subroutine
  end interface

  integer, intent(  out) :: ierr

  complex(8), parameter :: zero = cmplx(0,0,8)

  integer :: iunit, itype, i, j, k, n
  character(16) :: fname

  integer :: idummy(10)
  real(8) :: rdummy(10)
  logical :: ldummy(10), qmc_solver = .false.

  ierr  = 0
  iunit = 10

  allocate( impurity(n_imp_type) )

  do itype = 1, n_imp_type
  
    write(fname,*) itype
    fname = 'impurity_'//trim(adjustl(fname))//'.ini'
    
    open( iunit, file=fname, form='formatted', status='old', iostat=ierr, action='read' )
    if( ierr /= 0 ) return

!  General (main) parameters for each type of impurity
!
!     Impurity label
    impurity(itype)%name = 'imp1'
    call free_format_reader( iunit, ierr, 'name', 1, cvalue=impurity(itype)%name )

!     Number of orbitals on impurity
    impurity(itype)%nlm = 1
    call free_format_reader( iunit, ierr, 'nlm', 1, ivalue=idummy(1) )
    if( ierr == 0 ) impurity(itype)%nlm = idummy(1)
    n = impurity(itype)%nlm

!     Number of impurities of this type
    impurity(itype)%n_imp = 1
    call free_format_reader( iunit, ierr, 'n_imp', 1, ivalue=idummy(1) )
    if( ierr == 0 ) impurity(itype)%n_imp = idummy(1)
    
!     Positions of impurities in Hamiltonian
    allocate( impurity(itype)%hposition(impurity(itype)%n_imp) )
    forall( i = 1:impurity(itype)%n_imp ) impurity(itype)%hposition(i) = n * ( i - 1 ) + 1
    call free_format_reader( iunit, ierr, 'himppos', impurity(itype)%n_imp, ivalue=impurity(itype)%hposition )

!     Magnetic type
    allocate( impurity(itype)%magtype(impurity(itype)%n_imp) )
    impurity(itype)%magtype = 1
    call free_format_reader( iunit, ierr, 'magtype', impurity(itype)%n_imp, ivalue=impurity(itype)%magtype )
    if( nspin == 1 ) impurity(itype)%magtype = 1

!     Local magnetic field oriented acoording to magtype
    impurity(itype)%h_field_local = 0
    call free_format_reader( iunit, ierr, 'h_field_local', 1, rvalue=rdummy(1) )
    if( ierr == 0 ) impurity(itype)%h_field_local = rdummy(1)

!     Orbital symmetry mask
    allocate( impurity(itype)%smask(n,n) )
    k = 0
    do i = 1, n
      do j = 1, n
        k = k + 1
        impurity(itype)%smask(i,j) = k
      end do
    end do

    call free_format_reader( iunit, ierr, 'smask', n*n, ivalue=impurity(itype)%smask )

!     Constant potential correction to the impurity level
    allocate( impurity(itype)%cpoc(n,n,nspin) )
    impurity(itype)%cpoc = 0
    call free_format_reader( iunit, ierr, 'cpoc', nspin*n*n, rvalue=impurity(itype)%cpoc )
    
!     Fix constant potential correction
    impurity(itype)%cpoc_only = .false.
    call free_format_reader( iunit, ierr, 'cpoc_only', 1, lvalue=ldummy(1) )
    if( ierr == 0 ) impurity(itype)%cpoc_only = ldummy(1)
     
!     One-site potential for CPA
    call free_format_reader( iunit, ierr, 'Vcpa', nspin*n*n, rvalue=impurity(itype)%cpoc )

!     Screened Coulomb interaction
    impurity(itype)%u_value = 0
    call free_format_reader( iunit, ierr, 'U', 1, rvalue=rdummy(1) )
    if( ierr == 0 ) impurity(itype)%u_value = rdummy(1)
    
!     Hund exchange
    impurity(itype)%j_value = 0
    call free_format_reader( iunit, ierr, 'J', 1, rvalue=rdummy(1) )
    if( ierr == 0 ) impurity(itype)%j_value = rdummy(1)

!     Slater integrals
    impurity(itype)%F = 0
    if( mod(n,2) == 1 )  call free_format_reader( iunit, ierr, 'F', (n+1)/2, rvalue=impurity(itype)%F(0:n-1:2) )

!     Type of double counting correction to use
    impurity(itype)%dc_type = 'FLL'
    call free_format_reader( iunit, ierr, 'dc_type', 1, cvalue=impurity(itype)%dc_type )

!     Factor for simplified FLL
    impurity(itype)%xsfll = 1.0
    call free_format_reader( iunit, ierr, 'x_sfll', 1, rvalue=rdummy(1) )
    if( ierr == 0 ) impurity(itype)%xsfll = rdummy(1)
    
!     Density-density part of U-matrix
    if ( so ==0 ) then
      allocate( impurity(itype)%ummss(2*n,2*n) )
      impurity(itype)%ummss = 0
      call free_format_reader( iunit, ierr, 'ummss', 4*n*n, rvalue=impurity(itype)%ummss )
    elseif (so == 1) then
      allocate( impurity(itype)%ummss(n,n) )
      impurity(itype)%ummss = 0
      call free_format_reader( iunit, ierr, 'ummss', n*n, rvalue=impurity(itype)%ummss )
    endif
!     Transformation matrix to local coordinate system
    allocate( impurity(itype)%lcs(n,n) )
    impurity(itype)%lcs = 0
    call free_format_reader( iunit, ierr, 'lcs', n*n, rvalue=impurity(itype)%lcs )

!     Solver name to use
    if( cpa )then
      impurity(itype)%solver = 'CPA'
    else
      impurity(itype)%solver = 'NONE'
    end if
    call free_format_reader( iunit, ierr, 'solver', 1, cvalue=impurity(itype)%solver )
    
!     Concentration of this atomic sort
    impurity(itype)%x_cpa = 1.0
    call free_format_reader( iunit, ierr, 'x_cpa', 1, rvalue=rdummy(1) )
    if( ierr == 0 ) impurity(itype)%x_cpa = rdummy(1)
    
    if( index( impurity(itype)%solver, 'QMC' ) /= 0 ) qmc_solver = .true.
    if( index( impurity(itype)%solver, 'HYB' ) /= 0 ) qmc_solver = .true.
    
!     Fix impurity level for G0 or not
    impurity(itype)%fix_ed = .false.
    call free_format_reader( iunit, ierr, 'fix_ed', 1, lvalue=ldummy(1) )
    if( ierr == 0 ) impurity(itype)%fix_ed = ldummy(1)
     
!     Fit type
    impurity(itype)%fit = -1
    call free_format_reader( iunit, ierr, 'fit', 1, ivalue=idummy(1) )
    if( ierr == 0 ) impurity(itype)%fit = idummy(1)

!-----------------------------------------------------------------------------------------
!     Allocation of common for all type of solver arrays    
    allocate( impurity(itype)%Sigma(n,n,n_energy,nspin) )
    impurity(itype)%Sigma = zero
    
    allocate( impurity(itype)%Sigma_old(n,n,n_energy,nspin) )
    impurity(itype)%Sigma_old = zero
    
    allocate( impurity(itype)%Delta(n,n,n_energy,nspin) )
    impurity(itype)%Delta = zero
    
    allocate( impurity(itype)%Green(n,n,n_energy,nspin) )
    impurity(itype)%Green = zero
  
    allocate( impurity(itype)%Green0(n,n,n_energy,nspin) )
    impurity(itype)%Green0 = zero
  
    allocate( impurity(itype)%moments_g0(0:3,n,n,nspin) )
    impurity(itype)%moments_g0 = 0
  
    allocate( impurity(itype)%moments_g(0:3,n,n,nspin) )
    impurity(itype)%moments_g = 0
  
    allocate( impurity(itype)%moments_sigma(0:3,n,n,nspin) )
    impurity(itype)%moments_sigma = 0

    allocate( impurity(itype)%occupation(n,n,nspin) )
    impurity(itype)%occupation = 0

    allocate( impurity(itype)%n_cor(2*n) )
    impurity(itype)%n_cor = 0

    allocate( impurity(itype)%ninj(2*n,2*n) )
    impurity(itype)%ninj = 0

    allocate( impurity(itype)%umtrx(n,n,n,n) )
    impurity(itype)%umtrx = 0

    allocate( impurity(itype)%dcc(nspin) )
    impurity(itype)%dcc = 0

    allocate( impurity(itype)%e_d(n,n,nspin) )
    impurity(itype)%e_d = 0

!-----------------------------------------------------------------------------------------
!      Parameters for QMC solver
!-----------------------------------------------------------------------------------------
    if( qmc_solver )then
    
!    Number of QMC measures (sweeps)
      impurity(itype)%nqmc = 1000000
      call free_format_reader( iunit, ierr, 'nqmc', 1, ivalue=idummy(1) )
      if( ierr == 0 ) impurity(itype)%nqmc = idummy(1)
  
!    Number of warming sweeps in QMC
      impurity(itype)%nqmcwarm = 500
      call free_format_reader( iunit, ierr, 'nqmcwarm', 1, ivalue=idummy(1) )
      if( ierr == 0 ) impurity(itype)%nqmcwarm = idummy(1)
  
!    Calculate < n_i(t), n_j(t') > correlators in QMC
      impurity(itype)%lninj = .false.
      call free_format_reader( iunit, ierr, 'ninj', 1, lvalue=ldummy(1) )
      if( ierr == 0 ) impurity(itype)%lninj = ldummy(1)

!    Number of Legendre polynomials
      impurity(itype)%nlegendre = -1
      call free_format_reader( iunit, ierr, 'nlegendre', 1, ivalue=idummy(1) )
      if( ierr == 0 ) impurity(itype)%nlegendre = idummy(1)
      
!    Legendre cutoff
      allocate( impurity(itype)%legendre_cutoff(2*n) )
      impurity(itype)%legendre_cutoff = 0
      call free_format_reader( iunit, ierr, 'legendre_cutoff', 2*n, ivalue=impurity(itype)%legendre_cutoff )

!    Use argument cache
      impurity(itype)%use_arg_cache = 0
      call free_format_reader( iunit, ierr, 'use_arg_cache', 1, ivalue=idummy(1) )
      if( ierr == 0 ) impurity(itype)%use_arg_cache = idummy(1)

!   Use legendre polynomials to transform into matsubara space
      impurity(itype)%legendre_transform = 0
      call free_format_reader( iunit, ierr, 'legendre_transform', 1, ivalue=idummy(1) )
      if( ierr == 0 ) impurity(itype)%legendre_transform = idummy(1)
      
!    Frequency of QMC measurements
      impurity(itype)%meas_bin = 2000
      call free_format_reader( iunit, ierr, 'meas_bin', 1, ivalue=idummy(1) )
      if( ierr == 0 ) impurity(itype)%meas_bin = idummy(1)
      
!    Number of QMC updates per measurement
      impurity(itype)%nupdates = 6 * n
      call free_format_reader( iunit, ierr, 'nupdates', 1, ivalue=idummy(1) )
      if( ierr == 0 ) impurity(itype)%nupdates = idummy(1)
      
!    Size of bin for observables
      impurity(itype)%observable_bin_size = 1000
      call free_format_reader( iunit, ierr, 'observable_bin_size', 1, ivalue=idummy(1) )
      if( ierr == 0 ) impurity(itype)%observable_bin_size = idummy(1)

!    Maximum time of QMC measurements ( 32400 s = 9 h )
      impurity(itype)%max_qmc_time = 32400
      call free_format_reader( iunit, ierr, 'max_qmc_time', 1, ivalue=idummy(1) )
      if( ierr == 0 ) impurity(itype)%max_qmc_time = idummy(1)

!    Measure Green(w)
      impurity(itype)%measure_green_omega = .false.
      call free_format_reader( iunit, ierr, 'measure_green_omega', 1, lvalue=ldummy(1) )
      if( ierr == 0 ) impurity(itype)%measure_green_omega = ldummy(1)
      
!    Measure sector statistics
      impurity(itype)%measure_sector_statistics = .false.
      call free_format_reader( iunit, ierr, 'measure_sector_statistics', 1, lvalue=ldummy(1) )
      if( ierr == 0 ) impurity(itype)%measure_sector_statistics = ldummy(1)
      
!    Number of tau points for Delta-function in CT-QMC-W
      impurity(itype)%L4Delta = Ltau
      call free_format_reader( iunit, ierr, 'L4Delta', 1, ivalue=idummy(1) )
      if( ierr == 0 ) impurity(itype)%L4Delta = idummy(1)

!    CTQMC updates
      impurity(itype)%ctqmc_updates = 'full_line:1.0+segment:1.0+antisegment:1.0+global:0.0001'//char(0)
      call free_format_reader( iunit, ierr, 'ctqmc_updates', 1, cvalue=impurity(itype)%ctqmc_updates )
      if( ierr == 0 ) impurity(itype)%ctqmc_updates = adjustl(trim(impurity(itype)%ctqmc_updates))//char(0)
      
!    Random generator
      impurity(itype)%random_generator = 'boost_mt19937'//char(0)
      call free_format_reader( iunit, ierr, 'random_generator', 1, cvalue=impurity(itype)%random_generator )
      if( ierr == 0 ) impurity(itype)%random_generator = adjustl(trim(impurity(itype)%random_generator))//char(0)

!    Allocation 
      if( impurity(itype)%solver == 'CT-QMC-W' .or. impurity(itype)%solver == 'CT-HYB' )then
        allocate( impurity(itype)%G0tau(n,n,impurity(itype)%L4Delta,nspin) )
        forall( i = 1:n, j = 1:n, i /= j ) impurity(itype)%smask(i,j) = 0
      else
        allocate( impurity(itype)%G0tau(n,n,Ltau,nspin) )
      end if
      impurity(itype)%G0tau = 0
      allocate( impurity(itype)%Gtau(n,n,Ltau,nspin) )
      impurity(itype)%Gtau = 0
    end if

!  Measure Sigma*G      <-  MUST BE AT THIS PLACE
    if( impurity(itype)%nlegendre > 0 )then
      impurity(itype)%improved_estimator = .true.
    else
      impurity(itype)%improved_estimator = .false.
    end if
    call free_format_reader( iunit, ierr, 'improved_estimator', 1, lvalue=ldummy(1) )
    if( ierr == 0 ) impurity(itype)%improved_estimator = ldummy(1)

!-----------------------------------------------------------------------------------------
!      Parameters for ED solver
!-----------------------------------------------------------------------------------------
    if( impurity(itype)%solver == 'ED' )then
!     Number of bathes for ED
      impurity(itype)%nbath = n
      call free_format_reader( iunit, ierr, 'nbath', 1, ivalue=idummy(1) )
      if( ierr == 0 ) impurity(itype)%nbath = idummy(1)
    
      allocate( impurity(itype)%Epsk(impurity(itype)%nbath,nspin),           &
                impurity(itype)%Vk(impurity(itype)%nbath,n,nspin) )

      impurity(itype)%Vk = 0.0

      k = impurity(itype)%nbath / n

      do i = 1, n
        do j = 1, k
          impurity(itype)%Epsk((i-1)*k+j,:) = -1.0 + 2.0*real(j)/real(k+1)
          impurity(itype)%Vk((i-1)*k+j,i,:) =  1.0 - abs( impurity(itype)%Epsk((i-1)*k+j,:) )
        end do
      end do

!       Min # of impurity electrons
      impurity(itype)%Nd_min = 0
      call free_format_reader( iunit, ierr, 'Nd_min', 1, ivalue=idummy(1) )
      if( ierr == 0 ) impurity(itype)%Nd_min = idummy(1)

!       Max # of impurity electrons
      impurity(itype)%Nd_max = 2*n
      call free_format_reader( iunit, ierr, 'Nd_max', 1, ivalue=idummy(1) )
      if( ierr == 0 ) impurity(itype)%Nd_max = idummy(1)

!       Max # of conductivity electrons in bath
      impurity(itype)%Nc_max = 2*impurity(itype)%nbath
      call free_format_reader( iunit, ierr, 'Nc_max', 1, ivalue=idummy(1) )
      if( ierr == 0 ) impurity(itype)%Nc_max = idummy(1)

!       Max # of holes in bath valence levels
      impurity(itype)%Nh_max = 2*impurity(itype)%nbath
      call free_format_reader( iunit, ierr, 'Nh_max', 1, ivalue=idummy(1) )
      if( ierr == 0 ) impurity(itype)%Nh_max = idummy(1)

!       Use only density-density U-matrix in ED
      impurity(itype)%lunn = .false.
      call free_format_reader( iunit, ierr, 'Unn', 1, lvalue=ldummy(1) )
      if( ierr == 0 ) impurity(itype)%lunn = ldummy(1)

!       Calculate < s_i, s_j > correlators in ED
      impurity(itype)%lsisj = .false.
      call free_format_reader( iunit, ierr, 'sisj', 1, lvalue=ldummy(1) )
      if( ierr == 0 ) impurity(itype)%lsisj = ldummy(1)
      
!       Total number of eigenvalues to find in Arnoldi
      impurity(itype)%ed_gf_nev = 6*n
      call free_format_reader( iunit, ierr, 'ed_gf_nev', 1, ivalue=idummy(1) )
      if( ierr == 0 ) impurity(itype)%ed_gf_nev = idummy(1)
      
!       Total number of eigenvalues to find in Arnoldi
      impurity(itype)%ed_arno_nev = impurity(itype)%ed_gf_nev
      call free_format_reader( iunit, ierr, 'ed_arno_nev', 1, ivalue=idummy(1) )
      if( ierr == 0 ) impurity(itype)%ed_arno_nev = idummy(1)
      
!       Fraction of NEV to find eigenvalues in sector
      impurity(itype)%ed_arno_tol = -1.0
      call free_format_reader( iunit, ierr, 'ed_arno_tol', 1, rvalue=rdummy(1) )
      if( ierr == 0 ) impurity(itype)%ed_arno_tol = rdummy(1)
     
!       Boltzmann factor cutoff
      impurity(itype)%ed_gf_tol = 1.e-8
      call free_format_reader( iunit, ierr, 'ed_gf_tol', 1, rvalue=rdummy(1) )
      if( ierr == 0 ) impurity(itype)%ed_gf_tol = rdummy(1)

!       Probability cut for eigenvect decomposition
      impurity(itype)%ed_gf_decomp = 0.01
      call free_format_reader( iunit, ierr, 'ed_gf_decomp', 1, rvalue=rdummy(1) )
      if( ierr == 0 ) impurity(itype)%ed_gf_decomp = rdummy(1)

!       ED optimization
      impurity(itype)%ed_opt = 1
      call free_format_reader( iunit, ierr, 'ed_opt', 1, ivalue=idummy(1) )
      if( ierr == 0 ) impurity(itype)%ed_opt = idummy(1)

!       ncv0 =>>>  ncv = 2*newnev + ncv0
      impurity(itype)%ed_arno_ncv0 = 2
      call free_format_reader( iunit, ierr, 'ed_arno_ncv0', 1, ivalue=idummy(1) )
      if( ierr == 0 ) impurity(itype)%ed_arno_ncv0 = idummy(1)

!         Type of uncertainty function for fit
      impurity(itype)%weight = 1
      call free_format_reader( iunit, ierr, 'weight', 1, ivalue=idummy(1) )
      if( ierr == 0 ) impurity(itype)%weight = idummy(1)
     
!         Uncertainty function parameters 
      impurity(itype)%ufp = (/ 10.0, 30.0, 0.5 /)
      call free_format_reader( iunit, ierr, 'ufp', 3, rvalue=impurity(itype)%ufp )
     
    end if

    close(iunit) 

  end do

end subroutine read_impurity
