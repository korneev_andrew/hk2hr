!
!    This file contains set of routines to deal with self-energy
!
subroutine adjust_sigma
!
!    This program calculates the derivative of self-energy
!   with respect to Matsubara frequency. 
!
  use parameter_module
  use energy_module
  use impurity_module
  use iolib_module
  use tools_module
  use fit_module
  use moments_module
  use mpi_module

#ifdef MPI
  use mpi
#endif

  implicit none

  integer :: i, is, ierr, itype
  integer :: n_orb, n_cut
  real(8) :: xmu_save
  real(8), allocatable :: moms(:,:,:), Lorentzians(:,:,:,:)
 ! logical :: lmoms(0:3) = .true.
  logical :: lmoms(0:3) = (/ .true., .true., .false., .false. /)

  xmu_save = xmu
  xmu = 0.d0
  call complex_energy_mesh

  n_cut = floor( ( ecut4fit * beta / pi + 1 ) / 2 )

  do itype = 1, n_imp_type
    n_orb = impurity(itype)%nlm

!    Defines moments of self-energy numerically
    impurity(itype)%moments_sigma = 0.d0
    call get_moments( impurity(itype)%Sigma(:,:,n_energy-ntail:n_energy,:),            &
                      fermionic(n_energy-ntail:n_energy), impurity(itype)%moments_sigma, lmoms )

!     Print out
    if( verbos >= 20 )then
      write(14,"(20x,'Numerical moments of self-energy')")
      call write_matrix( 14, impurity(itype)%moments_sigma(0,:,:,:), 'Real part of Sigma(infty)$', 'Spin # $', ierr )
      call write_matrix( 14, impurity(itype)%moments_sigma(1,:,:,:), 'First moment of Sigma$',  'Spin # $', ierr )
      call write_matrix( 14, impurity(itype)%moments_sigma(2,:,:,:), 'Second moment of Sigma$', 'Spin # $', ierr )
      call write_matrix( 14, impurity(itype)%moments_sigma(3,:,:,:), 'Third moment of Sigma$',  'Spin # $', ierr )
    end if

!    Defines moments of self-energy via correlators (see PhD of Gull)
    allocate( moms(2,2*n_orb,2*n_orb), Lorentzians(2,n_orb,n_lor,nspin) )

    call moments_of_sigma( moms, impurity(itype)%n_cor, impurity(itype)%ninj, impurity(itype)%ummss )

    if( verbos >= 20 )then
      call write_matrix( 14, moms(1,:,:), 'Self-energy, Sigma(infty)$',   ierr )
      call write_matrix( 14, moms(2,:,:), 'First moment of self-energy$', ierr )
    end if

!    The numerically defined Real part of self-energy is more correct (need to fix it!!!)

    impurity(itype)%moments_sigma(1,:,:,1) = moms(2,1:n_orb,1:n_orb)
    if( nspin == 2 )    impurity(itype)%moments_sigma(1,:,:,2) = moms(2,n_orb+1:2*n_orb,n_orb+1:2*n_orb)

  !  forall( i = 1:n_orb ) impurity(itype)%moments_sigma(0,i,i,:) = impurity(itype)%moments_sigma(0,i,i,:) + impurity(itype)%dcc(:)

    if( verbos >= 20 )then
      write(14,"(/,' Moments of self-energy defined via Green function correlators',/)")
      call write_matrix( 14, impurity(itype)%moments_sigma(0,:,:,:), 'Real part of Sigma(infty)$', 'Spin # $', ierr )
      call write_matrix( 14, impurity(itype)%moments_sigma(1,:,:,:), 'First moment of Sigma$',  'Spin # $', ierr )
    end if

    forall( i = 1:n_energy ) impurity(itype)%Sigma(:,:,i,:) = impurity(itype)%Sigma(:,:,i,:) -  &
                                                              impurity(itype)%moments_sigma(0,:,:,:)

    call anneal_by_lorentzian( impurity(itype)%Sigma(:,:,1:n_cut,:), zenergy(1:n_cut),                              &
                               impurity(itype)%moments_sigma(1,:,:,:), emin, emax, Lorentzians, ierr ) 

    call lorentzian2fw( impurity(itype)%Sigma, zenergy, Lorentzians, ierr )

    forall( i = 1:n_energy ) impurity(itype)%Sigma(:,:,i,:) = impurity(itype)%Sigma(:,:,i,:) +  &
                                                              impurity(itype)%moments_sigma(0,:,:,:)

    if( verbos >= 10 )then
      do is = 1, nspin
        do i = 1, n_orb
          call write_function( mkfn( mkfn( mkfn('Lorentzians',itype), i ), is ),      &
                               Lorentzians(1,i,:,is), Lorentzians(2,i,:,is), ierr, 'r' )
        end do
      end do
    end if

    deallocate( moms, Lorentzians )

!      Print out
    if( verbos >= 15 )  call write_function( mkfn('Sigma_fit',resuf,itype), impurity(itype)%Sigma, prn_mesh, ierr, 'a' )

  end do

  xmu = xmu_save
  call complex_energy_mesh

#ifdef MPI
  call mpi_barrier( mpi_comm_world, ierr )
#endif

end subroutine adjust_sigma

subroutine save_sigma
!     Save self-energy from previous iteration
  use parameter_module
  use impurity_module

  implicit none

  integer :: i

  forall( i = 1:n_imp_type ) impurity(i)%Sigma_old = impurity(i)%Sigma

end subroutine save_sigma

subroutine mix_sigma
!     Mix self-energy
  use parameter_module
  use impurity_module
  use symmetry_module

  implicit none

  integer :: i, itype
  real(8) :: dS

  do itype = 1, n_imp_type
!      Error of self-energy
    dS = sum( abs( impurity(itype)%Sigma - impurity(itype)%Sigma_old ) ) /  sum( abs( impurity(itype)%Sigma ) ) 

!      Mix new and old self-energies
    impurity(itype)%Sigma = alpha*impurity(itype)%Sigma  +  ( 1 - alpha )*impurity(itype)%Sigma_old

    call set_symmetry( impurity(itype)%Sigma, impurity(itype)%smask )

!   Particle-hole symmetry case
#ifdef PHsym
    impurity(itype)%Sigma = cmplx( 0.d0, aimag(impurity(itype)%Sigma), 8 )
#endif

!      Print out
    if( verbos > 0 )then
      write(14,"(' Error of self-energy for impurity # ',i3,1x,f12.8)")  itype, dS
      do i = 1, impurity(itype)%nlm
        if( any( aimag(impurity(itype)%Sigma(i,i,:,:)) > 0 ) )    &
            write(14,"(/,' WARNING: Self-energy for the ',i0,'th orbital is not analytical!!!')") i
      end do
    end if

  end do

end subroutine mix_sigma
