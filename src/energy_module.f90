module energy_module
!   This module contains set of subroutines to generate real/imaginary energy mesh and time mesh.
  use units

  implicit none

  public
  save

  integer :: n_energy = 1000                       !  Number of energy points
  real(8) :: ecut = 628.d0                         !  Energy cutoff
  real(8), target :: xmu = 0.d0                    !  Chemical potential

!    Matsubara grid
  real(8) :: beta  = 10.d0                         !  Inverse temperature, beta=1/kT
  real(8), allocatable, target :: fermionic(:)     !  Matsubara's fermionic mesh
  real(8), allocatable, target :: bosonic(:)       !  Matsubara's bosonic mesh

!    Real energy grid
  real(8) :: emin = -20.d0                         !  Minimal energy
  real(8) :: emax =  20.d0                         !  Maximal energy
  real(8) :: eta  = 0.01d0                         !  Offset to complex plane
  real(8), allocatable, target :: ener(:)          !  Real energy mesh

!    Real or imaginary mesh ( imenergy = .not. reenergy )
  logical :: imenergy = .true.                     !  Calculate on imaginary axis
  logical :: reenergy = .false.                    !  ----//----    real   --//--

!    Pointer to fermionic or ener depending on the mesh
  real(8), pointer :: prn_mesh(:)
  character(8) :: resuf                           !  Suffix to add to filenames

!    Complex energy grid
!    zenergy  =  (xmu,fermionic)    or   (ener,eta)   depending on logical switch
  complex(8), allocatable :: zenergy(:)

!    Imaginary time grid
  integer   :: Ltau = 50                           !  Number of time points (slices)
  real(8), allocatable :: tau_mesh(:)              !  Imaginary time mesh

!-----------------------------------------------------------------------------  
  contains

  subroutine def_energy_params( ierr )
!   This subroutine defines parameters for the energy mesh
    implicit none

    interface
      subroutine free_format_reader( iunit, ios, vname, n, ivalue, rvalue, zvalue, lvalue, cvalue )
        implicit none 
        integer,      intent(in   ) :: iunit
        character(*), intent(in   ) :: vname
        integer,      intent(  out) :: ios
        integer,      intent(in   ) :: n
        integer,      intent(inout), optional :: ivalue(*)
        real(8),      intent(inout), optional :: rvalue(*)
        complex(8),   intent(inout), optional :: zvalue(*)
        logical,      intent(inout), optional :: lvalue(*)
        character(*), intent(inout), optional :: cvalue(*)
      end subroutine
    end interface

    integer, intent(  out) :: ierr

    integer :: iunit
    integer :: idummy(10)
    real(8) :: rdummy(10)
    logical :: ldummy(10)

    ierr  = 0
    iunit = 10
    open( iunit, file='amulet.ini', form='formatted', status='old', iostat=ierr, action='read' )

    if( ierr /= 0 ) return

!-------------------------------------------------------------------------------
!
!     General parameters
!
!-------------------------------------------------------------------------------

!     Defines temperature and inverse temperature, beta=1/kT
    call free_format_reader( iunit, ierr, 'TineV', 1, rvalue=rdummy(1) )
    if( ierr == 0 )then
      beta = 1 / rdummy(1)
    else
      call free_format_reader( iunit, ierr, 'TinK', 1, rvalue=rdummy(1) )
      if( ierr == 0 )then
        beta = eV2K / rdummy(1)
      else
        call free_format_reader( iunit, ierr, 'beta', 1, rvalue=rdummy(1) )
        if( ierr == 0 ) beta = rdummy(1)
      end if
    end if

!     Chemical potential
    call free_format_reader( iunit, ierr, 'mu', 1, rvalue=rdummy(1) )
    if( ierr == 0 ) xmu = rdummy(1)

!     Number of time points (slices)
    Ltau = 5 * nint(beta)
    call free_format_reader( iunit, ierr, 'l', 1, ivalue=idummy(1) )
    if( ierr == 0 ) Ltau = idummy(1)
    Ltau = Ltau + 1

!     Make calculations on the imaginary/real axis
    call free_format_reader( iunit, ierr, 'imaxis', 1, lvalue=ldummy(1) )
    if( ierr == 0 ) imenergy = ldummy(1)

!-------------------------------------------------------------------------------
!     Energy units to use for Hamiltonian
!   Use own units where energy_factor will scale Hamiltonian
    call free_format_reader( iunit, ierr, 'energy_factor', 1, rvalue=rdummy(1) )
    if( ierr == 0 ) energy_factor = rdummy(1)

!   Use Rydberg
    call free_format_reader( iunit, ierr, 'in_Ry', 1, lvalue=ldummy(1) )
    if( ierr == 0 ) in_Ry = ldummy(1)

!   Use Hartree (atomic units of energy)
    call free_format_reader( iunit, ierr, 'in_au', 1, lvalue=ldummy(1) )
    if( ierr == 0 ) in_au = ldummy(1)

!-------------------------------------------------------------------------------
!
!     Extracting parameters for imaginary energy grid
!
!-------------------------------------------------------------------------------

!     Energy cutoff which defines the largest Matsubara frequency
    call free_format_reader( iunit, ierr, 'ecut', 1, rvalue=rdummy(1) )
   
!     Number of energy points
    if( ierr == 0 )then
      ecut = rdummy(1)
      n_energy = ( ecut*beta/pi + 1 ) / 2
    else
      call free_format_reader( iunit, ierr, 'iwmax', 1, ivalue=idummy(1) )
     
      if( ierr == 0 )then
        n_energy = idummy(1)
      else
        open( 11, file='sigma_1', form='formatted', status='old',          &
                  iostat=ierr, action='read' )
        if( ierr == 0 )then
          read(11,*)
          read(11,*) n_energy
        else
          n_energy = ( ecut*beta/pi + 1 ) / 2
        end if
        close(11)
      end if
    end if

!---------------------------------------------------------------------------
!
!     Extracting parameters for real energy grid
!
!---------------------------------------------------------------------------

!     Offset to the complex energy plan
    eta = pi / beta
    call free_format_reader( iunit, ierr, 'eta', 1, rvalue=rdummy(1) )
    if( ierr == 0 ) eta = rdummy(1)
  
!     Minimal energy on the real axis
    call free_format_reader( iunit, ierr, 'emin', 1, rvalue=rdummy(1) )
    if( ierr == 0 ) emin = rdummy(1)
  
!     Maximal energy on the real axis
    call free_format_reader( iunit, ierr, 'emax', 1, rvalue=rdummy(1) )
    if( ierr == 0 ) emax = rdummy(1)
  
!     Number of energy points
    open( 11, file='sigma_re_1', form='formatted', status='old',           &
              iostat=ierr, action='read' )

    if( ierr == 0 )then
      read(11,*)
      read(11,*) n_energy
    else
      open( 11, file='Sigma_re_1_1,1.dat', form='formatted',               &
                status='old', iostat=ierr, action='read' )
      if( ierr == 0 )then
        read(11,*) emin
        n_energy = 1
        do
          n_energy = n_energy + 1
          read(11,*,iostat=ierr) emax
          if( ierr /= 0 ) exit
        end do
        n_energy = n_energy - 1
      end if
      close(11)
    end if
!gfortran case for ierr == 2
    if( ierr == -1 .or. ierr == 29 .or. ierr == 2 )  ierr = 0
    close(iunit)
    
  end subroutine def_energy_params
  
  subroutine set_energy_mesh
!   Generate all energy and time meshes
    implicit none

    integer :: i

!     Fermionic Matsubara frequencies
    if( .not. allocated( fermionic ) ) allocate( fermionic(n_energy) )
    forall( i = 1:n_energy ) fermionic(i) = ( 2*i - 1 ) * pi / beta

!     Bosonic Matsubara frequencies
    if( .not. allocated( bosonic ) ) allocate( bosonic(n_energy) )
    forall( i = 0:n_energy-1 ) bosonic(i+1) = 2 * i * pi / beta

!     Real energy mesh
    if( emin > emax )then
      emin = - max( abs(emin), abs(emax) )
      emax = - emin
    end if

    if( .not. allocated( ener ) )    allocate( ener(n_energy) )
    forall( i = 1:n_energy )  ener(i) = emin + ( emax - emin )*( i - 1 ) / ( n_energy - 1 )

!     Imaginary time mesh
    if( .not. allocated( tau_mesh ) ) allocate( tau_mesh(Ltau) )
    forall( i = 1:Ltau ) tau_mesh(i) = ( i - 1 ) * beta / ( Ltau - 1 )

  end subroutine set_energy_mesh
  
  subroutine complex_energy_mesh
!   Generate complex energy mesh
    implicit none

    if( .not. allocated( zenergy ) )  allocate( zenergy(n_energy) )

    if( imenergy )then
      zenergy = cmplx( xmu, fermionic, 8 )
    else
      zenergy = cmplx( xmu+ener, eta, 8 )
    end if

  end subroutine complex_energy_mesh
  
  subroutine use_real_mesh
    implicit none
    
    reenergy = .true.
    imenergy = .false.

    call complex_energy_mesh

    prn_mesh => ener

    resuf = '_re'

  end subroutine use_real_mesh
  
  subroutine use_imaginary_mesh
    implicit none
    
    imenergy = .true.
    reenergy = .false.
    
    call complex_energy_mesh
    
    prn_mesh => fermionic
    
    resuf = ''
  
  end subroutine use_imaginary_mesh
  
end module energy_module
