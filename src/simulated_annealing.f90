module simulated_annealing_module
!   This module contains subroutines for simulated annealing.

  implicit none 

  private

  public simulated_annealing

  interface simulated_annealing
     module procedure simanbylor, normedsimanbylor
  end interface

  contains

  subroutine simanbylor( x, y, sigma, n, a, iafix, p, emin, emax, S_old, beta, info )
!
!   This subroutine fits complex function by Lorentzians with use of the simulated annealing
!
    implicit none

    integer,    intent(in   ) :: n           !  # of points in function
    complex(8), intent(in   ) :: x(n)        !  x-mesh
    complex(8), intent(in   ) :: y(n)        !  data points on the x-mesh
    real(8),    intent(in   ) :: sigma(n)    !  standard deviation for y
    integer,    intent(  out) :: info        !  info status
    real(8),    intent(in   ) :: emin        !  lower bound for Lorentzians
    real(8),    intent(in   ) :: emax        !  upper bound for Lorentzians
    real(8),    intent(in   ) :: beta        !  inverse temperature
    real(8),    intent(inout) :: S_old       !  on input:
                                    !   required tolerance for merrit function
                                    !              on output
                                    !   merrit function

    integer,    intent(in   ) :: p              !  # of Lorentzians parameters
    real(8),    intent(inout) :: a(p)           !  parameters
    integer,    intent(in   ) :: iafix(p)       !  fix or not some parameters

    integer :: np

    real(8) :: Amp(p/2)         !  amplitude of Lorentzians
    real(8) :: e(p/2)           !  energy position
    integer :: ifixa(p/2)       !  fix of amplitude
    integer :: ifixe(p/2)       !  fix of energy position

    integer    :: i, j, is, ip1
    real(8)    :: rnd, rnda, rnde1, S_new, kT, tolchi
    complex(8) :: fn
    complex(8) :: D_old(n), D_new(n)

    info = 0
    tolchi = S_old

    if( all( iafix == 0 ) ) return

    if( mod(p,2) == 0 )then
      np = p / 2
    else
      info = 1
      return
    end if

!    Start from uniform distribution for Lorentzians except fixed

    Amp = 1.d0 / np
    forall( i = 1:np ) e(i) = emin + ( emax - emin ) * ( i-1 ) / ( np - 1 )
  !  forall( i = 1:np ) e(i) = -1 + 2 * ( i-1 ) / ( np - 1 )
  !  e = 0.d0

    ifixa = iafix(1:np)
    where( ifixa == 0 ) Amp = a(1:np)

    ifixe = iafix(np+1:p)
    where( ifixe == 0 ) e = a(np+1:p)

    S_new = 0.d0
    S_old = 0.d0
    do i = 1, n
      fn = cmplx(0.d0,0.d0,8)
      do j = 1, np
        fn = fn + Amp(j) / ( x(i) - e(j) )
      end do
      D_old(i) = ( y(i) - fn ) / sigma(i)
      S_old = S_old + abs( D_old(i) )**2
    end do

!    Simulated annealing

    kT = 0.51
!    write(6,*) ' kT ', kT, 1/beta, S_old, tolchi
    do while ( kT > 1/beta )    !.or. S_old < tolchi )
      do is = 1, 50000
        call random_number( rnd )
        ip1 = nint( np * rnd )

        if( ip1 > np .or. ip1 <= 0 ) cycle
        if( ifixa(ip1) == 0 .and. ifixe(ip1) == 0 ) cycle

        if( ifixa(ip1) == 0 )then
          rnda = 0.d0
        else
          call random_number( rnda )
          rnda = ( rnda - 0.5d0 ) * 0.1d0 / np
        end if

        if( Amp(ip1) + rnda < 0.d0 )  cycle

        if( ifixe(ip1) == 0 )then
          rnde1 = 0.d0
        else
          call random_number( rnde1 )
          rnde1 = ( rnde1 - 0.5d0 ) !* 0.5
        end if

        if( e(ip1) + rnde1 < emin .or. e(ip1) + rnde1 > emax )  cycle

        S_new = 0.d0
        do i = 1, n
          fn =   Amp(ip1) / ( x(i) - e(ip1) )  -  ( Amp(ip1) + rnda ) / ( x(i) - e(ip1) - rnde1 )

          D_new(i) = D_old(i) + fn / sigma(i)
          S_new = S_new + abs( D_new(i) )**2
        end do

        call random_number( rnd )

        if( exp( (S_old-S_new) / kT ) > rnd )then
          Amp(ip1) = Amp(ip1) + rnda
          e(ip1)   = e(ip1) + rnde1
          D_old    = D_new
          S_old    = S_new
        end if

      end do
      kT = kT / 1.3

  !    write(77,*) kT, S_old
    end do

    a(1:np)   = Amp
    a(np+1:p) = e
    S_old     = S_old / n

  end subroutine simanbylor

  subroutine normedsimanbylor( x, y, sigma, n, a, iafix, p, emin, emax, norm, info )
!
!   This subroutine fits complex function by Lorentzians with use of the simulated annealing
!  and keeping a normalization of function.
!
    implicit none

    integer,    intent(in   ) :: n           !  # of points in function
    complex(8), intent(in   ) :: x(n)        !  x-mesh
    complex(8), intent(in   ) :: y(n)        !  data points on the x-mesh
    real(8),    intent(in   ) :: sigma(n)    !  standard deviation for y
    integer,    intent(  out) :: info        !  info status
    real(8),    intent(in   ) :: emin        !  lower bound for Lorentzians
    real(8),    intent(in   ) :: emax        !  upper bound for Lorentzians
    real(8),    intent(in   ) :: norm        !  norm to preserve

    integer,    intent(in   ) :: p              !  # of Lorentzians parameters
    real(8),    intent(inout) :: a(p)           !  parameters
    integer,    intent(in   ) :: iafix(p)       !  fix or not some parameters

    integer :: np

    real(8) :: Amp(p/2)         !  amplitude of Lorentzians
    real(8) :: e(p/2)           !  energy position
    integer :: ifixa(p/2)       !  fix of amplitude
    integer :: ifixe(p/2)       !  fix of energy position

    integer :: i, j, is, ip1, ip2
    real(8) :: rnd, rnda, rnde1, rnde2, S_new, S_old, kT
    complex(8) :: fn
    
    complex(8) :: D_old(n), D_new(n)
    
    info = 0

    if( all( iafix == 0 ) ) return

    if( mod(p,2) == 0 )then
      np = p / 2
    else
      info = 1
      return
    end if

!    Start from uniform distribution for Lorentzians except fixed

    Amp = norm / np
    forall( i = 1:np ) e(i) = emin + ( emax - emin ) * ( i-1 ) / ( np - 1 )
  !  forall( i = 1:np ) e(i) = -1._8 + 2._8 * ( i-1 ) / ( np - 1 )

    ifixa = iafix(1:np)
    where( ifixa == 0 ) Amp = a(1:np)

    ifixe = iafix(np+1:p)
    where( ifixe == 0 ) e = a(np+1:p)

    S_new = 0.d0
    S_old = 0.d0
    do i = 1, n
      fn = cmplx(0.d0,0.d0,8)
      do j = 1, np
        fn = fn + Amp(j) / ( x(i) - e(j) )
      end do
      D_old(i) = ( y(i) - fn ) / sigma(i)
      S_old = S_old + abs( D_old(i) )**2
    end do

!    Simulated annealing

    kT = 0.51
    do while ( kT > 1.d-16 )
      do is = 1, 50000
        call random_number( rnd )
        ip1 = nint( np * rnd )

        if( ip1 > np .or. ip1 <= 0 ) cycle
        if( ifixa(ip1) == 0 .and. ifixe(ip1) == 0 ) cycle

        call random_number( rnd )
        ip2 = nint( np * rnd )

        if( ip2 > np .or. ip2 <= 0 ) cycle
        if( ifixa(ip2) == 0 .and. ifixe(ip2) == 0 ) cycle

        if( ip1 == ip2 ) cycle

        call random_number( rnda )

        rnda = ( rnda - 0.5 ) * 0.1 / np

        if( ifixa(ip1) == 0 .or. ifixa(ip2) == 0 )  rnda = 0

        if( Amp(ip1) + rnda < 0 .or. Amp(ip2) - rnda < 0 )  cycle

        if( ifixe(ip1) == 0 )then
          rnde1 = 0
        else
          call random_number( rnde1 )
          rnde1 = ( rnde1 - 0.5 ) !* 0.5
        end if

        if( e(ip1) + rnde1 < emin .or. e(ip1) + rnde1 > emax )  cycle

        if( ifixe(ip2) == 0 )then
          rnde2 = 0
        else
          call random_number( rnde2 )
          rnde2 = ( rnde2 - 0.5 ) !* 0.5
        end if

        if( e(ip2) + rnde2 < emin .or. e(ip2) + rnde2 > emax )  cycle

        S_new = 0.d0
        do i = 1, n
          fn =   Amp(ip1) / ( x(i) - e(ip1) )                    +            &
                 Amp(ip2) / ( x(i) - e(ip2) )                    -            &
               ( Amp(ip1) + rnda ) / ( x(i) - e(ip1) - rnde1 )   -            &
               ( Amp(ip2) - rnda ) / ( x(i) - e(ip2) - rnde2 )

          D_new(i) = D_old(i) + fn / sigma(i)
          S_new = S_new + abs( D_new(i) )**2
        end do

        call random_number( rnd )

        if( exp( (S_old-S_new) / kT ) > rnd )then
          Amp(ip1) = Amp(ip1) + rnda
          Amp(ip2) = Amp(ip2) - rnda
          e(ip1)   = e(ip1) + rnde1
          e(ip2)   = e(ip2) + rnde2
          D_old    = D_new
          S_old    = S_new
        end if

      end do
      kT = kT / 1.3
 !     write(77,*) kT, S_old
    end do

    a(1:np)   = Amp
    a(np+1:p) = e
    S_old     = S_old / n

  end subroutine normedsimanbylor
  
end module simulated_annealing_module
