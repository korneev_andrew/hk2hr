module timer_module
  implicit none
  private
  public add_timer,start_timer,stop_timer
  
  type, public :: t_timer
    real :: total_time = 0.
    real :: tmp_time   = 0.
    character,     pointer :: name(:) => null()
    type(t_timer), pointer :: next => null()
  end type t_timer

  type(t_timer), pointer :: timer => null()
  type(t_timer), pointer :: last_timer => null() 

  contains
  
  !------------------------------------
  subroutine add_timer( ptr_timer,tname )
    implicit none
    character(len=*), intent(in   ) :: tname
    type(t_timer), pointer :: ptr_timer
    integer :: ln,i

    if( associated(ptr_timer) )  return
    if( .not.associated(timer) )then
       allocate(timer)
       ptr_timer=>timer
     else
       allocate(ptr_timer)
       last_timer%next=>ptr_timer
    end if
    
    ln = len(tname)
    allocate(ptr_timer%name(ln))
    
    forall( i = 1:ln ) ptr_timer%name(i) = tname(i:i)
    last_timer=>ptr_timer
  end subroutine add_timer
  
  !------------------------------------
  subroutine start_timer( tm )
    implicit none
    type(t_timer),pointer::tm

    if( .not.associated(tm) )  return
    call cpu_time(tm%tmp_time)
  end subroutine start_timer
  
  !------------------------------------
  subroutine stop_timer( tm )
    implicit none
    integer max_len_name
    type(t_timer),pointer:: tm
    real t

    if( .not.associated(tm) )  return
    call cpu_time(t)
    tm%total_time = t - tm%tmp_time
    
    max_len_name=getMaxLenName()
    write(14,*)
    call show( tm,max_len_name )
    write(14,*)
  end subroutine stop_timer

  
  !------------------------------------
  subroutine show( tm,max_len_name )
    implicit none
    integer, intent(in   ) :: max_len_name
    type(t_timer), pointer :: tm
    
    integer t_min,len_name,i
    real t_sec
    character(1)      :: name(40) = ' '
    character(len=40) :: nname
    equivalence (nname,name)

    if(.not.associated(tm)) return
    len_name=size(tm%name)
    forall(i=1:len_name) name(i)=tm%name(i)
    forall(i=len_name+1:max_len_name) name(i)=' '
    t_min=int(tm%total_time/60.)
    t_sec=tm%total_time-real(t_min*60)
!gfortran
!Fortran runtime error: Insufficient data descriptors in format after reversion
    if(t_min>0) then
!      write(14,998) ' ',nname,t_min,' min ',t_sec,' sec '
       write(14,*) ' ',nname,t_min,' min ',t_sec,' sec '
     else
!      write(14,999) ' ',nname,t_sec,' sec '
       write(14,*) ' ',nname,t_sec,' sec '
    endif
998 format(a,a,I5,a,f6.2,a)
999 format(a,a,f6.2,a)

  end subroutine show

  !--------------------------------------
  function getMaxLenName() result(maxlen)
    implicit none
    integer maxlen
    type(t_timer),pointer:: cur_tm
    if(.not.associated(timer)) &
         return
    maxlen=0
    cur_tm=>timer
    do while(associated(cur_tm))
       if(maxlen<size(cur_tm%name)) &
            maxlen=size(cur_tm%name)
       cur_tm=>cur_tm%next
    end do    
  end function getMaxLenName
  
end module timer_module
