subroutine hartree_fock_energy( umtrx, ummss, occup, ninj2, n_orb, nspin )
!
!    This subroutine calculates Hartree-Fock (U<n><n>) and
!    Coulomb correlations (U<nn>) energies
!
  use iolib_module

  implicit none
  
  integer :: n_orb
  integer :: nspin
  real(8) :: umtrx(n_orb,n_orb,n_orb,n_orb)
  real(8) :: ummss(2*n_orb,2*n_orb)
  real(8) :: occup(n_orb,n_orb,2)
  real(8) :: ninj2(2*n_orb,2*n_orb)

  integer :: i, j, k, l, is, ierr
  real(8) :: energy
  real(8), allocatable :: potential(:,:,:)
  
!   Calculates Hartree-Fock energy, U<n><n>
      
  energy = 0.d0
  do is = 1, 2
    do i = 1, n_orb
      do j = 1, n_orb
        do k = 1, n_orb
          do l = 1, n_orb
            energy = energy                                  +                  &
               umtrx(i,k,j,l)*occup(i,j,is)*occup(k,l,3-is)  +                  &
               ( umtrx(i,k,j,l) - umtrx(i,k,l,j) )*occup(i,j,is)*occup(k,l,is)
          end do
        end do
      end do
    end do
  end do
  energy = energy / 2.d0
      
  write(14,"(/,' Hartree-Fock energy, U<n><n>/2',10x,f10.6)") energy
    
  allocate( potential(n_orb,n_orb,nspin) )
  potential = 0.d0
  
  do is = 1, nspin
    do i = 1, n_orb
      do j = 1, n_orb
        do k = 1, n_orb
          do l = 1, n_orb
            potential(i,j,is) = potential(i,j,is)            +                  &
                             umtrx(i,k,j,l)*occup(k,l,3-is)  +                  &
                           ( umtrx(i,k,j,l) - umtrx(i,k,l,j) )*occup(k,l,is)
          end do
        end do
      end do
    end do
  end do
      
  call write_matrix( 14, potential, 'Hartree-Fock potential$', 'Spin$', ierr )
  
!       Correlation energy U<nn>
  energy = 0.d0
  do i = 1, 2*n_orb
    do j = 1, 2*n_orb
      energy = energy + ummss(i,j)*ninj2(i,j)
    end do
  end do
  energy = energy / 2.d0

  write(14,"(/,' Coulomb correlation energy, U<nn>/2',5x,f10.6,/)") energy
  
  deallocate( potential )
 
end subroutine hartree_fock_energy
