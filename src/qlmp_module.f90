module qlmp_module
!   This module calculates occupation of the Green function on imaginary axis.
!
!   TO DO
!   Add real energy axis support.
!
  use units
  use moments_module

  public qlmp, zqlmp, occupation_matrix, occupation_matrix_z, sum_iwn_matrix_z

  contains

  real(8) function qlmp( f, x, moms )
!   This function calculates the occupation of the Green function
!  on the imaginary Matsubara grid with use of the moments to treat high-energy tail.
!
    implicit none

    complex(8), intent(in   ) :: f(:)      !   Green function
    real(8)   , intent(in   ) :: x(:)      !   Matsubara grid
    real(8)   , intent(in   ) :: moms(3)   !   First three moments of Green function

    integer    :: n
    real(8)    :: a1, a2, a3, beta
    complex(8) :: zsum

    n = size( f )
    if( n /= size(x) .or. n <= 0 ) stop

    beta = twopi / ( x(2) - x(1) )

    a1 =   moms(1) - moms(3)
    a2 = ( moms(3) + moms(2) ) / 2
    a3 = ( moms(3) - moms(2) ) / 2

    zsum = sum( f - a1/cmplx(0,x,8) - a2/cmplx(-1,x,8) - a3/cmplx(1,x,8) )

    qlmp = 2 * real(zsum,8) / beta
    qlmp = qlmp + ( moms(1) - moms(2)*dtanh(beta/2) )/2

  end function qlmp

  subroutine occupation_matrix( f, x, ntail, occupation )
!  Driver routine for above function
    integer,    intent(in   ) :: ntail            !   Number of points to treat tail
    real(8),    intent(in   ) :: x(:)             !   Matsubara grid
    complex(8), intent(in   ) :: f(:,:,:)         !   Green function
    real(8),    intent(  out) :: occupation(:,:)  !   Occupation matrix
    
    integer :: i, j, n, ndim
    logical :: lmom(0:3) = (/ .false., .false., .true., .true. /)
    real(8), allocatable :: moms(:,:,:)
    
    ndim = size( f, dim=1 )
    n    = size( f, dim=3 )
    
    allocate( moms(0:3,ndim,ndim) )
    moms = 0
    
    do i = 1, ndim
      do j = 1, ndim

!    Find moments of Green function from high-energy expansion
        if( i == j )  moms(1,i,j) = 1

        call def_moments_w( f(i,j,n-ntail:n), x(n-ntail:n), moms(:,i,j), lmom )

!    Calculate number of electrons
        occupation(i,j) = qlmp( f(i,j,:), x, moms(1:3,i,j) )
         
      end do
    end do

#ifdef DEBUG
    write(14,*) ' Numerical moments of Gf. Second.'
    do i = 1, ndim
      write(14,*) ( moms(2,i,j), j = 1, ndim )
    end do

    write(14,*) ' Numerical moments of Gf. Third.'
    do i = 1, ndim
      write(14,*) ( moms(3,i,j), j = 1, ndim )
    end do
#endif

    deallocate( moms )

  end subroutine occupation_matrix

  subroutine occupation_matrix_z( f, x, ntail, occupation )
!  Driver routine for above function
    integer,    intent(in   ) :: ntail            !   Number of points to treat tail
    real(8),    intent(in   ) :: x(:)             !   Matsubara grid
    complex(8), intent(in   ) :: f(:,:,:)         !   Green function
    complex(8), intent(  out) :: occupation(:,:)  !   Occupation matrix
    
    integer :: i, j, n, ndim
    logical :: lmom(0:3) = (/ .false., .false., .true., .false. /)
    real(8) :: moms(0:3)
    
    ndim = size( f, dim=1 )
    n    = size( f, dim=3 )
    
    do i = 1, ndim
      do j = 1, ndim

!    Find moments of Green function from high-energy expansion
        moms = 0
        if( i == j )  moms(1) = 1

        call def_moments_w( f(i,j,n-ntail:n), x(n-ntail:n), moms, lmom )

!    Calculate number of electrons
        occupation(i,j) = zqlmp( f(i,j,:), x, moms(1:3) )
         
      end do
    end do
    
    occupation = occupation + transpose( conjg( occupation ) ) 

  end subroutine occupation_matrix_z

  subroutine sum_iwn_matrix_z( f, x, ntail, sumz )
!  Driver routine for above function
    integer,    intent(in   ) :: ntail            !   Number of points to treat tail
    real(8),    intent(in   ) :: x(:)             !   Matsubara grid
    complex(8), intent(in   ) :: f(:,:,:)         !   Green function
    complex(8), intent(  out) :: sumz(:,:)  !   Occupation matrix
    
    integer :: i, j, n, ndim
    logical :: lmom(0:3) = (/ .true., .true., .true., .true. /)
    real(8), allocatable :: moms(:,:,:)
    
    ndim = size( f, dim=1 )
    n    = size( f, dim=3 )
    
    allocate( moms(0:3,ndim,ndim) )
    
    do i = 1, ndim
      do j = 1, ndim

!    Find moments of Green function from high-energy expansion
        call def_moments_w( f(i,j,n-ntail:n), x(n-ntail:n), moms(:,i,j), lmom )

!    Calculate number of electrons
        sumz(i,j) = zqlmp( f(i,j,:)-moms(0,i,j), x, moms(1:3,i,j) )
         
      end do
    end do
    
    sumz = sumz + transpose( conjg( sumz ) ) + moms(0,:,:) + transpose(moms(0,:,:))

    deallocate( moms )
  
  end subroutine sum_iwn_matrix_z

  complex(8) function zqlmp( f, x, moms )
!   This function calculates the occupation of the Green function
!  on the imaginary Matsubara grid with use of the moments to treat high-energy tail.
!
    implicit none
  
    complex(8), intent(in   ) :: f(:)      !   Green function
    real(8)   , intent(in   ) :: x(:)      !   Matsubara grid
    real(8)   , intent(in   ) :: moms(3)   !   First three moments of Green function
    
    integer    :: n
    real(8)    :: a1, a2, a3, beta
    complex(8) :: zsum
    
    n = size( f )
    if( n /= size(x) .or. n <= 0 ) stop
    
    beta = twopi / ( x(2) - x(1) )
    
    a1 =   moms(1) - moms(3)
    a2 = ( moms(3) + moms(2) ) / 2
    a3 = ( moms(3) - moms(2) ) / 2
        
    zsum = sum( f - a1/cmplx(0,x,8) - a2/cmplx(-1,x,8) - a3/cmplx(1,x,8) )
    
    zqlmp = zsum / beta
    zqlmp = zqlmp + ( moms(1) - moms(2)*tanh(beta/2) )/4
  
  end function zqlmp
  
end module qlmp_module
