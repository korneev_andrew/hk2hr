subroutine gk( g, h, sigma, n, z )
!
!   Calculation of Green function at given k,z point
!
!   G(k,z) =  [ z - H(k) - Sigma(z) ]^{-1}
!
  use lapack_module
    
  implicit none
  
  integer    :: n                   !   Matrix dimension
  complex(8) :: z                   !   Energy point
  complex(8) :: h(n,n)              !   Hamiltonian (at certain k-point)
  complex(8) :: sigma(n,n)          !   Self-energy
  complex(8) :: g(n,n)              !   Green function, G(k,z)

!----------------------------------------------------------------------
  
  integer :: i

  g = - h - sigma
  forall( i = 1:n ) g(i,i) = g(i,i) + z
  call inverse_matrix( g )

end subroutine gk
