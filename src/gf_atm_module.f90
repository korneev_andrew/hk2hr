module gf_atm_module
!
!    This module calculates Green function using analytical tetrahedron method.
!    Written by A. Poteryaev < A.Poteryaev _at_ cpht.polytechnique.fr >
!   
  use atm_module

  implicit none
  
  public :: gf_atm
  
  interface green_atm
    module procedure gf_atm, gf_atm_total
  end interface 
  
  contains

  subroutine gf_atm( z, ek, cl, cr, itt, Gf, m, diag )
!
!    Calculates Green function using analytical tetrahedron method.
!  
!    Inputs:
!   z    - complex energy at which Green function will be calculated
!   ek   - complex eigenvalues   
!      ek(n,nkp)
!         n   -  size 
!         nkp -  number of k-points
!   cl   - left  eigenvectors     cl(nm,n,nkp)
!   cr   - right eigenvectors     cr(nm,n,nkp)
!         nm=n -  leading dimension (orbital index) 
!   itt  - integer index array defining tetrahedron  itt(5,ntet)
!         itt(1,:)   - tetrahedron's multiplicity
!         itt(2-5,:) - points to the 4 k-points defining tetrahedron
!         ntet       - number of tetrahedra
!   m    - integer dimension of the Green function to be calculated (m by m block)
!   diag - logical switch for calculation of diagonal elements of Green function
!       .true.  - calculates diagonal elements only
!       .false. - calculates m by m block
!    Outputs:
!   Gf    - Green function   Gf(m,m)
!
    implicit none
    
    integer,    intent(inout) :: m
    integer,    intent(in   ) :: itt(:,:)
    complex(8), intent(in   ) :: z
    complex(8), intent(in   ) :: ek(:,:), cl(:,:,:), cr(:,:,:)
    logical,    intent(in   ) :: diag
    complex(8), intent(  out) :: Gf(:,:)
  
!--->  Local variables 
    integer :: n, ntet
    
    integer :: i, j, k, it
    complex(8) :: e(4), F(4), res(4)

    n    = size( ek,  1 )
    ntet = size( itt, 2 )
!
!    Crosscheck
!
    if( n /= size( cl, 1 ) .or. n /= size( cl, 2 )   .or.    &
        n /= size( cr, 1 ) .or. n /= size( cr, 2 )   )then
        write(6,*)' Dimension error in gf_atm'
        stop
    end if
    
    if( size( cl, 3 ) /= size( cr, 3 ) .or. size( cl, 3 ) /= size( ek, 2 ) )then
        write(6,*)' Array error in gf_atm'
        stop
    end if
    
    if( size( itt, 1 ) /= 5 )then
        write(6,*)' Tetrahedron array error in gf_atm'
        stop
    end if
    
    if( m > n ) m = n
!
!    Calculate Green function
!    
    Gf = cmplx(0.d0,0.d0,8)

    do i = 1, n               
      do it = 1, ntet
        
        e(1) = ek(i,itt(2,it))
        e(2) = ek(i,itt(3,it))
        e(3) = ek(i,itt(4,it))
        e(4) = ek(i,itt(5,it))
        
        call tetweight( z, e, res )
 
        do j = 1, m          
          do k = 1, m          
            if( ( j /= k ) .and. diag ) cycle
            
            F(1) = ( cl(i,j,itt(2,it)) ) * cr(k,i,itt(2,it))
            F(2) = ( cl(i,j,itt(3,it)) ) * cr(k,i,itt(3,it))
            F(3) = ( cl(i,j,itt(4,it)) ) * cr(k,i,itt(4,it))
            F(4) = ( cl(i,j,itt(5,it)) ) * cr(k,i,itt(5,it))
 
            Gf(j,k) = Gf(j,k) + res(1)*F(1) + res(2)*F(2) + res(3)*F(3) + res(4)*F(4)
          end do  
        end do  
           
      end do    !    it
    end do      !    i

    Gf = Gf / ntet

  end subroutine gf_atm

  subroutine gf_atm_total( z, ek, itt, Gftot )
!
!    Calculates Green function using analytical tetrahedron method.
!  
!    Inputs:
!   z    - complex energy at which Green function will be calculated
!   ek   - complex eigenvalues   
!      ek(n,nkp)
!         n   -  size 
!         nkp -  number of k-points
!   itt  - integer index array defining tetrahedron  itt(5,ntet)
!         itt(1,:)   - tetrahedron's multiplicity
!         itt(2-5,:) - points to the 4 k-points defining tetrahedron
!         ntet       - number of tetrahedra
!    Outputs:
!   Gftot - total Green function
!
    implicit none
    
    integer,    intent(in   ) :: itt(:,:)
    complex(8), intent(in   ) :: z
    complex(8), intent(in   ) :: ek(:,:)
    complex(8), intent(  out) :: Gftot
  
!--->  Local variables 
    integer :: n, ntet
    integer :: i, it
    complex(8) :: e(4), res(4)

    n    = size( ek,  1 )
    ntet = size( itt, 2 )
!
!    Crosscheck
!
    if( size( itt, 1 ) /= 5 )then
        write(6,*)' ITT array error in gf_atm'
        stop
    end if
!
!    Calculate Green function
!    
    Gftot = cmplx(0.d0,0.d0,8)
      
    do i = 1, n               
      do it = 1, ntet
        
        e(1) = ek(i,itt(2,it))
        e(2) = ek(i,itt(3,it))
        e(3) = ek(i,itt(4,it))
        e(4) = ek(i,itt(5,it))
        
        call tetweight( z, e, res )
 
        Gftot = Gftot + res(1) + res(2) + res(3) + res(4) 
           
      end do    !    it
    end do      !    i

    Gftot = Gftot / ntet

  end subroutine gf_atm_total
  
end module gf_atm_module
