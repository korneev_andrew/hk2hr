module parameter_module
  implicit none
  public

  integer, save :: istart = 1      ! 0-from scratch
  integer, save :: nspin  = 1      ! 1-paramagnetic calculation
                                   ! 2-magnetic calculation
  integer, save :: so = 0          ! 0- no spin-orbit in hamiltonian
                                   ! 1- with spin-orbit in hamiltonian
  integer, save :: niter = 10      ! Number of iteration
  real(8), save :: alpha = 0.5d0   ! Mixing coefficient

  real(8), save :: h_field = 0.d0  ! Magnetic field 

  integer, save :: verbos = 3      ! Verbosity level

  integer, save :: ntail = 99      ! Number of points in tail to use for fitting of moments

  integer, save :: index4pade(8)   ! Index to Matsubara mesh to use with Pade

!    Parameters for Data explorer
  logical, save :: dxreggrid       ! use regular grid for Data explorer
                                   ! otherwise the coordinaes of k-points
  real(8), save :: dxorigin(3)     ! origin for Data explorer grid
  real(8), save :: dxdelta(3,3)    ! delta for Data explorer grid

  integer, save :: fsbands(2)      ! Indexes of first and last bands crossing Fermi level

  logical, save :: use_blockinverse = .false.     ! use of block matrix inverse algorithm to calculate G
  logical, save :: use_atm = .false.              ! use ATM to calculate Green function
  logical, save :: diag = .true.
  real(8), save :: atm_cutoff = 5                 ! cutoff energy for ATM to calculate Green function

  real(8), save :: ntotal                         ! total number of particles

  real(8), save :: fentoler = 1.d-3        ! Energy tolerance to find Fermi level
  real(8), save :: fpatoler = 1.d-6        ! Number of particle tolerance to find Fermi level

  logical, save :: use_sigma = .true.      ! use self-energy to calculate bands, Fermi surface, etc...
  logical, save :: fit_sigma = .false.     ! Fit self-energy by Lorentzians to make it smooth

  real(8), save :: ecut4fit = 10.d0        ! Cutoff energy for fitting functions
  integer, save :: n_lor = 101             ! Number of Lorentzians to fit functions

  logical, save :: kanamori = .false.          !  Parametrize U-matrix for 3-band case
  character(16), save :: rhtm = 'UNDEFINED'    !  Use real harmonics in TB-LMTO order 

  logical, save :: use_hmlt          !  Use hamiltonian version of code 
  logical, save :: cpa = .false.     ! Make CPA+DMFT type of calculations

  real(8), save :: ksi  = 0.02d0     !  External parameter for different purposes
  real(8), save :: zeta = 0.02d0     !  External parameter for different purposes

end module parameter_module
