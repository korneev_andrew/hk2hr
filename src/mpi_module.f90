module mpi_module
#ifdef MPI
  use mpi
#endif

  implicit none

  private

  integer, public, parameter :: master = 0
  integer, public :: proc_id, numprocs

  public :: initialize_mpi, stop_program, finalize_mpi, mpi_broadcast, ierr_handler_mpi, mpi_collect

  interface mpi_broadcast
    module procedure mpi_broadcast_d2, mpi_broadcast_d3, mpi_broadcast_d4, mpi_broadcast_c4, mpi_broadcast_c5
  end interface

  interface mpi_collect
    module procedure mpi_allreduce_real1, mpi_allreduce_real12,                &
                     mpi_allreduce_rnumber, mpi_allreduce_cnumber,             &
                     mpi_allreduce_complex1
  end interface

  contains

!======================================================================

  subroutine mpi_allreduce_rnumber( a, nscale )
    implicit none
    
    integer, intent(in   ) :: nscale
    real(8), intent(inout) :: a
    
    integer :: ierr
    real(8) :: b

#ifdef MPI
    ierr = 0

    call mpi_allreduce( a, b, 1, mpi_double_precision, mpi_sum, mpi_comm_world, ierr )

    a = b / real(nscale, 8)
#endif
    
  end subroutine mpi_allreduce_rnumber

  subroutine mpi_allreduce_cnumber( a, nscale )
    implicit none
    
    integer,    intent(in   ) :: nscale
    complex(8), intent(inout) :: a
    
    integer :: ierr
    complex(8) :: b

#ifdef MPI
    ierr = 0
    
    call mpi_allreduce( a, b, 1, mpi_double_complex, mpi_sum, mpi_comm_world, ierr )

    a = b / real(nscale, 8)
#endif
    
  end subroutine mpi_allreduce_cnumber

  subroutine mpi_allreduce_real1( array, nscale )
    implicit none
    
    integer, intent(in   ) :: nscale
    real(8), intent(inout) :: array(:)
    
    integer :: n, ierr
    real(8), allocatable :: b(:)

#ifdef MPI
    ierr = 0
    
    n = size( array )

    allocate( b(n) )
    b = 0

    call mpi_allreduce( array, b, n, mpi_double_precision, mpi_sum, mpi_comm_world, ierr )

    array = b / real(nscale, 8)

    deallocate( b )
#endif
    
  end subroutine mpi_allreduce_real1
  
  subroutine mpi_allreduce_complex1( array, nscale )
    implicit none
    
    integer,    intent(in   ) :: nscale
    complex(8), intent(inout) :: array(:)
    
    integer :: n, ierr
    complex(8), allocatable :: b(:)

#ifdef MPI
    ierr = 0
    
    n = size( array )

    allocate( b(n) )
    b = cmplx(0.d0,0.d0, 8)

    call mpi_allreduce( array, b, n, mpi_double_complex, mpi_sum, mpi_comm_world, ierr )

    array = b / real(nscale, 8)

    deallocate( b )
#endif

  end subroutine mpi_allreduce_complex1

  subroutine mpi_allreduce_real12( array, erra, nscale )
    implicit none
    
    integer, intent(in   ) :: nscale
    real(8), intent(inout) :: array(:)
    real(8), intent(inout) :: erra(:)

    integer :: n, ierr
    real(8), allocatable :: a_mean(:), a_spread(:)

#ifdef MPI
    ierr = 0
    
    n = size( array )
    
    allocate( a_mean(n), a_spread(n) )
    a_mean = 0
    call mpi_reduce( array, a_mean, n, mpi_double_precision, mpi_sum, master, mpi_comm_world, ierr )
    a_mean = a_mean / real(nscale, 8)
   
    call mpi_bcast( a_mean, n, mpi_double_precision, master, mpi_comm_world, ierr )

!     Error of array from all processors
    a_spread = ( a_mean - array )**2
    call mpi_reduce( a_spread, erra, n, mpi_double_precision, mpi_sum, master, mpi_comm_world, ierr )
    call mpi_bcast( erra, n, mpi_double_precision, master, mpi_comm_world, ierr )
    if( numprocs /= 1 ) erra = sqrt( erra / real( numprocs*(numprocs-1), 8 ) )

    array = a_mean
    
    deallocate( a_mean, a_spread )
#endif
    
  end subroutine mpi_allreduce_real12

  subroutine mpi_allreduce_real42( array, erra, nscale )
    implicit none
    
    integer, intent(in   ) :: nscale
    real(8), intent(inout) :: array(:,:,:,:)
    real(8), intent(inout) :: erra(:,:,:,:)

    integer :: n, ni, nj, nom, ns, ierr
    real(8), allocatable :: a_mean(:,:,:,:), a_spread(:,:,:,:)

#ifdef MPI
    ierr = 0
    
    ni  = size( array, dim=1 )
    nj  = size( array, dim=1 )
    nom = size( array, dim=3 )
    ns  = size( array, dim=4 )
    
    n = ni * nj * nom * ns
    
    allocate( a_mean(ni,nj,nom,ns), a_spread(ni,nj,nom,ns) )
    a_mean = 0.d0
    call mpi_reduce( array, a_mean, n, mpi_double_precision, mpi_sum, master, mpi_comm_world, ierr )
    a_mean = a_mean / real(nscale, 8)
   
    call mpi_bcast( a_mean, n, mpi_double_precision, master, mpi_comm_world, ierr )

!     Error of array from all processors
    a_spread = ( a_mean - array )**2
    call mpi_reduce( a_spread, erra, n, mpi_double_precision, mpi_sum, master, mpi_comm_world, ierr )
    if( numprocs /= 1 ) erra = sqrt( erra / real( numprocs*(numprocs-1), 8 ) )

    array = a_mean
    
    deallocate( a_mean, a_spread )
#endif
    
  end subroutine mpi_allreduce_real42

!======================================================================

  subroutine mpi_broadcast_d2( array, sender )
    implicit none
    integer, intent(in   ) :: sender
    real(8), intent(inout) :: array(:,:)

    integer :: ierr

#ifdef MPI
    ierr = 0
    
    call mpi_bcast( array, size(array), mpi_double_precision, sender, mpi_comm_world, ierr )

    if( ierr /= 0 ) call stop_program(' Error in mpi_broadcast_d4.$')
#endif

  end subroutine mpi_broadcast_d2

  subroutine mpi_broadcast_d3( array, sender )
    implicit none

    integer, intent(in   ) :: sender
    real(8), intent(inout) :: array(:,:,:)

    integer :: ierr

#ifdef MPI
    ierr = 0
    
    call mpi_bcast( array, size(array), mpi_double_precision, sender, mpi_comm_world, ierr )

    if( ierr /= 0 ) call stop_program(' Error in mpi_broadcast_d4.$')
#endif

  end subroutine mpi_broadcast_d3

  subroutine mpi_broadcast_d4( array, sender )
    implicit none

    integer, intent(in   ) :: sender
    real(8), intent(inout) :: array(:,:,:,:)

    integer :: ierr

#ifdef MPI
    ierr = 0
    
    call mpi_bcast( array, size(array), mpi_double_precision, sender, mpi_comm_world, ierr )

    if( ierr /= 0 ) call stop_program(' Error in mpi_broadcast_d4.$')
#endif

  end subroutine mpi_broadcast_d4

  subroutine mpi_broadcast_c4( array, sender )
    implicit none

    integer,    intent(in   ) :: sender
    complex(8), intent(inout) :: array(:,:,:,:)

    integer :: ierr

#ifdef MPI
    ierr = 0
    
    call mpi_bcast( array, size(array), mpi_double_complex, sender, mpi_comm_world, ierr )

    if( ierr /= 0 ) call stop_program(' Error in mpi_broadcast_c4.$')
#endif

  end subroutine mpi_broadcast_c4

  subroutine mpi_broadcast_c5( array, sender )
    implicit none

    integer,    intent(in   ) :: sender
    complex(8), intent(inout) :: array(:,:,:,:,:)

    integer :: ierr

#ifdef MPI
    ierr = 0
    
    call mpi_bcast( array, size(array), mpi_double_complex, sender, mpi_comm_world, ierr )

    if( ierr /= 0 ) call stop_program(' Error in mpi_broadcast_c5.$')
#endif

  end subroutine mpi_broadcast_c5

  subroutine initialize_mpi( ierr )
!
!  Initializes some variables for MPI (if -DMPI flag is present in Makefile)
!     or serial run
!
    implicit none
    integer, intent(  out) :: ierr
    
    ierr = 0
    
#ifdef MPI
!     Get in contact with MPI

    call mpi_init( ierr )
    if( ierr /= 0 )then
      write(6,*) 'Error at MPI_INIT, ierr = ', ierr
      return
    end if
    
    call mpi_comm_rank( mpi_comm_world, proc_id,  ierr )
    if( ierr /= 0 )then
      write(6,*) 'Error at MPI_COMM_RANK, ierr = ', ierr
      return
    end if
    
    call mpi_comm_size( mpi_comm_world, numprocs, ierr )
    if( ierr /= 0 )then
      write(6,*) 'Error at MPI_COMM_SIZE, ierr = ', ierr
      return
    end if

#else
!     Only for single-processor
    proc_id  = 0
    numprocs = 1
#endif

  end subroutine initialize_mpi

!======================================================================

  subroutine stop_program( a )
!
!  Stops the execution of program
!
    implicit none
    character(*)  :: a

    integer :: n, ierr
    character(64) :: b

    n = scan( a, '$' ) - 1
    
    b = trim(adjustl(a(1:n)))
    if( proc_id == master )  write(14,*) b

#ifdef MPI
    ierr = 0
    
    call mpi_finalize( ierr )
#endif

    stop

  end subroutine stop_program

!======================================================================

  subroutine finalize_mpi
!
!  Finalizes MPI
!
    implicit none

    integer :: ierr
#ifdef MPI
    call mpi_finalize( ierr )
#endif

  end subroutine finalize_mpi

  subroutine ierr_handler_mpi( ios, a )
    implicit none

    integer, intent(in   ) :: ios
    character(*) :: a

    integer :: n, ierr
    logical :: lierr, lierr_g
    character(64) :: b

    lierr   = .false.
    lierr_g = .false.

    if( ios /= 0 ) lierr = .true.

#ifdef MPI
    call mpi_allreduce( lierr, lierr_g, 1, mpi_logical, mpi_lor, mpi_comm_world, ierr )
#else
    lierr_g = lierr
#endif

    if( lierr_g )then
      n = scan( a, '$' ) - 1
      b = trim(adjustl(a(1:n)))
      if( proc_id == master )  write(14,*) b

#ifdef MPI
      call mpi_finalize( ierr )
#endif
      stop
    end if

  end subroutine ierr_handler_mpi

end module mpi_module
