subroutine random_ini( pr_id )
!
!   Initialize random number generators for all processors
!
  implicit none

  integer, intent(in   ) :: pr_id   !    Processor number

  integer :: i, n, k
  integer :: dt(8)
  integer, allocatable :: seed(:)

  call random_seed( size=n )
  allocate( seed(n) )

  k = 2
  k = k**31 - 2

  do i = 1, n
    call date_and_time( values=dt )  
    dt = dt + 1
    seed(i) = mod( dt(8),k ) + dt(7)*i + dt(6)*pr_id
  end do

!  call system_clock( count=clock )
!  seed = clock + (37 + pr_id) * (/ (i - 1, i = 1, n) /)

  call random_seed( put=seed )

  deallocate( seed )

end subroutine random_ini
