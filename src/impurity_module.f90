module impurity_module
  implicit none
  public

  integer :: n_imp_type = 1

  type, public :: imp
!-----------------------------------------------------------------------------------------------
!      Main
    character(8)     :: name               !  Impurity name
    integer          :: nlm                !  (2*l+1) - # of orbitals
    integer          :: n_imp              !  # of impurities of this type
    integer, pointer :: hposition(:)       !  Positions in Hamiltonian
    integer, pointer :: magtype(:)         !  Magnetic ordering
    character(8)     :: solver             !  Type of quantum impurity solver to use
    integer, pointer :: smask(:,:)         !  Orbital symmetry mask
    logical          :: cpoc_only          !  Use only constant potential correction
    real(8)          :: h_field_local      !  Local magnetic field that will be directed according magtype

!-----------------------------------------------------------------------------------------------
!      Interaction                                            
    real(8)          :: u_value            !  Screened Coulomb interaction
    real(8)          :: j_value            !  Hund exchange
    real(8), pointer :: ummss(:,:)         !  Density-density part of U-matrix
    real(8), pointer :: lcs(:,:)           !  Transformation matrix into an LCS
    real(8), pointer :: umtrx(:,:,:,:)     !  Full rotationally invariant U matrix
    real(8)          :: F(0:6)             !  Slater integrals
    real(8), pointer :: e_d(:,:,:)         !  Position of impurity level
    character(8)     :: dc_type            !  Type of double counting correction
    real(8), pointer :: dcc(:)             !  Value of double counting
    real(8)          :: xsfll              !  Factor for simplified FLL

!-----------------------------------------------------------------------------------------------
!      ED
    integer :: nbath            !   Number of bathes for ED
    integer :: Nd_min           !   Minimal # of correlated electrons
    integer :: Nd_max           !   Maximal # of correlated electrons
    integer :: Nc_max           !   Maximal # of excited electrons
    integer :: Nh_max           !   Maximal # of holes
    logical :: lsisj            !   Calculate < s_i,s_j > correlators in ED
    logical :: lunn             !   Use only density-density U-matrix in ED

    integer :: ed_opt           !   Optimization = 0, 1, 2

    integer :: ed_arno_nev      !   Number of lowest eigenvalues to find in Arnoldi
    integer :: ed_arno_ncv0     !   ncv = 2*newnev + ncv0
    real(8) :: ed_arno_tol      !   Percentage from NEV to find in one sector

    integer :: ed_gf_nev        !   Number of lowest eigenvalues to find in Arnoldi
    real(8) :: ed_gf_tol        !   Boltzmann factor cutoff
    real(8) :: ed_gf_decomp     !   Probability cut for eigenvector decomposition

    real(8), pointer :: Epsk(:,:)   !   Bath's energies
    real(8), pointer :: Vk(:,:,:)   !   Hybridization to bath's orbitals

!-----------------------------------------------------------------------------------------------
!      Fit
    integer :: fit            !   Define choice of mathematical routine to fit
    logical :: fix_ed         !   Fix impurity level in fit of G0
    integer :: weight         !   Type of uncertainty function
    real(8) :: ufp(3)         !   Fiting parameters to define uncertainty function
                                 !                  (amplitude,position,spread)

!-----------------------------------------------------------------------------------------------
!      Arrays
    complex(8), pointer :: Sigma(:,:,:,:)          !   Sigma(iw)
    complex(8), pointer :: Sigma_old(:,:,:,:)      !   Sigma(iw) from -1 interation
    complex(8), pointer :: Green(:,:,:,:)          !   G(iw)
    complex(8), pointer :: Green0(:,:,:,:)         !   G0(iw)
    complex(8), pointer :: Delta(:,:,:,:)          !   Delta(iw)
    real(8),    pointer :: moments_g(:,:,:,:)      !   Moments of G
    real(8),    pointer :: moments_g0(:,:,:,:)     !   Moments of G0
    real(8),    pointer :: moments_sigma(:,:,:,:)  !   Moments of Sigma
    real(8),    pointer :: cpoc(:,:,:)             !   Constant potential correction of impurity level
    real(8),    pointer :: occupation(:,:,:)       !   Occupation of the Green function
    real(8),    pointer :: n_cor(:)                !   Correlator < n >    (out of QMC)
    real(8),    pointer :: ninj(:,:)               !   Correlator < n n >  (out of QMC)
    real(8)             :: x_cpa                   !   Concentration of this sort for CPA

!-----------------------------------------------------------------------------------------------
!      QMC
    integer          :: nqmc              !   Number of QMC measurements (sweeps)
    integer          :: nqmcwarm          !   Number of warming cycles (sweeps)
    logical          :: lninj             !   Calculate < n_i(t),n_j(t') > correlators in QMC
    real(8), pointer :: Gtau(:,:,:,:)     !   G(tau)
    real(8), pointer :: G0tau(:,:,:,:)    !   G0(tau)

!-----------------------------------------------------------------------------------------------
!      CT-QMC-HYB 
    integer :: nlegendre                   !   Number of Legendre polinoms to use
    integer, pointer :: legendre_cutoff(:) !   Legendre cutoff
    integer :: use_arg_cache               !   Use argument cache for non-tau calculations
    integer :: legendre_transform          !   Use legendre polynomials to transform into matsubara space
    integer :: meas_bin                    !   How often to do QMC measurements per sweep
    integer :: nupdates                    !   Number of updates before one measurement
    integer :: observable_bin_size         !   Bin size for jack-knife observables

    integer :: max_qmc_time                !   Maximum time to perform one QMC calculation
    logical :: improved_estimator          !   Use improved estimator (SigmaG) for self-energy calculation
    logical :: measure_sector_statistics   !   Measure sector statistics
    logical :: measure_green_omega         !   Measure Green(w)
    integer :: L4Delta                     !   Number of points for Delta (hybridization) function

    character(256) :: ctqmc_updates        !   Update types and their weights to perform during calculation
    character(256) :: random_generator     !   Boost built-in "boost_mt19937" or SSE2 optimized Mersenne-Twister "dsfmt"
!-----------------------------------------------------------------------------------------------
!      DOS    
    integer          :: ndos            !  Number of points in bare DOS
    real(8), pointer :: edos(:)         !  Energy mesh for bare DOS
    real(8), pointer :: dos(:,:,:)      !  Bare (LDA) DOS

  end type imp


  type(imp), allocatable, save :: impurity(:)
  
end module impurity_module
