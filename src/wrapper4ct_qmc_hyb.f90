subroutine wrapper4ct_qmc_hyb( itype )
!
!    This is calling sequence for CT-QMC-HYB solver
!
  use parameter_module
  use impurity_module
  use iso_c_binding
  use tools_module
  use iolib_module
  use energy_module
  use moments_module
  use mpi_module
  use math_module
  use symmetry_module

  implicit none

! Fortran interfaces to C++ library
  include 'libcthyb_finterface.h'

  integer, intent(in) :: itype

!   Local variables

  integer :: num_flavors

  integer :: i, is, iom, ierr, n_cut
  integer, parameter :: num_functions = 4, fun_data = 1, fun_error = 2, fun_variance = 3, fun_tau = 4

  logical :: lmom(0:3) = .true.

  real(8) :: xmu_save, m_z2, rnd
  real(8), allocatable :: qlm(:,:,:)
  character(64) :: prob_fmt

!   For communication with CT-QMC-HYB lib
  integer(8) :: solver, results, delta_num_points
  real(c_double), allocatable :: hybridization(:,:)
  real(c_double), allocatable :: Eimp(:)

  integer(c_int) :: num_legendre, arg_cache_size
  integer(c_int) :: vmpeak, vmsize, vmrss
  real(c_double), allocatable :: legendre_moments(:,:)
  integer(c_int), allocatable :: legendre_predicted(:), legendre_cutoff(:)

  real(c_double) :: fraction_done, last_fraction_done
  real(c_double), allocatable :: green_tau(:,:,:), sigmag_tau(:,:,:), green_legendre(:,:,:), sigmag_legendre(:,:,:),   &
                                 tau_function(:,:), density(:,:), wsign(:), statistics(:,:), sector_statistics(:),     &
                                 nnt(:,:,:,:), matrix_size(:), energy_estimator(:), gtmp(:,:,:,:)
  complex(c_double_complex), allocatable :: fnc(:,:,:), omega_function(:,:)
  integer(c_int), allocatable :: states(:,:)

  logical(c_bool), parameter :: c_true = .true. , c_false = .false.
  logical(c_bool) :: measure_green_legendre, measure_green_omega, measure_sigmag, measure_sector_statistics,           &
                     measure_nnt, use_arg_cache

!-------------------------------------------------------------------------------  

  if( so == 1 )then
    num_flavors = impurity(itype)%nlm
  else
    num_flavors = impurity(itype)%nlm * 2
  end if

  xmu_save = xmu
  xmu = 0.d0
  call complex_energy_mesh

!   Calculates Delta of effective medium: Delta = iwn - e_d - G0^{-1}

  impurity(itype)%Delta = -impurity(itype)%Green0
  call inverse( impurity(itype)%Delta )
  forall( i = 1:impurity(itype)%nlm, is = 1:nspin ) impurity(itype)%Delta(i,i,:,is) = impurity(itype)%Delta(i,i,:,is) + zenergy

!     Define constant term in real part

  if( imenergy )then 
    call get_moments( impurity(itype)%Delta(:,:,n_energy-ntail:n_energy,:), fermionic(n_energy-ntail:n_energy),        &
                      impurity(itype)%moments_g0, lmom )
    impurity(itype)%e_d = impurity(itype)%moments_g0(0,:,:,:)
  else
      impurity(itype)%e_d = real( impurity(itype)%Delta(:,:,1,:) + impurity(itype)%Delta(:,:,n_energy,:), 8 ) / 2
  end if

  forall( iom = 1:n_energy ) impurity(itype)%Delta(:,:,iom,:) = impurity(itype)%Delta(:,:,iom,:) - impurity(itype)%e_d

!   Particle-hole symmetry case
#ifdef PHsym
  impurity(itype)%Delta = cmplx( 0.d0, imag(impurity(itype)%Delta), 8 )
#endif

!     Print out
  if( verbos > 0 )      call write_matrix( 14, impurity(itype)%e_d, 'Calculated e_d$', 'Spin # $', ierr )
  if( verbos >= 15 )    call write_function( mkfn('Delta',resuf,itype), impurity(itype)%Delta, prn_mesh, ierr, 'a' )

  impurity(itype)%e_d = impurity(itype)%e_d + impurity(itype)%cpoc
  if( impurity(itype)%cpoc_only ) impurity(itype)%e_d = impurity(itype)%cpoc

!     Print out
  if( verbos > 0 .and. sum( abs(impurity(itype)%cpoc) ) > 1.e-8 )                                                      &
    call write_matrix( 14, impurity(itype)%e_d, 'Used e_d$', 'Spin # $', ierr )

!   Fourier transform of Delta to tau domain
  call get_moments( impurity(itype)%Delta(:,:,n_energy-ntail:n_energy,:), fermionic(n_energy-ntail:n_energy),          &
                    impurity(itype)%moments_g0, lmom )

  call fourier( impurity(itype)%Delta, impurity(itype)%G0tau, fermionic, impurity(itype)%moments_g0, 1 )

!     Print out
  if( verbos >= 10 )  call write_function( mkfn('Delta_tau',itype), impurity(itype)%G0tau, tau_mesh, ierr, 'a' )

!   Use simulated annealing and analytically transform Delta to tau
  if( impurity(itype)%fit == 1 )then
    n_cut = floor( ( ecut4fit * beta / pi + 1 ) / 2 )
    call fourier( impurity(itype)%Delta, zenergy, n_cut, impurity(itype)%G0tau, n_lor,                                   &
                  impurity(itype)%moments_g0(1,:,:,:), emin, emax, ierr )

    if( verbos >= 20 )  call write_function( mkfn('Delta_fit',itype), impurity(itype)%G0tau, ierr, 'a' )
  end if

  call set_symmetry( impurity(itype)%G0tau, impurity(itype)%smask )
!
!   Prepare data in the format required by CT-QMC-HYB library
!   and calling CT-QMC-HYB subroutines

!   Position of impurity level
  allocate( Eimp(num_flavors), qlm(num_flavors/2,num_flavors/2,2), hybridization(num_flavors, impurity(itype)%L4Delta) )
  Eimp = 0.d0
  qlm  = 0.d0
  hybridization = 0.d0

  if( nspin == 1 )then
! so case: nspin == 1 but n_orb == num_flavors
    if( so == 1 )then
      forall( i = 1:num_flavors )
        hybridization(i,:) = impurity(itype)%G0tau(i,i,:,1)
        Eimp(i) = -impurity(itype)%e_d(i,i,1) - impurity(itype)%dcc(1)
      end forall
    else
! general nspin == 1: i then i+n_orb from i
      forall( i = 1:(num_flavors/2) )
        hybridization(i,:)               = impurity(itype)%G0tau(i,i,:,1)
        hybridization(i+num_flavors/2,:) = impurity(itype)%G0tau(i,i,:,1)
        Eimp(i)               = -impurity(itype)%e_d(i,i,1) - impurity(itype)%dcc(1)
        Eimp(i+num_flavors/2) = -impurity(itype)%e_d(i,i,1) - impurity(itype)%dcc(1)
      end forall
    end if
  else
! npin == 2
    forall( i = 1:(num_flavors/2), is = 1:nspin )
      hybridization(i + (is-1) * num_flavors/2,:) = impurity(itype)%G0tau(i,i,:,is)
      Eimp(i + (is-1) * num_flavors/2) = -impurity(itype)%e_d(i,i,is) - impurity(itype)%dcc(is)
    end forall
  end if

  num_legendre = max( 1, impurity(itype)%nlegendre )

  measure_green_legendre = c_false
  if( impurity(itype)%nlegendre > 0 )  measure_green_legendre = c_true

  measure_sigmag = impurity(itype)%improved_estimator
  measure_green_omega = impurity(itype)%measure_green_omega
  measure_nnt = impurity(itype)%lninj
  measure_sector_statistics = impurity(itype)%measure_sector_statistics

  use_arg_cache = .false.
  arg_cache_size = 0
  if( impurity(itype)%use_arg_cache > 0 )then
    use_arg_cache = .true.
    arg_cache_size = impurity(itype)%use_arg_cache
  endif

  call random_number( rnd )

  solver = f_init_solver( verbos, int( 1.e6 * rnd ) + proc_id )

  call c_set_parameter_int( solver, 'thermalization_sweeps'//CHAR(0), impurity(itype)%nqmcwarm )
  call c_set_parameter_int( solver, 'total_sweeps'//CHAR(0), impurity(itype)%nqmc )
  call c_set_parameter_int( solver, 'number_of_measurements'//CHAR(0), impurity(itype)%meas_bin )
  call c_set_parameter_int( solver, 'number_of_updates'//CHAR(0), impurity(itype)%nupdates )
  call c_set_parameter_int( solver, 'observable_bin_size'//CHAR(0), impurity(itype)%observable_bin_size )
  call c_set_parameter_int( solver, 'number_of_tau_slices'//CHAR(0), Ltau-1 )
  call c_set_parameter_int( solver, 'number_of_flavors'//CHAR(0), num_flavors )
  call c_set_parameter_int( solver, 'maximum_time'//CHAR(0), impurity(itype)%max_qmc_time )
  call c_set_parameter_int( solver, 'nnt_slices'//CHAR(0), Ltau-1 )
  call c_set_parameter_int( solver, 'number_of_matsubara_frequencies'//CHAR(0), n_energy )
  call c_set_parameter_int( solver, 'number_of_legendre_coefficients'//CHAR(0), num_legendre )
  call c_set_parameter_int( solver, 'arg_cache_size'//CHAR(0), arg_cache_size )
  call c_set_parameter_bool( solver, 'use_arg_cache'//CHAR(0), use_arg_cache )
  call c_set_parameter_bool( solver, 'measure_nn'//CHAR(0), c_true )
  call c_set_parameter_bool( solver, 'measure_nnt'//CHAR(0), measure_nnt )
  call c_set_parameter_bool( solver, 'measure_green_legendre'//CHAR(0), measure_green_legendre )
  call c_set_parameter_bool( solver, 'measure_green_omega'//CHAR(0), measure_green_omega )
  call c_set_parameter_bool( solver, 'measure_sigmag'//CHAR(0), measure_sigmag )
  call c_set_parameter_bool( solver, 'measure_sector_statistics'//CHAR(0), measure_sector_statistics )
  call c_set_parameter_double( solver, 'beta'//CHAR(0), beta )
  call c_set_parameter_string( solver, 'updates'//CHAR(0), impurity(itype)%ctqmc_updates )
  call c_set_parameter_string( solver, 'random_generator'//CHAR(0), impurity(itype)%random_generator )

! this is to keep variable size (int64_t)
  delta_num_points = impurity(itype)%L4Delta
  call f_set_system( solver, delta_num_points, hybridization, Eimp, impurity(itype)%ummss )

!   Memory usage info
  if( verbos >= 10 ) then
    call c_mem_info( vmpeak, vmsize, vmrss )
    write(14,"(/,' Pre memory usage:',/,4x,'VmPeak',i8,' kB',/,4x,'VmSize',i8,' kB',/,4x,'VmRSS ',i8,' kB')") vmpeak, vmsize, vmrss
  end if

  fraction_done = 0.d0
  last_fraction_done = 0.1d0

  if( verbos > 0 ) then
    write(14,"(/,10x,'Start of CT-QMC simulation')")
    if( measure_green_legendre )then
      write(14,"(5x,'Using Legendre representation for G(tau)',/)")
    elseif( measure_green_omega )then
      write(14,"(5x,'Calculating G(w) directly on Matsubara grid',/)")
    else
      write(14,"(5x,'Calculating G(tau) -> FT -> for G(w) (classical)',/)")
    end if

    if( measure_sigmag ) write(14,"(5x,'Self-energy is calculated within solver')")
  end if

  do while( fraction_done < 1.d0 )
      call c_make_sweep( solver )

      fraction_done = c_fraction_done( solver )

      if( verbos > 0 .and. fraction_done >= last_fraction_done-0.01d0 )then
          write(14,"(' QMC simulations: ',i3,' % completed')") nint( 100*last_fraction_done )
          last_fraction_done = last_fraction_done + 0.1d0
      end if
  end do

  results = f_mc_results( solver )

!  Memory usage info
  if( verbos >= 10 )then
    call c_mem_info( vmpeak, vmsize, vmrss )
    write(14,"(/,' Post memory usage:',/,4x,'VmPeak',i8,' kB',/,4x,'VmSize',i8,' kB',/,4x,'VmRSS ',i8,' kB')")         &
                   vmpeak, vmsize, vmrss
  end if

  allocate( green_tau(num_flavors, Ltau, num_functions) )
  green_tau = 0.d0

  allocate( density(num_flavors, num_functions) )
  density = 0.d0

  allocate( matrix_size(num_flavors) ) 
  impurity(itype)%ninj = 0.d0
  matrix_size = 0.d0

  allocate( wsign(1) )
  wsign = 0.d0

  allocate( statistics(5,3) )
  statistics = 0.d0

  allocate( energy_estimator(2) )

  call f_get_result( solver, results, 0, green_tau )
  call f_get_result( solver, results, 7, density )
  call f_get_result( solver, results, 9, impurity(itype)%ninj )
  call f_get_result( solver, results, 4, matrix_size )
  call f_get_result( solver, results, 8, wsign )
  call f_get_result( solver, results, 11, statistics )
  call f_get_result( solver, results, 21, energy_estimator )

  green_tau(:,1,fun_data)    = - 1.d0 + density(:,fun_data)
  green_tau(:,Ltau,fun_data) = - density(:,fun_data)

  call vector2diagonal_matrix( green_tau(:,:,fun_data), impurity(itype)%Gtau, ierr )
  if( ierr /= 0 ) call stop_program( " Cannot copy Green function.$" )

  call vector2diagonal_matrix( density(:,fun_data), qlm, ierr )
  if( ierr /= 0 ) call stop_program( " Cannot copy occupation.$" )

  if( verbos > 0 )then
    call write_function( mkfn('Gtau',itype), impurity(itype)%Gtau, tau_mesh, ierr, 'a' )

    call write_matrix( 14, qlm, 'G(tau=0)$', 'Spin$', 'Number of electrons in spin channel$', ierr )

    write(14,"(' Number of impurity electrons',12x,f12.8,/,' Magnetization',27x,f12.8)")                    &
          spur( qlm(:,:,2)+qlm(:,:,1), num_flavors/2 ), spur( qlm(:,:,2)-qlm(:,:,1), num_flavors/2 )

    call write_matrix( 14, impurity(itype)%ninj, 'Correlator < n_i,n_j >$', ierr )

!     Instant squared magnetic moment
    m_z2 = sum( impurity(itype)%ninj(1:(num_flavors/2),1:(num_flavors/2)) )       +                         &
           sum( impurity(itype)%ninj(num_flavors/2+1:num_flavors,num_flavors/2+1:num_flavors) )    -        &
           sum( impurity(itype)%ninj(1:(num_flavors/2),num_flavors/2+1:num_flavors) )              -        &
           sum( impurity(itype)%ninj(num_flavors/2+1:num_flavors,1:(num_flavors/2)) )
    write(14,"(' Instant squared magnetic moment, < m_z^2 >, ',f12.8)") m_z2

!     Calculate Hartree-Fock energy, etc
    call hartree_fock_energy( impurity(itype)%umtrx, impurity(itype)%ummss, qlm, impurity(itype)%ninj, num_flavors/2, nspin )

    write(14,"(' Potential energy estimator:',f12.8)")   energy_estimator(1)
    write(14,"(' Kinetic energy estimator:  ',f12.8,/)") energy_estimator(2)

    write(14,"('   Average perturbation order',/,16x,'Up  Down')")
    do i = 1, num_flavors
      write(14,"(' Spin-orbital (flavor) # ',i2,3(1x,f10.6))") i, matrix_size(i)
    end do

    call print_qmc_statistics( statistics )

!    Print variance and autocorrelation
    allocate( gtmp(impurity(itype)%nlm,impurity(itype)%nlm,Ltau,nspin) )

    call vector2diagonal_matrix( green_tau(:,:,fun_variance), gtmp, ierr )
    if( ierr /= 0 ) call stop_program( " Cannot copy variance.$" )
    call write_function( mkfn('Gtau_var',itype), gtmp, tau_mesh, ierr, 'a' )

    if( verbos >= 25 )then
      call vector2diagonal_matrix( green_tau(:,:,fun_tau), gtmp, ierr )
      if( ierr /= 0 ) call stop_program( " Cannot copy autocorrelation.$" )
      call write_function( mkfn('Gtau_actime',itype), gtmp, tau_mesh, ierr, 'a' )
    end if

    deallocate( gtmp )
  end if

!     Atomic weight
  if( measure_sector_statistics .and. verbos > 0 )then
    allocate( sector_statistics(2**num_flavors), states(2**num_flavors,num_flavors) )
    sector_statistics = 0.0
    states = 0

    call f_get_result( solver, results, 18, sector_statistics )
    call c_get_states( solver, states )

    call print_sector_statistics( states, sector_statistics, num_flavors )

    prob_fmt = mkfn( 'Sector_statistics', itype )
    prob_fmt = trim(adjustl(prob_fmt))//'.dat'
    open( 11, file=prob_fmt, form='formatted', position='rewind' )

    do i = 1, 2**num_flavors
      write(prob_fmt,*) num_flavors
      write(11,"(' '," // adjustl(prob_fmt) // "i2,f14.7)") states(i,1:num_flavors), sector_statistics(i)
    end do
    close(11)

    deallocate( states, sector_statistics )
  end if

  if( measure_nnt .and. verbos > 0 )then
    allocate( nnt(num_flavors/2, num_flavors/2, Ltau, 4) )
    nnt = 0.0
    call f_get_result( solver, results, 5, nnt )
    call write_function( mkfn('ninj',itype), nnt, tau_mesh, ierr, 'a' )
    deallocate( nnt )
  end if

!-------------------------------------------------------------------------------
!     Use Legendre representation to calculate Green function, etc

  if( measure_green_legendre )then
    allocate( green_legendre(num_flavors,num_legendre,num_functions), legendre_moments(num_flavors,num_legendre),      &
              legendre_predicted(num_flavors), legendre_cutoff(num_flavors) )
    green_legendre = 0.0
    legendre_moments = 0.0
    legendre_predicted = 0
    legendre_cutoff = impurity(itype)%legendre_cutoff

    call f_get_result( solver, results, 1, green_legendre )

    allocate( omega_function(num_flavors,n_energy) )
    omega_function = cmplx( 0, 0, c_double_complex )

    call f_transform_legendre( solver, green_legendre, omega_function, legendre_moments, legendre_predicted, legendre_cutoff )

    call vector2diagonal_matrix( omega_function, impurity(itype)%Green, ierr )
    if( ierr /= 0 ) call stop_program( " Cannot copy Green function.$" )

    if( verbos > 0 )then
      do i = 1, num_flavors
        write(14,"(' Predicted optimal legendre ',i3,i3)") i, legendre_predicted(i)
      end do
      do i = 1, num_flavors
        write(14,"(' Used cutoff values ',i3,i3)") i, legendre_cutoff(i)
      end do
    end if

    if( measure_sigmag )then
      allocate( sigmag_legendre(num_flavors,num_legendre,num_functions) )
      sigmag_legendre = 0.d0
      omega_function = cmplx( 0, 0, c_double_complex )

      call f_get_result( solver, results, 10, sigmag_legendre )
      call f_sigma_legendre( solver, green_legendre, sigmag_legendre, omega_function, legendre_cutoff )

      call vector2diagonal_matrix( omega_function, impurity(itype)%Sigma, ierr )
      if( ierr /= 0 ) call stop_program( " Cannot copy Sigma function.$" )

      forall( i = 1:(num_flavors/2), is = 1:nspin )                                                                        &
        impurity(itype)%Sigma(i,i,:,is) = impurity(itype)%Sigma(i,i,:,is) + cmplx( impurity(itype)%dcc(is), 0, 8 )

 !     if( nspin == 1 )then
 !       if( so == 1 )then
 !         forall( i = 1:num_flavors ) impurity(itype)%Sigma(i,i,:,1) = omega_function(i,:) + cmplx( impurity(itype)%dcc(1), 0, 8 )
 !       else
 !         forall( i = 1:(num_flavors/2) )                                                                              &
 !           impurity(itype)%Sigma(i,i,:,1) = ( omega_function(i,:) + omega_function(i+num_flavors/2,:) ) * 0.5d0       &
 !                                                                      + cmplx( impurity(itype)%dcc(1), 0.d0, 8 )
 !       end if
 !     else
 !       forall( i = 1:(num_flavors/2) )
 !         impurity(itype)%Sigma(i,i,:,1) = omega_function(i,:) + cmplx( impurity(itype)%dcc(1), 0.d0, 8 )
 !         impurity(itype)%Sigma(i,i,:,2) = omega_function(i+num_flavors/2,:) + cmplx( impurity(itype)%dcc(2), 0.d0, 8 )
 !       end forall
 !     end if

      deallocate( sigmag_legendre )
    end if

    deallocate( green_legendre, legendre_moments, legendre_predicted, legendre_cutoff, omega_function )

!-------------------------------------------------------------------------------
!      Measure Green function directly on Matsubara grid

  else if( measure_green_omega )then

    allocate( fnc(num_flavors,n_energy,num_functions) )
    fnc = cmplx( 0, 0, c_double_complex )

    call f_get_complex_result( solver, results, 2, fnc )

    call vector2diagonal_matrix( fnc(:,:,fun_data), impurity(itype)%Green, ierr )
    if( ierr /= 0 ) call stop_program( " Cannot copy Green function.$" )

    if( measure_sigmag )then
      fnc = cmplx( 0, 0, c_double_complex )

      call f_get_complex_result( solver, results, 14, fnc )

      if( verbos >= 25 ) call write_function( mkfn('SigmaG',itype), fnc, prn_mesh, ierr, 'a' )
 
      if( nspin == 1 )then
        if( so == 1 )then
          forall( i = 1:num_flavors, iom = 1:n_energy )                                                                &
            impurity(itype)%Sigma(i,i,iom,1) = fnc(i,iom,fun_data) / impurity(itype)%Green(i,i,iom,1)   +              &
                                               cmplx( impurity(itype)%dcc(1), 0.d0, 8 )
        else
          forall( i = 1:(num_flavors/2), iom = 1:n_energy )                                                            &
            impurity(itype)%Sigma(i,i,iom,1) = ( fnc(i,iom,fun_data) + fnc(i+num_flavors/2,iom,fun_data) ) * 0.5d0 /   &
                                                 impurity(itype)%Green(i,i,iom,1) + cmplx( impurity(itype)%dcc(1), 0.d0, 8 )
        endif
      else
        forall( i = 1:(num_flavors/2), iom = 1:n_energy )
          impurity(itype)%Sigma(i,i,iom,1) = fnc(i,iom,fun_data) / impurity(itype)%Green(i,i,iom,1) +                  &
                                             cmplx( impurity(itype)%dcc(1), 0.d0, 8 )
          impurity(itype)%Sigma(i,i,iom,2) = fnc(i+num_flavors/2,iom,fun_data) / impurity(itype)%Green(i,i,iom,2)  +   &
                                             cmplx( impurity(itype)%dcc(2), 0.d0, 8 )
        end forall
      endif
    end if

    deallocate( fnc )
!-------------------------------------------------------------------------------
!     Use legendre polynomials for Fourier transform
  else if( impurity(itype)%legendre_transform /= 0 )then 
    allocate( tau_function(num_flavors, Ltau), omega_function(num_flavors, n_energy) )
    tau_function(:,:) = -green_tau(:,:,fun_data)

    call f_omega_from_tau( solver, tau_function, omega_function, impurity(itype)%legendre_transform )
 
    if( verbos >= 25 ) call write_function( mkfn('G_tau_l_iw',itype), omega_function, prn_mesh, ierr, 'a' )

    call vector2diagonal_matrix( omega_function, impurity(itype)%Green, ierr )
    if( ierr /= 0 ) call stop_program( " Cannot copy Green function.$" )

    if( measure_sigmag ) then
      allocate( sigmag_tau(num_flavors,Ltau,num_functions) )
      sigmag_tau = 0.0

      call f_get_result( solver, results, 19, sigmag_tau )

      if( verbos >= 25 ) call write_function( mkfn('SigmaGtau',itype), sigmag_tau(:,:,fun_data), tau_mesh, ierr, 'a' )

      tau_function = - sigmag_tau(:,:,fun_data)

      deallocate(sigmag_tau)

      call f_omega_from_tau( solver, tau_function, omega_function, impurity(itype)%legendre_transform )

      if( verbos >= 25 ) call write_function( mkfn('SigmaG_tau_l_iw',itype), omega_function, prn_mesh, ierr, 'a' )

      if( nspin == 1 )then
        if( so == 1 )then
          forall( i = 1:num_flavors, iom = 1:n_energy )                                                     &
            impurity(itype)%Sigma(i,i,iom,1) = omega_function(i,iom) / impurity(itype)%Green(i,i,iom,1) +   &
                                               cmplx( impurity(itype)%dcc(1), 0.d0, 8 )
        else
          forall( i = 1:(num_flavors/2), iom = 1:n_energy )                                                 &
            impurity(itype)%Sigma(i,i,iom,1) = ( (omega_function(i,iom) + omega_function(i+num_flavors/2,iom)) * 0.5) /  &
                                        impurity(itype)%Green(i,i,iom,1) + cmplx( impurity(itype)%dcc(1), 0.d0, 8 )
        end if
      else
        forall( i = 1:(num_flavors/2), iom = 1:n_energy )
          impurity(itype)%Sigma(i,i,iom,1) = omega_function(i,iom) / impurity(itype)%Green(i,i,iom,1)  +                 &
                                             cmplx( impurity(itype)%dcc(1), 0.d0, 8 )
          impurity(itype)%Sigma(i,i,iom,2) = omega_function(i+num_flavors/2,iom) / impurity(itype)%Green(i,i,iom,2) +    &
                                             cmplx( impurity(itype)%dcc(2), 0.d0, 8 )
        end forall
      end if
    end if

    deallocate( tau_function, omega_function )
!-------------------------------------------------------------------------------
!     Measure Green function on imaginary time grid (classical)
  else
    call fourier( impurity(itype)%Gtau(:,:,1:Ltau-1,:), impurity(itype)%Green, fermionic )

    if( measure_sigmag )then
      allocate( sigmag_tau(num_flavors,Ltau,num_functions) )
      sigmag_tau = 0.d0

      call f_get_result( solver, results, 19, sigmag_tau )

      if( verbos >= 25 ) call write_function( mkfn('SigmaGtau',itype), sigmag_tau(:,:,fun_data), tau_mesh, ierr, 'a' )

      call vector2diagonal_matrix( sigmag_tau(:,:,fun_data), impurity(itype)%G0tau, ierr )
      if( ierr /= 0 ) call stop_program( " Cannot copy SigmaG function.$" )

      deallocate( sigmag_tau )

      call fourier( impurity(itype)%G0tau(:,:,1:Ltau-1,:), impurity(itype)%Delta, fermionic )

      if( verbos >= 25 ) call write_function( mkfn('SigmaGLiw',itype), impurity(itype)%Delta, prn_mesh, ierr, 'a' )
 
      if( so == 1 )then
        forall( i = 1:num_flavors, is = 1:nspin, iom = 1:n_energy )
          impurity(itype)%Sigma(i,i,iom,is) = impurity(itype)%Delta(i,i,iom,is) / impurity(itype)%Green(i,i,iom,is) +  &
                                              cmplx(impurity(itype)%dcc(is), 0.d0, 8 )
        end forall
      else
        forall( i = 1:(num_flavors/2), is = 1:nspin, iom = 1:n_energy )
          impurity(itype)%Sigma(i,i,iom,is) = impurity(itype)%Delta(i,i,iom,is) / impurity(itype)%Green(i,i,iom,is) +  &
                                              cmplx(impurity(itype)%dcc(is), 0.d0, 8 )
        end forall
      endif
    end if
  end if

  if( verbos >= 25 )then
    call write_function( mkfn('Gw_ctqmc',itype), impurity(itype)%Green, prn_mesh, ierr, 'a' )
    if( measure_sigmag ) call write_function( mkfn('SigmaImproved',itype), impurity(itype)%Sigma, prn_mesh, ierr, 'a' )
  end if

  deallocate( green_tau, density, Eimp, qlm, matrix_size, statistics, wsign, energy_estimator )

  call f_destroy_solver( solver )
  call f_destroy_results( results )

  xmu = xmu_save
  call complex_energy_mesh

end subroutine wrapper4ct_qmc_hyb

subroutine print_sector_statistics( states, sector_statistics, n )
  implicit none

  integer, intent(in   ) :: n
  integer, intent(in   ) :: states(2**n,n)
  real(8), intent(in   ) :: sector_statistics(2**n)

  integer :: i, j, nup, ndn
  real(8), allocatable :: ntot(:), sz(:,:)
  character(128) :: prn_str
  character(10)  :: prn_val

  allocate( ntot(0:n), sz(0:n,0:n/2) )
  ntot = 0
  sz   = 0

  do i = 0, 2**n
    nup = sum( states(i,1:n/2) )
    ndn = sum( states(i,n/2+1:n) )

    ntot(nup+ndn)            = ntot(nup+ndn)    +   sector_statistics(i)
    sz(nup+ndn,abs(nup-ndn)) = sz(nup+ndn,abs(nup-ndn)) + sector_statistics(i)
  end do

  write(14,"(/,10x,'Sector statistics',/,' Ntot    Weight     Sz(i)',7(2x,i5,5x))") ( i, i = 0, n/2 )

  do i = 0, n
    prn_str = '|'
    prn_val = ''

    do j = 0, n/2
      if( sz(i,j) == 0 )then
        prn_str = trim(prn_str)//'        ----'
      else
        write(prn_val,"(f10.7)") sz(i,j)
        prn_str = trim(prn_str)//'  '//trim(prn_val)
      end if
    end do

    write(14,"(i3,2x,f12.7,4x,a)") i, ntot(i), trim( adjustl( prn_str ) )
  end do

  deallocate( ntot, sz )

end subroutine print_sector_statistics

subroutine print_qmc_statistics( statistics )
  implicit none

  real(8), intent(in   ) :: statistics(5,3)

  real(8) :: fl_moves, as_moves, segment_moves, shift_moves, global_moves, moves_total

  fl_moves = statistics(1, 1) + statistics(1, 2) + statistics(1, 3)
  as_moves = statistics(2, 1) + statistics(2, 2) + statistics(2, 3)
  segment_moves = statistics(3, 1) + statistics(3, 2) + statistics(3, 3)
  shift_moves   = statistics(4, 1) + statistics(4, 2)
  global_moves  = statistics(5, 1) + statistics(5, 2)
  moves_total   = fl_moves + as_moves + segment_moves + shift_moves + global_moves

  write(14,"(/,10x,'CT-QMC statistics',//,' Average sign',10x,f10.6,/, ' Acceptance rate',7x,f10.6)")                   &
             1.0, sum( statistics(2:3,2:3) ) / (as_moves + segment_moves)

  write(14,"(' Total number of moves   ',i0)") int(moves_total)

  write(14,"(/,' Full line moves:',8x,i0,13x,f7.3)") int(fl_moves), fl_moves * 100 / moves_total
  write(14,"('   rejected:',13x,i0,3(3x,f7.3))") int(statistics(1,1)), statistics(1,1) * 100 / max(real(fl_moves),1.0), &
                                                   statistics(1,1) * 100 / moves_total
  write(14,"('   inserted:',13x,i0,3(3x,f7.3))") int(statistics(1,2)), statistics(1,2) * 100 / max(real(fl_moves),1.0), &
                                                   statistics(1,2) * 100 / moves_total
  write(14,"('   removed :',13x,i0,3(3x,f7.3))") int(statistics(1,3)), statistics(1,3) * 100 / max(real(fl_moves),1.0), &
                                                   statistics(1,3) * 100 / moves_total

  write(14,"(/,' Antisegment moves:',6x,i0,13x,f7.3)") int(as_moves), as_moves * 100 / moves_total
  write(14,"('   rejected:',13x,i0,3(3x,f7.3))") int(statistics(2,1)), statistics(2,1) * 100 / max(real(as_moves),1.0), &
                                                   statistics(2,1) * 100 / moves_total
  write(14,"('   inserted:',13x,i0,3(3x,f7.3))") int(statistics(2,2)), statistics(2,2) * 100 / max(real(as_moves),1.0), &
                                                   statistics(2,2) * 100 / moves_total
  write(14,"('   removed :',13x,i0,3(3x,f7.3))") int(statistics(2,3)), statistics(2,3) * 100 / max(real(as_moves),1.0), &
                                                   statistics(2,3) * 100 / moves_total

  write(14,"(/,' Segment moves:',10x,i0,13x,f7.3)") int(segment_moves), segment_moves * 100 / moves_total
  write(14,"('   rejected:',13x,i0,3(3x,f7.3))") int(statistics(3,1)), statistics(3,1) * 100 / max(real(segment_moves),1.0), &
                                                   statistics(3,1) * 100 / moves_total
  write(14,"('   inserted:',13x,i0,3(3x,f7.3))") int(statistics(3,2)), statistics(3,2) * 100 / max(real(segment_moves),1.0), &
                                                   statistics(3,2) * 100 / moves_total
  write(14,"('   removed :',13x,i0,3(3x,f7.3))") int(statistics(3,3)), statistics(3,3) * 100 / max(real(segment_moves),1.0), &
                                                   statistics(3,3) * 100 / moves_total

  write(14,"(/,' Shift moves:',12x,i0,13x,f7.3)") int(shift_moves), shift_moves * 100 / moves_total
  write(14,"('   rejected:',13x,i0,3(3x,f7.3))") int(statistics(4,1)), statistics(4,1) * 100 / max(real(shift_moves),1.0),   &
                                                   statistics(4,1) * 100 / moves_total
  write(14,"('   accepted:',13x,i0,3(3x,f7.3))") int(statistics(4,2)), statistics(4,2) * 100 / max(real(shift_moves),1.0),   &
                                                   statistics(4,2) * 100 / moves_total

  write(14,"(/,' Global moves:',11x,i0,3x,f7.3)") int(global_moves), global_moves * 100 / moves_total
  write(14,"('   rejected:',13x,i0,3(3x,f7.3))") int(statistics(5,1)), statistics(5,1) * 100 / max(real(global_moves),1.0),  &
                                                   statistics(5,1) * 100 / moves_total
  write(14,"('   accepted:',13x,i0,3(3x,f7.3))") int(statistics(5,2)), statistics(5,2) * 100 / max(real(global_moves),1.0),  &
                                                   statistics(5,2) * 100 / moves_total

end subroutine print_qmc_statistics
