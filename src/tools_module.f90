module tools_module
!   This module contains a collection of wrappers that perform
!   a mathematical operation (e.g. inverse) on object (e.g. Green function)
!
  use units
  use lapack_module
  use fourier_transform_module
  use math_module
  use simulated_annealing_module

  implicit none
  private

  public inverse, fourier, anneal_by_lorentzian, lorentzian2fw, vector2diagonal_matrix

!    Inversion of function
  interface inverse
    module procedure inverse3,                  &         !   three dimensions
                     inverse4                             !   four  dimensions
  end interface 

!    Fourier transform (back and force) by different methods
  interface fourier
    module procedure fourier4_w2t,              &         !   from omega -> tau domain by conventional procedure
                     fourier4_t2w,              &         !   from tau -> omega domain by conventional procedure
                     annealandfourier_norm,     &         !   use simulated annealing with norm to perform FT (w->tau)
                     annealandfourier_wonorm              !   use simulated annealing without norm to perform FT (w->tau)
  end interface

!    Copy vector to diagonal of square matrix
  interface vector2diagonal_matrix
    module procedure vec2diagmat_re2_4, vec2diagmat_re1_3, vec2diagmat_cmplx2_4
  end interface

  contains

  subroutine vec2diagmat_cmplx2_4( f_in, f_out, ierr )
    implicit none

    integer, intent(  out) :: ierr
    complex(8), intent(in   ) :: f_in(:,:)
    complex(8), intent(  out) :: f_out(:,:,:,:)

    integer :: n_in(2), n_out(4)
    integer :: i

    ierr = 0

    n_in  = shape( f_in )
    n_out = shape( f_out )

    if( n_in(2) /= n_out(3) .or. n_out(1) /= n_out(2) )then
      ierr = -1
      return
    end if

    f_out = cmplx(0,0,8)

    if( n_in(1) == n_out(1) )then
      forall( i = 1:n_in(1) )        f_out(i,i,:,1) = f_in(i,:)
    elseif( n_in(1) == 2*n_out(1) )then
      if( n_out(4) == 1 )then
        forall( i = 1:n_out(1) )     f_out(i,i,:,1) = 0.5d0 * (  f_in(i,:) + f_in(i+n_out(1),:)  )
      else
        forall( i = 1:n_out(1) )
          f_out(i,i,:,1) = f_in(i,:)
          f_out(i,i,:,2) = f_in(i+n_out(1),:)
        end forall
      end if
    else
      ierr = -2
    end if

  end subroutine vec2diagmat_cmplx2_4

  subroutine vec2diagmat_re2_4( f_in, f_out, ierr )
    implicit none

    integer, intent(  out) :: ierr
    real(8), intent(in   ) :: f_in(:,:)
    real(8), intent(  out) :: f_out(:,:,:,:)

    integer :: n_in(2), n_out(4)
    integer :: i

    ierr = 0

    n_in  = shape( f_in )
    n_out = shape( f_out )

    if( n_in(2) /= n_out(3) .or. n_out(1) /= n_out(2) )then
      ierr = -1
      return
    end if

    f_out = 0.d0

    if( n_in(1) == n_out(1) )then
      forall( i = 1:n_in(1) )        f_out(i,i,:,1) = f_in(i,:)
    elseif( n_in(1) == 2*n_out(1) )then
      if( n_out(4) == 1 )then
        forall( i = 1:n_out(1) )     f_out(i,i,:,1) = 0.5d0 * (  f_in(i,:) + f_in(i+n_out(1),:)  )
      else
        forall( i = 1:n_out(1) )
          f_out(i,i,:,1) = f_in(i,:)
          f_out(i,i,:,2) = f_in(i+n_out(1),:)
        end forall
      end if
    else
      ierr = -2
    end if

  end subroutine vec2diagmat_re2_4

  subroutine vec2diagmat_re1_3( f_in, f_out, ierr )
    implicit none

    integer, intent(  out) :: ierr
    real(8), intent(in   ) :: f_in(:)
    real(8), intent(  out) :: f_out(:,:,:)

    integer :: n_in, n_out(3)
    integer :: i

    ierr = 0

    n_in  = size( f_in )
    n_out = shape( f_out )

    if( n_out(1) /= n_out(2) )then
      ierr = -1
      return
    end if

    f_out = 0.d0

    if( n_in == n_out(1) )then
      forall( i = 1:n_in )           f_out(i,i,1) = f_in(i)
    elseif( n_in == 2*n_out(1) )then
      if( n_out(3) == 1 )then
        forall( i = 1:n_out(1) )     f_out(i,i,1) = 0.5d0 * (  f_in(i) + f_in(i+n_out(1))  )
      else
        forall( i = 1:n_out(1) )
          f_out(i,i,1) = f_in(i)
          f_out(i,i,2) = f_in(i+n_out(1))
        end forall
      end if
    else
      ierr = -2
    end if

  end subroutine vec2diagmat_re1_3

  subroutine lorentzian2fw( fw, x, Lorentzians, ierr )
    complex(8), intent(  out) :: fw(:,:,:,:)               !  function on energy grid
    complex(8), intent(in   ) :: x(:)                      !  energy grid
    real(8),    intent(in   ) :: Lorentzians(:,:,:,:)      !  Lorentzians
    integer,    intent(  out) :: ierr                      !  error status

    integer :: i, j, iom, is, p, nd(4)

    nd = shape( fw )
    p  = size( Lorentzians, 3 )

    if( nd(1) /= nd(2) .or. nd(3) /= size(x) .or. nd(1) /= size(Lorentzians,2) .or. nd(4) /= size(Lorentzians,4) )then
      ierr = 1 
      return
    end if

    fw = zero
    do is = 1, nd(4)
      do i = 1, nd(1)
        do iom = 1, nd(3)
          do j = 1, p
            fw(i,i,iom,is) = fw(i,i,iom,is) + Lorentzians(1,i,j,is) / ( x(iom) - Lorentzians(2,i,j,is) )
          end do
        end do
      end do
    end do

  end subroutine lorentzian2fw

  subroutine anneal_by_lorentzian( fw, x, norm, emin, emax, Lorentzians, ierr )
!    Simulated annealing of the function on the Matsubara grid by Lorentzians
!
    complex(8), intent(in   ) :: fw(:,:,:,:)               !  function on energy grid
    complex(8), intent(in   ) :: x(:)                      !  energy grid
    real(8),    intent(in   ) :: norm(:,:,:)               !  norm
    real(8),    intent(  out) :: Lorentzians(:,:,:,:)      !  Lorentzians
    real(8),    intent(in   ) :: emin                         !  lower boundary for simulated annealing
    real(8),    intent(in   ) :: emax                         !  upper boundary for simulated annealing
    integer,    intent(  out) :: ierr                            !  error status

    integer :: i, is, p, nd(4)
    integer, allocatable :: iafix(:)
    real(8), allocatable :: a(:), sigma(:)

    nd = shape( fw )
    p  = size( Lorentzians, 3 )

    if( nd(1) /= nd(2) .or. nd(3) /= size(x) .or. nd(1) /= size(norm,1) .or. nd(1) /= size(norm,2) .or.    &
        nd(4) /= size(norm,3) .or. nd(1) /= size(Lorentzians,2) .or. nd(4) /= size(Lorentzians,4) )then
      ierr = 1 
      return
    end if

    allocate( iafix(2*p), a(2*p), sigma(nd(3)) )
    iafix = 1
    sigma = 1.d0

    do is = 1, nd(4)
      do i = 1, nd(1)
        a(1:p) = Lorentzians(1,i,:,is)
        a(p+1:2*p) = Lorentzians(2,i,:,is)

        call simulated_annealing( x(1:nd(3)), fw(i,i,1:nd(3),is), sigma, nd(3), a, iafix, 2*p, emin, emax, norm(i,i,is), ierr )

        Lorentzians(1,i,:,is) = a(1:p)
        Lorentzians(2,i,:,is) = a(p+1:2*p)
      end do
    end do

    deallocate( iafix, a, sigma )

  end subroutine anneal_by_lorentzian

!--------------------------------------------------------------------------------------------------
!     Fourier transform
  subroutine annealandfourier_norm( fw, x, n, ft, p, norm, emin, emax, ierr )
!     Simulated annealing with norm and Fourier transform from omega to tau domain
    complex(8), intent(in   ) :: fw(:,:,:,:)     !  f(w)   - function on Matsubara grid
    complex(8), intent(in   ) :: x(:)            !  Matsubara grid
    real(8),    intent(in   ) :: norm(:,:,:)     !  norm
    real(8),    intent(  out) :: ft(:,:,:,:)     !  f(tau) - function in tau domain
    integer, intent(in   ) :: n                  !  # of points to use for simulated annealing
    integer, intent(in   ) :: p                  !  # of Lorentzians for simulated annealing
    real(8), intent(in   ) :: emin               !  lower boundary for simulated annealing
    real(8), intent(in   ) :: emax               !  upper boundary for simulated annealing
    integer, intent(  out) :: ierr               !  error status

    integer :: i, j, l, is, nt, nd(4)
    integer, allocatable :: iafix(:)
    real(8) :: tau, beta
    real(8), allocatable :: a(:), sigma(:)
    real(16) :: fdexp, qdum

    nd = shape( fw )
    nt = size( ft, 3 )
    beta = twopi / aimag( x(2) - x(1) )

    if( nd(1) /= nd(2) .or. nd(1) /= size(ft,1) .or. nd(2) /= size(ft,2) .or. nd(4) /= size(ft,4) .or.    &
        nd(3) /= size(x) .or. nd(3) < n .or. nd(1) /= size(norm,1) .or. nd(1) /= size(norm,2) .or. nd(4) /= size(norm,3) )then
      ierr = 1 
      return
    end if

    allocate( iafix(2*p), a(2*p), sigma(n) )
    iafix = 1
    sigma = 1.d0

    ft = 0

    do is = 1, nd(4)
      do i = 1, nd(1)
        call simulated_annealing( x(1:n), fw(i,i,1:n,is), sigma, n, a, iafix, 2*p, emin, emax, norm(i,i,is), ierr )

        do l = 1, nt
          tau = ( l - 1 )*beta / real(nt-1,8)

          do j = 1, p
            if( tau < beta / 4 )then
              fdexp = -a(j+p) * tau
              qdum  = -a(j+p) * beta
              fdexp = exp( fdexp ) / ( 1 + exp( qdum ) )
            elseif( tau > 3 * beta / 4 )then
              fdexp = a(j+p) * ( beta - tau )
              qdum  = a(j+p) * beta
              fdexp = exp( fdexp ) / ( 1 + exp( qdum ) )
            else
              fdexp = a(j+p) * ( beta/2 - tau )
              qdum  = a(j+p) * beta / 2
              fdexp = exp( fdexp ) / cosh( qdum ) / 2
            end if
 
            ft(i,i,l,is) = ft(i,i,l,is) - a(j) * fdexp
          end do
        end do

      end do
    end do

    deallocate( iafix, a, sigma )

  end subroutine annealandfourier_norm

  subroutine annealandfourier_wonorm( fw, x, n, ft, p, emin, emax, ierr )
!     Simulated annealing without norm and Fourier transform from omega to tau domain
    complex(8), intent(in   ) :: fw(:,:,:,:)     !  f(w)   - function on Matsubara grid
    complex(8), intent(in   ) :: x(:)            !  Matsubara grid
    real(8),    intent(  out) :: ft(:,:,:,:)     !  f(tau) - function in tau domain
    integer, intent(in   ) :: n                  !  # of points to use for simulated annealing
    integer, intent(in   ) :: p                  !  # of Lorentzians for simulated annealing
    real(8), intent(in   ) :: emin               !  lower boundary for simulated annealing
    real(8), intent(in   ) :: emax               !  upper boundary for simulated annealing
    integer, intent(  out) :: ierr               !  error status

    integer :: i, j, l, is, nt, nd(4)
    integer, allocatable :: iafix(:)
    real(8) :: tau, beta, chi
    real(8), allocatable :: a(:), sigma(:)
    real(16) :: fdexp, qdum

    nd = shape( fw )
    nt = size( ft, 3 )
    beta = twopi / aimag( x(2) - x(1) )

    if( nd(1) /= nd(2) .or. nd(1) /= size(ft,1) .or. nd(2) /= size(ft,2) .or.         &
        nd(4) /= size(ft,4) .or. nd(3) /= size(x) .or. nd(3) < n )then
      ierr = 1 
      return
    end if

    allocate( iafix(2*p), a(2*p), sigma(n) )
    iafix = 1
    sigma = 1
    chi   = 1.d-8

    ft = 0

    do is = 1, nd(4)
      do i = 1, nd(1)
        call simulated_annealing( x(1:n), fw(i,i,1:n,is), sigma, n, a, iafix, 2*p, emin, emax, chi, beta, ierr )

        do l = 1, nt
          tau = ( l - 1 )*beta / real(nt-1,8)

          do j = 1, p
            if( tau < beta / 4 )then
              fdexp = -a(j+p) * tau
              qdum  = -a(j+p) * beta
              fdexp = exp( fdexp ) / ( 1 + exp( qdum ) )
            elseif( tau > 3 * beta / 4 )then
              fdexp = a(j+p) * ( beta - tau )
              qdum  = a(j+p) * beta
              fdexp = exp( fdexp ) / ( 1 + exp( qdum ) )
            else
              fdexp = a(j+p) * ( beta/2 - tau )
              qdum  = a(j+p) * beta / 2
              fdexp = exp( fdexp ) / cosh( qdum ) / 2
            end if

            ft(i,i,l,is) = ft(i,i,l,is) - a(j) * fdexp
          end do
        end do

      end do
    end do

    deallocate( iafix, a, sigma )

  end subroutine annealandfourier_wonorm

  subroutine fourier4_w2t( fw, ft, xw, moms, inb )
!     Fourier transform from omega to tau domain
    complex(8), intent(in   ) :: fw(:,:,:,:)              !     f(w)   - function on Matsubara grid
    real(8),    intent(  out) :: ft(:,:,:,:)              !     f(tau) - function in tau domain
    real(8),    intent(in   ) :: xw(:)                    !     Matsubara grid
    real(8),    intent(in   ) :: moms(0:,:,:,:)           !     moments of function
    integer,    intent(in   ) :: inb                      !     0 - dont calculate tau=beta point
                                                          !     1 -      calculate tau=beta point

    integer :: i, j, is, n, ns

    if( inb < 0 .or. inb > 1 ) stop

    n  = size( fw, dim=1 )
    ns = size( fw, dim=4 )

    if( n /= size( fw, dim=2 ) .or. n /= size( ft, dim=1 ) .or. n /= size( ft, dim=2 ) .or. ns /= size( ft, dim=4 ) )  stop

    do is = 1, ns
      do i = 1, n
        do j = 1, n
          call fourier_transform( fw(i,j,:,is), xw, ft(i,j,:,is), moms(1:3,i,j,is), inb )
        end do
      end do
    end do

  end subroutine fourier4_w2t

  subroutine fourier4_t2w( ft, fw, xw )
!     Fourier transform tau to omega
    real(8),    intent(in   ) :: ft(:,:,:,:)              !     f(tau) - function in tau domain
    complex(8), intent(  out) :: fw(:,:,:,:)              !     f(w)   - function on Matsubara grid
    real(8),    intent(in   ) :: xw(:)                    !     Matsubara grid

    integer :: i, j, is, n, ns, L
    real(8) :: de, derivs(2)
    real(8), allocatable :: f_t(:)

    n  = size( fw, dim=1 )
    L  = size( ft, dim=3 )
    ns = size( fw, dim=4 )

    if( n /= size( fw, dim=2 ) .or. n /= size( ft, dim=1 ) .or. n /= size( ft, dim=2 ) .or. ns /= size( ft, dim=4 ) )  stop

    de = 2*pi / ( xw(2) - xw(1) ) / L

    allocate( f_t(L+1) )

    do is = 1, ns
      do i = 1, n
        do j = 1, n

          f_t(1:L) =   ft(i,j,1:L,is)
          f_t(L+1) = - ft(i,j,1,is)
          if( i == j ) f_t(L+1) = f_t(L+1) - 1          !   Jump condition for diagonal Gf

!    Find left and right derivatives of Green function

          derivs(1) = first_deriv( f_t(1:5),     de, 5, 'r' )
          derivs(2) = first_deriv( f_t(L-3:L+1), de, 5, 'l' )

          call fourier_transform( f_t, fw(i,j,:,is), xw, derivs )
        end do
      end do
    end do

    deallocate( f_t )

  end subroutine fourier4_t2w

!--------------------------------------------------------------------------------------------------
!     Inversion
  subroutine inverse3( f )
!    Inversion of three dimensional function
    complex(8) :: f(:,:,:)

    integer :: i, n

    if( size( f, dim=1 ) /= size( f, dim=2 ) )  stop

    n = size( f, dim=3 )

    do i = 1, n
      call inverse_matrix( f(:,:,i) )
    end do

  end subroutine inverse3

  subroutine inverse4( f )
!    Inversion of four dimensional function
    complex(8) :: f(:,:,:,:)

    integer :: i, is, n, ns

    if( size( f, dim=1 ) /= size( f, dim=2 ) )  stop

    n  = size( f, dim=3 )
    ns = size( f, dim=4 )

    do i = 1, n
      do is = 1, ns
        call inverse_matrix( f(:,:,i,is) )
      end do
    end do

  end subroutine inverse4

end module tools_module
