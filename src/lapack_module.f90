module lapack_module
!
!   Simple interface for the LAPACK procedures
!
!   Written by A. Poteryaev  < Alexander.Poteryaev _at_ cpht.polytechnique.fr >
!
  implicit none

  private
  public inverse_matrix, eigenvalue, eigenproblem
  
  interface inverse_matrix
    module procedure inverse_matrix_s, inverse_matrix_d,        &
                     inverse_matrix_c, inverse_matrix_z
  end interface 
  
  interface eigenvalue
    module procedure zhegv_eigenvalue, zgeev_eigenvalue, cgeev_eigenvalue
  end interface 
  
  interface eigenproblem
    module procedure zheev_eigenproblem, cheev_eigenproblem,    &
                     zgeev_eigenproblem, cgeev_eigenproblem
  end interface 
  
  contains
!
!   Calculation of the inverse matrix
!
  subroutine inverse_matrix_c( a )
    implicit none
    complex, intent (inout) :: a(:,:)
    integer              :: dim, info
    integer, allocatable :: ipiv(:)
    complex, allocatable :: work(:)
  
    dim = size( a,1 )
    if( dim == 1 .and. abs( a(1,1) ) > 1.e-12 )then
      a(1,1) = 1 / a(1,1)
      return
    end if

    allocate( ipiv(dim), work(dim) )

    call cgetrf( dim, dim, a, dim, ipiv, info )
    if( info /= 0 )then
      write(6,"(//,'INVERSE_MATRIX: error in CGETRF, info =',1x,i3)") info
      stop
    end if
    call cgetri( dim, a, dim, ipiv, work, dim, info )
    if( info /= 0 )then
      write(6,"(//,'INVERSE_MATRIX: error in CGETRI, info =',1x,i3)") info
      stop
    end if

    deallocate( ipiv, work )
  
  end subroutine inverse_matrix_c
  
  subroutine inverse_matrix_z( a )
    implicit none
    complex(8), intent (inout) :: a(:,:)

    integer                 :: dim, info
    integer, allocatable    :: ipiv(:)
    complex(8), allocatable :: work(:)

    dim = size( a,1 )
    allocate( ipiv(dim), work(dim) )
    
    call zgetrf( dim, dim, a, dim, ipiv, info )
    if( info /= 0 )then
      write(6,"(//,'INVERSE_MATRIX: error in ZGETRF, info =',1x,i3)") info
      stop
    end if
    call zgetri( dim, a, dim, ipiv, work, dim, info )
    if( info /= 0 )then
      write(6,"(//,'INVERSE_MATRIX: error in ZGETRI, info =',1x,i3)") info
      stop
    end if

    deallocate( ipiv, work )
  
  end subroutine inverse_matrix_z
  
  subroutine inverse_matrix_d( a )
    implicit none
    double precision, intent (inout) :: a(:,:)
    integer                          :: dim, info
    integer,          allocatable    :: ipiv(:)
    double precision, allocatable    :: work(:)
  
    dim = size( a,1 )
    allocate( ipiv(dim), work(dim) )

    call dgetrf( dim, dim, a, dim, ipiv, info )
    if( info /= 0 )then
      write(6,"(//,'INVERSE_MATRIX: error in DGETRF, info =',1x,i3)") info
      stop
    end if
    call dgetri( dim, a, dim, ipiv, work, dim, info )
    if( info /= 0 )then
      write(6,"(//,'INVERSE_MATRIX: error in DGETRI, info =',1x,i3)") info
      stop
    end if

    deallocate( ipiv, work )
  
  end subroutine inverse_matrix_d
  
  subroutine inverse_matrix_s( a )
    implicit none
    real, intent (inout) :: a(:,:)
    integer              :: dim, info
    integer, allocatable :: ipiv(:)
    real, allocatable    :: work(:)
  
    dim = size( a,1 )
    if( dim == 1 .and. abs( a(1,1) ) > 1.e-12 )then
      a(1,1) = 1 / a(1,1)
      return
    end if

    allocate( ipiv(dim), work(dim) )

    call sgetrf( dim, dim, a, dim, ipiv, info )
    if( info /= 0 )then
      write(6,"(//,'INVERSE_MATRIX: error in SGETRF, info =',1x,i3)") info
      stop
    end if
    call sgetri( dim, a, dim, ipiv, work, dim, info )
    if( info /= 0 )then
      write(6,"(//,'INVERSE_MATRIX: error in SGETRI, info =',1x,i3)") info
      stop
    end if

    deallocate( ipiv, work )
  
  end subroutine inverse_matrix_s
!=======================================================================================
!=======================================================================================
!=======================================================================================
!
!     Calculates eigenvalues and eigenvectors
! 
!=======================================================================================

  subroutine zgeev_eigenproblem( a, w, vl, vr )
!
!   General complex problem (double precision)
!
    implicit none
    
    complex(8), intent(inout) :: a(:,:)            ! Matrix to solve eigenproblem
    complex(8), intent(  out) :: w(:)              ! Eigenvalues
    complex(8), intent(  out) :: vl(:,:), vr(:,:)  ! Left and right eigenvectors
    
    integer :: n, lwork, info
    real(8),    allocatable :: rwork(:)
    complex(8), allocatable :: work(:)
     
    n   = size( a, 2 )
    lwork = 4 * n
    allocate( work(lwork), rwork(2*n) )

    call zgeev( 'V', 'V', n, a, n, w, vl, n, vr, n, work, lwork, rwork, info )
    if( info /= 0 )then
      write(*,"(/,' Error in subr. zgeev,  info = ',i3,/)") info
      stop
    end if   
    
    deallocate( work, rwork )
    
  end subroutine zgeev_eigenproblem
  
  subroutine cgeev_eigenproblem( a, w, vl, vr )
!
!   General complex problem (single precision)
!
    implicit none
    
    complex(4), intent(inout) :: a(:,:)            ! Matrix to solve eigenproblem
    complex(4), intent(  out) :: w(:)              ! Eigenvalues
    complex(4), intent(  out) :: vl(:,:), vr(:,:)  ! Left and right eigenvectors
    
    integer :: n, lwork, info
    real(4),    allocatable :: rwork(:)
    complex(4), allocatable :: work(:)
     
    n   = size( a, 2 )
    lwork = 4 * n
    allocate( work(lwork), rwork(2*n) )

    call cgeev( 'V', 'V', n, a, n, w, vl, n, vr, n, work, lwork, rwork, info )
    if( info /= 0 )then
      write(6,"(/,' Error in subr. zgeev,  info = ',i3,/)") info
      stop
    end if   
    
    deallocate( work, rwork )
    
  end subroutine cgeev_eigenproblem
  
  subroutine zheev_eigenproblem( a, w )
!
!   Computes eigenvalues and eigenvectors of Hermitian problem,
!   A*X=W*X
!
    implicit none
    
    complex(8),   intent(inout) :: a(:,:)
    real(8),      intent(  out) :: w(:)

    integer :: n, lwork, info
    real(8),    allocatable :: rwork(:)
    complex(8), allocatable :: work(:)
     
    n   = size( a, 2 )
    lwork = 4 * n
    allocate( work(lwork), rwork(3*n-2) )

    call zheev( 'v', 'u', n, a, n, w, work, lwork, rwork, info )
    if( info /= 0 )then
      write(6,"(/,' Error in subr. zheev,  info = ',i3,/)") info
      stop
    end if   
     
    deallocate( work, rwork )
    
  end subroutine zheev_eigenproblem
  
  subroutine cheev_eigenproblem( a, w )
!
!   Computes eigenvalues and eigenvectors of Hermitian problem,
!   A*X=W*X
!
    implicit none
    
    complex(4),   intent(inout) :: a(:,:)
    real(4),      intent(  out) :: w(:)

    integer :: n, lwork, info
    real(4),    allocatable :: rwork(:)
    complex(4), allocatable :: work(:)
     
    n   = size( a, 2 )
    lwork = 4 * n
    allocate( work(lwork), rwork(3*n-2) )

    call cheev( 'v', 'u', n, a, n, w, work, lwork, rwork, info )
    if( info /= 0 )then
      write(6,"(/,' Error in subr. zheev,  info = ',i3,/)") info
      stop
    end if   
     
    deallocate( work, rwork )
    
  end subroutine cheev_eigenproblem
  
  subroutine zhegv_eigenvalue( itype, a, b, w )
!
!    Computes all the eigenvalues of a complex generalized 
!    Hermitian-definite eigenproblem, of the form
!         A*x   = (lambda)*B*x,  
!         A*Bx  = (lambda)*x,         or 
!         B*A*x = (lambda)*x.
!    Here A and B are assumed to be square, Hermitian and B is also positive definite.
! 
    implicit none
    
    integer,    intent(in   ) :: itype     ! = 1, A*x   = (lambda)*B*x
                                           ! = 2, A*Bx  = (lambda)*x
                                           ! = 3, B*A*x = (lambda)*x
    complex(8), intent(inout) :: a(:,:)    ! A matrix
    complex(8), intent(inout) :: b(:,:)    ! B matrix
    real(8),    intent(  out) :: w(:)      ! Eigenvalues
                                    
    integer                 :: n, lwork, info
    real(8),    allocatable :: rwork(:)
    complex(8), allocatable :: work(:)
    
    n     = size( a, 2 )
    lwork = 4 * n
    
    allocate( work(lwork), rwork(3*n-2) )
    
    call zhegv( itype, 'N', 'U', n, a, n, b, n, w, work, lwork, rwork, info )
    if( info /= 0 )then
      write(6,"(/,' Error in subr. zhegv_eigenvalue,  info = ',i3,/)") info
      stop
    end if   
     
    deallocate( work, rwork )
 
  end subroutine zhegv_eigenvalue

  subroutine zgeev_eigenvalue( a, w )
!
!   General complex problem (double precision)
!
    implicit none
    
    complex(8), intent(inout) :: a(:,:)            ! Matrix to solve eigenproblem
    complex(8), intent(  out) :: w(:)              ! Eigenvalues
    
    integer :: n, lwork, info
    real(8),    allocatable :: rwork(:)
    complex(8), allocatable :: work(:)
    complex(8), allocatable :: vl(:,:), vr(:,:)  
     
    n     = size( a, 2 )
    lwork = 4 * n
    allocate( work(lwork), rwork(2*n), vl(n,n), vr(n,n) )

    call zgeev( 'N', 'N', n, a, n, w, vl, n, vr, n, work, lwork, rwork, info )
    if( info /= 0 )then
      write(*,"(/,' Error in subr. zgeev_eigenvalue,  info = ',i3,/)") info
      stop
    end if   
    
    deallocate( work, rwork, vl, vr )
    
  end subroutine zgeev_eigenvalue

  subroutine cgeev_eigenvalue( a, w )
!
!   General complex problem (single precision)
!
    implicit none
    
    complex(4), intent(inout) :: a(:,:)            ! Matrix to solve eigenproblem
    complex(4), intent(  out) :: w(:)              ! Eigenvalues
    
    integer :: n, lwork, info
    real(4),    allocatable :: rwork(:)
    complex(4), allocatable :: work(:)
    complex(4), allocatable :: vl(:,:), vr(:,:) 
     
    n   = size( a, 2 )
    lwork = 4 * n
    allocate( work(lwork), rwork(2*n), vl(n,n), vr(n,n) )

    call cgeev( 'N', 'N', n, a, n, w, vl, n, vr, n, work, lwork, rwork, info )
    if( info /= 0 )then
      write(6,"(/,' Error in subr. cgeev_eigenvalue,  info = ',i3,/)") info
      stop
    end if   
    
    deallocate( work, rwork )
    
  end subroutine cgeev_eigenvalue

end module lapack_module
