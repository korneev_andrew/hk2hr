module hamiltonian_tools
!
!    This module contains subroutines related to Hamiltonian, tetrahedrons, k-mesh, etc.
!
  use hamiltonian_module

  implicit none

  public  def_nkxyz,                &       !    define divisions along kx, ky and kz directions 
          def_tetra,                &       !    define tetrahedron
          dealloc_hamiltonian,      &       !    deallocate arrays related to Hamiltonian
          dh_dk                             !    calculate derivative of the Hamiltonian over k-points

  contains

  subroutine dealloc_hamiltonian
!   Deallocate hamiltonian arrays to free memory
    implicit none

    if( allocated(itt) )  deallocate( itt )
    if( allocated(wtkp) ) deallocate( wtkp )
    if( allocated(bk) )   deallocate( bk )
    if( allocated(h) )    deallocate( h )
    if( allocated(h_ll) ) deallocate( h_ll )
    if( allocated(h_il) ) deallocate( h_il )
    if( allocated(h_ii) ) deallocate( h_ii )
    if( allocated(e2) )   deallocate( e2 )

  end subroutine dealloc_hamiltonian

  subroutine dh_dk( )
!    Calculates derivative of the Hamiltonian over k
!
    implicit none

    integer :: ikp, ik1, ik2, ios, i, j
    real(8) :: a, b, c
    complex(8), allocatable :: dhtmp(:,:,:,:,:)

    call def_nkxyz( ios )

#ifdef DEBUG
    write(6,*) ' ios ', ios, nkxyz
    do i = 1, 3
      write(6,*) ( qp(i,j), j = 1, 3 )
    end do
#endif

    if( .not. allocated( dhdk ) ) allocate( dhdk(3,ndim,ndim,nkp,2) )

    do ikp = 1, nkp
!
!     Derivative of the Hamiltonian in kz direction (dH/dkz)
!
#ifdef DEBUG
     write(6,*) ' ============================= ikp ', ikp
#endif

      ik1 = ikp + 1
      if( ik1 > nkp )  ik1 = ik1 - nkxyz(3)

      a = ( bk(2,ikp) - bk(2,ik1) )*qp(3,3) - ( bk(3,ikp) - bk(3,ik1) )*qp(2,3)
      b = ( bk(3,ikp) - bk(3,ik1) )*qp(1,3) - ( bk(1,ikp) - bk(1,ik1) )*qp(3,3)
      c = ( bk(1,ikp) - bk(1,ik1) )*qp(2,3) - ( bk(2,ikp) - bk(2,ik1) )*qp(1,3)

      if( ( abs(a) > 1.d-8 ) .or. ( abs(b) > 1.d-8 ) .or. ( abs(c) > 1.d-8 ) )then
        ik1 = ik1 - nkxyz(3)

        a = ( bk(2,ikp) - bk(2,ik1) )*qp(3,3) - ( bk(3,ikp) - bk(3,ik1) )*qp(2,3)
        b = ( bk(3,ikp) - bk(3,ik1) )*qp(1,3) - ( bk(1,ikp) - bk(1,ik1) )*qp(3,3)
        c = ( bk(1,ikp) - bk(1,ik1) )*qp(2,3) - ( bk(2,ikp) - bk(2,ik1) )*qp(1,3)

        if( ( abs(a) > 1.d-8 ) .or. ( abs(b) > 1.d-8 ) .or. ( abs(c) > 1.d-8 ) )then
          write(6,*) ' ikp  ', ikp,  bk(:,ikp)
          write(6,*) ' ikz1 ', ik1,  bk(:,ik1)
          stop
        end if
      end if

      ik2 = ikp - 1
      if( ik2 < 1 )  ik2 = ik2 + nkxyz(3)

      a = ( bk(2,ikp) - bk(2,ik2) )*qp(3,3) - ( bk(3,ikp) - bk(3,ik2) )*qp(2,3)
      b = ( bk(3,ikp) - bk(3,ik2) )*qp(1,3) - ( bk(1,ikp) - bk(1,ik2) )*qp(3,3)
      c = ( bk(1,ikp) - bk(1,ik2) )*qp(2,3) - ( bk(2,ikp) - bk(2,ik2) )*qp(1,3)

      if( ( abs(a) > 1.d-8 ) .or. ( abs(b) > 1.d-8 ) .or. ( abs(c) > 1.d-8 ) )then
        ik2 = ik2 + nkxyz(3)

        a = ( bk(2,ikp) - bk(2,ik2) )*qp(3,3) - ( bk(3,ikp) - bk(3,ik2) )*qp(2,3)
        b = ( bk(3,ikp) - bk(3,ik2) )*qp(1,3) - ( bk(1,ikp) - bk(1,ik2) )*qp(3,3)
        c = ( bk(1,ikp) - bk(1,ik2) )*qp(2,3) - ( bk(2,ikp) - bk(2,ik2) )*qp(1,3)

        if( ( abs(a) > 1.d-8 ) .or. ( abs(b) > 1.d-8 ) .or. ( abs(c) > 1.d-8 ) )then
          write(6,*) ' ikp  ', ikp,  bk(:,ikp)
          write(6,*) ' ikz2 ', ik2,  bk(:,ik2)
          write(6,*) ' STOP inside'
          stop
        end if
      end if

#ifdef DEBUG
      write(6,*) ' ikz1 ', ik1
      write(6,*) ' ikz2 ', ik2
#endif

      dhdk(3,:,:,ikp,:) = ( h(:,:,ik1,:) - h(:,:,ik2,:) ) / sqrt( qp(1,3)**2 + qp(2,3)**2 + qp(3,3)**2 ) / 2
!
!========================================================================================================================
!
!     Derivative of the Hamiltonian in ky direction (dH/dky)
!
      ik1 = ikp + nkxyz(3)
      if( ik1 > nkp )  ik1 = ik1 - nkxyz(3)*nkxyz(2)

      a = ( bk(2,ikp) - bk(2,ik1) )*qp(3,2) - ( bk(3,ikp) - bk(3,ik1) )*qp(2,2)
      b = ( bk(3,ikp) - bk(3,ik1) )*qp(1,2) - ( bk(1,ikp) - bk(1,ik1) )*qp(3,2)
      c = ( bk(1,ikp) - bk(1,ik1) )*qp(2,2) - ( bk(2,ikp) - bk(2,ik1) )*qp(1,2)

      if( ( abs(a) > 1.d-8 ) .or. ( abs(b) > 1.d-8 ) .or. ( abs(c) > 1.d-8 ) )then
        ik1 = ik1 - nkxyz(3)*nkxyz(2)

        a = ( bk(2,ikp) - bk(2,ik1) )*qp(3,2) - ( bk(3,ikp) - bk(3,ik1) )*qp(2,2)
        b = ( bk(3,ikp) - bk(3,ik1) )*qp(1,2) - ( bk(1,ikp) - bk(1,ik1) )*qp(3,2)
        c = ( bk(1,ikp) - bk(1,ik1) )*qp(2,2) - ( bk(2,ikp) - bk(2,ik1) )*qp(1,2)
        if( ( abs(a) > 1.d-8 ) .or. ( abs(b) > 1.d-8 ) .or. ( abs(c) > 1.d-8 ) )then
          write(6,*) ' ikp  ', ikp,  bk(:,ikp)
          write(6,*) ' iky1 ', ik1,  bk(:,ik1)
          stop
        end if
      end if

      ik2 = ikp - nkxyz(3)
      if( ik2 < 1 )  ik2 = ik2 + nkxyz(3)*nkxyz(2)

      a = ( bk(2,ikp) - bk(2,ik2) )*qp(3,2) - ( bk(3,ikp) - bk(3,ik2) )*qp(2,2)
      b = ( bk(3,ikp) - bk(3,ik2) )*qp(1,2) - ( bk(1,ikp) - bk(1,ik2) )*qp(3,2)
      c = ( bk(1,ikp) - bk(1,ik2) )*qp(2,2) - ( bk(2,ikp) - bk(2,ik2) )*qp(1,2)
      if( ( abs(a) > 1.d-8 ) .or. ( abs(b) > 1.d-8 ) .or. ( abs(c) > 1.d-8 ) )then
        ik2 = ik2 + nkxyz(3)*nkxyz(2)

        a = ( bk(2,ikp) - bk(2,ik2) )*qp(3,2) - ( bk(3,ikp) - bk(3,ik2) )*qp(2,2)
        b = ( bk(3,ikp) - bk(3,ik2) )*qp(1,2) - ( bk(1,ikp) - bk(1,ik2) )*qp(3,2)
        c = ( bk(1,ikp) - bk(1,ik2) )*qp(2,2) - ( bk(2,ikp) - bk(2,ik2) )*qp(1,2)
        if( ( abs(a) > 1.d-8 ) .or. ( abs(b) > 1.d-8 ) .or. ( abs(c) > 1.d-8 ) )then
          write(6,*) ' ikp  ', ikp,  bk(:,ikp)
          write(6,*) ' iky2 ', ik2,  bk(:,ik2)
          write(6,*) ' STOP inside'
          stop
        end if
      end if

#ifdef DEBUG
     write(6,*) ' iky1 ', ik1
     write(6,*) ' iky2 ', ik2
#endif

      dhdk(2,:,:,ikp,:) = ( h(:,:,ik1,:) - h(:,:,ik2,:) ) / sqrt( qp(1,2)**2 + qp(2,2)**2 + qp(3,2)**2 ) / 2
!
!========================================================================================================================
!
!     Derivative of the Hamiltonian in kx direction (dH/dkx)
!
      ik1 = ikp + nkxyz(3)*nkxyz(2)
      if( ik1 > nkp )  ik1 = ik1 - nkxyz(3)*nkxyz(2)*nkxyz(1)

      a = ( bk(2,ikp) - bk(2,ik1) )*qp(3,1) - ( bk(3,ikp) - bk(3,ik1) )*qp(2,1)
      b = ( bk(3,ikp) - bk(3,ik1) )*qp(1,1) - ( bk(1,ikp) - bk(1,ik1) )*qp(3,1)
      c = ( bk(1,ikp) - bk(1,ik1) )*qp(2,1) - ( bk(2,ikp) - bk(2,ik1) )*qp(1,1)

      if( ( abs(a) > 1.d-8 ) .or. ( abs(b) > 1.d-8 ) .or. ( abs(c) > 1.d-8 ) )then
        ik1 = ik1 - nkxyz(3)*nkxyz(2)*nkxyz(1)

        a = ( bk(2,ikp) - bk(2,ik1) )*qp(3,1) - ( bk(3,ikp) - bk(3,ik1) )*qp(2,1)
        b = ( bk(3,ikp) - bk(3,ik1) )*qp(1,1) - ( bk(1,ikp) - bk(1,ik1) )*qp(3,1)
        c = ( bk(1,ikp) - bk(1,ik1) )*qp(2,1) - ( bk(2,ikp) - bk(2,ik1) )*qp(1,1)
        if( ( abs(a) > 1.d-8 ) .or. ( abs(b) > 1.d-8 ) .or. ( abs(c) > 1.d-8 ) )then
          write(6,*) ' ikp  ', ikp,  bk(:,ikp)
          write(6,*) ' ikx1 ', ik1,  bk(:,ik1)
          stop
        end if
      end if

      ik2 = ikp - nkxyz(3)*nkxyz(2)
      if( ik2 < 1 )  ik2 = ik2 + nkxyz(3)*nkxyz(2)*nkxyz(1)

      a = ( bk(2,ikp) - bk(2,ik2) )*qp(3,1) - ( bk(3,ikp) - bk(3,ik2) )*qp(2,1)
      b = ( bk(3,ikp) - bk(3,ik2) )*qp(1,1) - ( bk(1,ikp) - bk(1,ik2) )*qp(3,1)
      c = ( bk(1,ikp) - bk(1,ik2) )*qp(2,1) - ( bk(2,ikp) - bk(2,ik2) )*qp(1,1)
      if( ( abs(a) > 1.d-8 ) .or. ( abs(b) > 1.d-8 ) .or. ( abs(c) > 1.d-8 ) )then
        ik2 = ik2 + nkxyz(3)*nkxyz(2)*nkxyz(1)

        a = ( bk(2,ikp) - bk(2,ik2) )*qp(3,1) - ( bk(3,ikp) - bk(3,ik2) )*qp(2,1)
        b = ( bk(3,ikp) - bk(3,ik2) )*qp(1,1) - ( bk(1,ikp) - bk(1,ik2) )*qp(3,1)
        c = ( bk(1,ikp) - bk(1,ik2) )*qp(2,1) - ( bk(2,ikp) - bk(2,ik2) )*qp(1,1)
        if( ( abs(a) > 1.d-8 ) .or. ( abs(b) > 1.d-8 ) .or. ( abs(c) > 1.d-8 ) )then
          write(6,*) ' ikp  ', ikp,  bk(:,ikp)
          write(6,*) ' ikx2 ', ik2,  bk(:,ik2)
          write(6,*) ' STOP inside'
          stop
        end if
      end if

#ifdef DEBUG
     write(6,*) ' ikx1 ', ik1
     write(6,*) ' ikx2 ', ik2
#endif

      dhdk(1,:,:,ikp,:) = ( h(:,:,ik1,:) - h(:,:,ik2,:) ) / sqrt( qp(1,1)**2 + qp(2,1)**2 + qp(3,1)**2 ) / 2

    end do   !  ikp

    allocate( dhtmp(3,ndim,ndim,nkp,2) )
    dhtmp = cmplx(0.d0,0.d0,8)

    do i = 1, 3
      do j = 1, 3
        dhtmp(i,:,:,:,:) = dhtmp(i,:,:,:,:) + dhdk(j,:,:,:,:)*qp(i,j)
      end do
    end do

    dhdk = dhtmp

    deallocate( dhtmp )

  end subroutine dh_dk

  subroutine def_nkxyz( ios )
!
!   This subroutine defines the divisions along k-directions and              &
!   primitive microcell (based on the assumption of using Monkhorst-Pack mesh)
!
    implicit none

    integer, intent(  out) :: ios

    integer    :: i, j, k, ikp
    real(8) :: a, b, c, r1(3), r2(3), r21(3)

    ios = 0

    if( .not. allocated(bk) )then
      ios = 777
      return
    end if

    qp(1,1) = 11
    qp(1,2) = 12
    qp(1,3) = 13
    qp(2,1) = 21
    qp(2,2) = 22
    qp(2,3) = 23
    qp(3,1) = 31
    qp(3,2) = 32
    qp(3,3) = 33
    
!    do i = 1, 3
!      write(6,*) ( qp(i,j), j = 1,3 )
!    end do
!    
!    write(6,*) qp(:,1)
!    write(6,*) qp(:,2)
!    write(6,*) qp(:,3)
    
!
!   Define divisions in k-space and basis vectors of microcell
!
    r1  = bk(:,1)

    r2  = bk(:,2) 
    r21 = r2 - r1
    do i = 1, nkp
      a = ( bk(2,i) - r2(2) )*r21(3) - ( bk(3,i) - r2(3) )*r21(2)
      b = ( bk(3,i) - r2(3) )*r21(1) - ( bk(1,i) - r2(1) )*r21(3)
      c = ( bk(1,i) - r2(1) )*r21(2) - ( bk(2,i) - r2(2) )*r21(1)
      if( ( abs(a) > 1.d-8 ) .or. ( abs(b) > 1.d-8 ) .or. ( abs(c) > 1.d-8 ) ) exit
    end do

    k = maxloc( abs(r21), dim=1 )
!    write(6,*) ' k1 ', k

    qp(:,3)  = r21
    nkxyz(3) = i - 1

    r2  = bk(:,nkxyz(3)+1) 
    r21 = r2 - r1
    ikp = 0
    do i = 1, nkp, nkxyz(3)
      a = ( bk(2,i) - r2(2) )*r21(3) - ( bk(3,i) - r2(3) )*r21(2)
      b = ( bk(3,i) - r2(3) )*r21(1) - ( bk(1,i) - r2(1) )*r21(3)
      c = ( bk(1,i) - r2(1) )*r21(2) - ( bk(2,i) - r2(2) )*r21(1)
      if( ( abs(a) > 1.d-8 ) .or. ( abs(b) > 1.d-8 ) .or. ( abs(c) > 1.d-8 ) ) exit
      ikp = ikp + 1
    end do
    qp(:,2)  = r21
    nkxyz(2) = ikp
  
    k = maxloc( abs(r21), dim=1 )
!    write(6,*) ' k2 ', k
    
    r2  = bk(:,nkxyz(3)*nkxyz(2)+1) 
    r21 = r2 - r1
    ikp = 0
    do i = 1, nkp, nkxyz(3)*nkxyz(2)
      a = ( bk(2,i) - r2(2) )*r21(3) - ( bk(3,i) - r2(3) )*r21(2)
      b = ( bk(3,i) - r2(3) )*r21(1) - ( bk(1,i) - r2(1) )*r21(3)
      c = ( bk(1,i) - r2(1) )*r21(2) - ( bk(2,i) - r2(2) )*r21(1)
      if( ( abs(a) > 1.d-8 ) .or. ( abs(b) > 1.d-8 ) .or. ( abs(c) > 1.d-8 ) ) exit
      ikp = ikp + 1
    end do
    qp(:,1)  = r21
    nkxyz(1) = ikp

    k = maxloc( abs(r21), dim=1 )
!    write(6,*) ' k3 ', k
    
!    Foolproof
 
    if( product(nkxyz) /= nkp )   ios = 1
    
    a = qp(1,1) * ( qp(2,2)*qp(3,3) - qp(2,3)*qp(3,2) ) -     &
        qp(1,2) * ( qp(2,1)*qp(3,3) - qp(2,3)*qp(3,1) ) +     &
        qp(1,3) * ( qp(2,1)*qp(3,2) - qp(2,2)*qp(3,1) )

    if( abs(a) < 1.d-8 )     ios = ios + 2

  end subroutine def_nkxyz

!==============================================================================

  subroutine def_tetra( ios )
!
!   Analyze k-points and prepare information for tetrahedra
!
    implicit none
  
    integer, intent(  out) :: ios

    integer :: i  

    if( .not. allocated( bk ) )then
      ios = 777
      return
    end if
    
    call def_nkxyz( ios )
    if( ios /= 0 ) return

!   Debug  
!    write(14,303) nkp, nkxyz
!    write(14,304)
!    do i = 1, nkp
!      write(14,305) i, bk(:,i)
!    end do
    
    call tetirr( qp )

!    write(14,301)
!    write(14,302) ( i, itt(1:5,i), i=1,ntet )

301 format(/' Tetirr:  itet  weight   kp1    kp2    kp3    kp4',/,9x,43('-'))
302 format(7x,i6,4i7,4x,i3)
303 format(/' BZMESH:',i4,' k-points  (',3i3,' )')
304 format(4x,'IKP',6x,'Qx',8x,'Qy',8x,'Qz',/3x,62('-'))
305 format(i5,2x,3f10.4)
  
  end subroutine def_tetra

subroutine tetirr( qb )
!
!  Finds tetrahedra (modified version of original TB-LMTO subroutine)
!
!  Input:
!    qb  - vectors of first microcell
!  Output:
!   itt  - integer index array defining tetrahedron  itt(5,ntet)
!         itt(1,:)   - tetrahedron's multiplicity
!         itt(2-5,:) - points to the 4 k-points defining tetrahedron
!         ntet       - number of tetrahedra
!
  implicit none

  real(8), intent(in   ) :: qb(3,3)

!    Local variables
  integer :: i1, i2, i3, j1, j2, j3, k1, k2, k3, n1, n2, n3, icorn, itet, it
  integer :: idk(0:1,0:1,0:1), kcut(3,4,6)       
  integer, allocatable :: ibk(:,:,:)          

  if( allocated( itt ) ) return

  ntet = 6 * nkp
  itet = 0
  allocate( itt(5,ntet) )
  
  n1 = nkxyz(1)
  n2 = nkxyz(2)
  n3 = nkxyz(3)
  allocate( ibk(0:n1-1,0:n2-1,0:n3-1) )

  k1 = 0
  do i3 = 0, n3-1
    do i2 = 0, n2-1
      do i1 = 0, n1-1
        k1 = k1 + 1
        ibk(i1,i2,i3) = k1
      end do
    end do
  end do

  call ccutup( qb, kcut )

! --- Start looping over microcells

  do i3 = 0, n3-1
    do i2 = 0, n2-1
      do i1 = 0, n1-1

! --------- Set up identifiers at 8 corners of microcell
        do k1 = 0, 1
          j1 = mod(i1+k1,n1)
          do k2 = 0, 1
            j2 = mod(i2+k2,n2)
            do k3 = 0, 1
              j3 = mod(i3+k3,n3)
              idk(k1,k2,k3) = ibk(j1,j2,j3)
            end do
          end do
        end do

! --------- loop over tetrahedra
        do it = 1, 6
          itet = itet + 1
          do icorn = 1, 4
            k1 = kcut(1,icorn,it)
            k2 = kcut(2,icorn,it)
            k3 = kcut(3,icorn,it)
            itt(icorn+1,itet) = idk(k1,k2,k3)
          end do
          itt(1,itet) = 1
! ----------- order the 4 edges of each tetrahedron
          call ishell( 1, 4, itt(2:5,itet) )
        end do

      end do
    end do
  end do

  deallocate( ibk )
  
! --- Order the ntet0 tetrahedra
  call ishell( 5, ntet, itt )

end subroutine tetirr

subroutine ccutup( qb, kcut )
!- Cut the microcell into six tetrahedra with shortest max edge
! ----------------------------------------------------------------------
!i Inputs:
!i   qb    :vectors of first microcell
!o Outputs:
!o   kcut  :contains information how microcell is cut
! ----------------------------------------------------------------------
  implicit none
! Passed variables:
  real(8), intent(in   ) :: qb(3,3)
  integer, intent(  out) :: kcut(3,4,6)
! Local variables:
  integer :: i, itet, icorn, jcorn, lx, ly, lz, lxx, lyy, kcut0(3,4,6)
  real(8) :: p(3,4), dp(3), edgmax, edgmin, edmin, edmax, xx

! Data statements:
  data kcut0/                                                       &
    0,0,0, 0,1,0, 1,1,0, 1,1,1,  0,0,0, 1,0,0, 1,1,0, 1,1,1,        &
    0,0,0, 1,0,0, 1,0,1, 1,1,1,  0,0,0, 0,1,0, 0,1,1, 1,1,1,        &
    0,0,0, 0,0,1, 0,1,1, 1,1,1,  0,0,0, 0,0,1, 1,0,1, 1,1,1 /

! --- CHOSE CUTUP WITH SHORTEST MAX EDGE ---------
  lz  = 0
  lxx = 0
  lyy = 0
  edgmax = HUGE(edgmax)
  edgmin = 0.d0
  do lx = 0, 1
    do ly = 0, 1
      do itet = 1, 6
        do icorn = 1, 4
        call mxmymz(kcut0(1,icorn,itet),kcut(1,icorn,itet),lx,ly,lz)
        enddo
      enddo
      edmin = HUGE(edmin)
      edmax = 0.d0
      do itet = 1, 6
        do icorn = 1, 4
          do i=1,3
            p(i,icorn)=qb(i,1)*kcut(1,icorn,itet)                   &
                      +qb(i,2)*kcut(2,icorn,itet)                   &
                      +qb(i,3)*kcut(3,icorn,itet)
          enddo
        enddo
        do icorn = 1, 3
          do jcorn = icorn+1, 4
            dp(:)=p(:,icorn)-p(:,jcorn)
            xx    = danrm2(dp)
            edmax = MAX(edmax,xx)
            edmin = MIN(edmin,xx)
          enddo
        enddo
      enddo
      if (edmax  <  edgmax) then
        lxx = lx
        lyy = ly
        edgmax = edmax
        edgmin = edmin
      endif
    enddo
  enddo
  do itet = 1, 6
    do icorn = 1, 4
      call mxmymz(kcut0(1,icorn,itet),kcut(1,icorn,itet),lxx,lyy,lz)
    enddo
  enddo

end subroutine ccutup

subroutine mxmymz( kin, kout, lx, ly, lz )
!- Do mirrors in x,y,z if lx,ly,lz=1, respectively
! ----------------------------------------------------------------
!i Inputs:
!i   kin   :
!i   lx-z  :
!o Outputs:
!i   kout  :
! 
  implicit none
! Passed variables:
  integer, intent(in   ) :: kin(3)
  integer, intent(in   ) :: lx
  integer, intent(in   ) :: ly
  integer, intent(in   ) :: lz
  integer, intent(  out) :: kout(3)

  kout = kin
  if (lx  ==  1) kout(1) = 1-kout(1)
  if (ly  ==  1) kout(2) = 1-kout(2)
  if (lz  ==  1) kout(3) = 1-kout(3)

end subroutine mxmymz

subroutine ishell( m, n, iarray )
!- shell sort of a array of integer vectors
! ----------------------------------------------------------------------
!i Inputs:
!i   m     :number of components in iarray
!i   n     :number of elements in iarray
!i   iarray:array to be sorted
!o Outputs:
!o   iarray:array to be sorted
! ----------------------------------------------------------------------
  implicit none
! Passed variables:
  integer, intent(in   ) :: m
  integer, intent(in   ) :: n
  integer, intent(inout) :: iarray(m,0:n-1)
! Local variables:
  integer lognb2,i,j,k,l,n2,nn,it,mm,mmm

  lognb2 = int(log(float(n+1))*1.4426950)
  
  n2 = n
  do nn = 1, lognb2
    n2 = n2/2
    k = n - n2
    do  11  j = 1, k
      i = j - 1
 3    continue
      l = i + n2
      do  15  mm = 1, m
        if (iarray(mm,l) - iarray(mm,i)) 16,15,11
 16     continue
        do mmm = 1, m
          it = iarray(mmm,i)
          iarray(mmm,i) = iarray(mmm,l)
          iarray(mmm,l) = it
         enddo
         i = i - n2
         if (i  >=  0) goto 3
         goto 11
 15   continue
 11 continue
  enddo

end subroutine ishell

  real(8) function danrm2(r)
!- anisotropic 'norm'-square
! ----------------------------------------------------------------------
!i Inputs:
!i   r     :vector
!o Outputs:
!o   danrm2:anisotropic 'norm'-square
!r Remarks:
!r  A small anisotropy proportional to mtiny is added to d2 to
!r  distinguish between the three directions x,y,x and +x,-x ...
!r  mtiny should NOT be changed
! ----------------------------------------------------------------------
    implicit none
! Passed variables:
    real(8), intent(in   ) :: r(3)
! Local variables:
    real(8) :: anis, mtiny, x, y, z, d2

    mtiny = 450.d0 * EPSILON(mtiny)

    x = r(1)
    y = r(2)
    z = r(3)

    d2 = x*x + y*y + z*z

    anis = ( 30.34d0*x - 4.23d0*ABS(x) )*x +                                &
           ( 60.45d0*y - 2.12d0*ABS(y) )*y +                                &
           ( 90.56d0*z - 1.01d0*ABS(z) )*z

    danrm2 = d2 + anis*mtiny

  end function danrm2

end module hamiltonian_tools
