subroutine read_ini( ierr )
!======================================================================
!
!   Read initial data from amulet.ini input file
!
!======================================================================
  use impurity_module
  use parameter_module
  use energy_module
  use hamiltonian_module

  implicit none 

  interface
    subroutine free_format_reader( iunit, ios, vname, n, ivalue, rvalue, zvalue, lvalue, cvalue )
      implicit none 
      integer,      intent(in   ) :: iunit
      character(*), intent(in   ) :: vname
      integer,      intent(  out) :: ios
      integer,      intent(in   ) :: n
      integer,      intent(inout), optional :: ivalue(*)
      real(8),      intent(inout), optional :: rvalue(*)
      complex(8),   intent(inout), optional :: zvalue(*)
      logical,      intent(inout), optional :: lvalue(*)
      character(*), intent(inout), optional :: cvalue(*)
    end subroutine
  end interface

  integer, intent(  out) :: ierr

  integer :: iunit, i
  integer :: idummy(10)
  real(8) :: rdummy(10)
  logical :: ldummy(10)

  ierr  = 0
  iunit = 10
  open( iunit, file='amulet.ini', form='formatted', status='old', iostat=ierr, action='read' )

  if( ierr /= 0 ) return

!    Main parameters
!
!     How to start
  call free_format_reader( iunit, ierr, 'istart', 1, ivalue=idummy(1) )
  if( ierr == 0 ) istart = idummy(1)
  
!     Number of DMFT iterations
  call free_format_reader( iunit, ierr, 'niter', 1, ivalue=idummy(1) )
  if( ierr == 0 ) niter = idummy(1)
  
!     Mixing coefficient for self-energy
  call free_format_reader( iunit, ierr, 'alpha', 1, rvalue=rdummy(1) )
  if( ierr == 0 ) alpha = rdummy(1)

!     Number of different types of impurities
  call free_format_reader( iunit, ierr, 'n_imp_type', 1, ivalue=idummy(1) )
  if( ierr == 0 ) n_imp_type = idummy(1)

!     Paramagnetic (or not) calculations
  nspin = 1
  call free_format_reader( iunit, ierr, 'nspin', 1, ivalue=idummy(1) )
  if( ierr == 0 ) nspin = idummy(1)

!     Spin-block structure in hamiltonian
  call free_format_reader( iunit, ierr, 'so', 1, ivalue=idummy(1) )
  if( ierr == 0 ) so = idummy(1)

!     Magnetic field
  call free_format_reader( iunit, ierr, 'h_field', 1, rvalue=rdummy(1) )
  if( ierr == 0 ) h_field = rdummy(1)
    
!     CPA calculations mode
  call free_format_reader( iunit, ierr, 'cpa', 1, lvalue=ldummy(1) )
  if( ierr == 0 ) cpa = ldummy(1)

!     Verbosity
  call free_format_reader( iunit, ierr, 'verbos', 1, ivalue=idummy(1) )
  if( ierr == 0 ) verbos = idummy(1)

!     Total number of particles
  call free_format_reader( iunit, ierr, 'ntotal', 1, rvalue=rdummy(1) )
  if( ierr == 0 ) ntotal = rdummy(1)

!     Number of point in tail to use for different high-energy fittings
  call free_format_reader( iunit, ierr, 'ntail', 1, ivalue=idummy(1) )
  if( ierr == 0 ) ntail = idummy(1) - 1
  
!     Energy tolerance to find Fermi level
  call free_format_reader( iunit, ierr, 'fentoler', 1, rvalue=rdummy(1) )
  if( ierr == 0 ) fentoler = rdummy(1)
  
!     Total number of particle tolerance to find Fermi level
  call free_format_reader( iunit, ierr, 'fpatoler', 1, rvalue=rdummy(1) )
  if( ierr == 0 ) fpatoler = rdummy(1)
  
!     Type of real harmonics order to use (TB-LMTO, ESPRESSO, etc.)
  call free_format_reader( iunit, ierr, 'rhtm', 1, cvalue=rhtm )
  
!     Use block inverse for Green function calculations
  call free_format_reader( iunit, ierr, 'use_blockinverse', 1, lvalue=ldummy(1) )
  if( ierr == 0 ) use_blockinverse = ldummy(1)

!     Use analytical tetrahedron method to calculate Green function
  call free_format_reader( iunit, ierr, 'use_atm', 1, lvalue=ldummy(1) )
  if( ierr == 0 ) use_atm = ldummy(1)

!     Cuttof for analytical tetrahedron method
 ! call free_format_reader( iunit, ierr, 'atm_cutoff', 1, rvalue=rdummy(1) )
 ! if( ierr == 0 ) atm_cutoff = rdummy(1)

!     Fit self-energy by Lorentzians
  call free_format_reader( iunit, ierr, 'fit_sigma', 1, lvalue=ldummy(1) )
  if( ierr == 0 ) fit_sigma = ldummy(1)

!     Cuttof energy for fitting functions by Lorentzians
  call free_format_reader( iunit, ierr, 'ecut4fit', 1, rvalue=rdummy(1) )
  if( ierr == 0 ) ecut4fit = rdummy(1)

!     Number of Lorentzians
  call free_format_reader( iunit, ierr, 'n_lor', 1, ivalue=idummy(1) )
  if( ierr == 0 ) n_lor = idummy(1)


!-------------------------------------------------------------------------------  
!-------------------------------------------------------------------------------  
!-------------------------------------------------------------------------------  
!-------------------------------------------------------------------------------  
!-------------------------------------------------------------------------------  
!
!    Additional parameters for tools, etc
!
!     Use self-energy for calculation of bands, Fermi surface, etc...
  call free_format_reader( iunit, ierr, 'use_sigma', 1, lvalue=ldummy(1) )
  if( ierr == 0 ) use_sigma = ldummy(1)
  
!     External parameter with different meaning for different tools
  call free_format_reader( iunit, ierr, 'xi', 1, rvalue=rdummy(1) )
  if( ierr == 0 ) ksi = rdummy(1)

!     External parameter with different meaning for different tools
  call free_format_reader( iunit, ierr, 'zeta', 1, rvalue=rdummy(1) )
  if( ierr == 0 ) zeta = rdummy(1)

!     Index of energy points to use for Pade approxiamtion
  index4pade = (/ 1, 10, 15, 15, 20, 20, n_energy-2, n_energy /)
  call free_format_reader( iunit, ierr, 'index_4_pade', 8, ivalue=index4pade )
  
!     Division of the reciprocal space
  nkxyz = (/ -1, -1, -1 /)
  call free_format_reader( iunit, ierr, 'nkxyz', 3, ivalue=nkxyz )
  
!     Origin for Data explorer grid
  dxorigin = (/ 0.d0, 0.d0, 0.d0 /)  
  call free_format_reader( iunit, ierr, 'dxorigin', 3, rvalue=dxorigin )
  
!     Delta for Data explorer grid
  dxdelta = reshape( (/ (-777.d0, i=1,9 ) /), (/3,3/) ) 
  call free_format_reader( iunit, ierr, 'dxdelta', 9, rvalue=dxdelta )
  
!     Regular grid for Data explorer
  dxreggrid = .true.
  call free_format_reader( iunit, ierr, 'dxreggrid', 1, lvalue=ldummy(1) )
  if( ierr == 0 ) dxreggrid = ldummy(1)

!     Indexes of first and last bands crossing Fermi level
  fsbands = (/ 1, -1 /)
  call free_format_reader( iunit, ierr, 'fsbands', 2, ivalue=fsbands )
  
!     Use U, U-2J and U-3J parametrization for 3-band model (p electrons)
  call free_format_reader( iunit, ierr, 'kanamori', 1, lvalue=ldummy(1) )
  if( ierr == 0 ) kanamori = ldummy(1)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  
  close(iunit)

end subroutine read_ini
