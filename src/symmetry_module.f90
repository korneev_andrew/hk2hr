module symmetry_module
!   This module contains set of subroutines to symmetrize functions in orbital space.

  implicit none
  
  private
  
  integer :: i, j, k, is, iom
  integer :: n, n1, n2, ns, nom
  
  public set_symmetry
  
  interface set_symmetry
    module procedure set_symmetry_real2, set_symmetry_real4, set_symmetry_complex4
  end interface
  
!-------------------------------------------------------------------------------  
  contains
  
  subroutine set_symmetry_complex4( f, smask )
    implicit none
    
    integer    :: smask(:,:)
    complex(8) :: f(:,:,:,:)
    
    complex(8) :: fs
    integer,    allocatable :: diamask(:)
    complex(8), allocatable :: diaf(:,:,:)
    
    n   = size( f, dim=1 )
    nom = size( f, dim=3 )
    ns  = size( f, dim=4 )
    
    allocate( diamask(n), diaf(n,nom,ns) )
    
    forall( i = 1:n ) 
      diamask(i)  = smask(i,i)
      diaf(i,:,:) = f(i,i,:,:)
    end forall
    
    do iom = 1, nom
      do is = 1, ns
        where( smask == 0 ) f(:,:,iom,is) = cmplx(0.d0,0.d0,8)
      end do
    end do
    
    n1 = minval(diamask)
    if( n1 <= 0 ) n1 = 1
    n2 = maxval(diamask)
    
    do iom = 1, nom
      do is = 1, ns
        do i = n1, n2
          fs = sum( diaf(:,iom,is), diamask==i ) / count( diamask==i )
          do j = 1, n
            if( diamask(j) == i ) f(j,j,iom,is) = fs
          end do
        end do
      end do
    end do
    
    deallocate( diamask, diaf )
  
  end subroutine set_symmetry_complex4
  
  subroutine set_symmetry_real2( f, smask )
    implicit none
    
    integer :: smask(:,:)
    real(8) :: f(:,:)
    
    real(8) :: fs
    integer, allocatable :: diamask(:)
    real(8), allocatable :: diaf(:)
    
    n = size( f, dim=1 )
    allocate( diamask(n), diaf(n) )
    
    forall( i = 1:n ) 
      diamask(i) = smask(i,i)
      diaf(i)    = f(i,i)
    end forall
    
    where( smask == 0 ) f = 0.0
    
    n1 = minval(diamask)
    if( n1 <= 0 ) n1 = 1
    n2 = maxval(diamask)
    
    do i = n1, n2
      fs = sum( diaf, diamask==i ) / count( diamask==i )
      do j = 1, n
        if( diamask(j) == i ) f(j,j) = fs
      end do
    end do
    
    deallocate( diamask, diaf )
  
  end subroutine set_symmetry_real2
  
  subroutine set_symmetry_real4( f, smask )
    implicit none
    
    integer :: smask(:,:)
    real(8) :: f(:,:,:,:)
    
    real(8) :: fs
    integer, allocatable :: diamask(:)
    real(8), allocatable :: diaf(:,:,:)
    
    n   = size( f, dim=1 )
    nom = size( f, dim=3 )
    ns  = size( f, dim=4 )
    
    allocate( diamask(n), diaf(n,nom,ns) )
    
    forall( i = 1:n ) 
      diamask(i)  = smask(i,i)
      diaf(i,:,:) = f(i,i,:,:)
    end forall
    
    do iom = 1, nom
      do is = 1, ns
        where( smask == 0 ) f(:,:,iom,is) = 0.0
      end do
    end do
    
    n1 = minval(diamask)
    if( n1 <= 0 ) n1 = 1
    n2 = maxval(diamask)
    
    do iom = 1, nom
      do is = 1, ns
        do i = n1, n2
          fs = sum( diaf(:,iom,is), diamask==i ) / count( diamask==i )
          do j = 1, n
            if( diamask(j) == i ) f(j,j,iom,is) = fs
          end do
        end do
      end do
    end do
    
    deallocate( diamask, diaf )
  
  end subroutine set_symmetry_real4
  
end module symmetry_module
