subroutine free_format_reader( iunit, ios, vname, n, ivalue, rvalue, zvalue, lvalue, cvalue )
!
!   This subroutine reads data from a free form file
!
!   Inputs:
!     iunit -  unit identifier which refers to a file
!     vname -  variable name
!     n     -  size of array to be read
!   Outputs:
!     ios       -  error condition
!          < 0  -  variable not found in the file (end-of-file/end-of-record)
!          = 0  -  no error
!          > 0  -  read error condition occurs
!     ?value    -  data array of needed datatype where
!        ? = i  -  default integer
!          = r  -  real
!          = z  -  complex type of format x y without brackets, commas, etc 
!          = l  -  logical ( T, .TRUE., 1 for true, otherwise false )
!          = c  -  character
!   Remarks:
!     Only one datatype has to appear when calling.
!      Examples: 
!         ....
!         character(8) :: a(3)
!         real(8) :: rdummy(5), umtrx(3,3)
!         ....
!         ....
!         call free_format_reader( 5, ios, 'Names', 3, cvalue=a )    
!         call free_format_reader( 5, ios, 'umtrx', 9, rvalue=umtrx )    
!         ....
!     To read scalar variable use a dummy array of size 1
!      Example:   
!         ....
!         call free_format_reader( 5, ios, 'Beta', 1, rvalue=rdummy(1) )  
!         ....
!      Corresponding input file:
!
!           Names = Alex, John Richard  ! comma separates fields
!                            #   John and Richard are different fields
!           beta = 10.07
!           UMTRX = 0 3; 3       \      ! Continuation on the next line
!              #   semicolon separates fields
!                   3, 0 3       &      ! Continuation on the next line
!                   2 2 0.1
!                 
    use mpi_module

    implicit none 

    integer,      intent(in   ) :: iunit
    character(*), intent(in   ) :: vname
    integer,      intent(  out) :: ios
    integer,      intent(in   ) :: n

    integer,      intent(inout), optional :: ivalue(*)
    real(8),      intent(inout), optional :: rvalue(*)
    complex(8),   intent(inout), optional :: zvalue(*)
    logical,      intent(inout), optional :: lvalue(*)
    character(*), intent(inout), optional :: cvalue(*)

    integer, parameter :: length_limit = 8192           !  string length

    integer :: i, sym_end
    character(length_limit) :: acname, data_string
    logical :: presence_check(5)
    real(8), allocatable :: re(:), im(:)

!   Checks the presence of the variable of one type only

    presence_check(1) = present(ivalue)
    presence_check(2) = present(rvalue)
    presence_check(3) = present(zvalue)
    presence_check(4) = present(lvalue)
    presence_check(5) = present(cvalue)

    if( count(presence_check) /= 1 .or. n < 1 )  call stop_program('Debug message: wrong call of free_format_reader.$')

    acname = vname
    call change_case( 1, acname )

!   Reads string with a given name

    call string_reader( iunit, acname, ios, data_string )
    if( ios /= 0 ) return

!   Assignment

    if( presence_check(1) )    read(data_string,*,iostat=ios) ( ivalue(i), i = 1,n )

    if( presence_check(2) )    read(data_string,*,iostat=ios) ( rvalue(i), i = 1,n )

    if( presence_check(3) )then
      allocate( re(n), im(n) )
      read(data_string,*,iostat=ios) ( re(i), im(i), i = 1,n )
      zvalue(1:n) = cmplx( re(1:n), im(1:n), 8 )
      deallocate( re, im )
    end if

    if( presence_check(4) )then
      lvalue(1:n) = .false.
      do i = 1,n
        sym_end  = scan( adjustl(trim(data_string)), ' ' )
        if( sym_end == 0 )then
          ios = -1
          exit
        end if
        data_string = adjustl(trim(data_string(1:sym_end)))
        if( index( data_string(1:sym_end),'1' )       /= 0  .or.      &
            index( data_string(1:sym_end), '.TRUE.' ) /= 0  .or.      &
            index( data_string(1:sym_end), 'T' )      /= 0  .or.      &
            index( data_string(1:sym_end), 'YES' )    /= 0  .or.      &
            index( data_string(1:sym_end), 'Y' )      /= 0 )    lvalue(i) = .true.
        data_string = data_string(sym_end+1:)
      end do
    end if

    if( presence_check(5) )  read(data_string,*,iostat=ios) ( cvalue(i), i = 1,n )

    if( ios /= 0 )   call stop_program('Wrong format in input file for variable '//vname//'.$')

end subroutine free_format_reader
!
!=================================================================================
!
subroutine string_reader( iunit, vname, ios, data_string )
!
!   Reads the string with a given token from file 
!
!   Inputs:
!     iunit   -  unit identifier which refers to a file
!     vname   -  variable name
!   Outputs:  
!     ios         -  error condition
!          < 0    -  variable not found in the file (end-of-file/end-of-record)
!          = 0    -  no error
!          > 0    -  read error condition occurs
!     data_string -  string to read data
!     
!   Remarks:
!       There are few special characters and parameters
!     ! and #  - comments
!     & and \  - line continuation
!     , and ;  - data separator
!     =        - separates variable name and data
!
!     File is retricted to 80 columns (length_limit)
!
    implicit none 

    integer,      intent(in   ) :: iunit
    character(*), intent(in   ) :: vname
    integer,      intent(  out) :: ios
    character(*), intent(  out) :: data_string
    
    character(1), parameter :: comment1 = achar(33)   !  exclamation mark (!)
    character(1), parameter :: comment2 = achar(35)   !  number sign      (#)
    character(1), parameter :: cont1 = achar(38)      !  ampersand        (&)
    character(1), parameter :: cont2 = achar(92)      !  left slash       (\)
    character(1), parameter :: separ1 = achar(44)     !  comma            (,)
    character(1), parameter :: separ2 = achar(59)     !  semicolon        (;)
    character(1), parameter :: eql    = achar(61)     !  equal sign       (=)
    
    integer, parameter :: length_limit = 8192         !  string length 
    integer, parameter :: ncont = 14                  !  controls input field length
    
    integer :: line_start, line_end, vlength
    character(length_limit) :: tstring
    character(1) :: tchar
    
    rewind(iunit)

    vlength = len_trim(adjustl(vname))
    
    do 
      read( iunit, "(A)", iostat=ios )   tstring
      if( ios /= 0 ) exit
      call change_case( 1, tstring )
!
!    Checks for empty and commented strings
!
      tstring = adjustl(tstring)
    
      if( len_trim( tstring ) == 0 )                     cycle
      if( scan( tstring, comment1//comment2 ) == 1 )     cycle
!
!    Checks for comment inside string
!
      line_end = scan( tstring, comment1//comment2 )
      if( line_end == 0 )then
         data_string = trim(tstring)
        else
         data_string = tstring(1:line_end-1)
      end if
!
!    Search variable by name
!        
      line_start = index( data_string, adjustl(trim(vname)) )
      
      if( line_start == 0 )                       cycle
!
!    Check it is part of another name or not
!
      if( line_start >  1 )then
        tchar = data_string(line_start-1:line_start-1)
        if( tchar /= ' ' .and. tchar /= separ1 .and. tchar /= separ2 )     cycle
      end if
    
      data_string = data_string(line_start:)
      tchar = data_string(1+vlength:1+vlength)
      if( tchar /= ' ' .and. tchar /= eql ) cycle
    
      line_start = scan( data_string, eql )
      data_string = data_string(line_start+1:)
!
!    Checks line continuation
!
      do
        line_end = scan( data_string, cont1//cont2 )
        if( line_end == 0 ) exit
        
        read( iunit, "(A)", iostat=ios )   tstring
        if( ios /= 0 ) exit
        call change_case( 1, tstring )
!
!    Checks for empty or commented strings
!
        tstring = adjustl(tstring)
  
        if( len_trim( tstring ) == 0 )                      cycle
        if( scan( tstring, comment1//comment2 ) == 1 )      cycle
  
        data_string = data_string(1:line_end-1)
!
!    Checks for comment inside string
!
        line_end = scan( tstring, comment1//comment2 )
        if( line_end == 0 )then
           data_string = trim(data_string) //' '// trim(tstring)
          else
           data_string = trim(data_string) //' '// tstring(1:line_end-1)
        end if

      end do    
      
      exit
    end do

end subroutine string_reader
!
!=======================================================================
!
subroutine change_case( iopt, str )
!
!    Changes the case of a word
!
!i Inputs:
!i   iopt  = -1 to convert from uppercase to lowercase
!i            1 to convert from lowercase to uppercase
!io Inputs/Outputs:
!i   str   - string to convert
!
  implicit none
! Passed variables:
  integer     , intent(in   ) :: iopt
  character(*), intent(inout) :: str
! Local variables:
  integer :: i, icha

  do i = 1, len(str)
    icha = iachar(str(i:i))
    if(      iopt == -1 )then
      if( icha >= 65 .and. icha <= 90  )  str(i:i) = achar(icha+32)
    elseif( iopt ==  1 )then
      if( icha >= 97 .and. icha <= 122 )  str(i:i) = achar(icha-32)
    end if
  end do

end subroutine change_case

logical function tag_exist( iunit, tagname )
!
!    Checks existence of a tag in opened file.
!    It returns true if tag found successfully and false otherwise.
!
  implicit none

  integer,      intent(in   ) :: iunit              !   Unit specifier
  character(*), intent(in   ) :: tagname            !   Tag name

!  Local variables

  integer, parameter :: length_limit = 8192         !  string length 
  character(1), parameter :: comment2 = achar(35)   !  number sign      (#)
  character(1), parameter :: cont1 = achar(38)      !  ampersand        (&)

  integer :: ios, line_end
  character(length_limit) :: tstring, data_string, tname

  tag_exist = .false.

  rewind(iunit)

  tname = tagname
  call change_case( 1, tname )

  do
    read( iunit, "(A)", iostat=ios )   tstring
    if( ios /= 0 ) exit

    call change_case( 1, tstring )
!
!    Checks for empty and commented strings
!
    tstring = adjustl(tstring)

    if( len_trim( tstring ) == 0 )           cycle
    if( scan( tstring, comment2 ) == 1 )     cycle
!
!    Checks for comment inside string
!
    line_end = scan( tstring, comment2 )
    if( line_end == 0 )then
      data_string = trim(tstring)
    else
      data_string = tstring(1:line_end-1)
    end if

    if( scan( data_string, cont1 ) /= 1 )    cycle

    if( adjustl(trim( data_string(2:) )) == tname )then
      tag_exist = .true.
      return
    end if

  end do

end function tag_exist
