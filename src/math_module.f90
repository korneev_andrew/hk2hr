module math_module
  implicit none
  private
  public spur, first_deriv, pade_coefficients, pade, Factorial, ClebschGordan, testherm

  interface spur
    module procedure dspur, zspur
  end interface 

  interface first_deriv
    module procedure first_deriv_central, first_deriv_rl
  end interface

  contains

!==============================================================================

  real(8) function dspur( a, n )
    implicit none
    integer, intent (in   ) :: n
    real(8), intent (in   ) :: a(n,n)

    integer :: i

    dspur = 0.d0
    do i = 1, n
      dspur = dspur + a(i,i)
    end do
  end function dspur

  complex(8) function zspur( a, n )
    implicit none
    integer,    intent (in   ) :: n
    complex(8), intent (in   ) :: a(n,n)

    integer :: i

    zspur = cmplx(0.d0,0.d0, 8)
    do i = 1, n
      zspur = zspur + a(i,i)
    end do
  end function zspur

!==============================================================================

  real(8) function first_deriv_rl( f, h, n, key )
!
!    Calculates right/left (forward/backward) first derivative of a function
!
!   If key=R - right derivative will be calculated at point number 1
!      key=L - left  derivative will be calculated at point N
!   For reference see:
!        J.M. McCormick, M.G. Salvadori, Numerical Methods in Fortran,
!        Prentice-Hall Inc., 1964, p. 40, table 3.2.1
!        
    implicit none
    integer, intent(in   ) :: n          !  number of points to interpolate function
    real(8), intent(in   ) :: h          !  step (x_i+1 - x_i)
    real(8), intent(in   ) :: f(n)       !  function
    character(1), intent(in   ) :: key   !  right or left derivative to be calculated

    integer :: i
    real(8) :: g(n)

    integer, parameter, dimension(5,4) ::                &
                a = reshape( (/ -1,  1,  0,  0,  0,      &
                                -3,  4, -1,  0,  0,      &
                               -11, 18, -9,  2,  0,      &
                               -25, 48,-36, 16, -3 /), (/5,4/) )

    if( key == 'R' .or. key == 'r' )then
       g = f
      else
       forall( i = 1:n ) g(i) = - f(n-i+1)
    end if

    select case (n)
      case(2)
        first_deriv_rl = sum( a(1:n,1)*g ) 
      case(3)
        first_deriv_rl = sum( a(1:n,2)*g ) / 2
      case(4)
        first_deriv_rl = sum( a(1:n,3)*g ) / 6
      case(5)
        first_deriv_rl = sum( a(1:n,4)*g ) / 12
      case default
        write(14,*)' STOP in first_deriv (side): Sorry, no such case  '
        stop
    end select

    first_deriv_rl = first_deriv_rl / h

  end function first_deriv_rl

  real(8) function first_deriv_central( f, h, n )
!
!    Calculates central first derivative of a function
!
!   For reference see:
!        http://en.wikipedia.org/wiki/Finite_difference_coefficients
!
    implicit none

    integer, intent(in   ) :: n     !  number of points to interpolate function
    real(8), intent(in   ) :: h     !  step (x_i+1 - x_i)
    real(8), intent(in   ) :: f(n)  !  function

    integer, parameter, dimension(5,3) ::                &
                a = reshape( (/ -1,  1,  0,  0,  0,      &
                                -1,  0,  1,  0,  0,      &
                                 1, -8,  0,  8, -1 /), (/5,3/) )

    select case (n)
      case(2)
        first_deriv_central = sum( a(1:n,1)*f )
      case(3)
        first_deriv_central = sum( a(1:n,2)*f ) / 2
      case(5)
        first_deriv_central = sum( a(1:n,3)*f ) / 12
      case default
        write(14,*)' STOP in first_deriv (central): Sorry, no such case  '
        stop
    end select

    first_deriv_central = first_deriv_central / h

  end function first_deriv_central

!==============================================================================

  real(8) function Factorial( n )
    implicit none
    integer, intent(in   ) :: n
    
    integer :: l
    
    l = n

    Factorial = 1
    do while ( l > 0 )
      Factorial = Factorial * l
      l = l - 1
    end do
    
  end function Factorial

  real(8) function ClebschGordan( j1,m1, j2,m2, j,m )
!
!  Return Clebsch-Gordan coefficient <j1j2m1m2|j1j2jm>
!  Last definition from Wolfram site
!  http://functions.wolfram.com/HypergeometricFunctions/ClebschGordan/06/01/
!
    integer, intent(in   ) :: j1, m1       !   Angular momentum and its projection
    integer, intent(in   ) :: j2, m2
    integer, intent(in   ) :: j,  m
  
    integer :: k, si
    real(8) :: coef, a 
  
    ClebschGordan = 0
    
    if( abs(j1+j2) < j .or. j < abs(j1-j2) )  return     !  Triangle rule
    if( abs(m1+m2-m) > 0 )                    return     !  Selection rule
    if( mod(j1+j2+j,1) > 1.e-14 )             return     !  Implemented for integers only
    if( j1 < 0 .or. j2 < 0 )                  return 
    if( abs(m1) > j1 .or. abs(m2) > j2 )      return 
    
    coef = (2*j+1)*Factorial(1+j+j1+j2)/Factorial(j1+j2-j)/Factorial(j1-j2+j)/Factorial(-j1+j2+j)
    coef = coef * Factorial(j+m)*Factorial(j-m)*Factorial(j1+m1)*Factorial(j1-m1)
    coef = coef / Factorial(j2+m2) / Factorial(j2-m2)
    coef = sqrt(coef)

    if( mod(j1-m1,2) == 1 ) coef = -coef

    a  = 0
    si = 1
    do k = 0, min(j-m,j1-m1)
      a = a + si*Factorial(j1+j2-m-k)*Factorial(j+j2-m1-k) /        &
                 Factorial(k) / Factorial(j1-m1-k) /                &
                 Factorial(j-m-k)/Factorial(1+j+j1+j2-k)
      si = -si
    end do
    
    ClebschGordan = coef * a
    
  end function ClebschGordan


!==============================================================================
  subroutine pade_coefficients( a, z, n )
!  
!             Computation of Pade coefficients.		    
!    Inputs:							    
!io    a  -  On input is a function for approximation
!            on output contains Pade coefficients		
!i     z  -  complex points in which function a is determined
!i     n  -  size of arrays.					
! 
!r   Remarks: 						
!         (J. of Low Temp. Phys., v29, n3/4, 1977)		       
!						    
    implicit none
!--->  Passed variables
    integer,       intent(in   ) :: n
    complex(8), intent(in   ) :: z(n)
    complex(8), intent(inout) :: a(n)
!--->  Local variables 
    integer :: i, j
    complex(8), allocatable :: g(:,:)
     
    allocate( g(n,n) )
    
    g(1,:) = a   

    do j = 2, n
      do i = 2, j
        g(i,j) = ( g(i-1,i-1)-g(i-1,j) ) / ( z(j)-z(i-1) ) / g(i-1,j)
      end do
    end do
    
    forall( i = 1:n ) a(i) = g(i,i)
      
    deallocate( g )
    
  end subroutine pade_coefficients

  subroutine pade( f, z1, z, p, n )
!
!	     Calculation of analytical function	
!    in the arbitrary complex point for a given Pade coefficients 
!								       
!    Inputs:							       
!i     p  -  Pade coefficients				       
!i     z  -  set of points from which analytical continue is performed
!i     n  -  size of arrays				       
!i     z1 -  complex point					       
!    Outputs:							       
!i     f  -  value of function
!
    implicit none
!---> Passed variables
    integer,    intent(in   ) :: n
    complex(8), intent(in   ) :: z1
    complex(8), intent(in   ) :: z(n)
    complex(8), intent(in   ) :: p(n)
    complex(8), intent(  out) :: f
!---> Local variables
    integer :: i
    real(8) :: cof1, cof2
    complex(8) :: a1, a2, b1, b2, anew, bnew

    cof1 = 1.d0
    cof2 = 1.d0
  
    a1 = cmplx(0.d0,0.d0, 8)
    a2 = cof1*p(1)
    b1 = cmplx(cof1,0.d0, 8)
    b2 = cmplx(cof1,0.d0, 8)
    
    do i = 1,n-1
      anew = a2 + ( z1 - z(i) ) * p(i+1) * a1
      bnew = b2 + ( z1 - z(i) ) * p(i+1) * b1
      a1   = a2
      b1   = b2
      a2   = anew
      b2   = bnew
    end do
      
    a2 = cof2 * a2
    b2 = cof2 * b2
    f  = a2 / b2
  
  end subroutine pade

  subroutine testherm( a, n, aisherm )
!  Test matrix for s^{+} = s
    implicit none
    integer,    intent(in   ) :: n
    complex(8), intent(in   ) :: a(n,n)
    logical,    intent(  out) :: aisherm

    integer :: i, j
    real(8) :: sdr, sdi

    aisherm = .true.
    do i = 1, n
      do j = i+1, n
        sdr = real( a(i,j) - a(j,i), 8 )
        sdi = aimag( a(i,j) + a(j,i) )
        if( abs(sdr) > 1.d-10 .or. abs(sdi)  > 1.d-10 )then
          aisherm = .false.
          return
        end if
      end do
    end do
    
  end subroutine testherm

end module math_module
