subroutine cpa_sigma
!
!   This subroutine calculates the CPA self-energy
!
  use parameter_module
  use impurity_module
  use tools_module
  use iolib_module
  use energy_module

  implicit none

  integer :: i, j, itype, jtype, ierr
  integer :: ndims(4)

  integer, allocatable :: ija(:,:)
  logical, allocatable :: cpa_equiv(:)
  complex(8), allocatable :: G_cpa(:,:,:,:), Sigma_cpa(:,:,:,:)

  allocate( cpa_equiv(n_imp_type) )
  cpa_equiv = .false.

  do itype = 1, n_imp_type-1

    if( cpa_equiv(itype) ) cycle

    ndims = shape( impurity(itype)%Green )
    allocate( G_cpa(ndims(1),ndims(2),ndims(3),ndims(4)), Sigma_cpa(ndims(1),ndims(2),ndims(3),ndims(4)) )

    G_cpa = impurity(itype)%x_cpa * impurity(itype)%Green
    cpa_equiv(itype) = .true.

    do jtype = itype+1, n_imp_type
      if( impurity(itype)%nlm /= impurity(jtype)%nlm ) cycle

      if( allocated( ija ) ) deallocate( ija )
      allocate( ija(impurity(itype)%n_imp,impurity(jtype)%n_imp ) )

      forall( i = 1:impurity(itype)%n_imp, j = 1:impurity(jtype)%n_imp )       &
              ija(i,j) = impurity(itype)%hposition(i) - impurity(jtype)%hposition(j)

      if( all( ija /= 0 ) ) cycle

      G_cpa = G_cpa + impurity(jtype)%x_cpa * impurity(jtype)%Green
      cpa_equiv(jtype) = .true.
    end do

    if( verbos >= 20 )   call write_function( mkfn('G_cpa',resuf,itype), G_cpa, prn_mesh, ierr, 'r' )

    if( verbos >= 25 )   call write_function( mkfn('G0-1',resuf,itype), impurity(itype)%Green0, prn_mesh, ierr, 'r' )

!      Sigma = G0^{-1} - G^{-1} 
    call inverse( impurity(itype)%Green0 )

    call inverse( G_cpa )

    Sigma_cpa = impurity(itype)%Green0 - G_cpa

    if( verbos >= 20 )   call write_function( mkfn('Sigma_cpa',resuf,itype), Sigma_cpa, prn_mesh, ierr, 'a' )

    impurity(itype)%Sigma = Sigma_cpa
    do jtype = itype+1, n_imp_type
      if( impurity(itype)%nlm /= impurity(jtype)%nlm ) cycle

      if( allocated( ija ) ) deallocate( ija )
      allocate( ija(impurity(itype)%n_imp,impurity(jtype)%n_imp ) )

      forall( i = 1:impurity(itype)%n_imp, j = 1:impurity(jtype)%n_imp )       &
        ija(i,j) = impurity(itype)%hposition(i) - impurity(jtype)%hposition(j)

      if( all( ija /= 0 ) ) cycle

      impurity(jtype)%Sigma = Sigma_cpa
    end do

    deallocate( G_cpa, Sigma_cpa )

  end do

#ifdef DEBUG
  if( verbos >= 20 )then
    do itype = 1, n_imp_type
      call write_function( mkfn('Sigma_cpa1',resuf,itype), impurity(itype)%Sigma, prn_mesh, ierr, 'r' )
    end do
  end if
#endif

  deallocate( cpa_equiv )

end subroutine cpa_sigma
