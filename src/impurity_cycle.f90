subroutine impurity_cycle
!
!    This module does cycle over impurities with different type of solvers
!
  use parameter_module
  use impurity_module
  use tools_module
  use timer_module
  use iolib_module
  use energy_module
  use symmetry_module
  use total_energy_module
  use fit_module
  use moments_module

  implicit none

  integer :: ierr, n_orb
  integer :: itype, i
  real(8) :: e_n_dis
  type(t_timer), pointer :: imp_time=>null()
  complex(8) :: TrSigmaG
  complex(8), allocatable :: g_1(:,:,:,:)

  call save_sigma

  do itype = 1, n_imp_type

    if( verbos > 0 )then
      write(14,"(/,80('+'),//,30x,'Impurity #',i3,7x,a,/,10x,'Effective quantum impurity solver is ',a)")            &
                   itype, impurity(itype)%name, impurity(itype)%solver
    end if

    if( impurity(itype)%solver == 'NONE' )    cycle

    if( verbos > 0 )then
      call add_timer( imp_time,' Solver time:' )
      call start_timer( imp_time )
    end if

    n_orb = impurity(itype)%nlm

    allocate( g_1(n_orb,n_orb,n_energy,nspin) )

! Calculates Green function of effective medium: G0^{-1} = G^{-1} + Sigma
    impurity(itype)%Green0 = impurity(itype)%Green
    call inverse( impurity(itype)%Green0 )
    impurity(itype)%Green0 = impurity(itype)%Green0 + impurity(itype)%Sigma
    call inverse( impurity(itype)%Green0 )

    call set_symmetry( impurity(itype)%Green0, impurity(itype)%smask )

! Print out
    if( verbos >= 20 )  call write_function( mkfn('Green0',resuf,itype), impurity(itype)%Green0, prn_mesh, ierr, 'a' )

! Calculates position of the impurity level, e_d (double counting)
    call define_e_d( itype )

    if( verbos > 0 ) write(14,"(/,' Double counting type is ',a,/,' Double counting value ',2(1x,f10.4))")            &
                               impurity(itype)%dc_type, impurity(itype)%dcc

!        Call solver

    select case (impurity(itype)%solver)
!                   Hirsch-Fye QMC solver
    case ('HF-QMC')
      call wrapper4hf_qmc( itype )
!                   CT-QMC-HYB solver with segments
    case ('CT-QMC-W')
      call wrapper4ct_qmc_hyb( itype )
!                   Exact diagonalization (Arnoldi) solver
    case ('ED')
      call wrapper4ed( itype )
!                   CPA solver
    case ('CPA')
      call wrapper4cpa( itype )
    end select

    if( verbos >= 25 )    call write_function( mkfn('Green_imp',resuf,itype), impurity(itype)%Green, prn_mesh, ierr, 'a' )

    if( .not. impurity(itype)%improved_estimator )then
!      Calculates self-energy: Sigma = G0^{-1} - G^{-1}
      impurity(itype)%Sigma = impurity(itype)%Green0
      call inverse( impurity(itype)%Sigma )
      g_1 = impurity(itype)%Green
      call inverse( g_1 )
      impurity(itype)%Sigma = impurity(itype)%Sigma - g_1
    end if

    deallocate( g_1 )

    call set_symmetry( impurity(itype)%Sigma, impurity(itype)%smask )

!      Print out
    if( verbos > 0 )  call write_function( mkfn('Sigma',resuf,itype), impurity(itype)%Sigma, prn_mesh, ierr, 'a' )

    call TraceSigmaG( impurity(itype)%Sigma, impurity(itype)%Green, TrSigmaG )

    if( verbos > 0 )then
      write(14,"(/,' Tr Sigma_imp G_imp',30x,2f18.8)") TrSigmaG

      e_n_dis = 0
      do i = 1, n_orb
        e_n_dis = e_n_dis + impurity(itype)%cpoc(i,i,1) * impurity(itype)%n_cor(i)
      end do

      if( nspin == 1 )then
        e_n_dis = 2 * e_n_dis
      else
        do i = 1, n_orb
          e_n_dis = e_n_dis + impurity(itype)%cpoc(i,i,2) * impurity(itype)%n_cor(i+n_orb)
        end do
      end if

      if( cpa ) write(14,"(' Energy due to potential disorder, Vcpa*n, x*Vcp*n ',2(1x,f14.8))")                        &
                             e_n_dis, impurity(itype)%x_cpa*e_n_dis
    end if

    if( verbos > 0 )      call stop_timer( imp_time )

  end do

  call mix_sigma

end subroutine impurity_cycle
