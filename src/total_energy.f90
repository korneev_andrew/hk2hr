module total_energy_module
!
!   Calculation of different contributions to DMFT total energy
!
  use parameter_module
  use impurity_module
  use energy_module
  use hamiltonian_module
  use qlmp_module
  use math_module
  use mpi_module
  use iolib_module

  implicit none

  contains

  subroutine total_energy
    integer :: is, itype, i, ierr
    integer :: n_orb
    real(8) :: ntot(2), dcc
    character(8) :: dccname
    complex(8) :: e_kin, TrSigmaG, dc_energy

!      Kinetic term
    call kinetic_energy( e_kin )

    if( verbos > 0 ) write(14,"(/,' Kinetic part',12x,2f18.8)")  e_kin

!      Calculation of the Tr Sigma G (interacting term) and double-counting terms
!
    do itype = 1, n_imp_type
      n_orb = impurity(itype)%nlm

      if( verbos > 0 ) write(14,"(/,'  Impurity type ',i3)") itype

      call TraceSigmaG( impurity(itype)%Sigma, impurity(itype)%Green, TrSigmaG )

      if( verbos > 0 ) write(14,"(' Tr Sigma G',14x,2f18.8)") TrSigmaG
!
!        Double-counting term
!
      dccname = impurity(itype)%dc_type

      do i = 1, nspin
        ntot(i) = spur( impurity(itype)%occupation(:,:,i), n_orb )
      end do
      if( nspin == 1 ) ntot(2) = ntot(1)

      select case (dccname)
!   Fully localized limit
        case('FLL     ')
          dc_energy = impurity(itype)%u_value * sum(ntot) * ( sum(ntot) - 1 ) - &
                      impurity(itype)%j_value * ( ntot(1) * ( ntot(1) - 1 ) + ntot(2) * ( ntot(2) - 1 ) )
          dc_energy = dc_energy / 2
!   Simplified fully localized limit
        case('SFLL    ')
          dcc = impurity(itype)%u_value - ( n_orb - 1 ) * impurity(itype)%j_value / ( 2*n_orb - 1 )
          do is = 1, nspin
            impurity(itype)%dcc(is) = - dcc*( sum(ntot) - impurity(itype)%xsfll/2 )
          end do

          dc_energy = - sum(ntot) * sum(impurity(itype)%dcc(:)) / 2
!   Double counting is a given fixed constant.
!   Then energy is defined as Edc = - N * edc / 2
        case default
          read(dccname,*,iostat=ierr) dcc

          if( ierr == 0 )  impurity(itype)%dcc(:) = dcc

          dc_energy = - sum(ntot) * dcc / 2

      end select

      if( verbos > 0 )     write(14,"(' Double-counting energy  ',2f18.8 )") dc_energy

    end do         !  itype

  end subroutine total_energy

  subroutine TraceSigmaG( Sigma, Green, TrSigmaG )
!
!     Calculation of   1/2 Tr Sigma*G     (Coulomb correlation term)
!
    implicit none

    complex(8), intent(in   ) :: Sigma(:,:,:,:)
    complex(8), intent(in   ) :: Green(:,:,:,:)
    complex(8), intent(  out) :: TrSigmaG

    integer :: is, iom, n_orb, n, nsp
    complex(8), allocatable :: gkz(:,:,:), n_k(:,:)

    TrSigmaG = zero

    if( sum( abs( shape(Sigma) - shape(Green) ) ) /= 0 ) return

    n_orb = size( Sigma, dim=1 )
    n     = size( Sigma, dim=3 )
    nsp   = size( Sigma, dim=4 )

    allocate( n_k(n_orb,n_orb), gkz(n_orb,n_orb,n) )

    do is = 1, nsp
      gkz = zero
      do iom = 1, n
        gkz(1:n_orb,1:n_orb,iom) = matmul( Sigma(:,:,iom,is), Green(:,:,iom,is) )
      end do

!   Sum over Matsubara frequencies
      n_k = zero
      call sum_iwn_matrix_z( gkz(1:n_orb,1:n_orb,:), fermionic, ntail, n_k(1:n_orb,1:n_orb) )

      TrSigmaG = TrSigmaG + spur( n_k(1:n_orb,1:n_orb), n_orb ) / 2
    end do

    if( nspin == 1 ) TrSigmaG = 2 * TrSigmaG

    deallocate( n_k, gkz )

  end subroutine TraceSigmaG

  subroutine kinetic_energy( e_kin )
    implicit none

    complex(8), intent(  out) :: e_kin

    integer :: nkp_proc, ikp_glob
    integer :: is, iom, itype, ikp, i, j, n1, n2, isp
    complex(8) :: z
    complex(8), allocatable :: Vpot(:,:), gkz(:,:,:), n_k(:,:)

    allocate( Vpot(ndim,ndim), gkz(ndim,ndim,n_energy), n_k(ndim,ndim) )

    nkp_proc = nkp / numprocs
    if( proc_id < mod(nkp,numprocs) )  nkp_proc = nkp_proc + 1
!
!    Calculation of n_k occupation for thermal average of kinetic term
!
    e_kin = zero
    do is = 1, nspin
      do ikp = 1, nkp_proc
        ikp_glob = proc_id + 1 + (ikp-1)*numprocs

        do iom = 1, n_energy
          z = zenergy(iom)
          if( nspin == 2 .and. dabs(h_field) > 1.d-4 ) z = z - ( 1.5 - is )*h_field

!       Make "local potential": Sigma, Double counting, etc
          Vpot = zero
          do itype = 1, n_imp_type
            do i = 1, impurity(itype)%n_imp
              n1 = impurity(itype)%hposition(i)
              n2 = n1 + impurity(itype)%nlm - 1

              isp = is
              if( impurity(itype)%magtype(i) == -1 ) isp = 3 - is
              Vpot(n1:n2,n1:n2) = impurity(itype)%Sigma(:,:,iom,isp)

              forall( j = n1:n2 ) Vpot(j,j) = Vpot(j,j) + ( 1.5d0 - is )*impurity(itype)%h_field_local*impurity(itype)%magtype(i)
            end do
          end do

!       Calculate G(k,z)
          call gk( gkz(:,:,iom),  h(:,:,ikp_glob,is), Vpot, ndim, z )

        end do   ! n_energy

!         Calculate n_k
        call occupation_matrix_z( gkz, fermionic, ntail, n_k )

!       Thermal average of kinetic term
        do i = 1, ndim
          do j = 1, ndim
            e_kin = e_kin + wtkp(ikp_glob)*h(i,j,ikp_glob,is)*n_k(j,i)
          end do
        end do

      end do     ! nkp
    end do       ! nspin

    call mpi_collect( e_kin, 1 )

    if( nspin == 1 .and. so == 0 ) e_kin = 2 * e_kin

    deallocate( Vpot, gkz, n_k )

  end subroutine kinetic_energy

end module total_energy_module
