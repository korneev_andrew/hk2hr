subroutine wrapper4ed( itype )
!
!    This module does cycle over impurities with different type of solvers
!
  use parameter_module
  use impurity_module
  use tools_module
  use iolib_module
  use energy_module
  use moments_module
  use mpi_module
  use ed_arnoldi_module
  use symmetry_module
  
  implicit none

  integer, intent(in   ) :: itype 
  
  integer :: n_orb, n_bath
  integer :: ierr, i, k, is, iom
  logical :: lmom(0:3) = .true.
  logical :: lsisj
  real(8) :: xmu_save, dS
  
  integer, allocatable :: ie(:,:), iv(:,:,:)
  real(8), allocatable :: Epsk_loc(:,:), Vk_loc(:,:,:)
  real(8), allocatable :: Eimp(:,:)
  complex(8), allocatable :: Gw(:,:,:)
  complex(8), allocatable :: sisj(:,:)

  n_orb  = impurity(itype)%nlm
  n_bath = impurity(itype)%nbath

  xmu_save = xmu
  xmu = 0.d0
  call complex_energy_mesh

!   Calculates Delta of effective medium: Delta = iwn - e_d - G0^{-1}

   !     forall( i = 1:n_orb ) impurity(itype)%Green0(i,i,:,:) = impurity(itype)%Green0(i,i,:,:) - cmplx(0,0.1)

  impurity(itype)%Delta = -impurity(itype)%Green0
  call inverse( impurity(itype)%Delta )
  forall( i = 1:n_orb, is = 1:nspin )  impurity(itype)%Delta(i,i,:,is) = impurity(itype)%Delta(i,i,:,is) + zenergy

!     Define constant term in real part

  if( imenergy )then 
    call get_moments( impurity(itype)%Delta(:,:,n_energy-ntail:n_energy,:), fermionic(n_energy-ntail:n_energy),        &
                      impurity(itype)%moments_g0, lmom )
    impurity(itype)%e_d = impurity(itype)%moments_g0(0,:,:,:)
  else
    impurity(itype)%e_d = real( impurity(itype)%Delta(:,:,1,:) + impurity(itype)%Delta(:,:,n_energy,:), 8 ) / 2.d0
  end if

  forall( iom = 1:n_energy ) impurity(itype)%Delta(:,:,iom,:) = impurity(itype)%Delta(:,:,iom,:) - impurity(itype)%e_d

!   Particle-hole symmetry case
#ifdef PHsym
  impurity(itype)%Delta = cmplx( 0, imag(impurity(itype)%Delta), prec )
#endif

!     Print out
  if( verbos > 0 )      call write_matrix( 14, impurity(itype)%e_d, 'Calculated e_d$', 'Spin # $', ierr )
  if( verbos >= 15 )    call write_function( mkfn('Delta',resuf,itype), impurity(itype)%Delta, prn_mesh, ierr, 'a' )

  impurity(itype)%e_d = impurity(itype)%e_d + impurity(itype)%cpoc
  if( impurity(itype)%cpoc_only ) impurity(itype)%e_d = impurity(itype)%cpoc

!     Print out
  if( verbos > 0 .and. sum(abs(impurity(itype)%cpoc)) > 1.d-8 )                                                        &
      call write_matrix( 14, impurity(itype)%e_d, 'Used e_d$', 'Spin # $', ierr )
!
!     Fit hybridization function (Delta) or bath Green function (G0)
!
  allocate( Epsk_loc(n_bath,nspin), Vk_loc(n_bath,n_orb,nspin), ie(n_bath,nspin), iv(n_bath,n_orb,nspin) )

  if( proc_id == master )then
    call rw_epsk( itype, ierr, 'r', Epsk_loc, Vk_loc, ie, iv, nspin, n_bath, n_orb )

    if( ierr == 0 )then
      impurity(itype)%Epsk = Epsk_loc
      impurity(itype)%Vk   = Vk_loc
    else
      Epsk_loc = impurity(itype)%Epsk
      Vk_loc   = impurity(itype)%Vk  
    end if

    select case ( impurity(itype)%fit )
      case(1:)
        call fitting_delta( impurity(itype)%Delta, zenergy, Epsk_loc, Vk_loc, impurity(itype)%e_d,                     &
                            ie, iv, n_orb, n_bath, nspin, n_energy, impurity(itype)%fit, impurity(itype)%weight,       &
                            impurity(itype)%ufp, impurity(itype)%fix_ed, ntail )
      case(:-1)
        call fitting_delta( impurity(itype)%Green0, zenergy, Epsk_loc, Vk_loc, impurity(itype)%e_d,                    &
                            ie, iv, n_orb, n_bath, nspin, n_energy, impurity(itype)%fit, impurity(itype)%weight,       &
                            impurity(itype)%ufp, impurity(itype)%fix_ed, ntail )
      case default
        write(14,"(/,' There is no fit of cluster parameters.')")
    end select
   
!    Error of parameter fitting
    dS = sum( abs( impurity(itype)%Epsk - Epsk_loc ) ) / size(Epsk_loc)
    write(14,*) 'Error of Epsk parameters ', dS

 !  where( abs( impurity(itype)%Epsk - Epsk_loc ) < 10.0 )
    impurity(itype)%Epsk = ( 1 - alpha )*impurity(itype)%Epsk + alpha*Epsk_loc
 !  else where( abs(Epsk_loc) > 10 )
 !    impurity(itype)%Epsk = impurity(itype)%Epsk + 1/Epsk_loc
 !  end where

    dS = sum( abs( impurity(itype)%Vk   - Vk_loc ) ) / size(Vk_loc)
    write(14,*) 'Error of Vk parameters   ', dS

 !  where( abs( impurity(itype)%Vk - Vk_loc ) < 10.0 )
      impurity(itype)%Vk   = ( 1 - alpha )*impurity(itype)%Vk   + alpha*Vk_loc
 !  else where( abs(Vk_loc) > 10.0 )
 !    impurity(itype)%Vk = impurity(itype)%Vk + 1/Vk_loc
 !  end where

 !   dS = sum( abs( impurity(itype)%cpoc - impurity(itype)%e_d ) ) / size(impurity(itype)%e_d)
 !   write(14,*) 'Error of cpoc parameters ', dS
 !
 !  if( .not. impurity(itype)%fix_ed )then
 !    where( abs( impurity(itype)%cpoc - impurity(itype)%e_d ) < 10.0 )
 !      impurity(itype)%cpoc = ( 1 - alpha )*impurity(itype)%cpoc + alpha*impurity(itype)%e_d
 !    else where( abs(impurity(itype)%e_d) > 10.0 )
 !      impurity(itype)%cpoc = impurity(itype)%cpoc + 1/impurity(itype)%e_d
 !    end where
 !  end if

    if( .not. impurity(itype)%fix_ed )     call write_matrix( 14, impurity(itype)%e_d, 'Fitted e_d$', 'Spin # $', ierr )

    call rw_epsk( itype, ierr, 'w', impurity(itype)%Epsk, impurity(itype)%Vk, ie, iv, nspin, impurity(itype)%nbath, n_orb )
  end if

  deallocate( Epsk_loc, Vk_loc, ie, iv )

  call mpi_broadcast( impurity(itype)%Epsk,  master )
  call mpi_broadcast( impurity(itype)%Vk,    master )
  call mpi_broadcast( impurity(itype)%e_d,  master )
  call mpi_broadcast( impurity(itype)%umtrx, master )

!   Construct Delta out of fitting parameters
  impurity(itype)%Delta = zero
  do is = 1, nspin
    do i = 1, n_orb
      do iom = 1, n_energy
        do k = 1, n_bath
          impurity(itype)%Delta(i,i,iom,is) = impurity(itype)%Delta(i,i,iom,is)       +   &
                             impurity(itype)%Vk(k,i,is) * impurity(itype)%Vk(k,i,is)  /   &
                             ( zenergy(iom) - impurity(itype)%Epsk(k,is) )
        end do ! k
      end do ! iw
    end do ! i
  end do ! is

  if( verbos >= 25 )    call write_function( mkfn('Delta_cl', resuf, itype), impurity(itype)%Delta, prn_mesh, ierr, 'a' )

!   Construct G0 out of fitting parameters
  impurity(itype)%Green0 = - impurity(itype)%Delta

  forall( iom = 1:n_energy )            impurity(itype)%Green0(:,:,iom,:) =  impurity(itype)%Green0(:,:,iom,:) - impurity(itype)%e_d

  forall( i = 1:n_orb, is = 1:nspin )   impurity(itype)%Green0(i,i,:,is) = impurity(itype)%Green0(i,i,:,is) + zenergy

  call inverse( impurity(itype)%Green0 )

  if( verbos >= 25 )    call write_function( mkfn('Green0_cl', resuf, itype), impurity(itype)%Green0, prn_mesh, ierr, 'a' )

!   Initialize the impurity level and call Arnoldi solver
  allocate( Eimp(n_orb,nspin), Gw(n_energy,n_orb,nspin), sisj(n_energy,n_orb) )

  forall ( i = 1:n_orb )   Eimp(i,:) = impurity(itype)%e_d(i,i,:) + impurity(itype)%dcc(:)

!   Solver

  lsisj = .false.
  call ed_arnoldi( impurity(itype)%umtrx, Eimp, impurity(itype)%Epsk,          &
              impurity(itype)%Vk, beta, verbos, Gw, sisj, zenergy,             &
              impurity(itype)%lunn, lsisj, impurity(itype)%Nd_min,             &
              impurity(itype)%Nd_max, impurity(itype)%Nc_max,                  &
              impurity(itype)%Nh_max, impurity(itype)%ed_opt,                  &
              impurity(itype)%ed_arno_nev, impurity(itype)%ed_arno_ncv0,       &
              impurity(itype)%ed_arno_tol, impurity(itype)%ed_gf_nev,          &
              impurity(itype)%ed_gf_decomp, impurity(itype)%ed_gf_tol  )
                  
  impurity(itype)%Green = zero
  forall ( i = 1:n_orb )   impurity(itype)%Green(i,i,:,:) = Gw(:,i,:)

  if( verbos >= 25 )    call write_function( mkfn('Green_cl', resuf, itype), impurity(itype)%Green, prn_mesh, ierr, 'a' )

  deallocate( Eimp, Gw, sisj )

!   Restore Fermi level
  xmu = xmu_save
  call complex_energy_mesh

end subroutine wrapper4ed
