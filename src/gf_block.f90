module gf_block
!
!   Calculation of the Green function with the block inversion.
!
!   Written by A. Poteryaev  < Alexander.Poteryaev _at_ optics.imp.uran.ru >
!
  use lapack_module

  implicit none
  
  complex(8), private, parameter :: zero = (0.d0,0.d0)

  public gf_block_inversion

  interface gf_block_inversion
    module procedure gf_block_inversion_11, gf_block_inversion_all
  end interface 
  
  contains
  
  subroutine gf_block_inversion_11( h_11, h_21, c, e2, gf_11, z )
    implicit none
    
    complex(8), intent(in   ) :: z
    complex(8), intent(in   ) :: h_11(:,:)
    complex(8), intent(in   ) :: h_21(:,:)
    complex(8), intent(in   ) :: c(:,:)
    real(8),    intent(in   ) :: e2(:)

    complex(8), intent(  out) :: gf_11(:,:)
    
    integer :: ldim, idim
    integer :: i, j, k
    complex(8), allocatable :: d(:,:)
    
    ldim = size( gf_11, dim=1 )
    idim = size( c, dim=1 )
    
    allocate( d(idim,idim) )
    
!              C^+ [z-e2]^{-1} C    
    d = zero
    do i = 1, idim
      do j = 1, idim
        do k = 1, idim
          d(i,j) = d(i,j) + conjg(c(j,k)) * c(i,k) / ( z - e2(k) )
        end do
      end do
    end do
    
    gf_11 = matmul( transpose( conjg(h_21) ), matmul( d, h_21 ) )

    gf_11 = - gf_11 - h_11
    forall( i = 1:ldim ) gf_11(i,i) = gf_11(i,i) + z
    
    call inverse_matrix( gf_11 )
    
    deallocate( d )

  end subroutine gf_block_inversion_11
  
  
  subroutine gf_block_inversion_all( h_11, h_21, c, e2, gf_11, gf_21, gf_22, z )
    implicit none
    
    complex(8), intent(in   ) :: z
    complex(8), intent(in   ) :: h_11(:,:)
    complex(8), intent(in   ) :: h_21(:,:)
    complex(8), intent(in   ) :: c(:,:)
    real(8),    intent(in   ) :: e2(:)
    complex(8), intent(  out) :: gf_11(:,:)
    complex(8), intent(  out) :: gf_21(:,:)
    complex(8), intent(  out) :: gf_22(:,:)
    
    integer :: ldim, idim
    integer :: i, j, k
    
    ldim = size( gf_11, dim=1 )
    idim = size( gf_22, dim=1 )
    
!              C^+ [z-e2]^{-1} C    
    gf_22 = zero
    do i = 1, idim
      do j = 1, idim
        do k = 1, idim
          gf_22(i,j) = gf_22(i,j) + conjg(c(j,k)) * c(i,k) / ( z - e2(k) )
        end do
      end do
    end do
    
    gf_21 = matmul( Gf_22, h_21 )
    
    gf_11 = matmul( transpose( conjg(h_21) ), gf_21 )

    gf_11 = - gf_11 - h_11
    forall( i = 1:ldim ) gf_11(i,i) = gf_11(i,i) + z
    
    call inverse_matrix( gf_11 )

!     Gf_21    
    gf_21 = matmul( gf_21, gf_11 )
   
!     Gf_22

    gf_22 = gf_22 + matmul( gf_21, matmul(transpose( conjg(h_21) ), gf_22 ) )
 
  end subroutine gf_block_inversion_all
  
end module gf_block
