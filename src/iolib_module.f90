module iolib_module
  implicit none
  private

  character(32), parameter :: cformat="(1x,es13.5,50(1x,2es19.10))"
  character(32), parameter :: iformat="(1x,i5,50(1x,2es19.10))"
  character(32), parameter :: mformat="(50(1x,f10.5))"

  integer       :: i, j, k, l, ni, nj, nom, ns
  character(8)  :: pos
  character(32) :: fi, fj
  real(8)       :: ddum, re(2), im(2)
  
    
  interface write_function
    module procedure write_complex_function1, write_complex_function2,     &
                     write_complex_function3, write_complex_function4,     &
                     write_real_function1, write_real_function2,           &
                     write_real_function3, write_real_function4,           &
                     write2_real_function4, write_real_function11,         &
                     write_real_function21, write_real_function31, write_real_function41
  end interface
  
  interface write_matrix
    module procedure write_real_matrix2, write_real_matrix3, write_real_matrix3_1
  end interface
  
  interface read_function
    module procedure read_complex_function4
  end interface
  
  interface mkfn
    module procedure mkfn1, mkfn2
  end interface

  public :: write_function, read_function, mkfn, write_matrix

  contains

  character(32) function mkfn1( filename, iname )
    implicit none

    integer,      intent(in   ) :: iname
    character(*), intent(in   ) :: filename

    write(fi,*) iname
    mkfn1 = trim(adjustl(filename))//'_'//trim(adjustl(fi))

  end function mkfn1

  character(32) function mkfn2( filename, suff, iname )
    implicit none

    integer,      intent(in   ) :: iname
    character(*), intent(in   ) :: filename
    character(*), intent(in   ) :: suff

    write(fi,*) iname
    mkfn2 = trim(adjustl(filename))//trim(adjustl(suff))//'_'//trim(adjustl(fi))

  end function mkfn2
  
!============================================================================
!   This set of subroutines writes multidimensional array to file.
!============================================================================
  
  subroutine write_complex_function1( filename, f, x, ios, ra )
    implicit none
    
    integer      :: ios
    character(*) :: filename
    real(8)      :: x(:)
    complex(8)   :: f(:)
    character(1), optional :: ra
    
    nom = size( f, dim=1 )
    
    if( size(x) /= nom )then
      ios = 777
      return
    end if
    
    pos = 'rewind'
    if( present(ra) .and. ra == 'a' ) pos = 'append'
    
    fj = trim(adjustl(filename))//'.dat'
    open( 72, file=fj, form='formatted', status='unknown', position=pos, iostat=ios )
    if( ios /= 0 ) return

    do k = 1, nom
      write(72,cformat,iostat=ios) x(k), f(k)
    end do
    write(72,*);  write(72,*);   close(72)
    
  end subroutine write_complex_function1
  
  subroutine write_complex_function2( filename, f, x, ios, ra )
    implicit none
    
    integer      :: ios
    character(*) :: filename
    real(8)      :: x(:)
    complex(8)   :: f(:,:)
    character(1), optional :: ra
    
    nom = size( f, dim=1 )
    ns  = size( f, dim=2 )
    
    if( size(x) /= nom )then
      ios = 777
      return
    end if
    
    pos = 'rewind'
    if( present(ra) .and. ra == 'a' ) pos = 'append'
    
    fj = trim(adjustl(filename))//'.dat'
    open( 72, file=fj, form='formatted', status='unknown', position=pos, iostat=ios )
    if( ios /= 0 ) return

    do k = 1, nom
      write(72,cformat,iostat=ios) x(k), ( f(k,l), l=1,ns )
    end do
    write(72,*);  write(72,*);   close(72)
 
  end subroutine write_complex_function2
  
  subroutine write_complex_function3( filename, f, x, ios, ra )
    implicit none
    
    integer      :: ios
    character(*) :: filename
    real(8)      :: x(:)
    complex(8)   :: f(:,:,:)
    character(1), optional :: ra
    
    ni  = size( f, dim=1 )
    nom = size( f, dim=2 )
    ns  = size( f, dim=3 )
    
    if( size(x) /= nom )then
      ios = 777
      return
    end if
    
    pos = 'rewind'
    if( present(ra) .and. ra == 'a' ) pos = 'append'
    
    do i = 1, ni
      write(fi,*) i 
      fj = trim(adjustl(filename))//'_'//trim(adjustl(fi))//'.dat'
      open( 72, file=fj, form='formatted', status='unknown', position=pos, iostat=ios )
      if( ios /= 0 ) return

      do k = 1, nom
        write(72,cformat,iostat=ios) x(k), ( f(i,k,l), l=1,ns )
      end do
      write(72,*);  write(72,*);   close(72)
    end do
    
  end subroutine write_complex_function3
  
  subroutine write_complex_function4( filename, f, x, ios, ra )
    implicit none
    
    integer      :: ios
    character(*) :: filename
    real(8)      :: x(:)
    complex(8)   :: f(:,:,:,:)
    character(1), optional :: ra
    
    ni  = size( f, dim=1 )
    nj  = size( f, dim=2 )
    nom = size( f, dim=3 )
    ns  = size( f, dim=4 )
    
    if( size(x) /= nom )then
      ios = 777
      return
    end if
    
    pos = 'rewind'
    !if( present(ra) .and. ra == 'a' ) pos = 'append'
    if( present(ra) )then
      if( ra == 'a' ) pos = 'append'
    end if
    
    if( aimag( sum( f(1,1,:,ns) ) ) == 0.d0 .and. ns == 2 ) ns = 1
    
    do i = 1, ni
      write(fi,*) i 
      do j = 1, nj
        if( abs( aimag( sum( f(i,j,:,1) )/nom ) ) < 1.e-8 ) cycle
    
        write(fj,*) j 
        fj = trim(adjustl(filename))//'_'//trim(adjustl(fi))//','//trim(adjustl(fj))//'.dat'
        open( 72, file=fj, form='formatted', status='unknown', position=pos, iostat=ios )
        if( ios /= 0 ) return

        do k = 1, nom
          write(72,cformat,iostat=ios) x(k), ( f(i,j,k,l), l=1,ns )
        end do
        write(72,*);  write(72,*);   close(72)
      end do
    end do
    
  end subroutine write_complex_function4

!==============================================================================================
  
  subroutine write_real_function1( filename, f, x, ios, ra )
    implicit none
    
    integer      :: ios
    character(*) :: filename
    real(8)      :: x(:)
    real(8)      :: f(:)
    character(1), optional :: ra
    
    nom = size( f, dim=1 )
    
    if( size(x) /= nom )then
      ios = 777
      return
    end if
    
    pos = 'rewind'
    if( present(ra) .and. ra == 'a' ) pos = 'append'
    
    fj = trim(adjustl(filename))//'.dat'
    open( 72, file=fj, form='formatted', status='unknown', position=pos, iostat=ios )
    if( ios /= 0 ) return

    do k = 1, nom
      write(72,cformat,iostat=ios) x(k), f(k)
    end do
    write(72,*);  write(72,*);   close(72)
    
  end subroutine write_real_function1
  
  subroutine write_real_function2( filename, f, x, ios, ra )
    implicit none
    
    integer      :: ios
    character(*) :: filename
    real(8)      :: x(:)
    real(8)      :: f(:,:)
    character(1), optional :: ra
    
    nom = size( f, dim=1 )
    ns  = size( f, dim=2 )
    
    if( size(x) /= nom )then
      ios = 777
      return
    end if
    
    pos = 'rewind'
    if( present(ra) .and. ra == 'a' ) pos = 'append'
    
    fj = trim(adjustl(filename))//'.dat'
    open( 72, file=fj, form='formatted', status='unknown', position=pos, iostat=ios )
    if( ios /= 0 ) return

    do k = 1, nom
      write(72,cformat,iostat=ios) x(k), ( f(k,l), l=1,ns )
    end do
    write(72,*);  write(72,*);   close(72)
    
  end subroutine write_real_function2
  
  subroutine write_real_function3( filename, f, x, ios, ra )
    implicit none
    
    integer      :: ios
    character(*) :: filename
    real(8)      :: x(:)
    real(8)      :: f(:,:,:)
    character(1), optional :: ra
    
    ni  = size( f, dim=1 )
    nom = size( f, dim=2 )
    ns  = size( f, dim=3 )
    
    if( size(x) /= nom )then
      ios = 777
      return
    end if
    
    pos = 'rewind'
    if( present(ra) .and. ra == 'a' ) pos = 'append'
    
    do i = 1, ni
      write(fi,*) i 
      fj = trim(adjustl(filename))//'_'//trim(adjustl(fi))//'.dat'
      open( 72, file=fj, form='formatted', status='unknown', position=pos, iostat=ios )
      if( ios /= 0 ) return

      do k = 1, nom
        write(72,cformat,iostat=ios) x(k), ( f(i,k,l), l=1,ns )
      end do
      write(72,*);  write(72,*);   close(72)
    end do
    
  end subroutine write_real_function3
 
  subroutine write_real_function11( filename, f, ios, ra )
    implicit none
    
    integer      :: ios
    character(*) :: filename
    real(8)      :: f(:)
    character(1), optional :: ra
    
    nom = size( f, dim=1 )

    pos = 'rewind'
    if( present(ra) .and. ra == 'a' ) pos = 'append'
    
    fj = trim(adjustl(filename))//'.dat'
    open( 72, file=fj, form='formatted', status='unknown', position=pos, iostat=ios )
    if( ios /= 0 ) return

    do k = 1, nom
      write(72,iformat,iostat=ios) k, f(k)
    end do
    write(72,*);  write(72,*);
    close(72)

  end subroutine write_real_function11

  subroutine write_real_function21( filename, f, ios, ra )
    implicit none
    
    integer      :: ios
    character(*) :: filename
    real(8)      :: f(:,:)
    character(1), optional :: ra
    
    ni  = size( f, dim=1 )
    nom = size( f, dim=2 )
    
    pos = 'rewind'
    if( present(ra) .and. ra == 'a' ) pos = 'append'
    
    do i = 1, ni
      write(fi,*) i 
      fj = trim(adjustl(filename))//'_'//trim(adjustl(fi))//'.dat'
      open( 72, file=fj, form='formatted', status='unknown', position=pos, iostat=ios )
      if( ios /= 0 ) return

      do k = 1, nom
        write(72,iformat,iostat=ios) k, f(i,k)
      end do
      write(72,*);  write(72,*);   close(72)
    end do
    
  end subroutine write_real_function21

  subroutine write_real_function31( filename, f, ios, ra )
    implicit none
    
    integer      :: ios
    character(*) :: filename
    real(8)      :: f(:,:,:)
    character(1), optional :: ra
    
    ni  = size( f, dim=1 )
    nom = size( f, dim=2 )
    ns  = size( f, dim=3 )
    
    pos = 'rewind'
    if( present(ra) .and. ra == 'a' ) pos = 'append'
    
    do i = 1, ni
      write(fi,*) i 
      fj = trim(adjustl(filename))//'_'//trim(adjustl(fi))//'.dat'
      open( 72, file=fj, form='formatted', status='unknown', position=pos, iostat=ios )
      if( ios /= 0 ) return

      do k = 1, nom
        write(72,iformat,iostat=ios) k, ( f(i,k,l), l=1,ns )
      end do
      write(72,*);  write(72,*);   close(72)
    end do
    
  end subroutine write_real_function31

  subroutine write_real_function4( filename, f, x, ios, ra )
    implicit none
    
    integer      :: ios
    character(*) :: filename
    real(8)      :: x(:)
    real(8)      :: f(:,:,:,:)
    character(1), optional :: ra
    
    ni  = size( f, dim=1 )
    nj  = size( f, dim=2 )
    nom = size( f, dim=3 )
    ns  = size( f, dim=4 )
    
    if( size(x) /= nom )then
      ios = 777
      return
    end if

    pos = 'rewind'
    if( present(ra) .and. ra == 'a' ) pos = 'append'
    
    if( sum( f(1,1,:,ns) ) == 0.d0 .and. ns == 2 ) ns = 1
    
    do i = 1, ni
      write(fi,*) i 
      do j = i, nj
        if( sum( f(i,j,:,1) ) == 0.d0 ) cycle
    
        write(fj,*) j 
        fj = trim(adjustl(filename))//'_'//trim(adjustl(fi))//','//trim(adjustl(fj))//'.dat'
        open( 72, file=fj, form='formatted', status='unknown', position=pos, iostat=ios )
        if( ios /= 0 ) return

        do k = 1, nom
          write(72,cformat,iostat=ios) x(k), ( f(i,j,k,l), l=1,ns )
        end do
        write(72,*);  write(72,*);   close(72)
      end do
    end do
    
  end subroutine write_real_function4

  subroutine write_real_function41( filename, f, ios, ra )
    implicit none
    
    integer      :: ios
    character(*) :: filename
    real(8)      :: f(:,:,:,:)
    character(1), optional :: ra
    
    ni  = size( f, dim=1 )
    nj  = size( f, dim=2 )
    nom = size( f, dim=3 )
    ns  = size( f, dim=4 )
    
    pos = 'rewind'
    if( present(ra) .and. ra == 'a' ) pos = 'append'
    
    if( sum( f(1,1,:,ns) ) == 0.d0 .and. ns == 2 ) ns = 1
    
    do i = 1, ni
      write(fi,*) i 
      do j = i, nj
        if( sum( f(i,j,:,1) ) == 0.d0 ) cycle
    
        write(fj,*) j 
        fj = trim(adjustl(filename))//'_'//trim(adjustl(fi))//','//trim(adjustl(fj))//'.dat'
        open( 72, file=fj, form='formatted', status='unknown', position=pos, iostat=ios )
        if( ios /= 0 ) return

        do k = 1, nom
          write(72,iformat,iostat=ios) k, ( f(i,j,k,l), l=1,ns )
        end do
        write(72,*);  write(72,*);   close(72)
      end do
    end do
    
  end subroutine write_real_function41

  subroutine write2_real_function4( filename, f, f2, x, ios, ra )
    implicit none
    
    integer      :: ios
    character(*) :: filename
    real(8)      :: x(:)
    real(8)      :: f(:,:,:,:)
    real(8)      :: f2(:,:,:,:)
    character(1), optional :: ra
    
    ni  = size( f, dim=1 )
    nj  = size( f, dim=2 )
    nom = size( f, dim=3 )
    ns  = size( f, dim=4 )
    
    i = sum( abs( shape(f) - shape(f2) ) )
    if( i /= 0 )then
      ios = 666
      return
    end if

    if( size(x) /= nom )then
      ios = 777
      return
    end if
    
    pos = 'rewind'
    if( present(ra) .and. ra == 'a' ) pos = 'append'
    
    if( sum( f(1,1,:,ns) ) == 0.d0 .and. ns == 2 ) ns = 1
    
    do i = 1, ni
      write(fi,*) i 
      do j = i, nj
        if( sum( f(i,j,:,1) ) == 0.d0 ) cycle
    
        write(fj,*) j 
        fj = trim(adjustl(filename))//'_'//trim(adjustl(fi))//','//trim(adjustl(fj))//'.dat'
        open( 72, file=fj, form='formatted', status='unknown', position=pos, iostat=ios )
        if( ios /= 0 ) return

        do k = 1, nom
          write(72,cformat,iostat=ios) x(k), ( f(i,j,k,l), l=1,ns ), ( f2(i,j,k,l), l=1,ns )
        end do
        write(72,*);  write(72,*);   close(72)
      end do
    end do
    
  end subroutine write2_real_function4
  
!==============================================================================================
  
  subroutine read_complex_function4( filename, f, ios )
    implicit none
    
    integer      :: ios
    character(*) :: filename
    complex(8)   :: f(:,:,:,:)
    
    ni  = size( f, dim=1 )
    nj  = size( f, dim=2 )
    nom = size( f, dim=3 )
    ns  = size( f, dim=4 )
    
    do i = 1, ni
      write(fi,*) i 
      do j = i, nj
        write(fj,*) j 
        fj = trim(adjustl(filename))//'_'//trim(adjustl(fi))//','//trim(adjustl(fj))//'.dat'
        open( 72, file=fj, form='formatted', status='old', position='rewind', iostat=ios )

        if( ios /= 0 )then
          f(i,j,:,:) = cmplx(0.d0,0.d0, 8)
        else
          do k = 1, nom
            read(72,*,iostat=ios) ddum, ( re(l), im(l), l=1,ns )
            if( ios /= 0 ) return
            f(i,j,k,1:ns) = cmplx( re(1:ns), im(1:ns), 8 )
          end do
        end if
        
        close(72)
      end do
    end do
 
  end subroutine read_complex_function4
!
!    This set of subroutines writes a matrix 
!
  subroutine write_real_matrix2( iunit, a, atext, ios )
    implicit none

    integer :: iunit
    integer :: ios
    real(8) :: a(:,:)
    character(*) :: atext
    
    integer :: n

    ni = size( a, dim=1 )
    nj = size( a, dim=2 )
    
    n = scan( atext, '$' ) - 1
    write(iunit,"(/,5x,a)") trim(adjustl(atext(1:n)))

    do i = 1, ni
      write(iunit,mformat,iostat=ios) ( a(i,j), j = 1, nj )
    end do
    
  end subroutine write_real_matrix2
  
  subroutine write_real_matrix3( iunit, a, atext, btext, ios )
    implicit none

    integer :: iunit
    integer :: ios
    real(8) :: a(:,:,:)
    character(*)  :: atext
    character(*)  :: btext
   
    integer :: n
    
    ni = size( a, dim=1 )
    nj = size( a, dim=2 )
    ns = size( a, dim=3 )
    
    n = scan( atext, '$' ) - 1
    !gfortran
    !Fortran runtime error: Insufficient data descriptors in format after reversion
    !write(iunit,"(/,5x,a)") trim(adjustl(atext(1:n)))
    write(iunit,*)
    write(iunit,*) "     "//trim(adjustl(atext(1:n)))

    do k = 1, ns
      n = scan( btext, '$' ) - 1
      !gfortran
      !Fortran runtime error: Insufficient data descriptors in format after reversion
      !write(iunit,"(1x,a,i5)") trim(adjustl(btext(1:n))), k
      write(iunit,*) " "//trim(adjustl(btext(1:n))), k
      do i = 1, ni
        write(iunit,mformat,iostat=ios) ( a(i,j,k), j = 1, nj )
      end do
    end do
    
  end subroutine write_real_matrix3
  
  subroutine write_real_matrix3_1( iunit, a, atext, btext, ctext, ios )
    implicit none

    integer :: iunit
    integer :: ios
    real(8) :: a(:,:,:)
    character(*)  :: atext
    character(*)  :: btext
    character(*)  :: ctext

    integer :: i, n
    real(8) :: spur
    
    ni = size( a, dim=1 )
    nj = size( a, dim=2 )
    ns = size( a, dim=3 )
    
    n = scan( atext, '$' ) - 1
    write(iunit,"(/,5x,a)") trim(adjustl(atext(1:n)))

    do k = 1, ns
      n = scan( btext, '$' ) - 1
      write(iunit,"(1x,a,i5)") trim(adjustl(btext(1:n))), k
      do i = 1, ni
        write(iunit,mformat,iostat=ios) ( a(i,j,k), j = 1, nj )
      end do
     
      n = scan( ctext, '$' ) - 1
      spur = 0.d0
      do i = 1, min(ni,nj)
        spur = spur + a(i,i,k)
      end do
     
      write(iunit,"(1x,a,5x,f12.8)") trim(adjustl(ctext(1:n))), spur
    end do
    
  end subroutine write_real_matrix3_1
  
end module iolib_module
