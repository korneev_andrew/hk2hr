subroutine hamiltonian_name( iuham, ham_type, ios )
!
!   Defines the name of the Hamiltonian file.
!   It attempts to identify given name with the known format.
!
!   The possible formats are well described in documentation and listed below.
!    ham.ini     - tag-based format (suppose to be main).
!    WANN_H.OUT  - ELK format.
!    HMLT        - TB-LMTO format.
!    hamiltonian - Quantum Espresso
!    hamilt      - Quantum Espresso (simplified).
!    HK          - very old fashioned (from old versions of code).
!
  implicit none

  integer,        intent(in   ) :: iuham                     !   Unit specifier for Hamiltonian
  character(128), intent(inout) :: ham_type                  !   Name of the Hamiltonian
  integer,        intent(  out) :: ios                       !   Error status

  integer :: i
  character(128), dimension(6) :: names_of_h

  names_of_h(1) = 'ham.ini'
  names_of_h(2) = 'WANN_H.OUT'
  names_of_h(3) = 'HMLT'
  names_of_h(4) = 'hamiltonian'
  names_of_h(5) = 'hamilt'
  names_of_h(6) = 'HK'
!
!  Attempt to open Hamiltonian file by given name and connect it with known file format
!
  open( iuham, file=ham_type, form='formatted', status='old', iostat=ios, action='read', position='rewind' )

  if( ios == 0 )then
    do i = 1, 6
      if( index( trim(ham_type), trim(names_of_h(i)) ) /= 0 )then
        ham_type = names_of_h(i)
        return
      end if
    end do
  end if
!
!  Attempt to open Hamiltonian file by standard name
!
  do i = 1, 6
    open( iuham, file=names_of_h(i), form='formatted', status='old', iostat=ios, action='read', position='rewind' )

    if( ios == 0 )then
      ham_type = names_of_h(i)
      return
    end if

    ios = -11111
  end do

end subroutine hamiltonian_name

subroutine read_hamiltonian( iuham, ham_name, ios )
!
!   Read Hamiltonian form disk file 
!
  use hamiltonian_module
  use units

  implicit none

  integer,       intent(in   ) :: iuham                     !   Unit specifier for Hamiltonian
  character(32), intent(in   ) :: ham_name                  !   Type of format for Hamiltonian
  integer,       intent(  out) :: ios                       !   Error status

  integer :: i, j, ikp, is, natom
  real(8) :: efermi, hrr, hii
  real(8), allocatable :: hr(:,:,:,:), hi(:,:,:,:)
  logical :: tag_exist

  nsham = 1

  select case ( trim(ham_name) )
    case( 'ham.ini' )
!
!  Read tag-based Hamiltonian
!

!   Defines main dimensions
      nsham = 1
      if( tag_exist( iuham, 'nspin' ) )    read(iuham,*) nsham
      if( nsham <= 0 )then
        ios = -1
        return
      end if

      nkp = -1
      if( tag_exist( iuham, 'nkp' ) )      read(iuham,*) nkp
      if( nkp <= 0 )then
        ios = -1
        return
      end if

      ndim = -1
      if( tag_exist( iuham, 'dim' ) )      read(iuham,*) ndim
      if( ndim <= 0 )then
        ios = -1
        return
      end if

      ntet = -1
      if( tag_exist( iuham, 'ntet' ) )     read(iuham,*) ntet

!    Reads k-points
      if( nkp > 0 .and. tag_exist( iuham, 'kpoints' ) )then
        allocate( wtkp(nkp), bk(3,nkp), stat=ios )
        do ikp = 1, nkp
          read(iuham,*,iostat=ios) wtkp(ikp), ( bk(i,ikp), i=1,3 )
          if( ios /= 0 )then
            ios = -abs(ios)
            return
          end if
        end do
        wtkp = wtkp / sum(wtkp)
      end if

!    Reads tetrahedra
      if( ntet > 0 .and. tag_exist( iuham, 'tetrahedron' ) )then
        if( .not. allocated( itt ) ) allocate( itt(5,ntet) )
        read(iuham,*,iostat=ios) ( (itt(i,j), i=1,5 ), j=1, ntet )
        if( ios /= 0 )then
          ios = -abs(ios)
          return
        end if
      end if

!    Reads Hamiltonian
      if( .not. tag_exist( iuham, 'hamiltonian' ) )then
        ios = -1
        return
      end if

      allocate( h(ndim,ndim,nkp,2), hr(ndim,ndim,nkp,2), hi(ndim,ndim,nkp,2), stat=ios )

      do is = 1, nsham
        do ikp = 1, nkp
          read(iuham,*,iostat=ios) (( hr(i,j,ikp,is), j=i,ndim), i=1,ndim )
          if( ios /= 0 )then
            ios = -abs(ios)
            return
          end if
          read(iuham,*,iostat=ios) (( hi(i,j,ikp,is), j=i,ndim), i=1,ndim )
          if( ios /= 0 )then
            ios = -abs(ios)
            return
          end if
        end do
      end do

      h = cmplx( hr, hi, 8 )

      deallocate( hr, hi )

      if( nsham == 1 ) h(:,:,:,2) = h(:,:,:,1)

!    Reads DFT/LDA Fermi level
      efermi = 0
      if( tag_exist( iuham, 'fermi' ) )      read(iuham,*) efermi

      forall( i = 1:ndim ) h(i,i,:,:) = h(i,i,:,:) - efermi

      ios = -14001

    case( 'HK' )
!
!  Read HK format
!
      read(iuham,*,iostat=ios) nkp, ndim

      allocate( h(ndim,ndim,nkp,2), wtkp(nkp), bk(3,nkp), stat=ios )

      if( ios /= 0 )then
        ios = - abs(ios)
        return
      end if

      read(iuham,*,iostat=ios) ( wtkp(ikp), ikp=1, nkp )

      read(iuham,*,iostat=ios) (( bk(i,j), i=1,3 ), j=1, nkp )
      do ikp = 1, nkp
        read(1,*,iostat=ios) (( h(i,j,ikp,1), j=i,ndim ), i=1, ndim )
      end do

      if( is_iostat_end(ios) )then
        deallocate( bk )
        ios = 0
        rewind(iuham)
        read(iuham,*,iostat=ios) nkp, ndim

        read(iuham,*,iostat=ios) ( wtkp(ikp), ikp=1,nkp )
        do ikp = 1, nkp
          read(iuham,*,iostat=ios) (( h(i,j,ikp,1), j=i,ndim ), i=1, ndim )
        end do

        if( is_iostat_end(ios) )   return
      end if

      wtkp = wtkp / sum(wtkp)
      h(:,:,:,2) = h(:,:,:,1)

      h = h * energy_factor

      ios = -14001

    case( 'HMLT' )
!
!  Read HTML format (TB-LMTO)
!
      read(iuham,*,iostat=ios) nkp, ntet
      read(iuham,*,iostat=ios) nsham, ndim

      allocate( wtkp(nkp), bk(3,nkp), stat=ios )
      if( ios /= 0 )then
        ios = - abs(ios)
        return
      end if

!   Reading weights and inequivalent k-points from "HMLT"

      read(iuham,*,iostat=ios) efermi
      read(iuham,*,iostat=ios) ( wtkp(i), i=1, nkp )
      read(iuham,*,iostat=ios) ( ( bk(i,j), i=1,3 ), j=1, nkp )

!   Reading inequivalent tetrahedra from file "HMLT" 

      if( ntet > 0 )then
        if( .not. allocated( itt ) ) allocate( itt(5,ntet) )
        read(iuham,*,iostat=ios) ( (itt(i,j), i=1,5 ), j=1, ntet )
      end if

      allocate( h(1:ndim,1:ndim,nkp,2), hr(1:ndim,1:ndim,nkp,2), hi(1:ndim,1:ndim,nkp,2), stat=ios )
      if( ios /= 0 )then
        ios = - abs(ios)
        return
      end if

      do is = 1, nsham
        do ikp = 1, nkp
          read(iuham,*,iostat=ios) (( hr(i,j,ikp,is), j=i,ndim), i=1,ndim )
          read(iuham,*,iostat=ios) (( hi(i,j,ikp,is), j=i,ndim), i=1,ndim )
        end do
      end do

      if( is_iostat_end(ios) )  return

      h = cmplx( hr, hi, 8 )

      deallocate( hr, hi )

      if( nsham == 1 ) h(:,:,:,2) = h(:,:,:,1)

      forall( i = 1:ndim ) h(i,i,:,:) = h(i,i,:,:) - efermi

      wtkp = wtkp / sum(wtkp)

      h = h * energy_factor

      ios = -14001

    case( 'WANN_H.OUT' )
!
!  Read WANN_H.OUT format (ELK)
!
      read(iuham,*); read(iuham,*)

      read(iuham,*) efermi
      read(iuham,*)
      do i = 1, 3
        read(iuham,*)
      end do
      read(iuham,*)
      do i = 1, 3
        read(iuham,*)
      end do

      read(iuham,*);  read(iuham,*);  read(iuham,*)

      read(iuham,*) nkp
      read(iuham,*)

      read(iuham,*) ndim
      read(iuham,*)

      read(iuham,*) natom
      read(iuham,*);  read(iuham,*);  read(iuham,*)

      do i = 1, ndim
        read(iuham,*) j, j
      end do

      read(iuham,*)
      do i = 1, natom
        read(iuham,*) j, j
      end do

      do i = 1, natom
        read(iuham,*);  read(iuham,*);  read(iuham,*);  read(iuham,*)
        read(iuham,*) hrr, hrr, hrr
      end do

      allocate( wtkp(nkp), bk(3,nkp), stat=ios )
      if( ios /= 0 )then
        ios = - abs(ios)
        return
      end if

      allocate( h(ndim,ndim,nkp,2), hr(ndim,ndim,nkp,1), hi(ndim,ndim,nkp,1), stat=ios )
      if( ios /= 0 )then
        ios = - abs(ios)
        return
      end if

!    Read Hamiltonian, k-points, etc. in ELK format

      do ikp = 1, nkp
        read(iuham,*);  read(iuham,*)
        read(iuham,*)   wtkp(ikp)
        read(iuham,*);  read(iuham,*);  read(iuham,*)
        read(iuham,*) ( bk(i,ikp), i = 1, 3 )
        read(iuham,*)

        do i = 1, ndim
          read(iuham,*,iostat=ios) ( hr(i,j,ikp,1) , j = 1, ndim )
        end do
        read(iuham,*)
        do i = 1, ndim
          read(iuham,*,iostat=ios) ( hi(i,j,ikp,1) , j = 1, ndim )
        end do
        read(iuham,*);  read(iuham,*)
      end do

      if( is_iostat_end(ios) )  return

      h(:,:,:,1) = cmplx( hr(:,:,:,1), hi(:,:,:,1), 8 )
      h(:,:,:,2) = h(:,:,:,1)

      deallocate( hr, hi )

      forall( i = 1:ndim ) h(i,i,:,:) = h(i,i,:,:) - efermi

      wtkp = wtkp / sum(wtkp)

      h = h * 2 * energy_factor

      ios = -14001

    case( 'hamiltonian' )
!
!  Read hamiltonian format (Quantum espresso, old)
!
      read(iuham,*,iostat=ios) nkp, ndim
      if( is_iostat_end(ios) )    return

      allocate( h(ndim,ndim,nkp,2), wtkp(nkp), stat=ios )
      if( ios /= 0 )then
        ios = - abs(ios)
        return
      end if

      do ikp = 1, nkp
        read(iuham,*,iostat=ios) wtkp(ikp)
        read(iuham,*,iostat=ios) bk(:,ikp)

        do i = 1, ndim
          do j = 1, ndim
            read(iuham,*,iostat=ios) hrr, hii
            h(i,j,ikp,1) = cmplx(hrr,hii,8)
          end do
        end do
      end do

      if( is_iostat_end(ios) )    return

    !   wtkp = wtkp / sum(wtkp)
      wtkp = 1.0 / nkp                  ! Use with care
      h(:,:,:,2) = h(:,:,:,1)

      h = h * energy_factor

      ios = -14002

    case( 'hamilt' )
!
!  Read hamilt format (Quantum espresso but without k-points, old)
!
      read(iuham,*,iostat=ios) nkp, ndim
      if( is_iostat_end(ios) )    return

      allocate( h(ndim,ndim,nkp,2), wtkp(nkp), stat=ios )
      if( ios /= 0 )then
        ios = - abs(ios)
        return
      end if

      do ikp = 1, nkp
        read(iuham,*,iostat=ios) wtkp(ikp)

        do i = 1, ndim
          do j = 1, ndim
            read(iuham,*,iostat=ios) hrr, hii
            h(i,j,ikp,1) = cmplx(hrr,hii,8)
          end do
        end do
      end do

      if( is_iostat_end(ios) )    return

      wtkp = wtkp / sum(wtkp)
      h(:,:,:,2) = h(:,:,:,1)

      h = h * energy_factor / Ry2eV

      ios = -14002

    case default
      ios = -15000
  end select

  close(iuham)

  if( nsham < 1 .or. ndim < 1 .or. nkp < 1 ) ios = -15000

!   Hermitian conjugated

  do i = 1, ndim-1
    do j = i+1, ndim
      h(j,i,:,:) =  conjg(h(i,j,:,:))
    end do
  end do

end subroutine read_hamiltonian
