subroutine fitting_delta( Delta, xw, Epsk, Vk, eps0, iepsk, ivk, norb,    &
                          nb, nsp, nom, fit, weight, ufp, fix_eps, ntail )
!
! This subroutine fits hybridization function Delta by sets of Lorentzians
!
  use fit_module
  use moments_module

  implicit none
  
  integer, intent(in   ) :: norb
  integer, intent(in   ) :: nb
  integer, intent(in   ) :: nsp
  integer, intent(in   ) :: nom
  integer, intent(in   ) :: ntail

  integer, intent(in   ) :: fit
  integer, intent(in   ) :: weight
  logical, intent(in   ) :: fix_eps
  real(8), intent(in   ) :: ufp(3)

  integer, intent(in   ) :: iepsk(nb,nsp)
  integer, intent(in   ) :: ivk(nb,norb,nsp)
  complex(8), intent(in   ) :: xw(nom)
  complex(8), intent(inout) :: Delta(norb,norb,nom,nsp)

  real(8), intent(inout) :: Epsk(nb,nsp)
  real(8), intent(inout) :: Vk(nb,norb,nsp)
  real(8), intent(inout) :: eps0(norb,norb,nsp)


  integer :: i, p, is, iom, ploc, ierr, nparam
  real(8), parameter :: tola   = 1.d-15    ! Tolerance factor for E and V
  real(8), parameter :: tolchi = 1.d-12    ! Tolerance factor for merit function chi^2

  integer, allocatable :: iafix(:)
  real(8), allocatable :: a(:), b(:)
  real(8), allocatable :: sigma(:,:,:)

  logical :: lmom(0:3) = (/ .false., .false., .true., .true. /)
  real(8) :: moms(0:4)
  

  write(14,"(' Routine to fit hybridization # ',i3)") fit
  write(14,"(' Type of uncertainty function # ',i3)") weight
  if( weight == 4 ) write(14,"(' Parameters for uncertainty function',/,  &
       & '  Amplitude ',f8.4,/, '  Position  ',f8.4,/,'  Spread   ',f8.4)") ufp

  p = nb * ( norb + 1 )
  nparam = 2 * nb * norb
  allocate( a(p), iafix(p), b(nparam), sigma(norb,norb,nom) )

  select case (weight)
    case(2)
      forall( i = 1:nom ) sigma(:,:,i) = log( real(1+i,kind=8) )
    case(3)
      forall( i = 1:nom ) sigma(:,:,i) = real(5+i,kind=8)
    case(4)
      if( abs(real(xw(1)-xw(2),8)) < 1.d-8 )then
        forall( i = 1:nom ) sigma(:,:,i) = ufp(1) + ( 1-ufp(1) ) /      &
                                           ( 1 + exp( ufp(3)*(abs(aimag(xw(i)))-ufp(2)) ) )
      else
        forall( i = 1:nom ) sigma(:,:,i) = ufp(1) + ( 1-ufp(1) ) /      &
                                           ( 1 + exp( ufp(3)*(abs(real(xw(i),8))-ufp(2)) ) )
      end if
    case(5)
      forall( i = 1:nom ) sigma(:,:,i) = real(i**3,kind=8)
    case default
      sigma = 1.d0
  end select

  do is = 1, nsp
    select case (fit)
      case(1)              ! Non linear least square fitting ( diagonal case )
        ploc = nb / norb
        do i = 1, norb
          a(1:ploc)            = Epsk(1+ploc*(i-1):ploc*i,is)
          a(ploc+1:2*ploc)     = Vk(1+ploc*(i-1):ploc*i,i,is)
          iafix(1:ploc)        = iepsk(1+ploc*(i-1):ploc*i,is) 
          iafix(ploc+1:2*ploc) = ivk(1+ploc*(i-1):ploc*i,i,is)
          
          call svd_lsf( xw, Delta(i,i,:,is), sigma(i,i,:), nom, 1, a(1:2*ploc),  &
                        iafix(1:2*ploc), 2*ploc, tola, tolchi, ierr, f_delta )
 
      !    call least_square_fitting( xw, Delta(i,i,:,is), sigma(i,i,:), nom, 1,  &
      !                               a(1:2*ploc), iafix(1:2*ploc), 2*ploc, tola, tolchi, ierr )

          Epsk(1+ploc*(i-1):ploc*i,is) = a(1:ploc)
          Vk(1+ploc*(i-1):ploc*i,i,is) = a(ploc+1:2*ploc)
        end do ! i

      case(2)              ! Non linear least square fitting ( general case )
        a(1:nb)     = Epsk(:,is)
        iafix(1:nb) = iepsk(:,is)
        do i = 1, nb
          a(nb+1+(i-1)*norb:nb+i*norb)     = Vk(i,:,is)
          iafix(nb+1+(i-1)*norb:nb+i*norb) = ivk(i,:,is)
        end do

        call least_square_fitting( xw, Delta(:,:,:,is), sigma, nom, norb,      &
                                   a, iafix, p, tola, tolchi, ierr )
  !        call svd_lsf( xw, Delta(:,:,:,is), sigma, nom, norb, a,  &
  !                      iafix, p, tola, tolchi, ierr, f_delta )

        Epsk(1:nb,is) = a(1:nb) 
        forall( i = 1:nb ) Vk(i,:,is) = a(nb+1+(i-1)*norb:nb+i*norb)

      case(4)
!  Special case when at the begining the fitting procedure used to get
!  one bath level and second bath level is defined via moments

        if( nb /= 2*norb ) exit

        do i = 1, norb
          a(1)     = Epsk(2*i-1,is)
          a(2)     = Vk(2*i-1,i,is)
          iafix(1) = iepsk(2*i-1,is) 
          iafix(2) = ivk(2*i-1,i,is)

          forall( iom = 1:nom ) sigma(i,i,iom) = real(iom**3,kind=8)

          call svd_lsf( xw, Delta(i,i,:,is), sigma(i,i,:), nom, 1, a(1:2),     &
                        iafix(1:2), 2, tola, tolchi, ierr, f_delta )

          Epsk(2*i-1,is) = a(1)
          Vk(2*i-1,i,is) = a(2)
          
          forall( iom = 1:nom )                                                &
            Delta(i,i,iom,is) = Delta(i,i,iom,is) - a(2)**2 / ( xw(iom) - a(1) )
            
          sigma(i,i,:) = 1
          
          a(1)     = Epsk(2*i,is)
          a(2)     = Vk(2*i,i,is)
          iafix(1) = iepsk(2*i,is) 
          iafix(2) = ivk(2*i,i,is)

          call svd_lsf( xw, Delta(i,i,1:10,is), sigma(i,i,1:10), 10, 1, a(1:2),     &
                        iafix(1:2), 2, tola, tolchi, ierr, f_delta )

          Vk(2*i,i,is) = a(1)
          Epsk(2*i,is) = a(2)
        end do ! i

      case(-1)              ! Non linear least square fitting ( diagonal case )
    
        ploc = nb / norb

        do i = 1, norb
          if( 2*ploc <= p )then
            deallocate( a, iafix )
            allocate( a(2*ploc+1), iafix(2*ploc+1) )
          end if
          a(1:ploc)            = Epsk(1+ploc*(i-1):ploc*i,is)
          a(ploc+1:2*ploc)     = Vk(1+ploc*(i-1):ploc*i,i,is)
          a(2*ploc+1)          = eps0(i,i,is)
          iafix(1:ploc)        = iepsk(1+ploc*(i-1):ploc*i,is) 
          iafix(ploc+1:2*ploc) = ivk(1+ploc*(i-1):ploc*i,i,is)
          iafix(2*ploc+1)      = 1
          if( fix_eps ) iafix(2*ploc+1) = 0

          call svd_lsf( xw, Delta(i,i,:,is), sigma(i,i,:), nom, 1, a(1:2*ploc+1),  &
                        iafix(1:2*ploc+1), 2*ploc+1, tola, tolchi, ierr, f_g0 )

          Epsk(1+ploc*(i-1):ploc*i,is) = a(1:ploc)
          Vk(1+ploc*(i-1):ploc*i,i,is) = a(ploc+1:2*ploc)
          eps0(i,i,is) = a(2*ploc+1)
        end do ! i

        case(-3)
!  Special case when at the begining the fitting procedure used to get
!  one bath level and second bath level is defined via moments

        if( nb /= 2*norb ) exit
        
        do i = 1, norb
          if( 2 <= p )then
            deallocate( a, iafix )
            allocate( a(3), iafix(3) )
          end if
          a(1)     = Epsk(2*i-1,is)
          a(2)     = Vk(2*i-1,i,is)
          a(3)     = eps0(i,i,is)
          iafix(1) = iepsk(2*i-1,is) 
          iafix(2) = ivk(2*i-1,i,is)
          iafix(3) = 1
          if( fix_eps ) iafix(3) = 0

          call svd_lsf( xw, Delta(i,i,:,is), sigma(i,i,:), nom, 1, a(1:3),     &
                        iafix(1:3), 3, tola, tolchi, ierr, f_g0 )

          Epsk(2*i-1,is) = a(1)
          Vk(2*i-1,i,is) = a(2)
          eps0(i,i,is)   = a(3)
          
          moms = 0
          call def_moments_w( Delta(i,i,nom-ntail:nom,is), aimag(xw(nom-ntail:nom)),  &
                              moms(0:3), lmom )

          Vk(2*i,i,is) = sqrt( abs(moms(3) - a(2)**2 - a(3)**2) )
          Epsk(2*i,is) = moms(4) - a(3)**3 - ( a(1)+2*a(3) )*a(2)**2 -         &
                         2*a(3)*Vk(2*i,i,is)**2
          Epsk(2*i,is) = Epsk(2*i,is) / Vk(2*i,i,is)**2

        end do ! i

    end select
  end do ! is

  deallocate( a, b, iafix, sigma )
  
end subroutine fitting_delta

!====================================================================
subroutine rw_epsk( itype, ierr, rw, Epsk, Vk, iepsk, ivk, nsp, nb, norb )

  implicit none

  integer, intent(in   ) :: itype
  integer, intent(inout) :: ierr         !   Error status value
  integer, intent(in   ) :: nsp
  integer, intent(in   ) :: nb
  integer, intent(in   ) :: norb
  integer, intent(inout) :: iepsk(nb,nsp)
  integer, intent(inout) :: ivk(nb,norb,nsp)
  real(8), intent(inout) :: Epsk(nb,nsp)
  real(8), intent(inout) :: Vk(nb,norb,nsp)
  character(1) :: rw           !   Read/write switch
  
  integer :: iunit, is, i, j, k, m
  character(16) :: fname, stat, act

  ierr  = 0
  iunit = 33

  write(fname,*) itype
  fname = 'epsk_'//trim(adjustl(fname))//'.ini'
    
  if( index(rw,'r') == 1 )then
    stat = 'old'
    act  = 'read'
  else
    stat = 'unknown'
    act  = 'write'
  end if
  
  open( iunit, file=fname, form='formatted', status=stat,        &
               position='rewind', iostat=ierr, action=act )

  if( ierr == 0 )then
    if( rw == 'r' )then
      read(iunit,*,iostat=ierr)
      read(iunit,*,iostat=ierr)
      read(iunit,*,iostat=ierr)
      read(iunit,*,iostat=ierr)
      read(iunit,*,iostat=ierr)
      read(iunit,*,iostat=ierr)
      do is = 1, nsp
        do i = 1, nb
          read(iunit,*,iostat=ierr,err=100) Epsk(i,is), iepsk(i,is),         &
               ( Vk(i,m,is), ivk(i,m,is), m = 1,norb )
        end do
        read(iunit,*,iostat=ierr)
        read(iunit,*,iostat=ierr)
      end do
    else
      write(14,"(/,10x,'Parameters of hybridization function (Epsk, Vk)',/)")

      write(iunit,*)'#        Parameters of hybridization function,'
      write(iunit,*)'#  Delta_{i,j}(w) = Sum_{k} V_{i,k}^{*} V_{j,k} / ( w - e_{k} )'
      write(iunit,*)'#  First column contains energy of the bath, e_{k}.'
      write(iunit,*)'#  The rest of columns contain hybridization between bath and orbitals, V_{i,k}'
      write(iunit,*)'#  Integers after each parameter fix it (=0) or allow for minimization (=1).'
      write(iunit,*)'#'
      do is = 1, nsp
        do i = 1, nb
          write(iunit,"(100(1x,f8.4,i2))") Epsk(i,is), iepsk(i,is),           &
               ( abs(Vk(i,m,is)), ivk(i,m,is), m = 1,norb )
          write(14,"(100(1x,f8.4,i2))") Epsk(i,is), iepsk(i,is),              &
               ( abs(Vk(i,m,is)), ivk(i,m,is), m = 1,norb )
        end do
        write(iunit,*)
        write(iunit,*)
        write(14,*)
      end do
    end if
  end if

100 continue

!   Initialization of parameters
  if( ierr /= 0 )then
    Vk    = 0.0
    ivk   = 0
    iepsk = 1

    k = nb / norb
    do i = 1, norb
      do j = 1, k
        Epsk((i-1)*k+j,:) = -1.0 + 2.0*real(j,8)/real(k+1,8)
        Vk((i-1)*k+j,i,:) =  1.0 - abs( Epsk((i-1)*k+j,:) )
        ivk((i-1)*k+j,i,:) = 1
      end do
    end do
  end if

  if( any( iepsk < 0 ) .or. any( iepsk > 1 ) )then
    write(14,"(' Wrong elements of ie! ',/,' iepsk = ',100(i2))") iepsk
    write(14,"(' Change all elements of iepsk to 1.')")
    iepsk = 1
  end if
  if( any( ivk < 0 ) .or. any( ivk > 1 ) )then
    write(14,"(' Wrong elements of ivk! ',/,' ivk = ',100(i2))") ivk
    write(14,"(' Change all elements of ivk to 1.')")
    ivk = 1
  end if

  close(iunit)
   
end subroutine rw_epsk
