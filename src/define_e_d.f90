subroutine define_e_d( itype )
!
!   Calculation of impurity level, e_d
!
  use math_module
  use parameter_module
  use impurity_module

  implicit none

  integer, intent(in   ) :: itype

  integer :: is, ierr, n_orb
  real(8) :: ntot(2), average_u

  n_orb = impurity(itype)%nlm

  do is = 1, nspin
    ntot(is) = spur( impurity(itype)%occupation(:,:,is), n_orb )
  end do
  if( nspin == 1 ) ntot(2) = ntot(1)

  select case ( impurity(itype)%dc_type )
!      Fully localized limit
   case('FLL     ')
     do is = 1, nspin
       impurity(itype)%dcc(is) = - impurity(itype)%u_value*( sum(ntot) - 0.5 ) &
                                 + impurity(itype)%j_value*( ntot(is)  - 0.5 )
     end do
!      Simplified fully localized limit
   case('SFLL    ')
     average_u = impurity(itype)%u_value -                                     &
                 ( n_orb - 1 ) * impurity(itype)%j_value / ( 2*n_orb - 1 )
     do is = 1, nspin
       impurity(itype)%dcc(is) = - average_u*( sum(ntot) - impurity(itype)%xsfll/2 )
     end do
   case default
!      Double counting is a given fixed constant
     read(impurity(itype)%dc_type,*,iostat=ierr) average_u

     if( ierr == 0 )  impurity(itype)%dcc(:) = average_u

  end select

end subroutine define_e_d
