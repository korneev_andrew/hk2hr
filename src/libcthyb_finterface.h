 
!------------------------------------------------------------------------------
!     Interfaces for C++ functions

!extern "C" unsigned long long f_init_solver(const int &verbosity, const int &seed)
  interface
    integer(8) function f_init_solver(verbosity, seed) bind ( c )
      use iso_c_binding
      implicit none
      integer(c_int)  :: verbosity, seed
    end function f_init_solver
  end interface
  
!extern "C" unsigned long long f_mc_results(const unsigned long long &solver)
  interface
    integer(8) function f_mc_results(solver) bind ( c )
      use iso_c_binding
      implicit none
      integer(8)  :: solver
    end function f_mc_results
  end interface

!extern "C" void f_set_system(const unsigned long long &solver, const unsigned long long &hybridization_num_points, const double *hybridization_function, const double *mu, const double *u_matrix)
!
  interface
    subroutine f_set_system(solver, hybridization_num_points, hybridization_function, mu, u_matrix) bind ( c )
      use iso_c_binding
      implicit none
      integer(8) :: solver, hybridization_num_points
      real(c_double)  :: u_matrix(*), mu(*), hybridization_function(*) 
    end subroutine f_set_system
  end interface

!double c_fraction_done(void *solver);
  interface
    real(c_double) function c_fraction_done( solver ) bind ( c )
      use iso_c_binding
      implicit none
      integer(8) :: solver
    end function c_fraction_done
  end interface

!bool c_thermalized(void *solver);
  interface
    logical(c_bool) function c_is_thermalized( solver ) bind ( c )
      use iso_c_binding
      implicit none
      integer(8) :: solver
    end function c_is_thermalized
  end interface

!void c_make_sweep(void *solver);
  interface
    subroutine c_make_sweep( solver ) bind ( c )
      use iso_c_binding
      implicit none
      integer(8) :: solver
    end subroutine c_make_sweep
  end interface

!void f_destroy(void *solver);
  interface
    subroutine f_destroy_solver( solver )  bind ( c )
      use iso_c_binding
      implicit none
      integer(8) :: solver
    end subroutine f_destroy_solver
  end interface
  
  interface
    subroutine f_destroy_results( results )  bind ( c )
      use iso_c_binding
      implicit none
      integer(8) :: results
    end subroutine f_destroy_results
  end interface

!void c_gw_from_gl(double *gl, double *gwr, double *gwi, int flavors, 
!                  int nlegendre, int nmatsubara, double beta);
  interface
    subroutine c_green_omega_from_legendre(green_legendre, green_omega_real,   &
                           green_omega_imaginary, num_flavors, num_legendre,   &
                           num_matsubara) bind ( c )
      use iso_c_binding
      implicit none
      integer(c_int) :: num_legendre, num_matsubara, num_flavors
      real(c_double) :: green_legendre(*), green_omega_imaginary(*), green_omega_real(*) 
    end subroutine c_green_omega_from_legendre
  end interface

!extern void c_green_legendre_restore(double *green_legendre, const int &num_flavors, const int &num_legendre, const double &beta);
!
  interface
    subroutine c_green_legendre_restore(green_legendre, num_flavors, num_legendre, beta) bind ( c )
      use iso_c_binding
      implicit none
      integer(c_int) :: num_legendre,  num_flavors
      real(c_double) :: green_legendre(*)
      real(c_double) :: beta
    end subroutine c_green_legendre_restore
  end interface

!extern void c_get_green_legendre_moments(const double *green_legendre, const int &num_flavors, const int &num_legendre, const double &beta, double *c1, double *c2, double *c3);
!
  interface
    subroutine c_get_green_legendre_moments(green_legendre, num_flavors, num_legendre, beta, c1, c2, c3) bind ( c )
      use iso_c_binding
      implicit none
      integer(c_int) :: num_legendre,  num_flavors
      real(c_double) :: green_legendre(*), c1(*), c2(*), c3(*)
      real(c_double) :: beta
    end subroutine c_get_green_legendre_moments
  end interface

!extern void c_green_legendre_impose_c1(double *green_legendre, const int &num_flavors, const int &num_legendre, const double &beta);
!
  interface
    subroutine c_green_legendre_impose_c1(green_legendre, num_flavors, num_legendre, beta) bind ( c )
      use iso_c_binding
      implicit none
      integer(c_int) :: num_legendre,  num_flavors
      real(c_double) :: green_legendre(*)
      real(c_double) :: beta
    end subroutine c_green_legendre_impose_c1
  end interface

!extern void c_green_tau_from_legendre(const double *green_legendre, double *green_tau, const int &num_flavors, const int &num_legendre, const int &num_slices, const double &beta);
!
  interface
    subroutine c_green_tau_from_legendre(green_legendre, green_tau, num_flavors, num_legendre, num_slices, beta) bind ( c )
      use iso_c_binding
      implicit none
      integer(c_int) :: num_legendre,  num_flavors, num_slices
      real(c_double) :: green_legendre(*), green_tau(*)
      real(c_double) :: beta
    end subroutine c_green_tau_from_legendre
  end interface

!extern void c_set_parameter_int(const unsigned long long &solver, const char *string, const int &i);
!
  interface
    subroutine c_set_parameter_int(solver, string, i) bind ( c )
      use iso_c_binding
      implicit none
      integer(8) :: solver
      integer(c_int) :: i
      character :: string(*)
    end subroutine c_set_parameter_int
  end interface

!extern void c_set_parameter_double(const unsigned long long &solver, const char *string, const double &d);
!
  interface
    subroutine c_set_parameter_double(solver, string, d) bind ( c )
      use iso_c_binding
      implicit none
      integer(8) :: solver
      real(c_double) :: d
      character :: string(*)
    end subroutine c_set_parameter_double
  end interface

!extern void c_set_parameter_bool(const unsigned long long &solver, const char *string, const bool &b);
!
  interface
    subroutine c_set_parameter_bool(solver, string, b) bind ( c )
      use iso_c_binding
      implicit none
      integer(8) :: solver
      logical(c_bool) :: b
      character :: string(*)
    end subroutine c_set_parameter_bool
  end interface
  
!extern void c_set_parameter_string(const unsigned long long &solver, const char *string, const char *data);
!
  interface
    subroutine c_set_parameter_string(solver, string, data) bind ( c )
      use iso_c_binding
      implicit none
      integer(8) :: solver
      character :: string(*), data(*)
    end subroutine c_set_parameter_string
  end interface

!extern "C" void f_get_result(const unsigned long long &solver, const unsigned long long &results, const int &nspin, const int &type, const bool &get_statistics, double *data, double *error, double *variance, double *tau)
  interface
    subroutine f_get_result(solver, results, type, data) bind ( c )
      use iso_c_binding
      implicit none
      integer(8) :: solver, results
      integer(c_int) :: type
      real(c_double) :: data(*)
    end subroutine f_get_result
  end interface
  
!extern "C" void f_get_complex_result(const unsigned long long &solver, const unsigned long long &results, const int &nspin, const int &type, const bool &get_statistics, fortran_complex *data, fortran_complex *error, fortran_complex *variance, fortran_complex *tau)
  interface
    subroutine f_get_complex_result(solver, results, type, data) bind ( c )
      use iso_c_binding
      implicit none
      integer(8) :: solver, results
      integer(c_int) :: type
      complex(c_double_complex) :: data(*)
    end subroutine f_get_complex_result
  end interface
  
!extern "C" void c_get_states(const unsigned long long &solver, int *atomic_configurations)
  interface
    subroutine c_get_states(solver, states) bind ( c )
      use iso_c_binding
      implicit none
      integer(8) :: solver
      integer(c_int) :: states(*)
    end subroutine c_get_states
  end interface
  
  interface
    subroutine f_transform_legendre(solver, legendre, green_legendre, moments, predicted, legendre_cutoff) bind ( c )
      use iso_c_binding
      implicit none
      integer(c_int) :: predicted(*), legendre_cutoff(*)
      real(c_double) :: legendre(*), moments(*)
      complex(c_double_complex) :: green_legendre(*)
      integer(8) :: solver
    end subroutine f_transform_legendre
  end interface
  
  interface
    subroutine f_sigma_legendre(solver, green_legendre, sigmag_legendre, sigma, legendre_cutoff) bind ( c )
      use iso_c_binding
      implicit none
      integer(c_int) :: legendre_cutoff(*)
      real(c_double) :: green_legendre(*), sigmag_legendre(*)
      complex(c_double_complex) :: sigma(*)
      integer(8) :: solver
    end subroutine f_sigma_legendre
  end interface
  
  interface
    subroutine f_omega_from_tau(solver, tau, omega, nlegendre) bind ( c )
      use iso_c_binding
      implicit none
      integer(c_int) :: nlegendre
      real(c_double) :: tau(*)
      complex(c_double_complex) :: omega(*)
      integer(8) :: solver
    end subroutine f_omega_from_tau
  end interface
  
!extern void f_green_legendre_analyze(const double *green_legendre, const int &num_flavors, const int &num_legendre, const double &beta, int *best_legendre);
!
  interface
    subroutine f_green_legendre_analyze(green_legendre, num_flavors, num_legendre, beta, best_legendre) bind ( c )
      use iso_c_binding
      implicit none
      integer(c_int) :: num_legendre,  num_flavors
      real(c_double) :: green_legendre(*)
      integer(c_int) :: best_legendre(*)
      real(c_double) :: beta
    end subroutine f_green_legendre_analyze
  end interface
  
  interface
    subroutine f_array_test(array) bind ( c )
      use iso_c_binding
      implicit none
      real(c_double) :: array(*)
    end subroutine f_array_test
  end interface
  
!extern unsigned long long f_mc_container_init(const int &bin_size);
! Initialize containers with set bin size
  interface
    integer(8) function f_mc_container_init(bin_size) bind ( c )
      use iso_c_binding
      implicit none
      integer(c_int) :: bin_size
    end function f_mc_container_init
  end interface
  
!extern void f_mc_container_push_bin(const unsigned long long &container, const char *name, const double *data, const int &array_size);
! Push a bin of measurements with specific name into container
  interface
    subroutine f_mc_container_push_bin(container, name, data, array_size) bind ( c )
      use iso_c_binding
      implicit none
      integer(c_int) :: array_size
      real(c_double) :: data(*)
      character :: name(*)
      integer(8) :: container
    end subroutine f_mc_container_push_bin
  end interface
  
!extern void f_mc_container_push_single(const unsigned long long &container, const char *name, const double *data, const int &array_size);
! Push a single measurement with specific name into container
  interface
    subroutine f_mc_container_push_single(container, name, data, array_size) bind ( c )
      use iso_c_binding
      implicit none
      integer(c_int) :: array_size
      real(c_double) :: data(*)
      character :: name(*)
      integer(8) :: container
    end subroutine f_mc_container_push_single
  end interface
  
!extern void f_mc_container_get_mean(const unsigned long long &container, const char *name, double *data);
! Get a mean of a measurements with a specific name
  interface
    subroutine f_mc_container_get_mean(container, name, data) bind ( c )
      use iso_c_binding
      implicit none
      real(c_double) :: data(*)
      character :: name(*)
      integer(8) :: container
    end subroutine f_mc_container_get_mean
  end interface
  
!extern void f_mc_container_get_error(const unsigned long long &container, const char *name, double *data);
! Get a error of a measurements with a specific name
  interface
    subroutine f_mc_container_get_error(container, name, data) bind ( c )
      use iso_c_binding
      implicit none
      real(c_double) :: data(*)
      character :: name(*)
      integer(8) :: container
    end subroutine f_mc_container_get_error
  end interface
  
!extern void f_mc_container_get_variance(const unsigned long long &container, const char *name, double *data);
! Get a variance of a measurements with a specific name
  interface
    subroutine f_mc_container_get_variance(container, name, data) bind ( c )
      use iso_c_binding
      implicit none
      real(c_double) :: data(*)
      character :: name(*)
      integer(8) :: container
    end subroutine f_mc_container_get_variance
  end interface
  
!extern void f_mc_container_get_tau(const unsigned long long &container, const char *name, double *data);
! Get a autocorrelation time of a measurements with a specific name
  interface
    subroutine f_mc_container_get_tau(container, name, data) bind ( c )
      use iso_c_binding
      implicit none
      real(c_double) :: data(*)
      character :: name(*)
      integer(8) :: container
    end subroutine f_mc_container_get_tau
  end interface
  
!extern void f_mc_container_sync(const unsigned long long &container);
! Synchronize measurments between all MPI processes
  interface
    subroutine f_mc_container_sync(container) bind ( c )
      use iso_c_binding
      implicit none
      integer(8) :: container
    end subroutine f_mc_container_sync
  end interface
  
!extern void f_mc_container_destroy(const unsigned long long &container);
! Destroy container
  interface
    subroutine f_mc_container_destroy(container) bind ( c )
      use iso_c_binding
      implicit none
      integer(8) :: container
    end subroutine f_mc_container_destroy
  end interface
  
!extern "C" void c_mem_info(int &vmpeak, int &vmsize, int &vmrss)
  interface
    subroutine c_mem_info(vmpeak, vmsize, vmrss) bind ( c )
      use iso_c_binding
      implicit none
      integer(c_int) :: vmpeak, vmsize, vmrss
    end subroutine c_mem_info
  end interface
  
!------------------------------------------------------------------------------  
