module hf_qmc_module
!
!    Hirsh-Fye Quantum Monte-Carlo module
!
!    Features:   general matrix form with the calculation of the off-diagonal
!               elements of the Green function G_{i,j}(tau)
!
!    For an input see below
!
!    Written by A. Poteryaev  < Alexander.Poteryaev _at_ cpht.polytechnique.fr >
!          and  A. Lichtenstein  < alichten _at_ physnet.uni-hamburg.de >
!
  use lapack_module

  implicit none
  private
!
!   Array's dimensions
!
  integer :: ncl        !    Impurity dimension
  integer :: L          !    Number of time slices
  integer :: nfield     !    Number of Ising fields
  integer :: nl         !    L*ncl
  integer :: nsp        !    Number of spins
!
!   Array's iterators
!
  integer :: i, j, is, iks, jks
!
!   Different pointers
!
  integer, allocatable, save :: i_spin(:), i_orbit(:), i_spinorbit(:,:)
  real(8), allocatable, save :: del(:,:), sigmaf(:,:)
  real(8), allocatable, save, public :: Vns(:,:)

  public  :: qmc, vns_ini
  
  contains
  
  subroutine qmc( g0tau, gtau, ninj, beta, e_d, ummss, accept_rate, average_sign, lninj, Nsweep, nwarm )
    implicit none

    integer, intent(in   ) :: Nsweep                !  Number of measured sweeps
    integer, intent(in   ) :: nwarm                 !  Number of warming sweeps
    logical, intent(in   ) :: lninj
    real(8), intent(in   ) :: beta
    real(8), intent(in   ) :: e_d
    real(8), intent(inout) :: g0tau(:,:,:,:)
    real(8), intent(  out) :: gtau(:,:,:,:)
    real(8), intent(  out) :: ninj(:,:,:,:)
    real(8), intent(in   ) :: ummss(:,:)   
    real(8), intent(  out) :: accept_rate        !   Acceptance rate     
    real(8), intent(  out) :: average_sign       !   Average sign
!
!   Local variables
!    
    integer :: iqmc, nqmc, k1, k2, il, iv, naccept
  
    real(8) :: detratio, absdetratio, rnd, det_sign

    real(8), allocatable :: Green(:,:,:), Greens(:,:,:), nn(:,:,:,:)
    real(8), allocatable :: v1(:), v2(:)
!
!   Get dimensions and some checks and definitions  
!
    ncl    = size( g0tau, dim=1 )
    L      = size( g0tau, dim=3 )
    nsp    = size( g0tau, dim=4 )
    
    nfield = count( ummss > 0 ) / 2

    nl     = ncl * L
    
    i = sum( abs( shape(gtau) - shape(g0tau) ) )
    if( i /= 0 ) stop ' The shape of the G(tau) and G0(tau) does not coincide'

    i = size( g0tau, dim=2 )
    if( i /= ncl )  stop ' The dimension G(tau) is wrong'
!
!   Setup Ising fields etc
!
    call qmc_setup( ummss, beta )

    allocate( Green(nl,nl,2), Greens(nl,nl,2), v1(nl), v2(nl) )

    Green  = 0
    Greens = 0

    if( lninj )then
      allocate( nn(nl,nl,2,2) )
      nn = 0
    end if
   
    g0tau = - g0tau
    call initial( g0tau, Green )
    
    call g_clean_update( Green, e_d, beta )

    naccept  = 0
    det_sign = 1
    average_sign = 0
!
!    start of QMC simulation
!
    nqmc = nwarm + Nsweep
    do iqmc = 1, nqmc
      do iv = 1, nfield
        k1 = i_spinorbit(iv,1)
        k2 = i_spinorbit(iv,2)
        do il = 1, L
!
!    try flipping spin k
!
          call detrat( il, iv, k1, k2, detratio, Green )
          absdetratio = abs( detratio )
          call random_number( rnd )
          
          if( rnd < absdetratio / ( 1 + absdetratio ) )then
!
!    accept flip
!
            Vns(il,iv) = - Vns(il,iv)
            call g_fast_update( il, iv, 1, k1, Green, v1, v2 )
            call g_fast_update( il, iv,-1, k2, Green, v1, v2 )
            det_sign = det_sign * detratio / absdetratio
            naccept  = naccept + 1
          end if 
          
        end do !  L
      end do    ! nfield
!
!    end of sweep      
!      
      if( iqmc > nwarm )then
        average_sign = average_sign + det_sign
        Greens = Greens + det_sign*Green
        if( lninj ) call ninj_corr( Green, nn )
      end if
      
    end do    ! NQMC
!
!    end of QMC simulation
!
    deallocate( Green, v1, v2 )

    Greens = Greens / average_sign
    if( lninj ) nn = nn / average_sign

    average_sign = average_sign / Nsweep
    accept_rate  = real(naccept,8) / nqmc / L / nfield
!
!    transform form G(t,t') -> G(t-t')
!
    if( nsp == 1 ) Greens(:,:,1) = ( Greens(:,:,1) + Greens(:,:,2) ) / 2
    do is = 1, nsp
      call deinitial( Greens(:,:,is), gtau(:,:,:,is), -1 )
    end do

    if( lninj )then
      call deinitial( nn(:,:,1,1), ninj(:,:,:,1), 1 )
      call deinitial( nn(:,:,1,2), ninj(:,:,:,2), 1 )
      call deinitial( nn(:,:,2,1), ninj(:,:,:,3), 1 )
      call deinitial( nn(:,:,2,2), ninj(:,:,:,4), 1 )
      deallocate( nn )
    end if

    deallocate( Greens, i_spin, i_orbit, i_spinorbit, del, sigmaf )
!
    call avergf( gtau )
    
    g0tau = - g0tau
    gtau  = - gtau

  end subroutine qmc
  
  subroutine avergf( gtau )
!    
!    average non-diagonal part: G12 = G21
!
    implicit none
    real(8), intent (inout) :: gtau(:,:,:,:)

    do i = 1, ncl
      do j = i+1, ncl
        gtau(i,j,:,:) = ( gtau(i,j,:,:) + gtau(j,i,:,:) ) / 2.d0
        gtau(j,i,:,:) =   gtau(i,j,:,:)
      end do
    end do

  end subroutine avergf
  
  subroutine detrat( il, iv, k1, k2, detratio, Green )
!   Calculate determinant ratio
    implicit none
    integer, intent (in   ) :: il, iv, k1, k2
    real(8), intent (in   ) :: Green(:,:,:)
    real(8), intent (  out) :: detratio
!
!   Local variables
!
    integer :: k1s, k2s, k1orb, k2orb, lk1, lk2
    real(8) :: dv, r1, r2, r3

    dv  = 2 * vns(il,iv)
    k1s = i_spin(k1)
    k2s = i_spin(k2)
    k1orb = i_orbit(k1)
    k2orb = i_orbit(k2)
    lk1 = ( il - 1 ) * ncl + k1orb
    lk2 = ( il - 1 ) * ncl + k2orb
    
    r1 = 1 + ( 1 - Green(lk1,lk1,k1s) ) * ( exp(-dv) - 1 )
    r2 = 1 + ( 1 - Green(lk2,lk2,k2s) ) * ( exp( dv) - 1 )
    detratio = r1 * r2
    if( k1s == k2s )then
      r3 = Green(lk1,lk2,k1s) * Green(lk2,lk1,k2s) * ( exp(-dv) - 1 ) * ( exp(dv) - 1 )
      detratio = detratio - r3
    end if
       
  end subroutine detrat
  
  subroutine initial( g0tau, Green )
!
!    transform form G(t-t') -> G(t,t')
!
    implicit none
    real(8), intent(in   ) :: g0tau(:,:,:,:)
    real(8), intent(  out) :: Green(:,:,:)
!
!   Local variables
!
    integer :: k, m
    real(8), allocatable :: gtemp(:)
    
    allocate( gtemp(-L+1:L-1) )
    
    do is = 1, nsp
      do i = 1, ncl
        do j = 1, ncl
          do k = 0, L-1
            gtemp(k) = g0tau(i,j,k+1,is)
          end do
!
!    reflection of G to calculate Greens function for neg. arguments
!
          do k = 1, L-1
            gtemp(-k) = - gtemp(L-k)
          end do

          do k = 0, L-1
            iks = i + k*ncl
            do m = 0, L-1
              jks = j + m*ncl
              Green(iks,jks,is) = gtemp(k-m)
            end do
          end do
        end do   !j
      end do     !i
    end do   ! is
    
    if( nsp == 1 ) Green(:,:,2) = Green(:,:,1)
   
    deallocate( gtemp )
  
  end subroutine initial
  
  subroutine deinitial( Green, gtau, sng )
!
!    transform form G(t,t') -> G(t-t')
!
    implicit none
    integer, intent(in   ) :: sng
    real(8), intent(in   ) :: Green(:,:)
    real(8), intent(  out) :: gtau(:,:,:)
!
!   Local variables
!
    integer :: k, m, idel, inumb
    real(8), allocatable :: gtemp(:)
    
    allocate( gtemp(-L+1:L-1) )
    
    do i = 1, ncl
      do j = 1, ncl
        gtemp = 0
        do idel = -L+1, L-1
          inumb = min(L-idel,L) - max(1,1-idel) + 1
          do k = max(1,1-idel), min(L-idel,L)
            m   = k + idel
            iks = i + ( m - 1 )*ncl 
            jks = j + ( k - 1 )*ncl 
            gtemp(idel) = gtemp(idel) + Green(iks,jks)
          end do 
          gtemp(idel) = gtemp(idel) / real(inumb,8)
        end do
!   transform to cluster notations
        gtau(i,j,1) = gtemp(0)
        do k = 1, L-1
          gtau(i,j,k+1) = ( gtemp(k) + sng*gtemp(k-L) ) / 2
        end do 
      end do !   j
    end do  !   i
    
    deallocate( gtemp )

  end subroutine deinitial
  
  subroutine g_clean_update( Green, e_d, beta )
    implicit none
    real(8), intent (in   ) :: e_d
    real(8), intent (in   ) :: beta
    real(8), intent (inout) :: Green(:,:,:)
    
    integer :: ia, il, ik, iv, ial
    real(8), allocatable :: vv(:,:), a(:,:), b(:,:), c(:,:)
    
    allocate( vv(nl,2) )

    vv = 0
    do is = 1, 2
      do ia = 1, ncl
        ik = ( is - 1 )*ncl + ia
        do il = 1, L
          ial = ( il - 1 )*ncl + ia
          do iv = 1, nfield
            vv(ial,is) = vv(ial,is) + sigmaf(ik,iv)*vns(il,iv)
          end do
        end do
      end do
    end do    
    vv = vv + e_d * beta / real(L,8)
    
    allocate( a(nl,nl), b(nl,nl), c(nl,nl) ) 
    
    do is = 1, 2

      c = Green(:,:,is)

      do i = 1, nl
        do j = 1, nl
          a(i,j) = del(i,j) + ( del(i,j) - c(i,j)  ) * ( exp( vv(j,is) ) - 1 )
        end do
      end do
      
      call inverse_matrix( a )

      b = matmul( a, c )
      Green(:,:,is) = b
      
    end do    
    
    deallocate( vv, a, b, c ) 
    
  end subroutine g_clean_update
  
  subroutine g_fast_update( il, iv, isignf, k, Green, v1, v2 )
    implicit none
    integer, intent (in   ) :: il, iv, isignf, k
    real(8), intent (inout) :: Green(:,:,:)
    real(8), intent (  out) :: v1(:), v2(:)
!
!   Local variables
!
    integer :: kspin, korb, lk
    real(8) :: dv, ee, eg

    dv = 2 * isignf * Vns(il,iv)
    ee = exp(dv) - 1      
    kspin = i_spin(k)
    korb  = i_orbit(k)
    lk = ( il - 1 ) * ncl + korb
    eg = ee / ( 1 + ( 1 - Green(lk,lk,kspin) )*ee )
   
    v1 = Green(:,lk,kspin) - del(:,lk)
    v2 = Green(lk,:,kspin)
    
    call dger_opt( eg, v1, v2, Green(:,:,kspin), nl )
    
  end subroutine g_fast_update

  subroutine ninj_corr( Green, nn )
!
!   Calculates correlation function < n_i(t), n_j(t') > =
!
    implicit none
    real(8), intent (in   ) :: Green(:,:,:) 
    real(8), intent (inout) :: nn(:,:,:,:)

    do i = 1, nl
      do j = 1, nl

!         Up-up                
        nn(i,j,1,1) = nn(i,j,1,1) + 1 -                      &
                      Green(i,i,1) - Green(j,j,1) +          &
                      Green(i,i,1) * Green(j,j,1) -          &
                      Green(i,j,1) * Green(j,i,1)
!         Down-down               
        nn(i,j,2,2) = nn(i,j,2,2) + 1 -                      &
                      Green(i,i,2) - Green(j,j,2) +          &
                      Green(i,i,2) * Green(j,j,2) -          &
                      Green(i,j,2) * Green(j,i,2)
!         Up-down               
        nn(i,j,1,2) = nn(i,j,1,2) + 1 -                      &
                      Green(i,i,1) - Green(j,j,2) +          &
                      Green(i,i,1) * Green(j,j,2)
!         Down-up        
        nn(i,j,2,1) = nn(i,j,2,1) + 1 -                      &
                      Green(i,i,2) - Green(j,j,1) +          &
                      Green(i,i,2) * Green(j,j,1)
      end do ! j

!         Up-up                
      nn(i,i,1,1) = nn(i,i,1,1) + Green(i,i,1)
!         Down-down               
      nn(i,i,2,2) = nn(i,i,2,2) + Green(i,i,2)

    end do  ! i
    
  end subroutine ninj_corr
  
  subroutine vns_ini( Ltau, nfl )
    implicit none
  
    integer, intent(in   ) :: Ltau, nfl
    
    real(8) :: rnd
  
    if( allocated( Vns ) )  deallocate( Vns )
  
    allocate( Vns(Ltau,nfl) )
    Vns = 0
    
    do i = 1, Ltau
      do j = 1, nfl
        call random_number( rnd )
        if( rnd > 0.5d0 )then
           Vns(i,j) =  1
         else
           Vns(i,j) = -1
        end if
      end do
    end do
    
  end subroutine vns_ini

  subroutine qmc_setup( ummss, beta )
    implicit none

    real(8), intent (in   ) :: beta
    real(8), intent (in   ) :: ummss(:,:)
    
    integer :: i1, j1, ij, nfl, js
    real(8) :: d
    real(8), allocatable :: uf(:), lambda(:)
    
    allocate( del(nl,nl) )
    del = 0
    forall( i = 1:nl ) del(i,i) = 1
    
    nfl = 0
    do is = 1, 2
      do i = 1, ncl
        i1 = i + ( is - 1 )*ncl
        do js = 1, 2
          do j = 1, ncl
            j1 = j + ( js - 1 )*ncl
            if( i1 < j1 .and. ummss(i1,j1) > 1.e-8 ) nfl = nfl + 1
          end do
        end do
      end do
    end do

    if( nfl /= nfield )   stop 'Problems!!!'
    
    allocate( sigmaf(2*ncl,nfl), i_spin(2*ncl), i_orbit(2*ncl), i_spinorbit(nfl,2) )
    allocate( uf(nfl) )
    sigmaf = 0
    
    ij = 0
    do is = 1, 2
      do i = 1, ncl
        i1 = i + ( is - 1 )*ncl
        i_spin(i1)  = is
        i_orbit(i1) = i
        do js = 1, 2
          do j = 1, ncl
            j1 = j + ( js - 1 )*ncl
            if( i1 < j1 .and. ummss(i1,j1) > 1.d-8 )then
              ij = ij + 1
              uf(ij) = ummss(i1,j1)
              sigmaf(i1,ij) =  1
              sigmaf(j1,ij) = -1
              i_spinorbit(ij,1) = i1
              i_spinorbit(ij,2) = j1
            end if
          end do
        end do
      end do
    end do
    
    allocate( lambda(nfl) )
    
    do i = 1, nfl
      d = exp( 0.5 * beta * uf(i) / L )
      d = d + sqrt( d*d - 1 )
      lambda(i) = log( d )
      vns(:,i) = vns(:,i) * lambda(i)
    end do

!===============================================================
!     Debug print out
!    
!      write(6,"(/,7x,'Number of Ising fields',i5)") nfield
!      write(6,"('=====================================')")
!      write(6,"(' Field number  Field value  Lambda')")
!      write(6,"('=====================================')")
!      do i = 1,nfield
!        write(6,100) i, uf(i), lambda(i)
!      end do
!100 format(5x,i3,8x,f8.4,2x,f8.4)
!      
!      write(6,"('=====================================')")
!      write(6,"(' Field distribution')")
!      write(6,"('=====================================')")
!      do j = 1,nfield
!        write(6,"(1x,i3,500(1x,f8.4))") j, ( Vns(i,j), i=1,L )
!      end do
!===============================================================
  
    deallocate( lambda, uf )
  
  end subroutine qmc_setup

  subroutine dger_opt( alpha, x, y, a, n )
!
!   Optimized version of original dger routine written for particular case:
!    incx = incy = 1
!    n = m = lda
!   For further details see original BLAS dger.
!
    implicit none

    integer :: n
    real(8) :: alpha
    real(8) :: x(n), y(n), a(n,n)

    real(8) :: temp

    do j = 1, n
      if( abs(y(j)) < 1.d-10 ) cycle

      temp = alpha * y(j)
      do i = 1, n
        a(i,j) = a(i,j) + x(i)*temp
      end do

    end do

  end subroutine dger_opt

end module hf_qmc_module
