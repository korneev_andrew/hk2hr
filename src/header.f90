subroutine header
!======================================================================
!
!   Writes header and initial values to output file
!
!======================================================================
  use hamiltonian_module, only : nkp, ndim
  use parameter_module
  use energy_module
  use impurity_module
  use mpi_module
  use iolib_module

  implicit none

  integer :: i, j, k, l, itype, iunit, ierr, n, date_time(8)
  real(8) :: average_u
  character(16) :: fname

  write(14,50); write(14,50)
  write(14,"('==',76x,'==')")
  write(14,"('==',35x,'AMULET',35x,'==')")
  write(14,"('==',12x,'Advanced Materials simULation Ekaterinburg',a,'s Toolbox',12x,'==')") achar(39)
  write(14,"('==',76x,'==')")
  write(14,50); write(14,50)

!   Info about code version, etc

  write(14,"(' You are using ',a,' version of the revision ',a)") REVISION_MOD , REVISION
  write(14,"(' Revision date',3x,a,3x,'and time',3x,a)")  REVISION_DATE , REVISION_TIME
  write(14,"(' Built date',6x,a,3x,'and time',3x,a)")  BUILT_DATE , BUILT_TIME

  call date_and_time( values=date_time )
  write(14,"(' Execution date',2x,i4,'-',i2.2,'-',i2.2,3x,'and time',3x,i2,2(':',i2.2))")  &
                           date_time(1:3), date_time(5:7)

!                   getcwd
!
!
  write(14,50)
  write(14,"(' You have requested ',i5,' processors')") numprocs
  write(14,50)

  if( 16*ndim*nkp*n_energy*nspin < 0 )              &
      write(14,*) ' Problem with the array size ', 16*ndim*nkp*n_energy*nspin

  if( cpa )then
    write(14,"(/,15x,'CPA calculation',/)")
    write(14,60)
  end if

!   Energy axis data

  write(14,"(/,10x,'Energy mesh data',/,' Inverse temperature',11x,f8.3,/,                 &
                 & ' Temperature',18x,f8.4,/,' Temperature (in K)',9x,f10.4)") beta, 1/beta, eV2K/beta

  write(14,"(' Number of tau points',9x,i8)") Ltau
  write(14,"(' Delta_tau = beta / L',9x,f6.2)") beta / ( Ltau - 1 )
  write(14,"(' Number of energy points',6x,i8)") n_energy
  write(14,"(' Minimal energy for real mesh   ',f6.2,/,             &
           & ' Maximal energy for real mesh   ',f6.2,/,             &
           & ' Small offset to imaginary plan ',f6.2)") emin, emax, eta
  if( imenergy )then
    write(14,"(' Calculations are carried out on Matsubara mesh')")
  else
    write(14,"(' Calculations are carried out on real energy mesh')")
  end if
  write(14,*); write(14,60)

  do itype = 1, n_imp_type
    n = impurity(itype)%nlm
    if (so == 1 ) n = n/2
    write(14,"(/,10x,' Input parameters for impurity # ',i3)") itype

    write(14,"(/,' Screened Coulomb interaction, U =',f10.6)") impurity(itype)%u_value
    write(14,"(' Hund exchange coupling,',7x,'J =',f10.6)") impurity(itype)%j_value

    average_u = impurity(itype)%u_value - (n-1)*impurity(itype)%j_value/(2*n-1)
    write(14,"(' Average of interacting terms',5x,f10.6)") average_u

    write(14,"(' Slater integrals,',13x,'F =',4(f10.6))") impurity(itype)%F(0:n-1:2)

    call write_matrix( 14, impurity(itype)%ummss, 'Density-density interaction matrix, Ummss$', ierr )

!     Write to file U-matrix
    if( verbos >= 15 )then
      write(fname,*) itype
      fname = 'umtrx_'//trim(adjustl(fname))//'.dat'

      iunit = 10
      open( iunit, file=fname, form='formatted', position='rewind', iostat=ierr )

      do i = 1, n
       do j = 1, n
        do k = 1, n
         do l = 1, n
           if( impurity(itype)%umtrx(i,j,k,l) /= 0 )                           &
               write(iunit,"(4(i3),1x,f10.6)") i, j, k, l,  impurity(itype)%umtrx(i,j,k,l)
         end do
        end do
       end do
      end do

      close(iunit)
    end if

    if( cpa )then
      write(14,"(/,' Concentration of this impurity in alloy ',f8.4)") impurity(itype)%x_cpa
      call write_matrix( 14, impurity(itype)%cpoc, 'Vcpa$', 'Spin # $', ierr )
    end if

  end do

 50 format(80('='))
 60 format(80('-'))

end subroutine header
