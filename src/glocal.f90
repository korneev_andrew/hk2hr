subroutine glocal
!
!   Calculation of local Green function for impurity
!
  use energy_module
  use impurity_module
  use parameter_module
  use iolib_module
  use qlmp_module
  use math_module
  use hamiltonian_module

  implicit none

  integer :: is, itype, ierr
  real(8) :: ntot(2)

!   Calculate local Green function using hamiltonian or Hilbert transform
  if( use_hmlt )then
    call glocal_hmlt
  else
    call glocal_dos
  end if

!      Green function
  if( verbos >= 10 )then
    do itype = 1, n_imp_type
      call write_function( mkfn('Green',resuf,itype), impurity(itype)%Green, prn_mesh, ierr, 'a' )
    end do
  end if

  if( reenergy ) return

!    Calculates occupation matrix
  do itype = 1, n_imp_type
    do is = 1, nspin
      call occupation_matrix( impurity(itype)%Green(:,:,:,is), fermionic, ntail, impurity(itype)%occupation(:,:,is) )
    end do
  end do
!
!    Print out
!
  if( verbos > 0 )then
    do itype = 1, n_imp_type

!      Occupation matrix
      write(14,"(80('-'))")
      write(14,"(/,' Occupation matrix for impurity # ',i2)") itype

      call write_matrix( 14, impurity(itype)%occupation, ' $', 'Spin$', 'Number of electrons in spin channel$', ierr )

      ntot(1) = spur( impurity(itype)%occupation(:,:,1), impurity(itype)%nlm )

      if( nspin == 2 )then  
        ntot(2) = spur( impurity(itype)%occupation(:,:,2), impurity(itype)%nlm )
        write(14,100) ntot(2)+ntot(1), ntot(2)-ntot(1)
       else
        write(14,100) 2*ntot(1), 0.0
      end if

    end do   !  over impurity type (itype)
  end if

100 format(' Number of electrons per impurity',4x,f12.8,/,' Magnetization',23x,f12.8 )

end subroutine glocal

subroutine glocal_hmlt
!
!   Calculation of local Green function using hamiltonian
!
  use energy_module
  use impurity_module
  use parameter_module
  use hamiltonian_module
  use math_module
  use gf_block
  use gf_atm_module
  use mpi_module
  
#ifdef MPI
  use mpi
#endif
  
  implicit none
  
  integer :: n1, n2, nd, nom_proc
  integer :: is, isp, iom, iom_glob, itype, ikp, i, j, ierr
  real(8), allocatable :: ekr(:)
  complex(8) :: z
  complex(8), allocatable :: Gloc(:,:), Vpot(:,:), g(:,:)
  complex(8), allocatable :: hkz(:,:), ekz(:,:), cl(:,:,:), cr(:,:,:)
  logical :: aisherm
  
  nd = ndim
  if( use_blockinverse )  nd = ldim

  allocate( Gloc(nd,nd), Vpot(nd,nd), g(nd,nd) )
  allocate( ekr(ndim), hkz(ndim,ndim) )
  
  if( use_atm ) allocate( ekz(ndim,nkp), cl(ndim,ndim,nkp), cr(ndim,ndim,nkp) )
 
  nom_proc = n_energy / numprocs
  if( proc_id < mod(n_energy,numprocs) )  nom_proc = nom_proc + 1
  
  forall ( itype = 1:n_imp_type ) impurity(itype)%Green  = zero
  forall ( itype = 1:n_imp_type ) impurity(itype)%Green0 = zero

!  write(proc_id+100,*) ' proc_id', proc_id, ( proc_id+1+(iom-1)*numprocs, iom=1,nom_proc)

!
!    Calculation of local Green function
!
  do is = 1, nspin
    do iom = 1, nom_proc
      iom_glob = proc_id+1+(iom-1)*numprocs
      z = zenergy(iom_glob)
      if( nspin == 2 .and. abs(h_field) > 1.d-4 ) z = z - ( 1.5d0 - is )*h_field

      Gloc = zero
!
!    Make "local potential": Sigma, Double counting, etc
!     
      Vpot = zero

      do itype = 1, n_imp_type
        do i = 1, impurity(itype)%n_imp
          n1 = impurity(itype)%hposition(i)
          n2 = n1 + impurity(itype)%nlm - 1
 
          isp = is
          if( impurity(itype)%magtype(i) == -1 ) isp = 3 - is
          Vpot(n1:n2,n1:n2) = impurity(itype)%Sigma(:,:,iom_glob,isp)

          forall( j = n1:n2 ) Vpot(j,j) = Vpot(j,j) + ( 1.5d0 - is )*impurity(itype)%h_field_local*impurity(itype)%magtype(i)
        end do
      end do
!
!     Calculate G(z)
!    
      if( use_atm .and. aimag(zenergy(iom_glob)) < atm_cutoff )then
        do ikp = 1, nkp
          hkz = h(:,:,ikp,is)
          hkz(1:nd,1:nd) = hkz(1:nd,1:nd) + Vpot

          call testherm( hkz, ndim, aisherm )

          if( aisherm )then
            call eigenproblem( hkz, ekr )
            ekz(:,ikp) = cmplx( ekr, 0.d0, 8 )
            cr(:,:,ikp) = hkz
            cl(:,:,ikp) = transpose( conjg( cr(:,:,ikp) ) )
          else
            call eigenproblem( hkz, ekz(:,ikp), cl(:,:,ikp), cr(:,:,ikp) )
          end if
        end do
 
        call green_atm( z, ekz, cl, cr, itt, Gloc, ldim, .false. )
 
      else
        do ikp = 1, nkp
          if( use_blockinverse )then
            hkz(1:ldim,1:ldim) = Vpot + h_ll(:,:,ikp,is)
            call gf_block_inversion( hkz(1:nd,1:nd), h_il(:,:,ikp,is), h_ii(:,:,ikp,is), e2(:,ikp,is), g, z )
          else
            call gk( g, h(:,:,ikp,is), Vpot, ndim, z )
          end if
          Gloc = Gloc + g*wtkp(ikp)
        end do
      end if
!
!   Local Green functions of different types
!
      do itype = 1, n_imp_type
        n1 = impurity(itype)%hposition(1)
        n2 = n1 + impurity(itype)%nlm - 1
        impurity(itype)%Green0(:,:,iom_glob,is) = Gloc(n1:n2,n1:n2)
      end do
      
    end do   ! n_energy
  end do     ! nspin
 
  do itype = 1, n_imp_type
#ifdef MPI
    call mpi_allreduce( impurity(itype)%Green0, impurity(itype)%Green,           &
                        impurity(itype)%nlm*impurity(itype)%nlm*n_energy*nspin,  &
                        mpi_double_complex, mpi_sum, mpi_comm_world, ierr )
#else
    impurity(itype)%Green = impurity(itype)%Green0
#endif
  end do

  deallocate( Gloc, Vpot, g )
  deallocate( ekr, hkz )
  if( use_atm ) deallocate( ekz, cl, cr )

end subroutine glocal_hmlt

subroutine glocal_dos
!
!    Calculation of local Green function using Hilbert transform
!
  use energy_module
  use impurity_module
  use parameter_module
  
  implicit none
  
  integer :: is, isp, iom, itype, i, ie
  real(8) :: de, si
  complex(8) :: z
  
  do itype = 1, n_imp_type
    de = impurity(itype)%edos(2) - impurity(itype)%edos(1)
    
    impurity(itype)%Green = zero
    do is = 1, nspin
      isp = is
      if( impurity(itype)%magtype(i) == -1 ) isp = 3 - is

      do iom = 1, n_energy
        z = zenergy(iom)
        if( nspin == 2 .and. abs(h_field) > 1.d-4 ) z = z - ( 1.5d0 - is )*h_field

          do i = 1, impurity(itype)%nlm
            do ie = 1, impurity(itype)%ndos
              si = 2 * de / 3
              if( mod(ie,2) == 0 ) si = 2 * si
              
              impurity(itype)%Green(i,i,iom,is) = impurity(itype)%Green(i,i,iom,is) +   &
                                                  si*impurity(itype)%dos(i,ie,is)   /   &
                     ( z - impurity(itype)%edos(ie) - impurity(itype)%Sigma(i,i,iom,isp) )
            end do
          end do  ! i (orbitals)
      end do   ! n_energy
    end do     ! nspin

  end do  !  itype

end subroutine glocal_dos
