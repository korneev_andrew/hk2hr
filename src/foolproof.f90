subroutine foolproof
!======================================================================
!
!   Writes header and initial values to output file
!
!======================================================================
  use parameter_module
  use energy_module
  use mpi_module
  
  implicit none
  
  !if( ntail >= n_energy ) ntail = min( n_energy/2, Ltau ) - 1
  if( ntail >= n_energy ) ntail = n_energy/2 - 1
  
  if( so == 1 .and. nspin == 2 )   call stop_program('so = 1 and nspin = 2. Not compatible values.$')

end subroutine foolproof
