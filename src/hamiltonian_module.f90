module hamiltonian_module
!
!    This module contains different arrays related to Hamiltonian, tetrahedrons, k-mesh, etc.
!
  implicit none

  save

  integer :: ndim                              !   Hamiltonian dimension
  integer :: nkp                               !   number of k-points
  integer :: nsham                             !   number of spins
  integer :: ntet                              !   number of tetrahedrons

  integer :: ldim                              !   dimension of impurity block H_ll
  integer :: idim                              !   dimension of the rest H_ii  (ldim+idim=ndim)
  integer :: nkxyz(3)                          !   division of the k-space along kx, ky and kz directions
  real(8) :: qp(3,3)                           !   vectors of first microcell in k-space

  integer,    allocatable :: itt(:,:)          !   array of tetrahedrons   (5,ntet)
  real(8),    allocatable :: wtkp(:)           !   weights of k-points     (nkp)
  real(8),    allocatable :: bk(:,:)           !   k-points                (3,nkp)
  complex(8), allocatable :: h(:,:,:,:)        !   Hamiltonian             (ndim,ndim,nkp,2)

  complex(8), allocatable :: h_ll(:,:,:,:)     !   "Impurity" part of hamiltonian   (ldim,ldim,nkp,nspin)
  complex(8), allocatable :: h_il(:,:,:,:)     !                                    (idim,ldim,nkp,nspin)
  complex(8), allocatable :: h_ii(:,:,:,:)     !                                    (idim,idim,nkp,nspin)
  real(8),    allocatable :: e2(:,:,:)         !   eigenvalues of h_ii              (idim,nkp,nspin)
  complex(8), allocatable :: dhdk(:,:,:,:,:)   !   derivative of the Hamiltonian with respect to k-points, dH/dk (3,ndim,ndim,nkp,2)

end module hamiltonian_module
