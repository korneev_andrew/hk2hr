module atm_module
!
!    This is analytical tetrahedron method module.
!   It calculates tetrahedron weights for the general case of 
!   the complex energy and complex eigenenergies at tetrahedron corners.
!   For calling see tetweight subroutine.
!   Based on PRB 29,6,3430 (1984).
!                     
!   Written by A. Poteryaev < A.Poteryaev _at_ cpht.polytechnique.fr >
!              April 1996
!   Revised    April 2009
!
  implicit none
  private
  
  real(8),    parameter :: dsmall = 2.5d-2
  real(8),    parameter :: dtiny  = 1.d-8
  complex(8), parameter :: zero  = cmplx( 0.d0, 0.d0, 8 )
  complex(8), parameter :: one   = cmplx( 1.d0, 0.d0, 8 )
  complex(8), parameter :: half  = cmplx(0.5d0, 0.d0, 8 )
  complex(8), parameter :: two   = cmplx( 2.d0, 0.d0, 8 )
  complex(8), parameter :: three = cmplx( 3.d0, 0.d0, 8 )
  complex(8), parameter :: six   = cmplx( 6.d0, 0.d0, 8 )
  
  public :: tetweight
  
  contains

  subroutine tetweight( z, e, res )
!
!    Returns the tetrahedron weights 
!  
!    Inputs:                                        
!i     z   - complex energy at which tetrahedron weights are calculated     
!io    e   - complex energy points at corner of tetrahedron (destroyed on output)   
!    Outputs:                                               
!o     res - complex tetrahedron weights
!                      
    implicit none
!    Passed variables    
    complex(8), intent(in   ) :: z
    complex(8), intent(inout) :: e(4)
    complex(8), intent(  out) :: res(4)
!    Local variables    
    integer :: i, flg, isrt(4)
!
!    Sorts eigenvalues in special order
!    
    call creorder( e, isrt, flg )
     
    select case(flg)
      case(1)                              !  Arbitrary eigenvalues 
        call   arbek( z, e, res )
      case(2)                              !  Two eigenvalues are identical
        call  ek2idn( z, e, res )
      case(3)                              !  Two pairs of energy are identical
        call ek22idn( z, e, res )
      case(4)                              !  Three eigenvalues are identical
        call  ek3idn( z, e, res )
      case(5)                              !  Four (all) eigenvalues are identical
        res = 2.5d-1 / ( z - e(1) )
    end select
!
!    Backsorting
!    
    e = res
    do i = 1, 4
      res(isrt(i)) = e(i)
    end do
 
  end subroutine tetweight

  subroutine creorder( e, isrt, flg )
!                                                           
!        This subroutine sorts four complex energy points   
!     in the next manner. It saves to the lowest indexes    
!     an identical energies (if they exist)                 
!                                                           
!    Inputs/Outputs:                                        
!io    e    - energy points for ordering (overwritten)      
!    Outputs:                                               
!o     isrt - pointer array                                 
!o     flg  - 1 if it is not identical energies             
!o            2 two energies are identical                  
!o            3 two pairs of energy are identical           
!o            4 three energies are identical                
!o            5 four energies are identical                 
!o           -1 sorting is wrong                            
!                                                           
    implicit none
!--->  Passed variables
    complex(8), intent(inout) :: e(4)
    integer,    intent(  out) :: flg 
    integer,    intent(  out) :: isrt(4) 
!--->  Local variables 
    integer :: i, j, l, n1, n2
    real(8) :: rez, imz
    complex(8) :: z, e_new(4)

    flg = -1
    isrt(1) = 1
    isrt(2) = 2
    isrt(3) = 3
    isrt(4) = 4

    l = 0
    do i = 1,3
      do j = i+1,4
        z   = e(i) - e(j)
        rez = abs( real( z, 8 ) )
        imz = abs( aimag( z ) )
        if( rez <= dtiny .and. imz <= dtiny ) l = l + 1
      end do
    end do

    select case(l)
      case(0)                               ! all energies are different
        flg = 1
!                                           ! two energies are identical
      case(1:2)                             ! or two pairs of energies
        loopi: do i = 1,3                   
          do j = i+1,4
            z   = e(i) - e(j)
            rez = abs( real( z, 8 ) )
            imz = abs( aimag( z ) )
            if( rez <= dtiny .and. imz <= dtiny )then
              n1 = i
              n2 = j
              exit loopi
            end if
          end do
        end do loopi

        e_new(1) = e(n1)
        e_new(2) = e(n2)
        isrt(1) = n1
        isrt(2) = n2
        
        i = 3
        do j = 1,4
          if( j == n1 .or. j == n2 ) cycle
          e_new(i) = e(j)
          isrt(i) = j
          i = i + 1
        end do
        
        e = e_new
        flg = 2
        if( l == 2 ) flg = 3
!                                          three energies are identical
      case(3)
        n2 = 0
        do i = 2,4
          z   = e(1) - e(i)
          rez = abs( real( z, 8 ) )
          imz = abs( aimag( z ) )
          if( rez > dtiny .or. imz > dtiny )then
              n1 = i
              n2 = n2 + 1
          end if
        end do
     
        if( n2 == 3 ) n1 = 1

        e_new(4) = e(n1)
        isrt(4)  = n1

        i = 1
        do j = 1,4
          if( j == n1 ) cycle
          e_new(i) = e(j)
          isrt(i) = j
          i = i + 1
        end do
        
        e = e_new
        flg = 4
!                                          four energies are identical
      case(6)
        flg = 5
    end select
    
  end subroutine creorder

  complex(8) function A1( z, e, i, k, m )
!                                                                   
!       Formula A1 from PRB 29,6,3430 (1984)                        
!                                                                   
!   Inputs:                                                         
!i     z - complex energy                                            
!i     e - eigenvalues                                               
!i     i - number of corner in which complex weight factor is calculated
!i     k - number of unequivalent corner                             
!i     m - number of corner in which energies are identical          
!   Outputs:                                                        
!o     A1  - weight factor                                           
!                                                                   
    implicit none
!--->  Passed variables
    integer,    intent(in   ) :: i, k, m
    complex(8), intent(in   ) :: z, e(4)
!--->  Local variables
    complex(8) :: zi, zk, zm, zkm, zik, zim, y1, y2, r

    zi = z - e(i)
    zk = z - e(k)
    zm = z - e(m)
    zkm = e(k) - e(m) 
    zik = e(i) - e(k) 
    zim = e(i) - e(m) 

    r = zim / ( zm * zkm )
    
    y1 = zim / zm
    if( abs( y1 ) > dsmall )then 
       y2 = ( two*zi/zim + zk/zkm ) / ( zkm*y1**2 )
       r  = r + y2*( log( one - y1 ) + y1 + half*y1*y1 + y1**3/three )
      else
       y2 = three * cdln1( y1,4,30,2 ) / zkm
       r  = r - y2
       y2 = ( two + zim/zkm ) * cdln1( y1,4,30,3 ) / zkm
       r  = r + y2
    end if

    y1 = zik / zk
    if( abs( y1 ) > dsmall )then 
       y2 = zik / ( zkm**2 * y1**3 )
       r  = r - y2*( log( one - y1 ) + y1 + half*y1*y1 + y1**3/three )
      else
       y2 = zik * cdln1( y1,4,30,3 ) / ( zkm*zkm )
       r  = r - y2
    end if

    A1 = r

  end function A1

  complex(8) function cdln1( z, nmin, nmax, n )
!                                                           
!                 Evaluation of sum -z**(i-n)/i             
!   Inputs:                                                 
!i    nmin  - lower bound of sum (must be larger zero)      
!i    nmax  - upper bound of sum (must be larger nmin)      
!i    n     - n <=nmin                                      
!i    z     - variable (must be in rang  | z | < 1 )        
!   Outputs:                                                
!o    cdln1 - magnitude of sum                              
!   Remarks:                                                     
!r    If  nmin = 1,  n = 0 and  nmax --> inf then           
!r        cdln1 = ln( 1 - z )                               
!                                                           
    implicit none
!--->  Passed variables
    integer,    intent(in   ) :: nmin, nmax, n
    complex(8), intent(in   ) :: z
!--->  Local variables
    integer :: i
    complex(8) :: a, b

    a = zero
    b = z**( nmin - n )
    do i = nmin,nmax
      a = a + b / real( i, 8 )
      b = b*z
    end do

    cdln1 = - a

  end function cdln1

  complex(8) function cdln2( z, nmin, nmax, n )
!                                                           
!                 Evaluation of sum  -(-z)**i/i             
!   Inputs:                                                 
!i    nmin  - lower bound of sum (must be larger zero)      
!i    nmax  - upper bound of sum (must be larger nmin)      
!i    n     - n <= nmin                                     
!i    z     - variable (must be in rang  | z | < 1 )        
!   Outputs:                                                
!o    cdln2 - magnitude of sum                              
!   Remarks:                                                     
!r    If  nmin = 1, n = 0  and nmax --> inf then            
!r        cdln2 = ln( 1 + z )                               
!                                                           
    implicit none
!--->  Passed variables
    integer,    intent(in   ) :: nmin, nmax, n
    complex(8), intent(in   ) :: z
!--->  Local variables
    integer :: i
    complex(8) :: a, b

    a = zero
    b = z**( nmin - n )
    b = (-1)**nmin * b
    do i = nmin,nmax
      a = a + b / real( i, 8 )
      b = -b*z
    end do

    cdln2 = - a

  end function cdln2

  subroutine arbek( z, e, res )
!                                                                
!       Calculation of complex weight factor for different       
!                   complex eigenvalues                          
!                                                                
!   Inputs:                                                      
!i     z - complex energy                                        
!i     e - eigenvalues                                           
!   Outputs:                                                     
!o     res  - weight factor of Green function                    
!   Remarks:                                                     
!r     PRB 29,6,3430 (1984); (formulas 7,8)                      
!                                                                
    implicit none
!--->  Passed variables
    complex(8), intent(in   ) :: z
    complex(8), intent(in   ) :: e(4)
    complex(8), intent(  out) :: res(4)
!--->  Local variables
    integer    :: i, j, k
    complex(8) :: r, y, y1, t, zj, zij
      
    do i = 1,4
      r = zero
      do j = 1,4
        if( j == i ) cycle

        zj  = z - e(j)
        zij = e(i) - e(j)
        y   = zij / zj

        if( abs( y ) > dsmall )then
          t = zij
          do k = 1,4
            if( k == j ) cycle
            t = t*( e(k)-e(j) )
          end do
          
          y1 = zj**3 / t 
          r  = r - y1 * ( y + half*y*y + y**3 / three + log( one - y ) )
         else
          t = one
          do k = 1,4
            if( k == j .or. k == i ) cycle
            t = t*( e(k)-e(j) )
          end do

          y1 = zij / t
          r  = r - y1*cdln1( y,4,25,3 )
        end if 

      end do

      res(i) = r

    end do

  end subroutine arbek

  subroutine ek2idn( z, e, res )
!                                                                
!       Calculation of complex weight factor in case when       
!              two corner energies are identical                
!                                                               
!   Inputs:                                                     
!i     z - complex energy                                        
!i     e - eigenvalues                                           
!   Outputs:                                                    
!o     res  - weight factor of Green function of one orbital     
!   Remarks:                                                    
!r     PRB 29,6,3430 (1984); (formulas A1,A2)                    
!          
    implicit none
!--->  Passed variables
    complex(8), intent(in   ) :: z
    complex(8), intent(in   ) :: e(4)
    complex(8), intent(  out) :: res(4)
!--->  Local variables 
    complex(8) :: zm, zi, zk, zik, zim, zkm, y1, y2, r

    zm = z - e(1)
    zi = z - e(3)
    zk = z - e(4)
    zik = e(3) - e(4)
    zim = e(3) - e(1)
    zkm = e(4) - e(1)

    y1 = zim / zi
    if( abs( y1 ) > dsmall )then
       y2 = log( one + y1 ) / ( y1**3 ) - one / ( y1*y1 ) + half / y1 - one / three
       r  = - y2 / zik
      else
       r = - cdln2( y1,4,25,3 ) / zik
    end if

    y1 = zkm/zk
    if( abs( y1 ) > dsmall )then
       y2 = log( one + y1 ) / ( y1**3 ) - one / ( y1*y1 ) + half / y1 - one / three
       r  = r + y2 / zik
      else
       r = r + cdln2( y1,4,25,3 ) / zik
    end if

    res(1) = r
    res(2) = r
    res(3) = A1( z,e,3,4,1 )
    res(4) = A1( z,e,4,3,1 )

  end subroutine ek2idn

  subroutine ek22idn( z, e, res )
!                                                                
!       Calculation of complex weight factor in case when        
!              three corner energies are identical               
!                                                                
!   Inputs:                                                      
!i     z - complex energy                                        
!i     e - eigenvalues                                           
!   Outputs:                                                     
!o     res  - weight factor of Green function of one orbital     
!   Remarks:                                                     
!r     PRB 29,6,3430 (1984); (formula A4)                        
!                                                                
    implicit none
!--->  Passed variables
    complex(8), intent(in   ) :: z
    complex(8), intent(in   ) :: e(4)
    complex(8), intent(  out) :: res(4)
!--->  Local variables 
    real(8)    :: rez2, imz2, rez3, imz3
    complex(8) :: z2, z3, z32, y, r

    z2  = z - e(2)
    z3  = z - e(3)
    z32 = e(3) - e(2)
    rez2 = abs( real( z2, 8 ) )
    imz2 = abs( aimag( z2 ) )
    rez3 = abs( real( z3, 8 ) )
    imz3 = abs( aimag( z3 ) )
    
    if( abs( z32/z3 ) > dsmall )then
       y = - three*z2*( two*z3 - z32 ) / ( two*z32**3 ) - one / z32
       r = three*log( z2/z3 )*z3**2*z2 / z32**4 
       r = r + y
       res(1) = r
       res(2) = r
      else
       y = z32 / z3
       r = three*( cdln2( y,4,25,3 ) + cdln2( y,3,25,2 ) ) / z32 
       res(1) = r
       res(2) = r
    end if
        
    if( abs( z32/z2 ) > dsmall )then
       y = three*z3*( two*z2 + z32 ) / ( two*z32**3 ) + one / z32
       r = three*log( z3/z2 )*z2**2*z3 / z32**4 
       r = r + y
       res(3) = r
       res(4) = r
      else
       y = z32 / z2
       r = three*( cdln1( y,4,25,3 ) - cdln1( y,3,25,2 ) ) / z32 
       res(3) = r
       res(4) = r
    end if
 
  end subroutine ek22idn

  subroutine ek3idn( z,e,res )
!                                                                
!       Calculation of complex weight factor in case when        
!              three corner energies are identical               
!                                                                
!   Inputs:                                                      
!i     z - complex energy                                        
!i     e - eigenvalues                                           
!   Outputs:                                                     
!o     res  - weight factor of Green function of one orbital     
!   Remarks:                                                     
!r     PRB 29,6,3430 (1984); (formula A3)                        
!                                                                
    implicit none
!--->  Passed variables
    complex(8), intent(in   ) :: z
    complex(8), intent(in   ) :: e(4)
    complex(8), intent(  out) :: res(4)
!--->  Local variables 
    real(8)    :: rez3, imz3, rez4, imz4
    complex(8) :: z3, z4, z43, y, r

    z3  = z - e(3)
    z4  = z - e(4)
    z43 = e(4) - e(3)
    rez3 = abs( real( z3, 8 ) )
    imz3 = abs( aimag( z3 ) )
    rez4 = abs( real( z4, 8 ) )
    imz4 = abs( aimag( z4 ) )
    
    if( abs( z43/z4 ) > dsmall )then
       r = ( six*z4**2 - three*z43*z4 + two*z43**2 ) / ( six*z43**3 ) 
       y = zero
       if( rez4 > dtiny .or. imz4 > dtiny )  y = log( z4/z3 )*z4**3 / z43**4 
       r = r + y
       res(1) = r
       res(2) = r
       res(3) = r
       r = - three*z3*( two*z4 - z43 ) / ( two*z43**3 ) - one / z43
       y = three*log( z3/z4 )*z4**2*z3 / z43**4 
       r = r + y
       res(4) = r
      else
       y = z43 / z4
       r = -cdln2( y,4,25,3 ) / z43
       res(1) = r
       res(2) = r
       res(3) = r
       res(4) = three*( cdln2( y,3,25,2 ) + cdln2( y,4,25,3 ) ) / z43 
    end if

  end subroutine ek3idn

end module atm_module
