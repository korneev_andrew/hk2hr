module fourier_transform_module
!
!    This module contains a collection of routines for Fourier transform.
!
  implicit none

  real(8), private, parameter :: half  = 0.5d0
  real(8), private, parameter :: one   = 1.d0
  real(8), private, parameter :: two   = 2.d0
  real(8), private, parameter :: three = 3.d0
  real(8), private, parameter :: six   = 6.d0
  real(8), private, parameter :: pi = 3.1415926535897932384626433832795028841971693993751d0
  real(8), private, parameter :: twopi  = 2.d0 * pi
  complex(8), private, parameter :: zero = (0.d0, 0.d0)
  complex(8), private, parameter :: xi   = (0.d0, 1.d0)

  
  public :: fourier_transform
  
  interface fourier_transform
    module procedure w2t, t2w, nfourier
  end interface 
  
  contains
  
  subroutine nfourier( f_tau,n_tau,f_omega,n_omega,beta,der_f_1,der_f_n,iflag )
    implicit none
!
    integer,    intent(in   ) :: n_tau,n_omega,iflag
    real(8),    intent(in   ) :: beta
    real(8),    intent(in   ) :: der_f_1
    real(8),    intent(in   ) :: der_f_n
    real(8),    intent(in   ) :: f_tau(n_tau)
    complex(8), intent(  out) :: f_omega(n_omega)
!
    integer :: i,j,k
    real(8) :: delta,delta2,delta3,om,om2,om3,om4,p
    real(8), allocatable :: f_1(:),a(:),b(:),c(:),d(:),u(:),q(:),xm(:)
    complex(8) :: cdummy,ex,explus
    
    allocate( f_1(n_tau+1),u(n_tau+1),q(n_tau+1),xm(n_tau+1) )
       
    delta  = beta / real(n_tau,8)
    delta2 = delta * delta
    delta3 = delta * delta2
    forall( i = 1:n_tau ) f_1(i) = f_tau(i)

!            information on jump-condition

    if( iflag == 0 )then
      f_1(n_tau+1) = - one - f_tau(1)
     else
      f_1(n_tau+1) =       - f_tau(1)
    end if
!
!     spline interpolation:  the spline is given by
!     G(tau)=a(i) + b(i) (tau-tau_i) + c(i) ( )^2 + d(i) ( )^3
!     The following formulas are taken directly from  Stoer and
!     Bulirsch p. 102
!
    if( der_f_1 > 1.d50 )then
      q(1) = 0
      u(1) = 0
     else
      q(1) = - half
      u(1) = three * ( ( f_1(2) - f_1(1) )/delta - der_f_1 ) / delta
    end if

    do k = 2,n_tau
      p    =  half * q(k-1) + two
      q(k) = -half / p
      u(k) = three * ( f_1(k+1) + f_1(k-1) - two*f_1(k) ) / delta2
      u(k) = ( u(k) - u(k-1) * half ) / p
    end do
    
    if( der_f_n > 1.d50 )then
      xm(n_tau+1) = 0
     else
      xm(n_tau+1) = ( six*( der_f_n - ( f_1(n_tau+1) - f_1(n_tau) )/delta )/delta -   &
                      u(n_tau) ) / ( q(n_tau) + two )
    end if

    do k = n_tau,1,-1
      xm(k) = q(k) * xm(k+1) + u(k)
    end do
    
    deallocate( u,q )
!
!     The following formulas are taken directly from  Stoer and
!     Bulirsch p. 98
!
    allocate( a(n_tau),b(n_tau),c(n_tau),d(n_tau) )

    do j = 1,n_tau
      a(j) = f_1(j)
      c(j) = xm(j) * half
      b(j) = ( f_1(j+1) - f_1(j) ) / delta - ( two*xm(j) + xm(j+1) ) * delta / six
      d(j) = ( xm(j+1) - xm(j) ) / ( six*delta )
    end do
    
    deallocate( f_1,xm )
!
!     The Spline multiplied by the exponential can now be exlicitely
!     integrated. The following formulas were obtained using
!     MATHEMATICA
!
    f_omega = zero
    do i = 1,n_omega
      om  = ( two*i - one ) * pi / beta
      om2 = om * om
      om3 = om * om2
      om4 = om * om3
      do j = 1,n_tau
        cdummy = xi * om * delta * j
        explus = exp(cdummy)
        cdummy = xi * om * delta * ( j - 1 )
        ex     = exp(cdummy)

        f_omega(i) = f_omega(i) + explus * ( -six*d(j) / om4 +            &
                  two*xi *( c(j) + three*delta* d(j) ) / om3 +            &
                  ( b(j)+ two*delta*c(j) + three*delta2*d(j) ) / om2 -    &
                  xi*( a(j) + delta*b(j) + delta2*c(j) + delta3*d(j) ) / om )
 
        f_omega(i) = f_omega(i) + ex * ( six*d(j) / om4 -                 &
                     two*xi*c(j) / om3 - b(j) / om2 + xi*a(j) / om )
      end do
    end do
 
    deallocate( a,b,c,d )
       
  end subroutine nfourier
!===============================================================================
  
  subroutine t2w( f_tau, f_w, xw, derivs )
    implicit none
!
    real(8),    intent(in   ) :: f_tau(:)
    real(8),    intent(in   ) :: xw(:)
    real(8),    intent(in   ) :: derivs(2)
    complex(8), intent(  out) :: f_w(:)
!
    integer :: n_w, n_tau
    real(8) :: beta
    integer :: i, j, k
    real(8) :: delta, delta2, delta3, om, om2, om3, om4, p
    real(8), allocatable :: a(:), b(:), c(:), d(:), u(:), q(:), xm(:)
    complex(8) :: cdummy, ex, explus

!    Define size of arrays    
    n_w = size( f_w )
    if( n_w /= size(xw) .or. n_w <= 0 ) stop
    
    n_tau = size( f_tau ) - 1
    if( n_tau <= 0 ) stop
    
    beta = twopi / ( xw(2) - xw(1) )

!    Allocate working arrays    
    allocate( u(n_tau+1), q(n_tau+1), xm(n_tau+1) )
       
    delta  = beta / real(n_tau,8)
    delta2 = delta * delta
    delta3 = delta * delta2
!
!     Spline interpolation:  the spline is given by
!     G(tau)=a(i) + b(i) (tau-tau_i) + c(i) ( )^2 + d(i) ( )^3
!     The following formulas are taken directly from  Stoer and
!     Bulirsch p. 102
!
    if( derivs(1) > 1.d30 )then
      q(1) = 0.d0
      u(1) = 0.d0
    else
      q(1) = - half
      u(1) = three * ( ( f_tau(2) - f_tau(1) )/delta - derivs(1) ) / delta
    end if

    do k = 2, n_tau
      p    =  half * q(k-1) + two
      q(k) = -half / p
      u(k) = three * ( f_tau(k+1) + f_tau(k-1) - two*f_tau(k) ) / delta2
      u(k) = ( u(k) - u(k-1) * half ) / p
    end do
    
    if( derivs(2) > 1.d30 )then
      xm(n_tau+1) = 0.d0
    else
      xm(n_tau+1) = ( six*( derivs(2) - ( f_tau(n_tau+1) - f_tau(n_tau) )/delta )/delta -   &
                      u(n_tau) ) / ( q(n_tau) + two )
    end if

    do k = n_tau, 1, -1
      xm(k) = q(k) * xm(k+1) + u(k)
    end do
    
    deallocate( u, q )
!
!     The following formulas are taken directly from  Stoer and
!     Bulirsch p. 98
!
    allocate( a(n_tau), b(n_tau), c(n_tau), d(n_tau) )

    do j = 1, n_tau
      a(j) = f_tau(j)
      c(j) = xm(j) * half
      b(j) = ( f_tau(j+1) - f_tau(j) ) / delta - ( two*xm(j) + xm(j+1) ) * delta / six
      d(j) = ( xm(j+1) - xm(j) ) / ( six*delta )
    end do
    
    deallocate( xm )
!
!     The Spline multiplied by the exponential can now be exlicitely
!     integrated. The following formulas were obtained using
!     MATHEMATICA
!
    f_w = zero
    do i = 1, n_w
      om  = xw(i)
      om2 = om * om
      om3 = om * om2
      om4 = om * om3
      do j = 1, n_tau
        cdummy = xi * om * delta * j
        explus = exp(cdummy)
        cdummy = xi * om * delta * ( j - 1 )
        ex     = exp(cdummy)

        if( abs(om) > 1.d-4 )then
         f_w(i) = f_w(i) + explus * ( -six*d(j) / om4 +                   &
                  two*xi *( c(j) + three*delta* d(j) ) / om3 +            &
                  ( b(j)+ two*delta*c(j) + three*delta2*d(j) ) / om2 -    &
                  xi*( a(j) + delta*b(j) + delta2*c(j) + delta3*d(j) ) / om )
 
         f_w(i) = f_w(i) + ex * ( six*d(j) / om4 -                 &
                     two*xi*c(j) / om3 - b(j) / om2 + xi*a(j) / om )
        else
         f_w(i) = f_w(i) + a(j)*delta + b(j)*delta2/2 + c(j)*delta3/3 + d(j)*delta2*delta2/4
        end if
      end do
    end do
 
    deallocate( a, b, c, d )
       
  end subroutine t2w
  
  subroutine w2t( f_w, xw, f_tau, moms, inb )
!    Fourier transform from Matsubara frequencies to imaginary time domain.
!    This subroutine explicitly uses moments to accurately treat a high-energy tail.
!
!   TODO
!   Check large beta/tau equations
!
    implicit none
!
    complex(8), intent(in   ) :: f_w(:)           !   Function to Fourier transform
    real(8)   , intent(in   ) :: xw(:)            !   Matsubara (omega) mesh
    real(8)   , intent(in   ) :: moms(3)          !   High-energy moments of f_w
    real(8)   , intent(  out) :: f_tau(:)         !   Output function in time domain 
    integer   , intent(in   ) :: inb              !   0 - dont calculate tau=beta point
                                                  !   1 -      calculate tau=beta point
    
    integer :: n_w, n_tau, i, j
    real(8) :: a1, a2, a3, beta, tau
    complex(8) :: z
    
    n_w = size( f_w )
    if( n_w /= size(xw) .or. n_w <= 0 ) stop
    
    n_tau = size( f_tau )
    if( n_tau <= 0 ) stop
    
    beta = twopi / ( xw(2) - xw(1) )
    
    a1 =   moms(1) - moms(3)
    a2 = ( moms(3) + moms(2) ) / 2.d0
    a3 = ( moms(3) - moms(2) ) / 2.d0
        
    f_tau = 0.d0
    do i = 1, n_tau
      tau = ( i - 1 )*beta / real(n_tau-inb,8)
      
      do j = 1, n_w
        z = f_w(j) - a1/cmplx(0.d0,xw(j),8) - a2/cmplx(-1.d0,xw(j),8) - a3/cmplx(1.d0,xw(j),8)
        z = z * cmplx( cos(xw(j)*tau), -sin(xw(j)*tau), 8 )
    
        f_tau(i) = f_tau(i) + real(z,8)
      end do
      
      if( beta < 1400 )then
        f_tau(i) = 2 * f_tau(i) / beta - a1/2 -                         &
                   0.5 * ( moms(2)*sinh(beta/2-tau) + moms(3)*cosh(beta/2-tau) ) / cosh(beta/2)
      else
        if( tau < 700 )then
          f_tau(i) = 2 * f_tau(i) / beta - a1/2 - moms(2)*( -sinh(tau) + cosh(tau)*tanh(beta/2) )/2   &
                                                - moms(3)*(  cosh(tau) - sinh(tau)*tanh(beta/2) )/2
        elseif( tau > 2100 )then
          continue
        else
          f_tau(i) = 2 * f_tau(i) / beta - a1/2 -                         &
                     0.5 * ( moms(2)*sinh(beta/2-tau) + moms(3)*cosh(beta/2-tau) ) / cosh(beta/2)
        end if
      end if
  
    end do

  end subroutine w2t

end module fourier_transform_module
