subroutine make_hlhi
!
!   This subroutine checks a correctness of impurity positions and
!   divides the Hamiltonian onto H_ll, H_li, H_ii
!       H =  |  H_ll   H_li  |
!            |  H_il   H_ii  |
!   where H_ll contains all impurities
!  
  use hamiltonian_module
  use impurity_module
  use parameter_module
  use mpi_module
  use lapack_module
  
  implicit none

  integer :: i, j, itype, jtype, n1, n2, m1, m2, is, ikp
  integer, allocatable :: ipos(:)
  
  complex(8), allocatable :: htmp(:,:,:,:)
  
!-------------------------------------------------------------------------------
!   Checks the correctness of impurity positions

  allocate( ipos(ndim) )
  ipos = 0

  do itype = 1, n_imp_type
!   Negative positions
    if( any( impurity(itype)%hposition(:) < 1 ) )   call stop_program('Position of impurity cannot be negative.$')

!   Positions that exceed the Hamiltonian dimension  
  
    n1 = maxloc( impurity(itype)%hposition(:), dim=1 )
    n2 = impurity(itype)%hposition(n1) + impurity(itype)%nlm - 1
    if( n2 > ndim ) call stop_program('Position exceeds Hamiltonian dimension.$')

!   Overlap of impurities
    
    do i = 1, impurity(itype)%n_imp
      n1 = impurity(itype)%hposition(i)
      n2 = n1 + impurity(itype)%nlm - 1
      ipos(n1:n2) = ipos(n1:n2) + 1
    end do

  end do

  if( any( ipos > 1 ) ) call stop_program('Position overlap.$')
  
!------->    Debug
 ! ipos = 0
 ! do itype = 1, n_imp_type
 !   do i = 1, impurity(itype)%n_imp
 !     n1 = impurity(itype)%hposition(i)
 !     n2 = n1 + impurity(itype)%nlm - 1
 !     ipos(n1:n2) = ipos(n1:n2) + itype
 !   end do
 ! end do
 ! write(6,"(a,500(1x,i3))") ' ipos ', proc_id, ipos
!------->    Debug
  
!-------------------------------------------------------------------------------
!    Sorts Hamiltonian to gather "correlated" states in left-upper block
!    and redefines impurity positions

  allocate( htmp(ndim,ndim,nkp,2) )
  
  m1 = 1
  do itype = 1, n_imp_type
    do i = 1, impurity(itype)%n_imp
      n1 = impurity(itype)%hposition(i)
      n2 = n1 + impurity(itype)%nlm - 1
      m2 = m1 + impurity(itype)%nlm - 1
      
      htmp(m1:m2,    :,:,:) = h(n1:n2,    :,:,:)
      htmp(m2+1:n2,  :,:,:) = h(m1:n1-1,  :,:,:)
      htmp(n2+1:ndim,:,:,:) = h(n2+1:ndim,:,:,:)
      h = htmp
      
      htmp(:,m1:m2,    :,:) = h(:,n1:n2,    :,:)
      htmp(:,m2+1:n2,  :,:) = h(:,m1:n1-1,  :,:)
      htmp(:,n2+1:ndim,:,:) = h(:,n2+1:ndim,:,:)
      h = htmp

      impurity(itype)%hposition(i) = m1
      m1 = m2 + 1
      
      do j = i+1, impurity(itype)%n_imp
        if( impurity(itype)%hposition(j) < n1 )           &
            impurity(itype)%hposition(j) = impurity(itype)%hposition(j) + impurity(itype)%nlm
      end do
      
      do jtype = itype+1, n_imp_type
        do j = 1, impurity(jtype)%n_imp
          if( impurity(jtype)%hposition(j) < n1 )           &
              impurity(jtype)%hposition(j) = impurity(jtype)%hposition(j) + impurity(itype)%nlm
        end do
      end do
      
    end do
  end do

  deallocate( htmp, ipos )
  
  ldim = sum( impurity(:)%nlm * impurity(:)%n_imp )
  idim = ndim - ldim
  
  if( ldim == ndim ) use_blockinverse = .false.

!   Divides Hamiltonian onto H_ll, H_li, H_ii

  if( use_blockinverse )then
    allocate( h_ll(ldim,ldim,nkp,nspin), h_il(idim,ldim,nkp,nspin), h_ii(idim,idim,nkp,nspin), e2(idim,nkp,nspin) )
    
    h_ll(:,:,:,:) = h(1:ldim,      1:ldim,     :,:)
    h_il(:,:,:,:) = h(ldim+1:ndim, 1:ldim,     :,:)
    h_ii(:,:,:,:) = h(ldim+1:ndim, ldim+1:ndim,:,:)
    
    do is = 1, nspin
      do ikp = 1, nkp
        call eigenproblem( h_ii(:,:,ikp,is), e2(:,ikp,is) )
        
    !      C_22 * H_il  ->  H_il
    !    h_il(:,:,ikp,is) = matmul( h_ii(:,:,ikp,is), h_il(:,:,ikp,is) )
        
      end do
    end do
    
  end if
  
end subroutine make_hlhi
