subroutine wrapper4cpa( itype )
!
!    This is calling sequence for CPA
!
  use parameter_module
  use impurity_module
  use iolib_module
  use energy_module
  use symmetry_module
  use math_module
  use tools_module
  use qlmp_module

  implicit none

  integer, intent(in   ) :: itype

!   Local variables

  integer :: is, iom, ierr

  integer :: n_orb
  real(8), allocatable :: qlm(:,:,:)

  n_orb = impurity(itype)%nlm
  impurity(itype)%ninj = 0.d0

!       Calculates Green function: G^{-1} = G0^{-1} - V

  impurity(itype)%Green = impurity(itype)%Green0

  call inverse( impurity(itype)%Green )

  forall( iom = 1:n_energy )   impurity(itype)%Green(:,:,iom,:) = impurity(itype)%Green(:,:,iom,:) - impurity(itype)%cpoc

  call inverse( impurity(itype)%Green )

  call set_symmetry( impurity(itype)%Green, impurity(itype)%smask )

  allocate( qlm(n_orb,n_orb,2) )

  do is = 1, nspin
    call occupation_matrix( impurity(itype)%Green(:,:,:,is), fermionic, ntail, qlm(:,:,is) )
  end do
  if( nspin == 1 ) qlm(:,:,2) = qlm(:,:,1)

  if( nspin == 1 )then
    forall( iom = 1:n_orb )
      impurity(itype)%n_cor(iom)       = qlm(iom,iom,1)
      impurity(itype)%n_cor(iom+n_orb) = qlm(iom,iom,1)
    end forall
  else
    forall( iom = 1:n_orb )
      impurity(itype)%n_cor(iom)       = qlm(iom,iom,1)
      impurity(itype)%n_cor(iom+n_orb) = qlm(iom,iom,2)
    end forall
  end if

  if( verbos > 0 )then

    call write_matrix( 14, qlm, 'n(G_imp)$', 'Spin$', 'Number of electrons in spin channel$', ierr )

    write(14,"(' Number of impurity electrons',12x,f12.8,/,' Magnetization',27x,f12.8)")                      &
                 spur( qlm(:,:,2)+qlm(:,:,1), n_orb ), spur( qlm(:,:,2)-qlm(:,:,1), n_orb )

!     Dummy output for CPA that regarded non-interacting (U=0)

    write(14,"(' Instant squared magnetic moment, < m_z^2 >, ',f12.8)") 0
    write(14,"(/,' Hartree-Fock energy, U<n><n>/2',10x,f10.6)") 0
    write(14,"(/,' Coulomb correlation energy, U<nn>/2',5x,f10.6,/)") 0

  end if

  deallocate( qlm )

end subroutine wrapper4cpa