module ed_arnoldi_module

! Written by A.Belozerov, A.Poteryaev and A.Lichtenstein.
! ( questions to <alexander.s.belozerov _at_ gmail.com> )

! There are some notations in this code:
!   d-electrons  -- correlated electrons
!   s-electrons  -- bath electrons
!   _pr          -- per processor

! Verbosity:
!   verb =  0  -- doesn't print out anything
!   verb = 10  -- print out only the most important information
!   verb = 20  -- print out detailed information
!   verb = 30  -- debug mode

! Optimization levels:
!   opt = 0  -- does Arnoldi with required precision (no optimization)
!   opt = 1  -- does Arnoldi 

  use mpi_module

#ifdef MPI
  use mpi
#endif

  implicit none

  private
    character(4), parameter :: vers ='0.67'   ! version
    integer,      parameter :: prec = 4       ! precision = 4, 8
    integer,      parameter :: outf = 14      ! output file
    integer,      parameter :: comtype = 1    ! communications = 1(simple), 2(beta)

    real(prec),   parameter :: tny  = 1.e-8
    real(prec),   parameter :: zero = 0.0_prec
    real(prec),   parameter :: one  = 1.0_prec
    real(prec),   parameter :: two  = 2.0_prec
    real(prec),   parameter :: pi = 3.141592653589793238462643_prec

    integer :: verb       ! verbosity
    integer :: opt        ! optimization = 0, 1, 2
    integer :: ncv0       ! ncv = 2*newnev + ncv0

    integer :: Nt         ! # of electrons in problem per spin
    integer :: Nsp        ! # of spins 
    integer :: nlm        ! # of correlated bands
    integer :: Iwmax      ! # of points in energy mesh

    integer :: nev_arno   ! # of eigenvalues to calculate
    integer :: nev_gf     ! # of eigenvalues to calculate

    integer :: Nv(3)      ! # of valence bath levels (2sp,total)
    integer :: Nc(3)      ! # of conductivity bath levels (2sp,total)

    integer :: Nd_min(3)  ! min # of d-electrons (2sp,total)
    integer :: Nd_max(3)  ! max # of d-electrons (2sp,total)
    integer :: Nh_max(3)  ! max # of holes in bath valence levels (2sp,total)
    integer :: Nc_max(3)  ! max # of conductivity electrons in bath (2sp,total)

    integer :: comm       ! mpi_comm_world
    integer :: myid       ! processor id
    integer :: nprocs     ! # of processors
    integer :: nprocs_eff ! effective # of procs
    integer :: mpi_knd    ! mpi_kind (local value) 

    integer :: maxd       ! max sector dimension
    integer :: maxd_sp    ! max sector dimension per spin
    integer :: Nst_pr     ! # of states per processor
    integer :: Nst_dn     ! # of states for spin down

    real(prec) :: beta      ! inverse temperature
    real(prec) :: tol_gf    ! Green function tolerance
    real(prec) :: tol_arn   ! Arnoldi tolerance

    integer :: nup_min, nup_max  ! min and max # of spin up particles
    integer :: ndn_min, ndn_max  ! min and max # of spin down particles

    real(prec), allocatable :: U(:,:,:,:)   ! U-matrix
    real(prec), allocatable :: Hop(:,:,:)   ! Hoppings
    real(prec), allocatable :: Hdiag(:)     ! diag Hamilt elements
    real(prec), allocatable :: Hst(:)       ! non-diag Hamilt elements
    integer,    allocatable :: Hpos(:)      ! positions of rows in Hst
    integer,    allocatable :: Hcon(:)      ! connections for Hst

    integer(1), allocatable :: basis(:,:,:) ! basis vectors
    integer,    allocatable :: inv(:,:)     ! inverse basis vectors
    integer,    allocatable :: inv0(:,:)    ! inv() used in T-Lanczos
    integer,    allocatable :: Nst(:,:)     ! # of states for given nup,ndn
    integer,    allocatable :: inf(:,:)     ! information about eigenvalues

    real(prec), allocatable :: eval(:,:)    ! eigenvalues
    real(prec), allocatable :: evec(:,:)    ! eigenvectors
    real(prec), allocatable :: Ed(:,:)      ! Energies of d-electrons
    real(prec), allocatable :: Es(:,:)      ! Energies of s-electrons
    real(prec), allocatable :: vecinit(:)   ! init vector for T-Lanczos
    real(prec), allocatable :: vecold(:)    ! vector for operations in T-Lanczos
    complex(8), allocatable :: boson(:)     ! bosonic Matsubara's mesh
    real(prec), allocatable :: den(:,:)     ! diagonal density

    integer,    allocatable :: distr(:)     ! distribution on processors
    integer,    allocatable :: displ(:)     ! displacements on processors
    integer,    allocatable :: Nget(:)      ! # of elements to get from other procs
    integer,    allocatable :: connect(:)   ! connections for elements from other procs

!   Variables for timers
    real(8) :: t_stp, t_lcomm, t_arn0, t_lmvp, t_zos, t_saupd, t_lab,  t_hd,   &
               t_arn, t_seupd, t_stp0, t_amvp, t_lnc, t_asort, t_full, t_acomm

  public :: ed_arnoldi

  contains

  subroutine ed_arnoldi( U_ini, Eimp, Epsk, Vk, beta_ini, verb_ini, Gf,        &
                         sisj, zenergy, lunn, lsisj, Nd_min_ini, Nd_max_ini,   &
                         Nc_max_ini, Nh_max_ini, opt_ini, ed_arno_nev_ini,     &
                         ncv0_ini, ed_arno_tol_ini, ed_gf_nev_ini,             &
                         ed_gf_decomp_ini, ed_gf_tol_ini  )
                        
    implicit none

!   All allocations and deallocations of common var are placed in this subroutine

    real(8),    intent(in   ) :: U_ini(:,:,:,:)   ! U-matrix
    real(8),    intent(in   ) :: Eimp(:,:)        ! impurity levels
    real(8),    intent(in   ) :: Epsk(:,:)        ! bath levels
    real(8),    intent(in   ) :: Vk(:,:,:)        ! hoppings
    real(8),    intent(in   ) :: beta_ini         ! inverse temperature
    integer,    intent(in   ) :: verb_ini         ! verbosity

    complex(8), intent(  out) :: Gf(:,:,:)        ! Green function
    complex(8), intent(  out) :: sisj(:,:)        ! spin-spin correlation function
    complex(8), intent(in   ) :: zenergy(:)       ! energy mesh

    logical,    intent(in   ) :: lunn             ! switch for density-density U-matrix
    logical,    intent(in   ) :: lsisj            ! switch for spin-spin correlation function

    integer,    intent(in   ) :: Nd_min_ini       ! min # of d-electrons
    integer,    intent(in   ) :: Nd_max_ini       ! max # of d-electrons
    integer,    intent(in   ) :: Nc_max_ini       ! max # of conductivity electrons in bath
    integer,    intent(in   ) :: Nh_max_ini       ! max # of holes in bath valence levels

    integer,    intent(in   ) :: opt_ini          ! optimization = 0, 1, 2

    integer,    intent(inout) :: ed_arno_nev_ini  ! # of eigenvalues for Arnoldi
    integer,    intent(in   ) :: ncv0_ini         ! ncv = 2*nev + ncv0
    real(8),    intent(in   ) :: ed_arno_tol_ini  ! tolerance for eigenvalue search

    integer,    intent(in   ) :: ed_gf_nev_ini    ! # of eigenvalues to use for Green function calculations
    real(8),    intent(in   ) :: ed_gf_decomp_ini ! probability cut for evect decomposition
    real(8),    intent(in   ) :: ed_gf_tol_ini    ! tolerance for Green function calculations

!-------------------------------------------------------------------------------

    integer :: i, iv, it, ierr, maxd_pr, nit_arn, iopt
    integer :: nup, ndn, nup0, ndn0, mode, hdim, nsect
    real(prec) :: emin, emax
    logical :: sym                                ! marker for symmetric blocks
    integer, allocatable :: sect(:,:)
    real(prec), allocatable :: boltz(:)

    call timer( 'init', t_full )

    call init_checks( Gf, zenergy, U_ini, Eimp, Epsk, Vk )
    allocate( U(nlm,nlm,nlm,nlm), Hop(Nt,Nt,2), Ed(nlm,2), Es(Nt-nlm,2) )

    call init_values( U_ini, Eimp, Epsk, Vk, ed_arno_nev_ini, ed_gf_nev_ini,   &
                      verb_ini, opt_ini, ncv0_ini, ed_arno_tol_ini, beta_ini  )

    call particle_bounds( Nd_min_ini, Nd_max_ini, Nc_max_ini, Nh_max_ini )
    allocate( Nst(nup_min:nup_max,ndn_min:ndn_max),                            &
              distr(0:nprocs-1), displ(0:nprocs-1) )

    call header( Epsk, Vk )

    call search_maxd( maxd_pr )

    nsect = size( Nst )
    allocate( sect(nsect,2), basis(maxd_sp,Nt,2), inv(2,-1:2**Nt), boltz(nev_gf) )

    call sort_sectors( sect )
                                            
    call search_hdim( sect, lunn, hdim )

    allocate( Hst(hdim), Hpos(0:maxd_pr), Nget(0:nprocs), Hcon(hdim),          &
              Hdiag(maxd_pr), connect(maxd), eval(nev_gf,2), inf(nev_gf,3),    & 
              evec(nev_gf,maxd_pr), stat=ierr )
    if ( ierr /= 0 ) call stop_program("Allocation problem, 1.$")

    eval = huge(one)  

    if( opt_ini == 2 )then
      tol_arn  = 0.007
      nev_arno = nlm
    end if

    do iopt = 1, 2
      forall ( i = 1:nev_gf ) inf(i,3) = i
!>> Loop over sectors
      do i = 1, nsect
        nup = sect(i,1)
        ndn = sect(i,2)

        call check_sym( nup, ndn, sym )
        if ( sym .and. nup > ndn ) cycle
                                            
        call setup( nup, ndn, lunn )
        call arnoldi( nup, ndn, sym, emin, nit_arn )  

        call write_info_arn( i, it, nup, ndn, emin, nit_arn )
      end do ! i
!<< 
      if( opt_ini == 2 .and. iopt == 1 )then
        call distr_by_sector( nsect, sect )
        tol_arn = real(ed_arno_tol_ini,prec)
        nev_arno = ed_arno_nev_ini
        emax = eval(nev_gf,1) + maxval( eval(:,2) )
        eval = emax
      else
        exit
      end if

      if( verb > 30 ) write(outf,*) ' emax ', emax

    end do  ! iopt

!   Boltzmann factor calculation
    call boltzmann( eval(:,1), boltz )
    call write_eval( boltz )

!   Green function calculation
    Gf   = 0.d0
    sisj = 0.d0

    allocate( vecinit(maxd), vecold(maxd), inv0(2,-1:2**Nt), boson(Iwmax),     &
              den(nlm,Nsp), stat=ierr )
    if ( ierr /= 0 ) call stop_program("Allocation problem, 2.$")

    den = zero
    tol_gf = real( ed_gf_tol_ini, prec )

    boson = zenergy
    if ( all( real(zenergy) < tny ) ) then
      boson = zenergy - cmplx( 0.d0, pi/beta )
      boson(1) = cmplx( 0.d0, 1.d-3 )
    end if

!>> Loop over eigenvalues
    do iv = 1, nev_gf
      nup0 = inf(iv,1)
      ndn0 = inf(iv,2)
    
      call check_sym( nup0, ndn0, sym )
      if ( sym .and. nup0 < ndn0 ) sym  = .false.

      if ( sym ) nup0 = inf(iv,2)
      if ( sym ) ndn0 = inf(iv,1)

      call combine_evect( iv, nup0, ndn0 )
      call write_info_gf( iv, boltz(iv), ed_gf_decomp_ini, sym )

!     Calculation of spin-spin correlation function
      if ( lsisj ) call sisj_calc( iv, nup0, ndn0, boltz(iv), lunn, sisj )

!     Creation of a particle
      mode = 1
      call lanczos( mode, iv, nup0, ndn0, sym, lunn, boltz(iv), zenergy, Gf, den )

!     Destruction of a particle
      mode = -1
      call lanczos( mode, iv, nup0, ndn0, sym, lunn, boltz(iv), zenergy, Gf, den )
    end do ! iv 
!<<
    deallocate( Nst, Hst, Hdiag, Hcon, Hpos, distr, basis, U, Hop, Ed, Es,     &
                Nget, boson, den, connect, boltz, eval, evec, inv, displ,      &
                vecold, vecinit, inv0, sect, inf )

    call timer( 'stop', t_full )
    call time_stat

  end subroutine ed_arnoldi
!==============================================================================
  subroutine lanczos( mode, iv, nup0, ndn0, sym, lunn, boltz, zenergy, Gf, den )
    implicit none

    integer,    intent(in   ) :: mode, iv, nup0, ndn0
    logical,    intent(in   ) :: sym, lunn
    real(prec), intent(in   ) :: boltz
    complex(8), intent(in   ) :: zenergy(:)       ! energy mesh
    complex(8), intent(inout) :: Gf(:,:,:)        ! Green function
    real(prec), intent(inout) :: den(:,:)         ! density

    integer :: is, im, nup, ndn, pos
    real(prec) :: norm, coef

    do is = 1, Nsp
      if ( is == 2 .or. sym ) then
        nup = nup0 
        ndn = ndn0 + mode
        pos = Nt
      else
        nup = nup0 + mode
        ndn = ndn0
        pos = 0
      end if

      if ( nup < nup_min .or. ndn < ndn_min .or. nup > nup_max .or. ndn > ndn_max ) cycle

      call setup( nup, ndn, lunn )
      inv = inv0

      do im = 1, nlm
        call lanc( mode, pos+im, norm )    

        coef = norm * norm * boltz
        if ( mode == -1 ) den(im,is) = den(im,is) + coef

        call zos( mode, iv, im, nup, ndn, coef, zenergy, Gf(:,im,is) )                              
      end do ! im 
    end do ! is

    if ( verb > 10 .and. mode == -1 ) write(outf,"(2x,57('-'))")
    if ( verb >  9 .and. mode == -1 .and. iv /= nev_gf ) write(outf,*)   

    if ( verb > 0 .and. mode == -1 .and. iv == nev_gf ) then
      write(outf,"(/,' Density for spin 1 =',f9.4,'  :',7(f8.4))") sum(den(:,  1)), (den(im,1  ), im=1,nlm)
      write(outf,"(  ' Density for spin 2 =',f9.4,'  :',7(f8.4))") sum(den(:,Nsp)), (den(im,Nsp), im=1,nlm)
      write(outf,"(' Total density =',f14.4,'  :',7(f8.4))") sum(den(:,1)+den(:,Nsp)), (den(im,1)+den(im,Nsp),im=1,nlm)
    end if
   
  end subroutine lanczos
!=========================================================================
  subroutine sisj_calc( iv, nup0, ndn0, boltz, lunn, sisj )
    implicit none

    integer,    intent(in   ) :: iv, nup0, ndn0
    real(prec), intent(in   ) :: boltz
    logical,    intent(in   ) :: lunn
    complex(8), intent(inout) :: sisj(:,:)

    integer :: im, nup, ndn, mode, compon
    real(prec) :: norm, coef
    complex(8) :: mesh(Iwmax)

    mesh = boson
    if ( verb > 10 ) write(outf,"(2x,a)") '>> sisj calc'

!   < S_z S_z > 
    call setup( nup0, ndn0, lunn )
    do im = 1, nlm
      do mode = -1, 1, 2
        call prepare_vect( im, 1, norm )
        coef = norm * norm * boltz / two / two
        call zos( mode, iv, -im, nup0, ndn0, coef, mesh, sisj(:,im) )
      end do ! mode
    end do ! im

!   ( S^+S^- + S^-S^+ ) / 2
    do compon = 2, 3
      if ( compon == 2 ) then
        nup = nup0 + 1
        ndn = ndn0 - 1
      else
        nup = nup0 - 1
        ndn = ndn0 + 1
      end if

      if ( nup < nup_min .or. ndn < ndn_min .or. nup > nup_max .or. ndn > ndn_max ) cycle

      call setup( nup, ndn, lunn )
      inv = inv0

      do im = 1, nlm
        do mode = -1, 1, 2
          call prepare_vect( im, compon, norm )
          coef = norm * norm * boltz / two
          call zos( mode, iv, -im, nup, ndn, coef, mesh, sisj(:,im) )
        end do ! mode
      end do ! im
    end do ! compon

    if ( verb > 10 ) write(outf,"(2x,a)") '<< sisj calc'

  end subroutine sisj_calc
!=========================================================================
  subroutine prepare_vect( m, compon, norm )
    implicit none

    integer,    intent(in   ) :: m, compon
    real(prec), intent(  out) :: norm

    integer :: ist, pos, ierr, start
    integer(1) :: s1, s2, v1(2*Nt), v2(2*Nt), bvect(2*Nt)
    real(prec) :: rtmp  

    norm  = zero
    start = displ(myid)
    if ( compon /= 1 ) vecold(start+1:start+Nst_pr) = zero

    do ist = 1, Nst_pr
      call get_vect( ist, bvect )
      select case( compon )
        case(1); rtmp = real( bvect(m)-bvect(m+Nt), kind=prec )
                 rtmp = rtmp * vecinit(start+ist)
        case(2); if ( bvect(m+Nt) == 1 ) cycle
                 if ( bvect(m   ) == 0 ) cycle
                 call a( m, bvect, v1, s1 )
                 call adag( m+Nt, v1, v2, s2 )
                 call find_vect( v2, pos )
                 if ( pos == 0 ) cycle
                 rtmp = vecinit(pos) * s1 * s2 
        case(3); if ( bvect(m+Nt) == 0 ) cycle
                 if ( bvect(m   ) == 1 ) cycle          
                 call a( m+Nt, bvect, v1, s1 )
                 call adag( m, v1, v2, s2 )
                 call find_vect( v2, pos )
                 if ( pos == 0 ) cycle
                 rtmp = vecinit(pos) * s1 * s2            
      end select
      vecold(start+ist) = rtmp
      norm = norm + rtmp*rtmp  
    end do

    if ( nprocs_eff > 1 ) call exch( norm )
    norm = sqrt( norm )    

    if ( norm > tny ) vecold(start+1:start+Nst_pr) = vecold(start+1:start+Nst_pr) / norm 

#ifdef MPI
    if ( nprocs_eff > 1 )                                                      &
      call mpi_allgatherv( vecold(start+1), Nst_pr, mpi_knd, vecold, distr,    &
                           displ, mpi_knd, comm, ierr )
#endif

    end subroutine prepare_vect
!==============================================================================
  subroutine init_checks( Gf, zenergy, U_ini, Eimp, Epsk, Vk )
    implicit none

    complex(8), intent(in   ) :: Gf(:,:,:), zenergy(:)
    real(8),    intent(in   ) :: U_ini(:,:,:,:), Eimp(:,:), Epsk(:,:), Vk(:,:,:)

    integer :: nb   ! # of bath

    nb    = size( Vk, dim = 1 )
    nlm   = size( Vk, dim = 2 )
    Nsp   = size( Vk, dim = 3 )
    Iwmax = size( zenergy )

    Nt = nlm + nb

    if ( size( Gf,    dim = 1 ) /= Iwmax ) call stop_program("Dimensions don't coincide, 1$")
    if ( size( Gf,    dim = 2 ) /= nlm   ) call stop_program("Dimensions don't coincide, 2$")
    if ( size( Gf,    dim = 3 ) /= Nsp   ) call stop_program("Dimensions don't coincide, 3$")
    if ( size( Eimp,  dim = 1 ) /= nlm   ) call stop_program("Dimensions don't coincide, 4$")
    if ( size( Eimp,  dim = 2 ) /= Nsp   ) call stop_program("Dimensions don't coincide, 5$")
    if ( size( Epsk,  dim = 1 ) /= nb    ) call stop_program("Dimensions don't coincide, 6$")
    if ( size( Epsk,  dim = 2 ) /= Nsp   ) call stop_program("Dimensions don't coincide, 7$")
    if ( size( U_ini, dim = 1 ) /= nlm   ) call stop_program("Dimensions don't coincide, 8$")
    if ( size( U_ini, dim = 2 ) /= nlm   ) call stop_program("Dimensions don't coincide, 9$")
    if ( size( U_ini, dim = 3 ) /= nlm   ) call stop_program("Dimensions don't coincide, 10$")
    if ( size( U_ini, dim = 4 ) /= nlm   ) call stop_program("Dimensions don't coincide, 11$")

  end subroutine init_checks
!===============================================================================
  subroutine init_values( U_ini, Eimp, Epsk, Vk, ed_arno_nev_ini,              &
                          ed_gf_nev_ini, verb_ini, opt_ini, ncv0_ini,          &
                          tol_ini, beta_ini )
    implicit none

    real(8),    intent(in   ) :: U_ini(:,:,:,:), Eimp(:,:), Epsk(:,:),         &
                                 Vk(:,:,:), tol_ini, beta_ini
    integer,    intent(in   ) :: ed_arno_nev_ini, ed_gf_nev_ini, verb_ini,     &
                                 ncv0_ini, opt_ini

    integer :: i, j, k
    logical :: marks(Nt-nlm)

    U       = real( U_ini,       kind = prec ) / two  ! instead of setup()
    Ed(:,1) = real( Eimp(:,1),   kind = prec )
    Ed(:,2) = real( Eimp(:,Nsp), kind = prec )

    Hop = zero
 
    marks = .true. 
    do i = 1, Nt-nlm
      k = minloc( Epsk(:,1), dim=1, mask=marks )
      Es(i,1) = real( Epsk(k,1), kind = prec )
      marks(k) = .false.
      do j = 1, nlm
        Hop(j,i+nlm,1) = real( abs(Vk(k,j,1)),   kind = prec )
        Hop(i+nlm,j,1) = real( abs(Vk(k,j,1)),   kind = prec )
      end do ! j
    end do ! i

    marks = .true. 
    do i = 1, Nt-nlm
      k = minloc( Epsk(:,Nsp), dim=1, mask=marks )
      Es(i,2) = real( Epsk(k,Nsp), kind = prec )
      marks(k) = .false.
      do j = 1, nlm
        Hop(j,i+nlm,2) = real( abs(Vk(k,j,Nsp)), kind = prec )
        Hop(i+nlm,j,2) = real( abs(Vk(k,j,Nsp)), kind = prec )
      end do ! j
    end do ! i

    nev_arno = abs( ed_arno_nev_ini )
    nev_gf   = abs( ed_gf_nev_ini )

    opt  = opt_ini
    verb = verb_ini

    ncv0 = ncv0_ini

    beta = real( beta_ini, kind = prec )

    tol_arn   = tol_ini 

    t_lnc   = 0.d0
    t_zos   = 0.d0
    t_lab   = 0.d0
    t_stp   = 0.d0
    t_arn   = 0.d0
    t_stp0  = 0.d0
    t_arn0  = 0.d0
    t_amvp  = 0.d0
    t_lmvp  = 0.d0
    t_saupd = 0.d0
    t_seupd = 0.d0
    t_acomm = 0.d0
    t_lcomm = 0.d0
    t_asort = 0.d0

    myid = proc_id
    nprocs = numprocs

#ifdef MPI
    comm = mpi_comm_world

    select case (prec)
      case(4); mpi_knd = mpi_real
      case(8); mpi_knd = mpi_double_precision
    end select
#endif

    if ( myid /= 0 ) verb = 0

  end subroutine init_values
!=========================================================================
  subroutine particle_bounds( Nd_min_ini, Nd_max_ini, Nc_max_ini, Nh_max_ini )
    implicit none

    integer, intent(in   ) :: Nd_min_ini, Nd_max_ini, Nc_max_ini, Nh_max_ini
    integer :: is

    if ( Nc_max_ini < 0          ) call stop_program('Nc_max < 0$')
    if ( Nh_max_ini < 0          ) call stop_program('Nh_max < 0$')
    if ( Nd_min_ini < 0          ) call stop_program('Nd_min < 0$')
    if ( Nd_max_ini < 0          ) call stop_program('Nd_max < 0$')
    if ( Nd_min_ini > 2*nlm      ) call stop_program('Nd_min > 2*nlm$')
    if ( Nd_max_ini > 2*nlm      ) call stop_program('Nd_max > 2*nlm$')
    if ( Nd_min_ini > Nd_max_ini ) call stop_program('Nd_min > Nd_max$')

    do is = 1, 2
      Nv(is) = count( Es(:,is) <= zero )
      Nc(is) = count( Es(:,is)  > zero )
      Nc_max(is) = min( Nc_max_ini, Nc(is) )
      Nh_max(is) = min( Nh_max_ini, Nv(is) )   
    end do

    Nd_min(1:2) = max( Nd_min_ini-nlm, 0 )
    Nd_max(1:2) = min( Nd_max_ini, nlm )

    Nv(3) = Nv(1) + Nv(2)
    Nc(3) = Nc(1) + Nc(2)
    Nd_min(3) = Nd_min_ini
    Nd_max(3) = Nd_max_ini
    Nc_max(3) = min( Nc_max_ini, Nc(3) )
    Nh_max(3) = min( Nh_max_ini, Nv(3) )

    nup_min = Nd_min(1) + Nv(1) - Nh_max(1)
    ndn_min = Nd_min(2) + Nv(2) - Nh_max(2)
    nup_max = Nd_max(1) + Nv(1) + Nc_max(1)
    ndn_max = Nd_max(2) + Nv(2) + Nc_max(2)

  end subroutine particle_bounds
!=========================================================================
  subroutine header( Epsk, Vk )
    implicit none

    real(8), intent(in   ) :: Epsk(:,:), Vk(:,:,:)
    integer :: i, j, k, l, p, is

    if ( verb > 0 ) then
      write(outf,"(80('-'),/,20x,'Exact diagonalization')")
      write(outf,"(3x,a,a,/)")  'Version of ED-solver:  ', vers
      write(outf,"(2x,a,i5)") ' Nspin  = ', Nsp
      write(outf,"(2x,a,i5)") ' # of orbitals per spin    = ', Nt
      write(outf,"(2x,a,i5)") ' # of correlated orbitals  = ', nlm
      write(outf,"(2x,a,i5)") ' # of lowest ev to find for GF = ', nev_gf
      write(outf,"(2x,a,i5)") ' # of lowest ev to find in Arnoldi = ', nev_arno
      write(outf,"(2x,a,i5)") ' # of energy points in Green function = ', Iwmax
      write(outf,"(2x,a,i3)") ' # of procs = ', nprocs
      write(outf,"(2x,a,f9.6)") ' Tolerance for Green function = ', tol_gf
      write(outf,"(2x,a,f9.6)") ' Tolerance for Arnoldi  = ', tol_arn
      write(outf,"(/,3x,a)") 'Particle bounds'
      write(outf,"(3x,a,i3,2x,':',i3)") 'Nd = ', Nd_min(3), Nd_max(3)
      write(outf,"(3x,a,i3,2x,':',i3)") 'Nh = ', 0, Nh_max(3)
      write(outf,"(3x,a,i3,2x,':',i3)") 'Nc = ', 0, Nc_max(3)
      write(outf,"(3x,a,i3,2x,':',i3)") 'nup =', nup_min, nup_max
      write(outf,"(3x,a,i3,2x,':',i3)") 'ndn =', ndn_min, ndn_max
      write(outf,*)
    end if  ! verb > 0

    if ( verb > 15 ) then 
      write(outf,"(2x,a,i5)")   ' prec =', prec
      write(outf,"(2x,a,i5)")   ' opt  =', opt
      write(outf,"(2x,a,i5)")   ' ncv0 =', ncv0
      write(outf,"(2x,a,i5)")   ' verb =', verb
    end if  ! verb > 15

    if ( verb > 25 ) then 
      write(outf,"(/,1x,a)") 'Epsk, Vk:'
      do is = 1, Nsp
        if ( is == 2 ) write(outf,*)
        do k = 1, Nt-nlm
          write(outf,"(100(f8.4))") Epsk(k,is), ( Vk(k,j,is), j=1,nlm ) 
        end do
      end do
    end if  ! verb > 25

    if ( verb > 21 ) then
      write(outf,"(/,1x,a)") 'U-matrix:'
      p = 0
      do i = 1, nlm
        do j = 1, nlm
          do k = 1, nlm
            do l = 1, nlm
              if ( abs(U(i,j,k,l)) < tny ) cycle
              p = p + 1
              write(outf,"(i4,a,i4,i4,i4,i4,f11.4)") p, ')', i, j, k, l, two*U(i,j,k,l)
            enddo   
          enddo
        enddo   
      enddo
    end if  ! verb > 21

  end subroutine header
!=========================================================================
  subroutine search_maxd( maxd_pr )
    implicit none

    integer, intent(  out) :: maxd_pr

    integer :: i1, i2, j1, j2, k1, k2, m1, m2, nup, ndn, maxd_dn, maxd_up

!   Calculation of maxd_sp
    maxd_sp = 0

    do nup = nup_min, nup_max
      do ndn = ndn_min, ndn_max
        maxd_up = 0
        do i1 = Nd_min(1), Nd_max(1)
          do j1 = Nv(1)-Nh_max(1), Nv(1)
            do k1 = 0, Nc_max(1)
              if ( i1+j1+k1 == nup ) then
                m1 = comb(nlm,i1) * comb(Nv(1),j1) * comb(Nc(1),k1)
                maxd_up = maxd_up + m1
              end if
            end do ! k1
          end do ! j1
        end do ! i1

        maxd_dn = 0
        do i2 = Nd_min(2), Nd_max(2)
          do j2 = Nv(2)-Nh_max(2), Nv(2)
            do k2 = 0, Nc_max(2)
              if ( i2+j2+k2 == ndn ) then
                m2 = comb(nlm,i2) * comb(Nv(2),j2) * comb(Nc(2),k2)
                maxd_dn = maxd_dn + m2
              end if
            end do ! k2
          end do ! j2
        end do ! i2

        maxd_sp = max( maxd_sp, maxd_up, maxd_dn )
      end do ! nup
    end do ! ndn

!   Calculation of Nst(:,:)
    Nst = 0

    do i1 = Nd_min(1), Nd_max(1)
      do i2 = Nd_min(2), Nd_max(2)
        do j1 = Nv(1)-Nh_max(1), Nv(1)
          do j2 = Nv(2)-Nh_max(2), Nv(2)
            do k1 = 0, Nc_max(1)
              do k2 = 0, Nc_max(2)
                if ( i1+i2 < Nd_min(3) .or. i1+i2 > Nd_max(3) ) cycle
                if ( Nv(3)-j1-j2 > Nh_max(3) ) cycle 
                if ( k1+k2 > Nc_max(3) ) cycle
   
                m1 = comb(nlm,i1) * comb(Nv(1),j1) * comb(Nc(1),k1)               
                m2 = comb(nlm,i2) * comb(Nv(2),j2) * comb(Nc(2),k2)

                nup = i1 + j1 + k1
                ndn = i2 + j2 + k2  
                Nst(nup,ndn) =  Nst(nup,ndn) + m1*m2
              end do ! k2
            end do ! k1
          end do ! j2
        end do ! j1
      end do ! i2
    end do ! i1

    maxd = maxval( Nst ) 
    maxd_pr = maxd/nprocs                                   
    if ( myid < mod(maxd,nprocs) ) maxd_pr = maxd_pr + 1

  end subroutine search_maxd
!=========================================================================
  subroutine sort_sectors( sect )
    implicit none

    integer, intent(  out) :: sect(:,:)

    integer :: i
    logical :: marks( nup_min:nup_max, ndn_min:ndn_max )

    marks = .true.
    do i = 1, size( Nst )
      sect(i,:) = minloc( Nst, marks )
      sect(i,1) = sect(i,1) + nup_min - 1
      sect(i,2) = sect(i,2) + ndn_min - 1
      marks( sect(i,1), sect(i,2) ) = .false.
    end do

  end subroutine sort_sectors
!=========================================================================
  subroutine search_hdim( sect, lunn, hdim )
    implicit none

    integer, intent(in   ) :: sect(:,:)
    logical, intent(in   ) :: lunn 
    integer, intent(  out) :: hdim

    integer :: i, k, d, nup, ndn, nsect
                                                call timer( 'init', t_hd )
    nsect = size( Nst )
    d = min( 7, nsect )

    hdim = 1
    do i = 1, d
      nup = sect(nsect+1-i,1)
      ndn = sect(nsect+1-i,2)
      call setup_trial( nup, ndn, lunn, k )
      hdim = max( hdim, k )
    end do
    if ( verb > 10 ) write(outf,"(/,3x,a,i11,a,4x,a,i3)") &
               'Hamilt dim =', hdim, ',', 'on proc =', myid
                                                call timer( 'stop', t_hd )
  end subroutine search_hdim
!=========================================================================
  integer function comb(n,k)
    implicit none
    integer :: n, k, i, ans
      
    ans = 1
    do i = n, 1, -1
      ans = ans * i
      if ( i <=   k   ) ans = ans/i
      if ( i <= (n-k) ) ans = ans/i
    end do 
    comb = ans

  end function comb
!=========================================================================
  subroutine check_sym( nup, ndn, sym )
    implicit none

    integer, intent(in   ) :: nup, ndn
    logical, intent(  out) :: sym

    if ( Nsp == 1 .and. nup /= ndn .and. nup_min <= ndn .and. ndn <= nup_max &
                                   .and. ndn_min <= nup .and. nup <= ndn_max ) then
      sym = .true.
    else
      sym = .false.       
    end if

  end subroutine check_sym
!=========================================================================
  subroutine setup_trial( nup, ndn, lunn, hd )
    implicit none

    integer, intent(in   ) :: nup, ndn
    logical, intent(in   ) :: lunn 
    integer, intent(  out) :: hd

    integer(1) :: sgn, v1(2*Nt), v2(2*Nt), v3(2*Nt), v4(2*Nt), bvect(2*Nt)
    integer    :: j, m, p1, p2, p3, p4, ig, ist, pos, Hc(700)
    logical    :: dubl
    integer(8) :: size_check

    call buildbasis(nup,ndn)

    hd = 0
    ig = displ(myid)

    do ist = 1, Nst_pr
      m = 0
      ig = ig + 1
      call get_vect( ist, bvect )

!     up_up
      do p1 = 1, nlm
        if ( bvect(p1) == 0 ) cycle
        call a( p1, bvect, v1, sgn  )
        do p2 = 1, nlm
          if ( v1(p2) == 0 ) cycle
          call a( p2, v1, v2, sgn )
          do p3 = 1, nlm
            if ( v2(p3) == 1 ) cycle
            call adag( p3, v2, v3, sgn )
            do p4 = 1, nlm
              if ( v3(p4) == 1 ) cycle
              if ( abs(U(p4,p3,p1,p2)) < tny ) cycle

              if ( lunn .and. ( p2 /= p3 .or. p1 /= p4 ) .and. &
                              ( p2 /= p4 .or. p1 /= p3 ) ) cycle

              call adag( p4, v3, v4, sgn )
              call find_vect( v4, pos )
              if ( pos == ig ) cycle
              if ( pos == 0  ) cycle
              dubl = .false.
              do j = 1, m
                if ( pos == Hc(j) ) then
                  dubl = .true.
                  exit
                end if
              end do ! j
              if ( dubl ) cycle
              m = m + 1
              Hc(m) = pos
            end do ! p4
          end do ! p3
        end do ! p2
      end do ! p1

!     up_dn
      do p1 = 1, nlm
        if ( bvect(p1) == 0 ) cycle
        call a( p1, bvect, v1, sgn  )
        do p2 = 1, nlm
          if ( v1(Nt+p2) == 0 ) cycle
          call a( Nt+p2, v1, v2, sgn )
          do p3 = 1, nlm
            if ( v2( Nt+p3) == 1 ) cycle
            call adag( Nt+p3, v2, v3, sgn )
            do p4 = 1, nlm
              if ( v3(p4) == 1 ) cycle
              if ( abs(U(p4,p3,p1,p2)) < tny ) cycle

              if ( lunn .and. ( p2 /= p3 .or. p1 /= p4 ) ) cycle

              call adag( p4, v3, v4, sgn )
              call find_vect( v4, pos )
              if ( pos == ig ) cycle
              if ( pos == 0  ) cycle
              dubl = .false.
              do j = 1, m
                if ( pos == Hc(j) ) then
                  dubl = .true.
                  exit
                end if
              end do ! j
              if ( dubl ) cycle
              m = m + 1
              Hc(m) = pos
            end do ! p4
          end do ! p3
        end do ! p2
      end do ! p

!     dn_up
      do p1 = 1, nlm
        if ( bvect(Nt+p1) == 0 ) cycle
        call a( Nt+p1, bvect, v1, sgn  )
        do p2 = 1, nlm
          if ( v1(p2) == 0 ) cycle
          call a( p2, v1, v2, sgn )
          do p3 = 1, nlm
            if ( v2(p3) == 1 ) cycle
            call adag( p3, v2, v3, sgn )
            do p4 = 1, nlm
              if ( v3(Nt+p4) == 1 ) cycle
              if ( abs(U(p4,p3,p1,p2)) < tny ) cycle

              if ( lunn .and. ( p2 /= p3 .or. p1 /= p4 ) ) cycle

              call adag( Nt+p4, v3, v4, sgn )
              call find_vect( v4, pos )
              if ( pos == ig ) cycle
              if ( pos == 0  ) cycle
              dubl = .false.
              do j = 1, m
                if ( pos == Hc(j) ) then
                  dubl = .true.
                  exit
                end if
              end do ! j
              if ( dubl ) cycle
              m = m + 1
              Hc(m) = pos
             end do ! p4
          end do ! p3
        end do ! p2
      end do ! p1

!     dn_dn
      do p1 = 1, nlm
        if ( bvect(Nt+p1) == 0 ) cycle
        call a( Nt+p1, bvect, v1, sgn  )
        do p2 = 1, nlm
          if ( v1(Nt+p2) == 0 ) cycle
          call a( Nt+p2, v1, v2, sgn )
          do p3 = 1, nlm
            if ( v2(Nt+p3) == 1 ) cycle
            call adag( Nt+p3, v2, v3, sgn )
            do p4 = 1, nlm
              if ( v3(Nt+p4) == 1 ) cycle
              if ( abs(U(p4,p3,p1,p2)) < tny ) cycle

              if ( lunn .and. ( p2 /= p3 .or. p1 /= p4 ) .and. &
                              ( p2 /= p4 .or. p1 /= p3 ) ) cycle

              call adag( Nt+p4, v3, v4, sgn )
              call find_vect( v4, pos )
              if ( pos == ig ) cycle
              if ( pos == 0  ) cycle
              dubl = .false.
              do j = 1, m
                if ( pos == Hc(j) ) then
                  dubl = .true.
                  exit
                end if
              end do ! j
              if ( dubl ) cycle
              m = m + 1
              Hc(m) = pos
            end do ! p4
          end do ! p3
        end do ! p2
      end do ! p1

      hd = hd + m

!     Hoppings
      do  p1 = 1, Nt
        if ( bvect(p1) == 0 ) then
          do  p2 = 1, Nt
            if ( bvect(p2) == 0 ) cycle 
            if ( Hop(p2,p1,1) < tny ) cycle
            call a( p2, bvect, v1, sgn )
            call adag( p1, v1, v2, sgn )
            call find_vect( v2, pos )
            if ( pos /= 0 ) hd = hd + 1
          end do ! p2
        end if

        if ( bvect(p1+Nt) == 0 ) then
          do  p2 = 1, Nt
            if ( bvect(p2+Nt) == 0 ) cycle
            if ( Hop(p2,p1,2) < tny ) cycle 
            call a( p2+Nt, bvect, v1, sgn )
            call adag( p1+Nt, v1, v2, sgn )
            call find_vect( v2, pos )
            if ( pos /= 0 ) hd = hd + 1
          end do ! p2
        end if
      end do ! p1

    end do ! ist
    
    size_check = 2
    size_check = size_check**31

    if ( hd > size_check ) call stop_program('Hamiltonian dimension > 2^31.$')

  end subroutine setup_trial
!=========================================================================
  subroutine setup( nup, ndn, lunn )
    implicit none

    integer, intent(in   ) :: nup, ndn
    logical, intent(in   ) :: lunn 

    integer(1) :: s1, s2, s3, s4
    integer(1) :: v1(2*Nt), v2(2*Nt), v3(2*Nt), v4(2*Nt), bvect(2*Nt)
    integer    :: j, k, p1, p2, p3, p4, ig, ist, pos, first
    real(prec) :: rtmp
    logical    :: dubl
                                         call timer( 'start', t_stp )
    call buildbasis(nup,ndn)

    k = 0
    Hpos(0) = 0
    ig = displ(myid)

    do ist = 1, Nst_pr
      ig = ig + 1
      first = k + 1

      call get_vect( ist, bvect )

      rtmp = zero
      do j = 1, nlm
        rtmp = rtmp + bvect(j)*Ed(j,1) + bvect(Nt+j)*Ed(j,2)
      end do

      do j = nlm+1, Nt
        rtmp = rtmp + bvect(j)*Es(j-nlm,1) + bvect(Nt+j)*Es(j-nlm,2)
      end do

      Hdiag(ist) = rtmp

!     up_up
      do p1 = 1, nlm
        if ( bvect(p1) == 0 ) cycle
        call a( p1, bvect, v1, s1 )
        do p2 = 1, nlm
          if ( v1(p2) == 0 ) cycle
          call a( p2, v1, v2, s2 )
          s2 = s2 * s1
          do p3 = 1, nlm
            if ( v2(p3) == 1 ) cycle
            call adag( p3, v2, v3, s3 )
            s3 = s3 * s2
            do p4 = 1, nlm
              if ( v3(p4) == 1 ) cycle
              if ( abs(U(p4,p3,p1,p2)) < tny ) cycle

              if ( lunn .and. ( p2 /= p3 .or. p1 /= p4 ) .and. &
                              ( p2 /= p4 .or. p1 /= p3 ) ) cycle

              call adag( p4, v3, v4, s4 )
              call find_vect( v4, pos )
              if ( pos == 0 ) cycle
              rtmp = U(p4,p3,p1,p2) * s3 * s4

              if ( pos == ig ) then
                Hdiag(ist) = Hdiag(ist) + rtmp
              else
                dubl = .false.
                do j = first, k
                  if ( pos == Hcon(j) ) then
                    dubl = .true.
                    Hst(j) = Hst(j) + rtmp
                    exit
                  end if
                end do ! j
                if ( dubl ) cycle
                k = k + 1
                Hst(k) = rtmp
                Hcon(k) = pos
              end if

            end do ! p4
          end do ! p3
        end do ! p2
      end do ! p1

!     up_dn
      do p1 = 1, nlm
        if ( bvect(p1) == 0 ) cycle
        call a( p1, bvect, v1, s1  )
        do p2 = 1, nlm
          if ( v1(Nt+p2) == 0 ) cycle
          call a( Nt+p2, v1, v2, s2 )
          s2 = s2 * s1
          do p3 = 1, nlm
            if ( v2(Nt+p3) == 1 ) cycle
            call adag( Nt+p3, v2, v3, s3 )
            s3 = s3 * s2
            do p4 = 1, nlm
              if ( v3(p4) == 1 ) cycle
              if ( abs(U(p4,p3,p1,p2)) < tny ) cycle

              if ( lunn .and. ( p2 /= p3 .or. p1 /= p4 ) ) cycle

              call adag( p4, v3, v4, s4 )
              call find_vect( v4, pos )
              if ( pos == 0 ) cycle
              rtmp = U(p4,p3,p1,p2) * s3 * s4

              if ( pos == ig ) then
                Hdiag(ist) = Hdiag(ist) + rtmp
              else
                dubl = .false.
                do j = first, k
                  if ( pos == Hcon(j) ) then
                    dubl = .true.
                    Hst(j) = Hst(j) + rtmp
                    exit
                  end if
                end do ! j
                if ( dubl ) cycle
                k = k + 1
                Hst(k) = rtmp
                Hcon(k) = pos
              end if

            end do ! p4
          end do ! p3
        end do ! p2
      end do ! p1

!     dn_up
      do p1 = 1, nlm
        if ( bvect(Nt+p1) == 0 ) cycle
        call a( Nt+p1, bvect, v1, s1  )
        do p2 = 1, nlm
          if ( v1(p2) == 0 ) cycle
          call a( p2, v1, v2, s2 )
          s2 = s2 * s1
          do p3 = 1, nlm
            if ( v2(p3) == 1 ) cycle
            call adag( p3, v2, v3, s3 )
            s3 = s3 * s2
            do p4 = 1, nlm
              if ( v3(Nt+p4) == 1 ) cycle
              if ( abs(U(p4,p3,p1,p2)) < tny ) cycle

              if ( lunn .and. ( p2 /= p3 .or. p1 /= p4 ) ) cycle

              call adag( Nt+p4, v3, v4, s4 )
              call find_vect( v4, pos )
              if ( pos == 0 ) cycle
              rtmp = U(p4,p3,p1,p2) * s3 * s4

              if ( pos == ig ) then
                Hdiag(ist) = Hdiag(ist) + rtmp
              else
                dubl = .false.
                do j = first, k
                  if ( pos == Hcon(j) ) then
                    dubl = .true.
                    Hst(j) = Hst(j) + rtmp
                    exit
                  end if
                end do ! j
                if ( dubl ) cycle
                k = k + 1
                Hst(k) = rtmp
                Hcon(k) = pos
              end if

            end do ! p4
          end do ! p3
        end do ! p2
      end do ! p1

!     dn_dn
      do p1 = 1, nlm
        if ( bvect(Nt+p1) == 0 ) cycle
        call a( Nt+p1, bvect, v1, s1  )
        do p2 = 1, nlm
          if ( v1(Nt+p2) == 0 ) cycle
          call a( Nt+p2, v1, v2, s2 )
          s2 = s2 * s1
          do p3 = 1, nlm
            if ( v2(Nt+p3) == 1 ) cycle
            call adag( Nt+p3, v2, v3, s3 )
            s3 = s3 * s2
            do p4 = 1, nlm
              if ( v3(Nt+p4) == 1 ) cycle
              if ( abs(U(p4,p3,p1,p2)) < tny ) cycle

              if ( lunn .and. ( p2 /= p3 .or. p1 /= p4 ) .and. &
                              ( p2 /= p4 .or. p1 /= p3 ) ) cycle

              call adag( Nt+p4, v3, v4, s4 )
              call find_vect( v4, pos )
              if ( pos == 0 ) cycle
              rtmp = U(p4,p3,p1,p2) * s3 * s4

              if ( pos == ig ) then
                Hdiag(ist) = Hdiag(ist) + rtmp
              else
                dubl = .false.
                do j = first, k
                  if ( pos == Hcon(j) ) then
                    dubl = .true.
                    Hst(j) = Hst(j) + rtmp
                    exit
                  end if
                end do ! j
                if ( dubl ) cycle
                k = k + 1
                Hst(k) = rtmp
                Hcon(k) = pos
              end if

            end do ! p4
          end do ! p3
        end do ! p2
      end do ! p1

!     Hoppings

      do p1 = 1, Nt
        if ( bvect(p1) == 0 ) then
          do  p2 = 1, Nt
            if ( bvect(p2) == 0 ) cycle
            if ( Hop(p2,p1,1) < tny ) cycle
            call a( p2, bvect, v1, s2 )
            call adag( p1, v1, v2, s1 )
            call find_vect( v2, pos )
            if ( pos == 0 ) cycle
            k = k + 1
            Hst(k) = Hop(p2,p1,1) * s1 * s2
            Hcon(k) = pos
          end do ! p2
        end if

        if ( bvect(p1+Nt) == 0 ) then
          do  p2 = 1, Nt
            if ( bvect(p2+Nt) == 0 ) cycle
            if ( Hop(p2,p1,2) < tny ) cycle
            call a( p2+Nt, bvect, v1, s2 )
            call adag( p1+Nt, v1, v2, s1 )
            call find_vect( v2, pos )
            if ( pos == 0 ) cycle
            k = k + 1
            Hst(k) = Hop(p2,p1,2) * s1 * s2
            Hcon(k) = pos
          end do ! p2
        end if
      end do ! p1

      Hpos(ist) = k
    end do ! ist

    if ( comtype == 2 ) call analyze_hamilt( Nst(nup,ndn) )

!   if ( nprocs == 1 ) call check_hermit
                                         call timer( 'stop', t_stp )
  end subroutine setup
!=========================================================================
  subroutine check_hermit
    implicit none

    integer :: i, j, k, p
    real(prec) :: rtmp

    do i = 1, Nst_pr              
      do j = Hpos(i-1)+1, Hpos(i)
        rtmp = Hst(j)
        k = Hcon(j)
        do p = Hpos(k-1)+1, Hpos(k)
          if ( Hcon(p) == i .and. Hst(p) == rtmp ) exit
          if ( p == Hpos(k) ) call stop_program('Hamilt matrix isnt hermitian.$')
        end do ! p
      end do ! j
    end do ! i    

  end subroutine check_hermit
!=========================================================================
  subroutine analyze_hamilt( n )
    implicit none

    integer, intent(in   ) :: n

    integer :: i, k, p, ig
    logical(1) :: labels(n)

    labels = .false.

    do k = 1, Hpos(Nst_pr)
      labels(Hcon(k)) = .true.
    end do

    p = 0
    Nget = 0
    ig = displ(myid)

    do i = myid+1, nprocs-1
      Nget(i) = p
      do k = displ(i)+1, displ(i)+distr(i)
        if ( labels(k) ) then
          p = p + 1
          connect( p ) = k
        end if
      end do
    end do

    Nget( nprocs ) = p

    do i = 0, myid-1
      Nget(i) = p
      do k = displ(i)+1, displ(i)+distr(i)
        if ( labels(k) ) then
          p = p + 1
          connect( p ) = k
        end if
      end do
    end do

    Nget( myid ) = p

  end subroutine analyze_hamilt
!=========================================================================
  subroutine parameters( nup, ndn )
    implicit none

    integer, intent(in   ) :: nup, ndn
    integer :: i, k, n

!   Nst*nnz_Nst' < maxd*nnz_Nst''/nprocs
!   k*nprocs - 1 < maxd/nprocs
!   k*nprocs < maxd/nprocs
!   k < maxd/nprocs/nprocs
!   nprocs < sqrt(maxd/k) 

    n = Nst(nup,ndn)

    select case (maxd)
      case(       1:  100000); k = 4 
      case(  100001: 1000000); k = 8 
      case( 1000001:30000000); k = 30 
      case(30000001:        ); k = 100 
    end select

    i = maxd/nprocs/nprocs
    if ( k >= i ) k = max( 1, i-1 )

    if ( n < k*nprocs ) then
      nprocs_eff = 1
    else
      nprocs_eff = nprocs
    end if

    displ = 0
    distr = n/nprocs_eff
    k = mod( n, nprocs_eff )

    do i = 0, nprocs_eff-1
      if ( i < k ) distr(i) = distr(i) + 1
      if ( i > 0 ) displ(i) = displ(i-1) + distr(i-1)
    end do ! i

    Nst_pr = distr( myid )

  end subroutine parameters
!=========================================================================
  subroutine buildbasis( nup, ndn )
    implicit none

    integer, intent(in   )  :: nup, ndn

    integer(1) :: bparts(maxd_sp,2*Nt), vect1(Nt), vect2(Nt)                 
    integer    :: i, j, k, p, seed(12), stn(6), num(6), stn_up, stn_dn
!    integer    :: i, j, k, p, seed(2), stn(6), num(6), stn_up, stn_dn
    real(4)    :: rnd

    call parameters(nup,ndn)
    

    !!!!!!!!!!!!!
    !   Check the seed in ED
    !!!!!!!!!!!!!

    seed = 0
    seed(1) = nup
    seed(2) = ndn
    call random_seed( put=seed )

    inv = 0
    stn = 0
    basis  = 0
    bparts = 0
    stn_up = 0
    stn_dn = 0

    do i = Nd_min(1), Nd_max(1)
      do j = Nv(1)-Nh_max(1), Nv(1)
        do k = 0, Nc_max(1)
          if ( (i+j+k) == nup ) then
            call generate( nlm,   i, bparts(:,1:nlm),           stn(1), num(1) )
            call generate( Nv(1), j, bparts(:,nlm+1:nlm+Nv(1)), stn(2), num(2) )
            call generate( Nc(1), k, bparts(:,nlm+Nv(1)+1:Nt),  stn(3), num(3) )
            call unite( stn_up, stn(1:3), num(1:3), bparts(:,1:Nt), 1 )
          end if
        end do ! k
      end do ! j
    end do ! i

    do i = 1, 10*stn_up
      call random_number( rnd )
      k = int( rnd * stn_up ) + 1
      call random_number( rnd )
      p = int( rnd * stn_up ) + 1
      if ( p /= k ) then
        vect1 = basis(k,:,1)
        vect2 = basis(p,:,1)
        basis(k,:,1) = vect2
        basis(p,:,1) = vect1
      end if
    end do ! i

    do i = 1, stn_up
      k = 0
      do p = 1, Nt
        if ( basis(i,p,1) == 1 ) k = ibset(k,p-1)      
      end do ! p
      inv(1,k) = i
    end do ! i

    do i = Nd_min(2), Nd_max(2)
      do j = Nv(2)-Nh_max(2), Nv(2)
        do k = 0, Nc_max(2)
          if ( (i+j+k) == ndn ) then
            call generate( nlm,   i, bparts(:,Nt+1:Nt+nlm),           stn(4), num(4) )
            call generate( Nv(2), j, bparts(:,Nt+nlm+1:Nt+nlm+Nv(2)), stn(5), num(5) )
            call generate( Nc(2), k, bparts(:,Nt+nlm+Nv(2)+1:2*Nt),   stn(6), num(6) )
            call unite( stn_dn, stn(4:6), num(4:6), bparts(:,Nt+1:2*Nt), 2 ) 
          end if
        end do ! k
      end do ! j
    end do ! i

    do i = 1, 10*stn_dn
      call random_number( rnd )
      k = int( rnd * stn_dn ) + 1
      call random_number( rnd )
      p = int( rnd * stn_dn ) + 1
      if ( p /= k ) then
        vect1 = basis(k,:,2)
        vect2 = basis(p,:,2)
        basis(k,:,2) = vect2
        basis(p,:,2) = vect1
      end if
    end do ! i

    do i = 1, stn_dn
      k = 0
      do p = 1, Nt
        if ( basis(i,p,2) == 1 ) k = ibset(k,p-1)      
      end do ! p
      inv(2,k) = i
    end do ! i

    Nst_dn    = stn_dn
    inv(2,-1) = stn_dn

  end subroutine buildbasis
!=========================================================================
  subroutine generate( n, k, vect, ist, num )
    implicit none

    integer,    intent(in   ) :: n, k
    integer(1), intent(inout) :: vect(:,:)
    integer,    intent(inout) :: ist 
    integer,    intent(  out) :: num
    integer :: i, p, pos(k)

    num = -ist

    if ( k == 0 ) then
      ist = ist + 1
      vect(ist,:) = 0
      num = num + ist
      return
    end if

    if ( k == n ) then
      ist = ist + 1
      vect(ist,:) = 1
      num = num + ist
      return
    end if
!---
    pos = 0
    forall( i = 1:k ) pos(i) = i
    p = k

    do while ( p >= 1 )   
      ist = ist + 1
      do i = 1, k
        vect(ist,pos(i)) = 1 
      end do   
 
      if ( pos(k) == n ) then 
        p = p -1
      else
        p = k
      endif

      if ( p >= 1 ) then
        do i = k, p, -1 
          pos(i) = pos(p) + i - p + 1
        end do
      end if
    end do
            
    num = num + ist
                
  end subroutine generate
!=========================================================================
  subroutine unite( ist, pos, num, tmp, sp )
    implicit none

    integer,    intent(inout) :: ist 
    integer,    intent(in   ) :: pos(3), num(3), sp
    integer(1), intent(in   ) :: tmp(:,:) 

    integer :: i, j, k

    do i = pos(1)-num(1)+1, pos(1)
      do j = pos(2)-num(2)+1, pos(2)
        do k = pos(3)-num(3)+1, pos(3)
          ist = ist + 1
          basis( ist, 1:nlm, sp )            = tmp( i, 1:nlm )
          basis( ist, nlm+1:nlm+Nv(sp), sp ) = tmp( j, nlm+1:nlm+Nv(sp) )
          basis( ist, nlm+Nv(sp)+1:Nt,  sp ) = tmp( k, nlm+Nv(sp)+1:Nt  )
        end do ! k
      end do ! j
    end do ! i

  end subroutine unite
!=========================================================================
  subroutine adag( pos, invect, outvect, sgn )
    implicit none
    integer,    intent(in   ) :: pos         ! position
    integer(1), intent(in   ) :: invect(:)   ! input vector
    integer(1), intent(  out) :: outvect(:)  ! output vector
    integer(1), intent(  out) :: sgn         ! sign of a result

    sgn = sum( invect(1:pos-1) )

    if ( mod(sgn,2) == 0 ) then
      sgn = 1
    else
      sgn = -1
    end if
      
    outvect = invect
    outvect(pos) = 1

  end subroutine adag 
!=========================================================================
  subroutine a( pos, invect, outvect, sgn )
    implicit none
    integer,    intent(in   ) :: pos         ! position
    integer(1), intent(in   ) :: invect(:)   ! input vector
    integer(1), intent(  out) :: outvect(:)  ! output vector
    integer(1), intent(  out) :: sgn         ! sign of a result

    sgn = sum( invect(1:pos-1) )

    if ( mod(sgn,2) == 0 ) then
      sgn = 1
    else
      sgn = -1
    end if

    outvect = invect
    outvect(pos) = 0

  end subroutine a 
!=========================================================================
  subroutine find_vect( vect, stn )
    implicit none
    integer(1), intent(in   ) :: vect(:)     ! input vector
    integer,    intent(  out) :: stn         ! position
    integer :: i, j, p

    stn = 0

    i = 0
    do p = 1, Nt
      if ( vect(p) == 1 ) i = ibset(i,p-1)
    end do

    i = inv(1,i)
    if ( i == 0 ) return

    j = 0
    do p = 1, Nt
      if ( vect(p+Nt) == 1 ) j = ibset(j,p-1)
    end do

    j = inv(2,j)
    if ( j == 0 ) return

    stn = (i-1)*inv(2,-1) + j

  end subroutine find_vect
!=========================================================================
  subroutine get_vect( ist, vect )
    implicit none
    integer,    intent(in   ) :: ist         
    integer(1), intent(  out) :: vect(:)
    integer :: i, j, k

    k = displ( myid ) + ist

    i = 1 + k/Nst_dn
    j = mod( k, Nst_dn )

    if ( j == 0 ) then
      i = i - 1
      j = Nst_dn
    end if
 
    vect(1:Nt)      = basis(i,:,1)
    vect(Nt+1:2*Nt) = basis(j,:,2)

  end subroutine get_vect
!=========================================================================
  subroutine boltzmann( val, boltz )
    implicit none
    real(prec), intent(in   ) :: val(:)
    real(prec), intent(  out) :: boltz(:)

    real(prec) :: emin, statsum

    emin = val(1)
    statsum = sum( exp(-beta*(val(:)-emin)) )
    boltz(:) = exp(-beta*(val(:)-emin))/statsum

  end subroutine boltzmann 
!=========================================================================
  subroutine write_eval( boltz )
    implicit none

    real(prec), intent(in   ) :: boltz(:)

    character(32) :: str
    integer    :: i, j, nup, ndn, num(0:Nt,0:Nt)
    real(prec) :: emin, statsum

    if ( verb <= 0 ) return
    write(outf,"(1x,91('!'))")

!   List of eigenvalues
    emin = eval(1,1)
    statsum = sum( exp(-beta*(eval(:,1)-emin)) )

    write(outf,"(/,1x,a,f14.6,a,5x,a,f12.5)") 'Ground-state energy =', emin, ',', 'Statsum =', statsum
    write(outf,"(/,1x,a,11x,a,9x,a,8x,a,6x,a)") '@', 'Energy', 'error', 'Boltz', 'nup   ndn'
    write(outf,"(' @ ',59('-'))")
    do i = 1, nev_gf
      write(outf,"(a,i6,f12.6,f15.8,f12.5,i7,i6)") ' @', i, eval(i,1)-emin, eval(i,2), boltz(i), inf(i,1:2)
    end do
    write(outf,"(1x,61('@'))")

!   Distribution of eigenvalues by sectors
    num = 0
    do i = 1, nev_gf
      nup = inf(i,1)
      ndn = inf(i,2)
      num(nup,ndn) = num(nup,ndn) + 1
    end do

 !   write(outf,"(/,a,2x,a,50(i3))") ' %', 'ndn\nup |', ( i, i=0,Nt )
 !   forall( i = 1 : 3*(Nt+1) ) str(i:i) = '-'
 !   write(outf,"(a,1x,10('-'),a)") ' %', str(1:3*(Nt+1))

 !   do i = 0, Nt
 !     str = ' '
 !     do j = 0, Nt
 !       if ( num(i,j) > 0 ) write(str(1+3*j:3+3*j),"(i3)") num(i,j)
 !     end do
 !     write(outf,"(a,3x,i3,4x,a,a)") ' %', i, '|', str
 !   end do

    write(outf,*)
    write(outf,*) '%    Distribution of the eigenvalues over sectors'
    write(outf,*) '%    Ntot =  Nup + Ndn ;  amount'
    do i = 0, Nt
      do j = 0, Nt
        if ( num(i,j) > 0 )then
          if ( i == inf(1,1) .and. j == inf(1,2) )then
            write(outf,"(' % ',2x,i4,3x,i4,2x,i4,5x,i4,'  <- GS sector')") i+j, i, j, num(i,j)
          else
            write(outf,"(' % ',2x,i4,3x,i4,2x,i4,5x,i4)") i+j, i, j, num(i,j)
          end if
        end if
        if ( num(i,j) == nev_arno )                                    &
          write(outf,*) 'WARNING: Increase ED_ARNO_NEV.'
      end do
    end do
    write(outf,"(1x,50('%'))")

  end subroutine write_eval
!==============================================================================
  subroutine distr_by_sector( nsect, sect )
    implicit none
!   Sort eigenvalues in ascending order by sectors
    integer, intent(  out) :: nsect, sect(:,:)

    integer :: i, j, nup, ndn
    integer :: num(0:Nt,0:Nt)
    logical :: marks(0:Nt,0:Nt)

    num = 0
    do i = 1, nev_gf
      nup = inf(i,1)
      ndn = inf(i,2)
      num(nup,ndn) = num(nup,ndn) + 1
    end do
    nsect = count( num > 0 )

    marks = .true.

    nup = inf(1,1)
    ndn = inf(1,2)
    sect(1,1:2) = inf(1,1:2)
    marks(nup,ndn) = .false.

    j = 1
    do i = 2, nev_gf
      nup = inf(i,1)
      ndn = inf(i,2)
      if( marks(nup,ndn) )then
        j = j + 1
        sect(j,1) = nup
        sect(j,2) = ndn
        marks(nup,ndn) = .false.
      end if
    end do

  end subroutine distr_by_sector
!==============================================================================
  subroutine timer( mode, val )
    implicit none
    character(*), intent(in   ) :: mode
    real(8),      intent(inout) :: val

    integer :: t_min, itime(8)
    real(8) :: t_sec, cur_time, rtime(8)

    if ( myid /= 0 ) return
!   call cpu_time( cur_time )

    call date_and_time( values=itime )
    rtime = real( itime, kind=8 ) 

    select case( itime(2) )
      case(1);  cur_time = 0.0
      case(2);  cur_time = 2678400.0
      case(3);  cur_time = 5097600.0
      case(4);  cur_time = 7776000.0
      case(5);  cur_time = 10368000.0
      case(6);  cur_time = 13046400.0
      case(7);  cur_time = 15638400.0
      case(8);  cur_time = 18316800.0
      case(9);  cur_time = 20995200.0
      case(10); cur_time = 23587200.0
      case(11); cur_time = 26265600.0
      case(12); cur_time = 28857600.0
    end select
    cur_time = cur_time + 86400.0*rtime(3) + 3600.0*rtime(5) +         &
               60.0*rtime(6) + rtime(7) + rtime(8)/1000.0

    select case (mode)
      case("init")
        val = -cur_time
      case("start")
        val = val - cur_time  
      case("stop")
        val = val + cur_time
      case default
        t_min = int( val / 60.d0 , kind=4 )
        t_sec = val - real( t_min * 60, kind=8 )
        if ( t_min > 0 ) then
          write(outf,"(1x,a,i5,a,f6.2,a)") mode, t_min, ' min ', t_sec, ' sec'
        else
          write(outf,"(1x,a,10x,f6.2,a)") mode, t_sec, ' sec'
        endif
    end select

  end subroutine timer
!=========================================================================
  subroutine write_info_arn( isect, it, nup, ndn, emin, nit_arn )
    implicit none

    integer,    intent(in   ) :: isect, nup, ndn, nit_arn
    real(prec), intent(in   ) :: emin
    integer,    intent(inout) :: it

    integer       :: nnz_ln, t_min, t_sec
    character(10) :: time_stp, time_arn

    if ( isect == 1 ) then
      it = 0
      if ( verb > 0 ) write(outf,"(/,a,a,/,a,90('-'))") ' !   i     nup  ndn   nstates ', &
                    ' nprocs  nst_pr  nnz_ln  nit_arn    emin     setup    arnoldi',' !'
    end if
    it = it + 1

!   Number of nonzero elements per line
    nnz_ln = 1 + Hpos(Nst_pr)/Nst_pr

!   Timing
    t_min = int( (t_stp-t_stp0)/60.d0 )
    t_sec = int(  t_stp-t_stp0 - real(t_min*60,kind=4) )
    if ( t_sec < 10 ) then
      write(time_stp,"(i6,a,i1)") t_min, ':0', t_sec
    else
      write(time_stp,"(i6,a,i2)") t_min, ':',  t_sec
    endif

    t_min = int( (t_arn-t_arn0)/60.d0 )
    t_sec = int(  t_arn-t_arn0 - real(t_min*60,kind=4) )
    if ( t_sec < 10 ) then
      write(time_arn,"(i6,a,i1)") t_min, ':0', t_sec
    else
      write(time_arn,"(i6,a,i2)") t_min, ':',  t_sec
    endif

    t_stp0 = t_stp
    t_arn0 = t_arn

    if ( verb > 0 ) write(outf,"(a,i4,a,i6,i5,i11,i6,i9,i7,i9,f11.4,a,a)") ' !', it, ')', &
      nup, ndn, Nst(nup,ndn), nprocs_eff, Nst_pr, nnz_ln, nit_arn, emin, time_stp, time_arn

  end subroutine write_info_arn
!==============================================================================
  subroutine time_stat
    implicit none

    if ( verb > 0 ) then
      write(outf,"(/,8x,a)") 'Solver timing:'
      call timer( 'Hdim      ' , t_hd    )
      call timer( 'Setup     ' , t_stp   )
      call timer( 'Arnoldi   ' , t_arn   )
      call timer( '    _saupd' , t_saupd )
      call timer( '      comm' , t_acomm )
      call timer( '       mvp' , t_amvp  )
      call timer( '    _seupd' , t_seupd )
      call timer( '      sort' , t_asort )
      call timer( 'Lanc      ' , t_lnc   )
      call timer( 'Zos       ' , t_zos   )
      call timer( '       mvp' , t_lmvp  )
      call timer( '      comm' , t_lcomm )
      call timer( '       a,b' , t_lab   )
      call timer( 'Full      ' , t_full  )
    end if

  end subroutine time_stat 
!=========================================================================
  subroutine combine_evect( iv, nup0, ndn0 )
    implicit none

    integer, intent(in   ) :: iv, nup0, ndn0  
    integer :: k, ierr

    call buildbasis( nup0, ndn0 )
    inv0 = inv

    k = inf(iv,3)

    if ( nprocs_eff == 1 ) then 
      vecinit(1:Nst_pr) = evec(k,1:Nst_pr)
    else
#ifdef MPI
      call mpi_allgatherv( evec(k,1:Nst_pr), Nst_pr, mpi_knd, vecinit, distr, displ, mpi_knd, comm, ierr )
#endif
    end if

  end subroutine combine_evect
!=========================================================================
  subroutine write_info_gf( iv, boltz, decomp, sym )
    implicit none

    integer,    intent(in   ) :: iv
    real(prec), intent(in   ) :: boltz
    real(8),    intent(in   ) :: decomp
    logical,    intent(in   ) :: sym

    logical, allocatable :: marks(:)
    character(128) :: str
    integer(1) :: bvect(2*Nt)
    integer    :: i, j, k, n
    real(prec) :: prob, prsum, density(2*Nt)

    if ( verb > 0 .and. iv == 1 ) write(outf,"(/,1x,23('>'),a,23('<'))") "  Green function calculation  "

    select case ( sym ) 
      case(.true.);  if ( verb > 0 ) write(outf,"(i3,a,f11.6,4x,a,f9.5,4x,a,i4,i4,4x,a,2x,a)") &
        iv, ')  Ei =', eval(iv,1), 'boltz =', boltz, 'nup,ndn =', inf(iv,1:2), 'symm =', 'yes'
      case(.false.); if ( verb > 0 ) write(outf,"(i3,a,f11.6,4x,a,f9.5,4x,a,i4,i4,4x,a,2x,a)") &
        iv, ')  Ei =', eval(iv,1), 'boltz =', boltz, 'nup,ndn =', inf(iv,1:2), 'symm =', 'no'
    end select 

!   Eigenvalue decomposition
    n = Nst( inf(iv,1), inf(iv,2) )

    if ( verb > 9 ) write(outf,"(a,7x,a,1x,10(f5.1))") '  >', 'Probabil'

    allocate( marks(n) )
    marks = .true.

    prsum = zero
    do i = 1, n
      k = maxloc( abs(vecinit(1:n)), dim=1, mask=marks )
      marks(k) = .false.
      prob = vecinit(k)*vecinit(k)
      if ( prob < decomp ) exit
      prsum  = prsum + prob
      call get_vect( k, bvect )
      str = ' '
      write(str(1:3*nlm),"(90(i3))") bvect(1:nlm)
      write(str(3+3*nlm+1:3+3*(nlm+Nv(1))),"(90(i3))") bvect(nlm+1:nlm+Nv(1))
      write(str(5+3*(nlm+Nv(1))+1:5+3*Nt),"(90(i3))") bvect(nlm+Nv(1)+1:Nt)
      if ( verb > 9 ) write(outf,"(a,i5,f8.2,3x,a)") '  >', i, prob, str(1:5+3*Nt)
      str = ' '
      write(str(1:3*nlm),"(90(i3))") bvect(Nt+1:Nt+nlm)
      write(str(3+3*nlm+1:3+3*(nlm+Nv(2))),"(90(i3))") bvect(Nt+nlm+1:Nt+nlm+Nv(2))
      write(str(5+3*(nlm+Nv(2))+1:5+3*Nt),"(90(i3))") bvect(Nt+nlm+Nv(2)+1:2*Nt)
      if ( verb > 9 ) write(outf,"(a,       16x,a)") '  >',          str(1:5+3*Nt)
    end do ! i
    if ( verb > 9 ) write(outf,"(a,f7.2,/,a,57('-'))") '  >   sum', prsum, '  ' 

!   Averaged density
    density = zero
    do i = 1, n
      prob = vecinit(i)*vecinit(i)
      call get_vect( i, bvect )
      density = density + prob*bvect
    end do ! i

    str = ' '
    write(str(1:6*nlm),"(90(f6.2))") density(1:nlm)
    write(str(3+6*nlm+1:3+6*(nlm+Nv(1))),"(90(f6.2))") density(nlm+1:nlm+Nv(1))
    write(str(5+6*(nlm+Nv(1))+1:5+6*Nt),"(90(f6.2))") density(nlm+Nv(1)+1:Nt)
    if ( verb > 9 ) write(outf,"(a,a            )")  '      density ', str

    str = ' '
    write(str(1:6*nlm),"(90(f6.2))") density(Nt+1:Nt+nlm)
    write(str(3+6*nlm+1:3+6*(nlm+Nv(2))),"(90(f6.2))") density(Nt+nlm+1:Nt+nlm+Nv(2))
    write(str(5+6*(nlm+Nv(2))+1:5+6*Nt),"(90(f6.2))") density(Nt+nlm+Nv(2)+1:2*Nt)
    if ( verb > 9 ) write(outf,"(a,a,/,a,57('-'))")  '              ', str, '  ' 

    if ( verb > 10 ) write(outf,"(2x,a,4x,a,3x,a,6x,a,7x,a,4x,a)") 'mode', 'nup  ndn', & 
                                                     'iorb', 'coeff', 'gf_aver', 'niter'
    deallocate( marks )

  end subroutine write_info_gf
!=========================================================================
  subroutine lanc( mode, ims, norm )
    implicit none

    integer,    intent(in   ) :: mode, ims
    real(prec), intent(  out) :: norm

    integer(1) :: sgn, vect(2*Nt), resultV(2*Nt)
    integer    :: ist, start, pos, ierr
    real(prec) :: rtmp
                                             call timer( 'start', t_lnc )
    norm  = zero
    start = displ(myid)
    vecold(start+1:start+Nst_pr) = zero

    select case (mode)
      case(1)
        do ist = 1, Nst_pr
          call get_vect( ist, vect )
          if ( vect(ims) == 0 ) cycle
          call a( ims, vect, resultV, sgn )
          call find_vect( resultV, pos )
          if ( pos == 0 ) cycle
          rtmp = sgn * vecinit(pos)
          vecold(start+ist) = rtmp
          norm = norm + rtmp*rtmp
        end do
      case(-1)
        do ist = 1, Nst_pr
          call get_vect( ist, vect )
          if ( vect(ims) == 1 ) cycle
          call adag( ims, vect, resultV, sgn )
          call find_vect( resultV, pos )
          if ( pos == 0 ) cycle
          rtmp = sgn * vecinit(pos)
          vecold(start+ist) = rtmp
          norm = norm + rtmp*rtmp
        end do
    end select

    if ( nprocs_eff > 1 ) call exch( norm )
    norm = sqrt( norm )    

    if ( norm > tny ) vecold(start+1:start+Nst_pr) = vecold(start+1:start+Nst_pr) / norm

#ifdef MPI
    if ( nprocs_eff > 1 ) call mpi_allgatherv( vecold(start+1), Nst_pr, mpi_knd, vecold, distr, displ, mpi_knd, comm, ierr )
#endif
                                             call timer( 'stop', t_lnc )
  end subroutine lanc
!=========================================================================
  subroutine zos( mode, iv, iorb, nup, ndn, coef, zenergy, Gf )
    implicit none

    integer,    parameter     :: niter = 1000 ! max # of iterations

    integer,    intent(in   ) :: mode, iv, iorb, nup, ndn
    real(prec), intent(in   ) :: coef
    complex(8), intent(in   ) :: zenergy(:)
    complex(8), intent(inout) :: Gf(:)

    integer    :: i, j, it, start, iexit, ierr
    real(prec) :: rtmp, a(niter), b(niter), chi(5), vecnew(Nst_pr)
    complex(8) :: Gf_calc(Iwmax)
                                             call timer( 'start', t_zos )
    iexit = 0
    a = zero
    b = zero
    chi = -one 
    start = displ( myid )
    vecnew = zero

    do it = 1, niter
      if ( it > 1 ) then
        do i = 1, Nst_pr
          rtmp = vecold(start+i)
          vecold(start+i) = vecnew(i)/b(it)
          vecnew(i) = -b(it)*rtmp
        end do
#ifdef MPI
                                             call timer( 'start', t_lcomm )
        if ( nprocs_eff > 1 ) call mpi_allgatherv( vecold(start+1), Nst_pr, mpi_knd, vecold, distr, displ, mpi_knd, comm, ierr )
                                             call timer( 'stop' , t_lcomm )
#endif
      end if ! it > 1        

!     Matrix-vector product
                                             call timer( 'start', t_lmvp )
      do i = 1, Nst_pr
        vecnew(i) = vecnew(i) + Hdiag(i)*vecold(start+i)
      end do

      do i = 1, Nst_pr
        do j = Hpos(i-1)+1, Hpos(i)
          rtmp = Hst(j)*vecold(Hcon(j))
          vecnew(i) = vecnew(i) + rtmp
        end do
      end do
                                             call timer( 'stop' , t_lmvp  )
                                             call timer( 'start', t_lab   )
      do i =  1, Nst_pr
        rtmp = vecnew(i)*vecold(start+i)
        a(it) = a(it) + rtmp
      end do
      if ( nprocs_eff > 1 ) call exch( a(it) )
                                             call timer( 'stop',  t_lab   )

      call green_function( it, mode, iv, coef, a(1:it), b(1:it), zenergy, Gf_calc, chi, iexit )
      if ( iexit == 1 .or. Nst_pr == 1 ) exit

      if ( it == niter ) call stop_program("The precision cannot be achieved$")

      do i = 1, Nst_pr
        vecnew(i) = vecnew(i) - a(it)*vecold(start+i)
      end do
                                             call timer( 'start', t_lab   )
      do i = 1, Nst_pr
        rtmp = vecnew(i)*vecnew(i)
        b(it+1) = b(it+1) + rtmp
      end do
      if ( nprocs_eff > 1 ) call exch( b(it+1) )
                                             call timer( 'stop' , t_lab   )
      b(it+1) = sqrt( b(it+1) )  
      if ( b(it+1) < tny ) exit
    end do ! it

    if ( iorb > 0 ) then
      Gf = Gf + Gf_calc
    else
      Gf = Gf - Gf_calc * mode
    end if

    if ( verb > 10 ) then
      if ( abs(iorb) == 1 ) then
        write(outf,"(i5,i7,i5,i7,f13.6,f13.6,i7)") mode, nup, ndn, iorb, coef, sum( abs(Gf_calc) )/Iwmax, it
      else
        write(outf,"(     17x,i7,f13.6,f13.6,i7)")            abs(iorb), coef, sum( abs(Gf_calc) )/Iwmax, it   
      end if
    end if
                                             call timer( 'stop', t_zos )
  end subroutine zos
!=========================================================================
  subroutine green_function( it, mode, iv, coef, a, b, zenergy, Gf, chi, iexit )
    implicit none

    integer,    intent(in   ) :: it, mode, iv
    real(prec), intent(in   ) :: coef, a(it), b(it)
    complex(8), intent(in   ) :: zenergy(:)
    complex(8), intent(inout) :: Gf(:)
    real(prec), intent(inout) :: chi(:)
    integer,    intent(inout) :: iexit

    integer :: j, iw
    real(prec) :: term(it), numer(it)
    complex(prec) :: ztmp, denom, Gf_old(Iwmax)

    Gf_old = Gf

!   Calculation of Green function as a rational fraction

    numer = b * b
    term = mode*( eval(iv,1) - a )

    do iw = 1, Iwmax
      denom =  zenergy(iw) + term(it)

      do j = it-1, 1, -1
        ztmp = numer(j+1)/denom
        denom =  zenergy(iw) + term(j) - ztmp
      end do

      Gf(iw) = coef/denom
    end do ! iw

    if ( it == 1 ) return

    chi(1:4) = chi(2:5)
    chi(5) = maxval( abs(Gf-Gf_old) )  
    if ( verb > 29 ) write(outf,"(3x,i3,3(f20.15))") it, chi(5), a(it), b(it)     

    if ( all( chi >= 0 ) .and. all( chi < tol_gf ) ) iexit = 1       

  end subroutine green_function
!=========================================================================
  subroutine exch( val )
    implicit none

    real(prec), intent(inout) :: val

    integer    :: i, ierr, m_distr(0:nprocs-1), m_displ(0:nprocs-1)
    real(prec) :: mas(0:nprocs-1)

    mas = zero
    mas(myid) = val

    m_distr = 1
    forall( i = 0 : nprocs-1 ) m_displ(i) = i

#ifdef MPI
    call mpi_allgatherv( mas(myid), 1, mpi_knd, mas, m_distr, m_displ, mpi_knd, comm, ierr )
#endif

    val = sum( mas )

  end subroutine exch
!==============================================================================
  subroutine arnoldi( nup, ndn, sym, emin, it )
    implicit none

    integer,    intent(in   ) :: nup, ndn
    logical,    intent(in   ) :: sym
    real(prec), intent(  out) :: emin      ! energy minimum in sector
    integer,    intent(  out) :: it        ! # of Arnoldi iterations

    integer   :: i, j, k, p, n, ig, ido, ncv, ierr, info, nconv,               &
                 lworkl, start, win, itmin, nmin, iexit, newnev, nrepeat,      &
                 ipntr(11), iparam(11) 
    real(prec):: ecut, rtmp, sigma, oldval, tol_loc, boltz(nev_gf)
    character :: bmat*1, which*2
    logical :: rvec
    real(prec), allocatable :: v(:,:), d(:), workl(:), workd(:), resid(:), column(:)
    logical, allocatable :: marks(:)

    it = 0
    n  = Nst(nup,ndn)

    if ( n == 1 ) then
      p = count( eval(:,1) <= Hdiag(1) )
      if ( p == nev_gf ) return
      k = inf(nev_gf,3)
      eval(p+2:nev_gf,:) = eval(p+1:nev_gf-1,:)
      eval(p+1,1) = Hdiag(1)
      eval(p+1,2) = zero
      evec(k,1)   = one
      inf(p+2:nev_gf,:) = inf(p+1:nev_gf-1,:)
      inf(p+1,1) = nup 
      inf(p+1,2) = ndn
      inf(p+1,3) = k
      if ( sym ) then
        p = count( eval(:,1) <= Hdiag(1) )
        if ( p == nev_gf ) return
        k = inf(nev_gf,3)
        eval(p+2:nev_gf,:) = eval(p+1:nev_gf-1,:)
        eval(p+1,1) = Hdiag(1)
        eval(p+1,2) = zero
        evec(k,1)   = one
        inf(p+2:nev_gf,:) = inf(p+1:nev_gf-1,:)
        inf(p+1,1) = ndn  ! It's important
        inf(p+1,2) = nup  ! It's important
        inf(p+1,3) = k
      end if
      emin = Hdiag(1)
      return
    end if

    call timer( 'start', t_arn )

    newnev = 1
    if ( nev_arno > 1 ) newnev = min( n-1, nev_arno )

 !   write(6,*) ' newnev ', newnev, nev_arno

    ncv = min( n, 2*newnev+ncv0  )
    lworkl = ncv*(ncv+8)    
        
    allocate( v(Nst_pr,ncv), d(newnev), workl(lworkl), workd(3*Nst_pr),        &
              marks(ncv), resid(Nst_pr+1), column(n), stat=ierr )

    if ( ierr /= 0 ) call stop_program("Allocation problem in Arnoldi.$")

!   Arnoldi convergence criterium: err(Eval) < tol_loc*|Eval|
    tol_loc = tol_arn/two/max(abs(eval(1,1)),1.0)

    ido  = 0            ! flag for reverse communication   
    info = 0            ! info variable

    bmat  = 'I'         ! standard eigenvalue problem A*x = lambda*x
    which = 'SA'        ! compute newnev smallest eigenvalues                                                    
    
    iparam = 0
    iparam(1) = 1       ! exact shift strategy, it is recommended
    iparam(3) = 500000  ! max number of iterations            
    iparam(7) = 1       ! mode

    nmin = 3000         ! min number of states to do eigenvalues estimations
    itmin = 30          ! min number of iterations to do eigenvalues estimations
    nrepeat = 4         ! number of repeats to exit from arnoldi loop 

    iexit = 0           ! parameter for indication of eigenvalues estimations
    ipntr = 0
    oldval = zero
    ecut = energy_cut()
!-----------------------------------------------------------------------------------
    do 
                                                      call timer( 'start', t_saupd )
      if ( nprocs_eff == 1 ) then 
        select case (prec)
          case(4); call ssaupd( ido, bmat, Nst_pr, which, newnev, tol_loc, resid, ncv, &     
                                  v, Nst_pr, iparam, ipntr, workd, workl, lworkl, info )
          case(8); call dsaupd( ido, bmat, Nst_pr, which, newnev, tol_loc, resid, ncv, &
                                  v, Nst_pr, iparam, ipntr, workd, workl, lworkl, info )
        end select
      else
#ifdef MPI
        select case (prec)
          case(4); call pssaupd( comm, ido, bmat, Nst_pr, which, newnev, tol_loc, resid, ncv, &
                                         v, Nst_pr, iparam, ipntr, workd, workl, lworkl, info )
          case(8); call pdsaupd( comm, ido, bmat, Nst_pr, which, newnev, tol_loc, resid, ncv, &
                                         v, Nst_pr, iparam, ipntr, workd, workl, lworkl, info )
        end select
#endif
      end if  
                                                      call timer( 'stop' , t_saupd )
      if ( abs(ido) /= 1 ) exit
      it = it + 1

!     Tolerance estimations

      if ( opt > 0 .and. n > nmin .and. it > itmin .and. workl(3*ncv+1) /= oldval ) then
        oldval = workl(3*ncv+1)
        call eval_estim( newnev, ncv, workl, ecut, sym, iexit )
      end if

      if ( iexit == -nrepeat ) then
        emin = workl(3*ncv)
        it = -it
        goto 110
      end if

      if ( iexit == nrepeat ) tol_loc = 777.0

      if ( opt > 0 .and. iexit < nrepeat .and. workl(3*ncv) /= zero ) tol_loc = tol_arn/max(abs(workl(3*ncv)),1.0)
      if ( verb > 29 ) write(outf,"(3x,2(a,i5,5x),a,f16.9)") 'it =', it, 'iexit =', iexit, 'tol_loc =', tol_loc

!     Preparation of vector

      j = ipntr(1)
                                                        
      if ( nprocs_eff == 1 ) then     
        column(1:Nst_pr) = workd(j:j+Nst_pr-1)
      else
#ifdef MPI
                                                      call timer( 'start', t_acomm )
        select case (comtype)
          case(1)
            call mpi_allgatherv( workd(j), Nst_pr, mpi_knd, column, distr, displ, mpi_knd, comm, ierr )
          case(2)
            ig = displ(myid)  
            column( ig+1 : ig+Nst_pr ) = workd(j:j+Nst_pr-1)
 
            if ( it == 1 ) call mpi_win_create( workd(j), prec*Nst_pr, prec, mpi_info_null, comm, win, ierr )

            call mpi_win_fence( 0, win, ierr )

            do i = myid+1, nprocs-1
              ig = displ(i)+1
              do j = Nget(i)+1, Nget(i+1)
                call mpi_get( column(connect(j)), 1, mpi_knd, i, connect(j)-ig, 1, mpi_knd, win, ierr )  
              end do
            end do

            do i = 0, myid-1
              ig = displ(i)+1
              do j = Nget(i)+1, Nget(i+1)
                call mpi_get( column(connect(j)), 1, mpi_knd, i, connect(j)-ig, 1, mpi_knd, win, ierr )   
              end do
            end do   

            call mpi_win_fence( 0, win, ierr )
          end select
                                                      call timer( 'stop' , t_acomm ) 
#endif
      end if

!     Matrix-vector product
                                                      call timer( 'start', t_amvp )
      start = ipntr(2) - 1                           
      ig = displ(myid)                               

      do i = 1, Nst_pr                                ! diagonal part
        workd(start+i) = Hdiag(i)*column(ig+i)
      end do

      do i = start+1, start+Nst_pr                    ! non-diagonal part
        do j = Hpos(i-start-1)+1, Hpos(i-start)
          rtmp = Hst(j)*column(Hcon(j))
          workd(i) = workd(i) + rtmp
        end do
      end do     
                                                      call timer( 'stop' , t_amvp )
    end do ! abs(ido) == 1
!-----------------------------------------------------------------------------------       

    if ( info <  0 ) call stop_program("Error in _saupd$")  
    if ( info == 1 ) call stop_program("Tol wasn't reached$")
    if ( info == 3 ) call stop_program("Try to increase ncv$")                                                            
   
    rvec = .true.  ! computing of eigenvectors
                                                      call timer( 'start', t_seupd )
    if ( nprocs_eff == 1 ) then 
      select case (prec)
        case(4); call sseupd (       rvec, 'All', marks, d, v, Nst_pr, sigma,       &
                               bmat, Nst_pr, which, newnev, tol_loc, resid, ncv, v, &
                               Nst_pr, iparam, ipntr, workd, workl, lworkl, ierr    )
        case(8); call dseupd (       rvec, 'All', marks, d, v, Nst_pr, sigma,       &
                               bmat, Nst_pr, which, newnev, tol_loc, resid, ncv, v, &
                               Nst_pr, iparam, ipntr, workd, workl, lworkl, ierr    )
      end select     
    else  
#ifdef MPI
      select case (prec)
        case(4); call psseupd( comm, rvec, 'All', marks, d, v, Nst_pr, sigma,       &
                               bmat, Nst_pr, which, newnev, tol_loc, resid, ncv, v, &
                               Nst_pr, iparam, ipntr, workd, workl, lworkl, ierr    )
        case(8); call pdseupd( comm, rvec, 'All', marks, d, v, Nst_pr, sigma,       &
                               bmat, Nst_pr, which, newnev, tol_loc, resid, ncv, v, &
                               Nst_pr, iparam, ipntr, workd, workl, lworkl, ierr    )
      end select
#endif
    end if        
                                                      call timer( 'stop' , t_seupd )
    if ( ierr /= 0 ) call stop_program("Error in _seupd$")

    if ( verb > 29 ) write(outf,"(3x,11('='),2x,a,2x,11('='))") 'Accepted'
    emin = d(1)
    nconv = iparam(5)
                                                      call timer( 'start', t_asort )
    do i = 1, nconv
      p = count( eval(:,1) <= d(i) )
      if ( p == nev_gf ) exit
      rtmp = workl(5*ncv+i)  ! = error
      if ( verb > 29 ) write(outf,"(1x,i3,f14.6,f17.10)") i, d(i), rtmp
      k = inf(nev_gf,3)
      eval(p+2:nev_gf,:) = eval(p+1:nev_gf-1,:)
      eval(p+1,1) = d(i)
      eval(p+1,2) = rtmp
      evec(k,1:Nst_pr) = v(:,i)
      inf(p+2:nev_gf,:) = inf(p+1:nev_gf-1,:)
      inf(p+1,1) = nup 
      inf(p+1,2) = ndn
      inf(p+1,3) = k
      if ( sym ) then
        p = count( eval(:,1) <= d(i) )
        if ( p == nev_gf ) exit
        k = inf(nev_gf,3)
        eval(p+2:nev_gf,:) = eval(p+1:nev_gf-1,:)
        eval(p+1,1) = d(i)
        eval(p+1,2) = rtmp
        evec(k,1:Nst_pr) = v(:,i)
        inf(p+2:nev_gf,:) = inf(p+1:nev_gf-1,:)
        inf(p+1,1) = ndn   ! It's important
        inf(p+1,2) = nup   ! It's important
        inf(p+1,3) = k
      end if
    end do ! i   
                                                      call timer( 'stop' , t_asort )
    if ( verb > 29 ) then
      call boltzmann( eval(:,1), boltz )
      write(outf,"(3x,13('='),2x,a,2x,13('='))") ' All'
      do i = 1, nev_gf
        if ( eval(i,1) == huge(one) ) exit
        write(outf,"(1x,i3,f14.6,f17.10,f12.6,i6,i6)") i, eval(i,:), boltz(i), inf(i,1:2)
      end do
    end if

110 continue

#ifdef MPI
    if ( comtype == 2 .and. nprocs_eff > 1 ) call mpi_win_free( win, ierr )
#endif

    deallocate( v, d, workl, workd, resid, marks, column )
    call timer( 'stop', t_arn )

  end subroutine arnoldi
!=========================================================================
  real(prec) function energy_cut()
    implicit none

    real(prec) :: emin, rtmp

    if ( any( eval(1:nev_gf-1,1) == huge(one) ) ) then
      energy_cut = huge(one)
    else
      emin = eval(1,1)

      rtmp = 0.0001

      energy_cut = min( rtmp + emin, eval(nev_gf,1) )
      if ( verb > 29 ) write(outf,"(2x,a,f12.6)") 'energy_cut =', energy_cut
    end if

  end function energy_cut
!=========================================================================
  subroutine eval_estim( newnev, ncv, workl, ecut, sym, iexit )
    implicit none

!   This subroutine does eigenvalues estimations. One can see
!   it's quite conservative, so any mistakes are hardly possible.

    integer,    intent(in   ) :: newnev, ncv
    real(prec), intent(in   ) :: ecut, workl(:)
    logical,    intent(in   ) :: sym
    integer,    intent(inout) :: iexit

    integer :: i, k, p, nnc, nconv, newnev_eff
    real(prec) :: emin, statsum, val(ncv), error(ncv), eval_new(nev_gf), boltz(newnev)

!   Initialization of Ritz values and their errors
    k = 3*ncv + 1
    p = 4*ncv + 1

    do i = 1, ncv
      val(i) = workl(k-i)
      error(i) = workl(p-i)
    end do
    val = val - error               ! extreme case

   if ( verb > 29 ) then
      write(outf,"(3x,8('~'),1x,a,1x,8('~'))") 'Ritz values'
      do i = 1, ncv
        write(outf,"(1x,i3,f12.6,f16.10,f14.6)") i, val(i)+error(i), error(i), val(i)
      end do
    end if

!   Check, whether this sector is of interest.   
    if ( ecut < minval(val) ) then
      iexit = iexit - 1 
      return
    end if  

!   Counting of converged values
    nconv = 0
    do i = 1, newnev
      if ( error(i) >= tol_arn ) exit
      nconv = nconv + 1
    end do
    if ( verb > 29 ) write(outf,"(3x,29('~'),/,2x,a,i3)") 'nconv =', nconv

    if ( nconv == newnev ) return

!   Counting of non-crossing values
    nnc = 0
    do i = 1, newnev
      if ( val(i)+two*error(i)+tol_arn >= minval(val(i+1:ncv))-tol_arn ) exit
      nnc = nnc + 1
    end do
    if ( verb > 29 ) write(outf,"(2x,a,i5)") 'nnc =', nnc

    if ( nnc == 0 ) return

    if ( nconv > nnc ) nconv = nnc

!   Estimations of Ritz values
    newnev_eff = 0
    eval_new = eval(:,1)

    do i = 1, nnc
      p = count( eval_new <= val(i) )
      if ( p == nev_gf ) exit
      eval_new(p+2:nev_gf) = eval_new(p+1:nev_gf-1)
      eval_new(p+1) = val(i)

      newnev_eff = newnev_eff + 1

      if ( sym ) then
        p = count( eval_new <= val(i) )
        if ( p == nev_gf ) exit 
        eval_new(p+2:nev_gf) = eval_new(p+1:nev_gf-1)
        eval_new(p+1) = val(i)
      end if
    end do ! i
    if ( verb > 29 ) write(outf,"(2x,a,i3)") 'newnev_eff_1 =', newnev_eff

    if ( newnev_eff < nnc .and. newnev_eff <= nconv ) then
      iexit = iexit + 1
      return
    end if

!   Estimations by means of Boltzmann factor
    if ( opt > 0 ) then
      emin = eval_new(1)
      statsum = sum( exp(-beta*(eval_new(:)-emin)) )
      boltz(1:newnev_eff) = exp(-beta*(val(1:newnev_eff)-emin))/statsum

      if ( verb > 29 ) then
        do i = 1, newnev_eff
          write(outf,"(1x,i2,f12.6,f16.10,3(f12.6))") i, val(i)+error(i), error(i), boltz(i)
        end do
      end if

!       Not sure gf or arno
      newnev_eff = nev_gf

      if ( verb > 29 ) write(outf,"(2x,a,i3)") 'newnev_eff_2 =', newnev_eff

      if ( newnev_eff < nnc .and. newnev_eff <= nconv ) iexit = iexit + 1
    end if ! opt > 0

  end subroutine eval_estim
end module ed_arnoldi_module
