!  Copyright (c) 2015, AMULET Developers Team
!  All rights reserved.
!
!  Redistribution and use in source and binary forms, with or without
!  modification, are permitted provided that the following conditions are met:
!
!  1. Redistributions of source code must retain the above copyright notice, this
!     list of conditions and the following disclaimer.
!  2. Redistributions in binary form must reproduce the above copyright notice,
!     this list of conditions and the following disclaimer in the documentation
!     and/or other materials provided with the distribution.
!
!  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
!  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
!  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
!  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
!  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
!  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
!  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
!  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
!  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
!  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
!
!  The views and conclusions contained in the software and documentation are those
!  of the authors and should not be interpreted as representing official policies,
!  either expressed or implied, of the FreeBSD Project.

program amulet
!==================================================================================================
!
!         AMULET project
!     www.amulet-code.org
!     code.amulet _at_ gmail.com
!
!==================================================================================================
  use mpi_module
  use timer_module
  use parameter_module
  use hamiltonian_module
  use hamiltonian_tools
  use energy_module
  use impurity_module
  use iolib_module
  use units
  use symmetry_module
  use total_energy_module

  implicit none

  integer :: it = 0, iuham = 2
  integer :: ierr, itype
  real(8) :: xmu_old, ntotal_old
  character(128) :: ham_name
  type(t_timer), pointer :: total_time=>null(), gk_time=>null(), iter_time=>null()

!===================================================================================

  call initialize_mpi( ierr )
  if( ierr /= 0 ) call stop_program('Error of MPI initialization.$')

!   Open output file and start timer

  if( proc_id == master )then
    open( 14, file='amulet.out', form='formatted', position='rewind' )
    call add_timer( total_time,' AMULET (total time):' )
    call start_timer( total_time )
  end if

!   Defines number of points in energy mesh

  call def_energy_params( ierr )
  if( ierr == 777 ) call stop_program('Wrong call in read_ini subroutine.$')
  if( ierr  > 0 )   call stop_program('Cannot locate or read amulet.ini file (1).$')
 
!   Define energy and time meshes
  call set_energy_mesh
  if( imenergy )then
    call use_imaginary_mesh
  else
    call use_real_mesh
  end if

!   Read initial data from amulet.ini file

  call read_ini( ierr )
  if( ierr == 777 ) call stop_program('Wrong call in read_ini subroutine.$')
  if( ierr  > 0 )   call stop_program('Cannot locate or read amulet.ini file (2).$')
  if( proc_id /= master ) verbos = -1

  atm_cutoff = ecut

  call setup_energy_units

!   Read Hamiltonian
  call hamiltonian_name( iuham, ham_name, ierr )
  if( ierr /= 0 )   call stop_program( 'Cannot locate hamiltonian file.$' )

  call read_hamiltonian( iuham, ham_name, ierr )

  select case( ierr )
  case( -14001 )
    use_hmlt = .true.
    if( rhtm == 'UNDEFINED' ) rhtm = 'TBLMTO'
  case( -14002 )
    use_hmlt = .true.
    if( rhtm == 'UNDEFINED' ) rhtm = 'ESPRESSO'
  case( :-14003, -14000:0 )
    call stop_program('There is a problem with format of hamiltonian file.$')
  case default
    rhtm = 'TBLMTO'
    use_hmlt = .false.
  end select

!   Read impurity.ini files

  call read_impurity( ierr )
  if( ierr == 777 ) call stop_program('Wrong call in read_impurity subroutine.$')
  if( ierr  > 0 )   call stop_program('Cannot locate or read one of impurity.ini files.$')
  impurity(:)%nqmc = impurity(:)%nqmc / numprocs

!   Read bare (LDA) density of states
  if( .not. use_hmlt )then
    call read_dos( ierr )
    if( ierr /= 0 )then
      call stop_program('Cannot locate or read one of dos.ini files.$')
    else
      ndim = sum( impurity(:)%nlm )
    end if
  end if

!   Make matrix of Coulomb interaction

  call make_umtrx( ierr )
  if( ierr == 777 )                                                           &
    call stop_program('Use umtrx.ini or impurity.ini file to define U matrix.$')
  if( ierr /= 0 )                                                             &
    call stop_program('Cannot locate or read one of umtrx.ini files.$')

!   Write info 

  call foolproof
  if( proc_id == master ) call header

!   Define tetrahedra
  if( use_atm .and. use_hmlt )then
    call def_tetra( ierr )
    if( ierr == 777 ) call stop_program('k-points are not defined.$')
    if( ierr /= 0 )   call stop_program('Cannot define tetrahedra.$')
  end if

!   Check impurity positions and define H_ll, H_li, H_ii, etc
  if( use_hmlt .and. (.not. cpa ) )  call make_hlhi

!-----------   Calculate DFT quantities   ------------------------
!   Calculates total Green function and finds Fermi level
  xmu_old = xmu
  ntotal_old = ntotal
  ntotal = abs(ntotal)

  if( verbos > 0 ) write(14,"(/,11('==DFT=='),//,' DFT Fermi level')")
  call find_fermi_level

!   Calculates local Green function of impurities
  if( verbos > 0 ) write(14,"(/,' DFT Green function and occupations')")
  call glocal

!   Calculates DFT total energy (works on Matsubara grid only)
  if( imenergy .and. use_hmlt )then
    if( verbos > 0 )   write(14,"(/,' DFT contributions to total energy')")
    call total_energy
  end if

  xmu = xmu_old
  ntotal = ntotal_old
  if( verbos > 0 ) write(14,"(/,11('==DFT=='),/)")
!-----------------------------------------------------------------

!   Clean Hamiltonian arrays to free memory
 ! call dealloc_hamiltonian

!   Read self-energy

  call rw_sigma( ierr, 'r', 'imag' )
  if( ierr /= 0 ) call stop_program('Cannot locate or read one of sigma files.$')
!    Start from scratch
  if( istart == 0 ) forall( itype = 1:n_imp_type ) impurity(itype)%Sigma = zero

!   Initialize random seed for QMC use
  call random_ini( proc_id )

!
!     DMFT self-consistent cycle
!
  do it = 1, niter

    if( verbos > 0 )then
      write(14,"(80('='),/,20x,'Iteration # ',i3,/)") it
      call add_timer( iter_time,' Iteration time:' )
      call start_timer( iter_time )
      call add_timer( gk_time,' Green function (gk time):' )
      call start_timer( gk_time )
    end if

    do itype = 1, n_imp_type
      call set_symmetry( impurity(itype)%Sigma, impurity(itype)%smask )
    end do

!   Read Hamiltonian
 !   if( use_hmlt )then
 !     call read_hamiltonian( ierr )
 !     call make_hlhi
 !   end if

!   Calculates total Green function and finds Fermi level
    call find_fermi_level

!   Calculates local Green function of impurities
    call glocal

!   Calculates LDA+DMFT total energy
    if( imenergy .and. use_hmlt )   call total_energy

!   Clean Hamiltonian arrays to free memory
 !   call dealloc_hamiltonian

    if( verbos > 0 )  call stop_timer( gk_time )

    call save_sigma

!   Cycle over impurities
    call impurity_cycle

!    if( fit_sigma ) call adjust_sigma

!   CPA
    if( cpa ) call cpa_sigma

    call mix_sigma

!   Write self-energy
    if( verbos > 0 )then
      call rw_sigma( ierr, 'w', 'imag' )
      if( ierr /= 0 ) call stop_program( 'Cannot write one of sigma files.$' )
      call stop_timer( iter_time )
    end if

  end do


!  Does ED job on the real energy mesh
  if( istart == 2 )then
    if( verbos > 0 )  write(14,"(80('='),/,10x,'Real axis calculations after self-consistency')")

    alpha = 1
    impurity(:)%fit = 0
    impurity(:)%fix_ed = .true.

    call use_real_mesh

    call impurity_cycle

    if( verbos > 0 )  call rw_sigma( ierr, 'w', 'real' )
  end if

  if( verbos > 0 )  call stop_timer( total_time )

  call finalize_mpi

end program amulet