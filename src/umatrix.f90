subroutine umatrix( umtrx, l, F )
!
!   Make <ij|V|kl> matrix of Coulomb interaction
!   (eq. (10) from JPCM 9, 767 (1997))
!   ATTENTION: The output matrix is in basis of complex spherical harmonics
!
  use math_module

  implicit none 

  integer, intent(in   ) :: l
  real(8), intent(in   ) :: F(0:6)
  real(8), intent(  out) :: umtrx(2*l+1,2*l+1,2*l+1,2*l+1)
  
  integer :: m1, m2, m3, m4, k

  umtrx = 0
  
  do m1 = -l, l
    do m2 = -l, l
      do m3 = -l, l
        do m4 = -l, l
          do k = 0, 2*l+1, 2
            umtrx(m1+l+1,m2+l+1,m3+l+1,m4+l+1) = umtrx(m1+l+1,m2+l+1,m3+l+1,m4+l+1) +   &
                                         F(k) * a( k, l, m1, m2, m3, m4 )
          end do
        end do
      end do
    end do
  end do
    
  
  contains
  
  real(8) function a( k, l, m1, m2, m3, m4 )
    integer, intent(in   ) :: k, l
    integer, intent(in   ) :: m1, m2, m3, m4
    
    integer :: q
    
    a = 0
    do q = -k, k
      a = a + ClebschGordan( l,m3, k,q, l,m1 )*ClebschGordan( l,m2, k,q, l,m4 )
    end do
    a = a * ClebschGordan( l,0, k,0, l,0 )*ClebschGordan( l,0, k,0, l,0 )
  
  end function a

end subroutine umatrix
