subroutine rw_vns( itype, ierr, rw, Vns, nfield, Ltau )
  use mpi_module

#ifdef MPI
  use mpi
#endif

  implicit none

  integer :: itype
  integer :: ierr
  integer :: nfield
  integer :: Ltau
  real(8) :: Vns(Ltau,nfield)
  character(1) :: rw           !   Read/write switch

  integer :: nmp, nfl, Lt
  integer :: i, j, k
  character(16) :: fname, acha
  logical       :: fexst

  ierr  = 0

  write(fname,*) itype
  fname = 'vns_'//trim(adjustl(fname))
!
!       Read Ising-spin configuration
!
  if( rw == 'r' )then
    inquire( file=fname, iostat=ierr, exist=fexst )

    if( ierr == 0 .and. fexst )then
      open( 60, file=fname, form='formatted', position='rewind', status='old', iostat=ierr )
      if( ierr == 0 ) read(60,"(12x,i5,/,12x,i5,/,12x,i5)") nmp, nfl, Lt

      if( nfl == nfield .and. Ltau == Lt )then

        do k = 1, nmp*(Lt+3)
          read(60,"(a12,i5)",iostat=ierr,end=100) acha, i

          if( i == proc_id .and. ierr == 0 .and. index( trim(adjustl(acha)), '# proc' ) == 1 )then
            do i = 1, Ltau
              read(60,*) Lt, ( Vns(i,j), j=1,nfield )
            end do
          end if
        end do

 100    continue
      end if    ! nfield
      close(60)
    end if    !   fexst
  else
!
!       Write Ising-spin configuration
!
    do k = 0, numprocs-1
      if( k == proc_id )then

        if( proc_id == master )then
          open( 60, file=fname, form='formatted', position='rewind' )
          write(60,"('# numprocs  ',i5,/,'# nfield    ',i5,/,'# Ltau      ',i5)") numprocs, nfield, Ltau
        else
          open( 60, file=fname, form='formatted', position='append' )
        end if
        write(60,"('# proc      ',i5)") proc_id

        do i = 1, Ltau
          write(60,"(1x,i3,1x,1000(1x,i2))") i, ( int(sign(1.d0,Vns(i,j))), j=1,nfield )
        end do
        write(60,*); write(60,*)
        close(60)
      end if
#ifdef MPI
      call mpi_barrier( mpi_comm_world, ierr )
#endif
    end do
  end if

end subroutine rw_vns

subroutine hf_data_reduce( Gtau, deltaG, ninj, n_orb, Ltau, nsp, acceptance_rate, average_sign )
!
!   Collect data from all processors
!
  use mpi_module

#ifdef MPI
  use mpi
#endif

  implicit none

  integer :: n_orb
  integer :: Ltau
  integer :: nsp
  real(8) :: acceptance_rate
  real(8) :: average_sign
  real(8) :: Gtau(n_orb,n_orb,Ltau,nsp)
  real(8) :: deltaG(n_orb,n_orb,Ltau,nsp)
  real(8) :: ninj(n_orb,n_orb,Ltau,4)

  integer :: i, j, n, ierr
  real(8) :: ddum
  real(8), allocatable :: gtmp(:,:,:,:)

  deltaG = 0.d0

  forall( i = 1:n_orb ) Gtau(i,i,Ltau,:) = - Gtau(i,i,1,:) - 1
  forall( i = 1:n_orb, j = 1:n_orb, i/=j ) Gtau(i,j,Ltau,:) = - Gtau(i,j,1,:)

  ninj(:,:,Ltau,:) = ninj(:,:,1,:)

#ifdef MPI

  deltaG = Gtau

!     Correlator < n_i, n_j >
  allocate( gtmp(n_orb,n_orb,Ltau,4) )
  gtmp = 0.d0
  n    = 4 * n_orb * n_orb * Ltau
  call mpi_reduce( ninj, gtmp, n, mpi_double_precision, mpi_sum, master, mpi_comm_world, ierr )
  ninj = gtmp / numprocs
  deallocate( gtmp )

!     Green function
  allocate( gtmp(n_orb,n_orb,Ltau,nsp) )
  gtmp = 0.d0
  n    = nsp * n_orb * n_orb * Ltau
  call mpi_reduce( Gtau, gtmp, n, mpi_double_precision, mpi_sum, master, mpi_comm_world, ierr )

  Gtau = gtmp / numprocs
  call mpi_bcast( Gtau, n, mpi_double_precision, master, mpi_comm_world, ierr )

!     Error of G(tau) from all processors
  gtmp = ( deltaG - Gtau )**2
  deltaG = 0.d0
  call mpi_reduce( gtmp, deltaG, n, mpi_double_precision, mpi_sum, master, mpi_comm_world, ierr )
  if( numprocs /= 1 ) deltaG = dsqrt( deltaG / real( numprocs*(numprocs-1), 8 ) )

  deallocate( gtmp )

!     Acceptance rate
  call mpi_reduce( acceptance_rate, ddum, 1, mpi_double_precision, mpi_sum, master, mpi_comm_world, ierr )
  acceptance_rate = ddum / numprocs

!     Average sign
  call mpi_reduce( average_sign, ddum, 1, mpi_double_precision, mpi_sum, master, mpi_comm_world, ierr )
  average_sign = ddum / numprocs

#endif

  if( proc_id == master )                          &
      write(14,"(' Average sign',8x,f10.6,/,' Acceptance rate',5x,f10.6)") average_sign, acceptance_rate

end subroutine hf_data_reduce