module moments_module
!    This module contains a set of routines to calculate moments of Green function.
!
!   TODO
!   1. Moments from physics
!
  implicit none

  public def_moments_w, get_moments, moments_of_sigma

  interface get_moments
    module procedure get_moments4
  end interface 

  contains

  subroutine moments_of_gf( moms, n, nn, u, t, mu )
!
!    Calculates moments of Green function for density-density type of interaction in Hubbard model.
!   See PhD Thesis of Gull (eqs. B.26-B.31) and my notes.
!
    real(8) :: moms(:,:,:)            !  Moments of self-energy
    real(8) :: n(:)                   !  Correlators, < n >
    real(8) :: nn(:,:)                !  Correlators, < n n >
    real(8) :: u(:,:)                 !  Interaction matrix
    real(8) :: t(:,:)                 !  Hopping matrix
    real(8) :: mu(:)                  !  Energy level

    integer :: i, j, k, n_orb

    n_orb = size( moms, dim=2 )

    moms = 0.d0

!     Definition of first moments
    forall( i = 1:n_orb ) moms(1,i,i) = 1.d0

!     Definition of second moments
    moms(2,:,:) = t
    forall( i = 1:n_orb ) moms(2,i,i) = moms(2,i,i) - mu(i)

    do i = 1, n_orb

        do k = 1, n_orb
   !       moms(2,i,i) = moms(2,i,i) + 2 * u(i,k) * n(k) + mu
          moms(2,i,i) = moms(2,i,i) + u(i,k) * n(k)
        end do

    end do

  end subroutine moments_of_gf

  subroutine moments_of_sigma( moms, n, nn, u )
!
!    Calculates moments of self-energy for density-density type of interaction in Hubbard model.
!   See PhD Thesis of Gull (eqs. B.36-B.37) and my notes.
!
    real(8) :: moms(:,:,:)            !  Moments of self-energy
    real(8) :: n(:)                   !  Correlators, < n >
    real(8) :: nn(:,:)                !  Correlators, < n n >
    real(8) :: u(:,:)                 !  Interaction matrix

    integer :: i, j, k, n_orb

    n_orb = size( moms, dim=2 )          !  Degeneracy

    moms = 0.d0

!     Definition of constant part (Hartree-Fock)

    do i = 1, n_orb
      do j = 1, n_orb
        moms(1,i,i) = moms(1,i,i) + u(i,j)*n(j)

        do k = 1, n_orb
          if( k == j )then
            moms(2,i,i) = moms(2,i,i) + u(i,j)*u(i,k)*( n(k) - n(j)*n(k) )
          else
            moms(2,i,i) = moms(2,i,i) + u(i,j)*u(i,k)*( nn(j,k) - n(j)*n(k) )
          end if
        end do

      end do
    end do

  end subroutine moments_of_sigma


!     Obtain numerically moments of function
  subroutine get_moments4( fw, xw, moms, lmoms )
    complex(8) :: fw(:,:,:,:)
    real(8)    :: xw(:)
    real(8)    :: moms(:,:,:,:)
    logical    :: lmoms(:)
   
    integer :: i, j, is, n, ns
   
    n  = size( fw, dim=1 )
    ns = size( fw, dim=4 )

    do is = 1, ns
      do i = 1, n
        do j = 1, n
   !       if( i == j ) moms(2,i,j,is) = 1
         
          call def_moments_w( fw(i,j,:,is), xw, moms(:,i,j,is), lmoms )
                             
       end do
      end do
    end do

  end subroutine get_moments4

  subroutine def_moments_w( func, x, moment, lm )
!
!   This subroutine numerically defines moments of complex function
!   using high-energy expansion of the form:
!      f(x) = M0 + M1/x + M2/(x*x) + M3/(x*x*x)
!   lm is a logical switch that defines to calculate or not the corresponding moment.
!
    implicit none
    
    complex(8) :: func(:)
    real(8)    :: x(:)
    real(8)    :: moment(0:3)
    logical    :: lm(0:3)
    
    integer :: icase
    integer :: i, n
    real(8) :: a, b, c, d, e, f, g
    
    icase = 0
    do i = 0, 3
      if( lm(i) ) icase = icase + 2**i
    end do
    
 !   write(6,*) ' icase ', icase, moment
    
    if( icase == 0 ) return
    
    n = size( x )
    if( n /= size( func ) ) stop
    
    a = sum( 1/(x*x) )
    b = sum( 1/(x*x*x*x) )
    c = sum( 1/(x*x*x*x*x*x) )
    
    d = aimag( sum( func/x ) )
    e = aimag( sum( func/(x*x*x) ) )
    f = real( sum( func/(x*x) ), 8 )
    g = real( sum( func ), 8 )
    
 !   e = 0.0
 !   do i = n, 1, -1
 !     e = e + aimag(func(i)) / ( x(i)*x(i)*x(i) )
 !   end do
    
!    write(6,*) 'in moments module'
!    write(6,*) a, b, c, d, e, f, g
  
    select case (icase)
      case(1)
        moment(0) = ( g + moment(2)*a ) / n
      case(2)
        moment(1) = ( moment(3)*b - d ) / a
      case(3)
        moment(0) = ( g + moment(2)*a ) / n
        moment(1) = ( moment(3)*b - d ) / a
      case(4)
        moment(2) = ( moment(0)*a - f ) / b
      case(5)
        moment(0) = ( b*g - f*a ) / ( b*n - a*a )
        moment(2) = ( a*g - f*n ) / ( b*n - a*a )
      case(6)
        moment(1) = ( moment(3)*b - d ) / a
        moment(2) = ( moment(0)*a - f ) / b
      case(7)
        moment(0) = ( b*g - f*a ) / ( b*n - a*a )
        moment(1) = ( moment(3)*b - d ) / a
        moment(2) = ( a*g - f*n ) / ( b*n - a*a )
      case(8)
        moment(3) = ( moment(1)*b + e ) / c
      case(9)
        moment(0) = ( g + moment(2)*a ) / n
        moment(3) = ( moment(1)*b + e ) / c
      case(10)
        moment(1) = ( b*e - c*d ) / ( a*c - b*b )
        moment(3) = ( a*e - b*d ) / ( a*c - b*b )
      case(11)
        moment(0) = ( g + moment(2)*a ) / n
        moment(1) = ( b*e - c*d ) / ( a*c - b*b )
        moment(3) = ( a*e - b*d ) / ( a*c - b*b )
      case(12)
        moment(2) = ( moment(0)*a - f ) / b
        moment(3) = ( moment(1)*b + e ) / c
      case(13)
        moment(0) = ( b*g - f*a ) / ( b*n - a*a )
        moment(2) = ( a*g - f*n ) / ( b*n - a*a )
        moment(3) = ( moment(1)*b + e ) / c
      case(14)
        moment(1) = ( b*e - c*d ) / ( a*c - b*b )
        moment(2) = ( moment(0)*a - f ) / b
        moment(3) = ( a*e - b*d ) / ( a*c - b*b )
      case(15)
        moment(0) = ( b*g - f*a ) / ( b*n - a*a )
        moment(1) = ( b*e - c*d ) / ( a*c - b*b )
        moment(2) = ( a*g - f*n ) / ( b*n - a*a )
        moment(3) = ( a*e - b*d ) / ( a*c - b*b )
      case default
        stop
    end select
  
  end subroutine def_moments_w
  
end module moments_module
