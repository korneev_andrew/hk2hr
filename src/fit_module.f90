module fit_module
!   This module collects a number of routines to fit function.
  use mpi_module

  implicit none 

  private  
  integer, parameter :: outf = 14     ! output file

  public least_square_fitting, svd_lsf
  public f_delta, f_delta_num, f_g0

  contains

  subroutine svd_lsf( x, y, sigma, n, m, a, iafix, p, tola, tolchi, info, funcs )
!
! This is non linear least square fitting subroutine with use of SVD
!
    implicit none
  
    integer, intent(in   ) :: n      
    integer, intent(in   ) :: m     
    integer, intent(in   ) :: p
    integer, intent(  out) :: info    
    integer, intent(in   ) :: iafix(p)     
    real(8), intent(in   ) :: tola
    real(8), intent(in   ) :: tolchi
    real(8), intent(inout) :: a(p)   
    real(8), intent(in   ) :: sigma(m,m,n)
    complex(8), intent(in   ) :: x(n)   
    complex(8), intent(in   ) :: y(m,m,n)   
    
    external funcs
  
    real(8) :: chi_square_a, chi_square_ada

    integer :: i, j, iscf, icount, nd
    real(8) :: lambda, w_min, w_max, s
    real(8), allocatable :: alpha(:,:), beta(:), da(:)
    real(8), allocatable :: alpha_lambda(:,:), w(:), u(:,:), vt(:,:)
  
    real(8) :: work(10*p)

    if( all( iafix == 0 ) ) return
 
    lambda = 5.5
    
    nd = sum( iafix )
    allocate( alpha(nd,nd), beta(nd), da(nd), alpha_lambda(nd,nd), w(nd), u(nd,nd), vt(nd,nd) )
!  
!    Calculate chi_square, gradient and Hessian matrix 
!    at initial values of parameters {a}
!  
    call mrqcof( x, y, sigma, n, m, a, iafix, p, alpha_lambda, beta, nd, chi_square_a, funcs )
!  
!   Minimization cycle
!  
    icount = 0
    do iscf = 1, 60000
!
!     SVD to calculate da
!
      alpha_lambda = alpha
  !      forall( i = 1:p ) alpha_lambda(i,i) = alpha_lambda(i,i) * ( 1.d0 + lambda )
      forall( i = 1:nd ) alpha_lambda(i,i) = alpha_lambda(i,i) + lambda 
  
      call dgesvd( 'A', 'A', nd, nd, alpha_lambda, nd, w, u, nd, vt, nd, work, 10*p, info )
      if ( info /= 0 .and. proc_id == master ) write(outf,*) 'SVD info = ', info
!
!     Check for the bad values of w and nullify them in ill-conditioned cases
!  
      w_max = maxval(w)
      w_min = w_max * n * tiny(1.d0)
    
      do i = 1, nd
        if( w(i) < w_min )then
          w(i) = 0
          if ( proc_id == master ) write(outf,*) ' w(',i,') was nullified'
        end if
      end do
!
!    Calculate new da after SVD
!
      do i = 1, nd
        s = 0
        if( w(i) /= 0 )then 
          do j = 1, nd
            s = s + u(j,i)*beta(j)
          end do
          s = s / w(i) 
        end if
        work(i) = s
      end do 
   
      da = matmul( transpose( vt ), work(1:nd) )
!
!     Build a+da (stored in work array) and 
!     calculate chi_square for a new set of parameters
!
      j = 0
      work(1:p) = a
      do i = 1, p
        if( iafix(i) == 0 ) cycle
              
        j = j + 1
        work(i) = work(i) + da(j)
      end do

      call mrqcof( x, y, sigma, n, m, work(1:p), iafix, p, alpha_lambda, da, nd, chi_square_ada, funcs )
    
      if( lambda == 0 ) exit
    
      if( chi_square_ada < chi_square_a+tolchi )then
!
!       Step accepted
!       Decrease lambda
!
        a = work(1:p)
        alpha = alpha_lambda
        beta = da
        chi_square_a = chi_square_ada
        lambda = lambda / 10
      else
!
!       Step rejected
!       Increase lambda
!
        lambda = lambda * 7
      end if
!
!   Check for convergence
!    
   ! if( all( dabs(da) < tola )      .or.                            &
   !     dabs( chi_square_a - chi_square_ada ) < tolchi )then
      if( all( abs(da) < tola ) .or. all( abs(beta) < tolchi ) )then
        icount = icount + 1
        if( icount == 10 ) lambda = 0
      else
        icount = 0
      end if
      
    end do
  
    if( all( abs(da) < tola ) ) info = 1
    if( abs( chi_square_a - chi_square_ada ) < tolchi ) info = 2
    if( icount == 10 ) info = 0
  
    deallocate( alpha, beta, da, alpha_lambda, w, u, vt )
    
  end subroutine svd_lsf

  subroutine mrqcof( x, y, sigma, n, m, a, iafix, p, alpha, beta, np, chisq, funcs )
!     Calculates Hessian matrix, derivative of merrit function and chi2
    implicit none
    
    
    integer, intent(in   ) :: n      
    integer, intent(in   ) :: m     
    integer, intent(in   ) :: p
    integer, intent(in   ) :: np
    integer, intent(in   ) :: iafix(p)     
    real(8), intent(in   ) :: a(p)   
    real(8), intent(in   ) :: sigma(m,m,n)
    complex(8), intent(in   ) :: x(n)   
    complex(8), intent(in   ) :: y(m,m,n)   
    
    real(8), intent(  out) :: alpha(np,np)
    real(8), intent(  out) :: beta(np)
    real(8), intent(  out) :: chisq
    
    external funcs
  
    integer :: i, j, k, m1, m2, i1, j1
    real(8) :: sig2i
    complex(8) :: dy, yf
    complex(8), allocatable :: dyda(:)
  
    allocate( dyda(p) )
    
    alpha = 0
    beta  = 0
    chisq = 0
    
    do k = 1, n
      do i = 1, m
        do j = 1, m
          call funcs( x(k), a, yf, dyda, p, m, i, j )

          sig2i = sigma(i,j,k) * sigma(i,j,k) 
          dy = y(i,j,k) - yf
   
!         chi^2
          chisq = chisq  +  dy * conjg(dy) / sig2i
          
          m1 = 0
          do i1 = 1, p
            if( iafix(i1) == 0 ) cycle
            
            m1 = m1 + 1

!          beta = - dchi^2/da
            beta(m1) = beta(m1)  +  real( dyda(m1)*conjg(dy), 8 ) / sig2i
            
            m2 = 0
            do j1 = 1, p
              if( iafix(j1) == 0 ) cycle
              
              m2 = m2 + 1

!           Hessian matrix, alpha = d^2chi^2/da/da
              alpha(m1,m2) = alpha(m1,m2) + real( dyda(m1)*conjg(dyda(m2)), 8 ) / sig2i
            end do
          end do
        end do 
      end do
    end do    !   n
    
    chisq = chisq / n
    beta  = beta  / n
    alpha = alpha / n
    
    deallocate( dyda )

  end subroutine mrqcof

  subroutine least_square_fitting( x, y, sigma, n, m, a, iafix, p, tola, tolchi, info )

! This is non linear least square fitting subroutine

    implicit none
  
    integer, intent(in   ) :: n      
    integer, intent(in   ) :: m     
    integer, intent(in   ) :: p
    integer, intent(  out) :: info    
    integer, intent(in   ) :: iafix(p)     
    real(8), intent(in   ) :: tola
    real(8), intent(in   ) :: tolchi
    real(8), intent(inout) :: a(p)   
    real(8), intent(in   ) :: sigma(m,m,n)
    complex(8), intent(in   ) :: x(n)   
    complex(8), intent(in   ) :: y(m,m,n)   
  
    real(8) :: chi_square_a, chi_square_ada

    integer :: i, j, iscf, icount
    real(8) :: lambda, w_min, w_max, s
    real(8) :: alpha(p,p), beta(p), da(p)
    real(8) :: alpha_lambda(p,p), w(p), u(p,p), vt(p,p)
  
    real(8) :: work(10*p)
!
!   Checks that all elements of iafix are 0 or 1 
!
    if( any( iafix < 0 ) .or. any( iafix > 1 ) )then
      if ( proc_id == master ) write(outf,*) ' Wrong elements of iafix'
      info = 100
      return
    endif
 
    if( all( iafix == 0 ) ) return
 
    lambda = 5.5d0
!  
!    Calculate chi_square, gradient and Hessian matrix 
!    at initial values of parameters {a}
!  

    call chi2( x, y, n, m, a, p, chi_square_a, 1, sigma, alpha, beta )
    call chi2( x, y, n, m, a, p, chi_square_a, 2, sigma, alpha, beta )

!
!   Nullify the derivative and Hessian matrix of the elements to be fixed
!  
    forall( i = 1:p, iafix(i) == 0 ) beta(i) = 0.d0
    forall( i = 1:p, j = 1:p, iafix(i) == 0 .or. iafix(j) == 0  ) alpha(i,j) = 0.d0
    forall( i = 1:p, iafix(i) == 0   ) alpha(i,i) = 1.d0
!  
!   Minimization cycle
!  
    icount = 0
    do iscf = 1, 60000
 !    if ( proc_id == master ) then
 !      write(outf,*)'---------- Debug print out --------------- iter # ', iscf
 !      do i = 1, p
 !        write(outf,"(1x,i3,5(1x,e15.8))") i, a(i), da(i), beta(i)
 !        write(outf,"(1x,i3,5(1x,f20.12))") i, a(i), da(i), beta(i)
 !      end do
 !    end if
!
!     alpha = alpha + lambda
!  
      alpha_lambda = alpha
  !    forall( i = 1:p ) alpha_lambda(i,i) = alpha_lambda(i,i) * ( 1.d0 + lambda )
      forall( i = 1:p ) alpha_lambda(i,i) = alpha_lambda(i,i) + lambda 
!
!     SVD to calculate da
!
      call dgesvd( 'A', 'A', p, p, alpha_lambda, p, w, u, p, vt, p, work, 10*p, info )
      if ( info /= 0 .and. proc_id == master ) write(outf,*) 'SVD info = ', info
!
!     Check for the bad values of w and nullify them in ill-conditioned cases
!  
      w_max = maxval(w)
      w_min = w_max * n * tiny(1.d0)
    
      do i = 1, p
        if( w(i) < w_min )then
          w(i) = 0.d0
          if ( proc_id == master ) write(outf,*) ' w(',i,') was nullified'
        end if
      end do
!
!    Calculate new da after SVD
!
      do i = 1, p
        s = 0.d0
        if( w(i) /= 0.d0 )then 
          do j = 1, p
            s = s + u(j,i)*beta(j)
          end do
          s = s / w(i) 
        end if
        work(i) = s
      end do 
   
      da = matmul( transpose( vt ), work(1:p) )
      forall( i = 1:p, iafix(i) == 0 ) da(i) = 0.d0
!
!     Build a+da (stored in work array) and 
!     calculate chi_square for a new set of parameters
!
      work(1:p) = a + da
      call chi2( x, y, n, m, work(1:p), p, chi_square_ada, 1, sigma, alpha, beta )
    
      if( lambda == 0.d0 ) exit
    
      if( chi_square_ada < chi_square_a+tolchi )then
!
!       Step accepted
!       Update first derivatives and Hessian matrix
!       Decrease lambda
!
        chi_square_a = chi_square_ada
        a = work(1:p)
        call chi2( x, y, n, m, a, p, chi_square_a, 2, sigma, alpha, beta )

        forall( i = 1:p, iafix(i) == 0 ) beta(i) = 0.d0
        forall( i = 1:p, j = 1:p, iafix(i) == 0 .or. iafix(j) == 0  ) alpha(i,j) = 0.d0
        forall( i = 1:p, iafix(i) == 0   ) alpha(i,i) = 1.d0

        lambda = lambda / 10.d0
      else
!
!       Step rejected
!       Increase lambda
!
        lambda = lambda * 7
      end if
!
!   Check for convergence
!    
   ! if( all( dabs(da) < tola )      .or.                            &
   !     dabs( chi_square_a - chi_square_ada ) < tolchi )then
      if( all( abs(da) < tola ) .or. all( abs(beta) < tolchi ) )then
        icount = icount + 1
        if( icount == 10 ) lambda = 0.d0
      else
        icount = 0
      end if
    
    end do
  
    if( all( abs(da) < tola ) ) info = 1
    if( abs( chi_square_a - chi_square_ada ) < tolchi ) info = 2
    if( icount == 10 ) info = 0
  
    if ( proc_id == master )   write(outf,"(/,'  Convergence has been reached after ',i7,          &
                                             &' iterations (icount=',i2,')')") iscf, icount
  
    if ( proc_id == master ) write(outf,*) ' Chi square = ', chi_square_a

!
!   Uncomment for more info if needed
!  
!  if ( proc_id == master ) write(outf,"(/,10x,'Gradient of the chi_square (beta)')")
!  if ( proc_id == master ) write(outf,"('   i    d chi_square / da(i)         da ',/)")
!  do i = 1, p
!      if ( proc_id == master ) write(outf,"(1x,i3,5(1x,f20.12))") i, a(i), da(i), beta(i)
!  end do
  
!
!   Store covariance matrix
! 
 ! alpha = 0.d0
 ! do j = 1, m 
 !   do k = 1, m
 !     do i = 1, m
 !       if( w(i) /= 0.d0 )then
 !         alpha(j,k) = alpha(j,k) + vt(i,j)*u(k,i) / w(i)
 !       end if
 !     end do
 !   end do 
 ! end do 
 !  
 !  write(outf,*)
 !  write(outf,*)'  Covariance matrix'
 !  write(outf,*)
 !  do i = 1, m
 !    write(outf,"(50(1x,f12.4))") ( alpha(i,j), j=1,m )
 !  end do
  
  end subroutine least_square_fitting
!====================================================================

  complex(8) function delta_analytical( x, e, V, n, m )
    implicit none
    
    integer, intent(in   ) :: n, m
    real(8), intent(in   ) :: e(n), V(n,m)
    complex(8), intent(in   ) :: x
    
    integer :: i, m1, m2
  
    delta_analytical = cmplx(0.d0,0.d0,8)
    do i = 1, n
      do m1 = 1, m
       do m2 = 1, m
         delta_analytical = delta_analytical  + V(i,m1) * V(i,m2) / ( x - e(i) )
       end do 
      end do
    end do
    
  end function delta_analytical

  subroutine f_g0( x, a, yf, dyda, p, m, i, j )
    implicit none

    integer, intent(in   ) :: i, j   
    integer, intent(in   ) :: m     
    integer, intent(in   ) :: p
    real(8), intent(in   ) :: a(p)
    complex(8), intent(in   ) :: x
    complex(8), intent(  out) :: yf
    complex(8), intent(  out) :: dyda(*)
    

    integer :: nbath, ib, k, m1
    real(8), allocatable :: e(:), V(:,:)

    if( mod(p-1,m+1) /= 0 ) stop ' Error in f_g0'
    
    nbath = ( p - 1 ) / ( m + 1 )
    
!   Define array of bath energies and hybridization parameters

    allocate( e(nbath), V(nbath,m) )
  
    do ib = 1, nbath
      e(ib)   = a(ib)   
      V(ib,:) = a(nbath+1+(ib-1)*m:nbath+ib*m)
    end do
    
!  y(x,a)
    yf = delta_analytical( x, e, V, nbath, m )
    yf = 1 / ( x - a(p) - yf )
    
    k = 0
        
!  d chi^2/ d e(i)
    do ib = 1, nbath
      k = k + 1
      dyda(k) = V(k,i) * V(k,j) / ( x - e(k) )**2
      dyda(k) = dyda(k) * yf * yf
    end do   ! ib1, nbahh

!  d chi^2/ d V(i,m)
    do ib = 1, nbath
      do m1 = 1, m
        k = k + 1
        dyda(k) = V(ib,j) / ( x - e(ib) )
        if( i == j ) dyda(k) = 2 * dyda(k)
        dyda(k) = dyda(k) * yf * yf
      end do
    end do   ! ib1, nbahh

!  d chi^2/ d eps
    dyda(p) = yf * yf
    
    deallocate( e, V )
    
  end subroutine f_g0

  subroutine f_delta_num( x, a, yf, dyda, p, m, i, j )
    implicit none

    integer, intent(in   ) :: i, j   
    integer, intent(in   ) :: m     
    integer, intent(in   ) :: p
    real(8), intent(in   ) :: a(p)
    complex(8), intent(in   ) :: x
    complex(8), intent(  out) :: yf
    complex(8), intent(  out) :: dyda(*)
    

    integer :: nbath, ib, k, m1
    real(8) :: h
    real(8), allocatable :: e(:), ea(:), V(:,:), Va(:,:)
    complex(8) :: yfa

    if( mod(p,m+1) /= 0 ) stop ' Error in f_delta'
    
    nbath = p / ( m + 1 )
    
!   Define array of bath energies and hybridization parameters

    allocate( e(nbath), V(nbath,m), ea(nbath), Va(nbath,m) )
  
    do ib = 1, nbath
      e(ib)   = a(ib)   
      V(ib,:) = a(nbath+1+(ib-1)*m:nbath+ib*m)
    end do
    
!  y(x,a)
    yf = delta_analytical( x, e, V, nbath, m )
    
    k = 0
        
!  d chi^2/ d e(i)
    do ib = 1, nbath
      k = k + 1
      h = 1.e-8 * e(ib)
      if( h == 0.e0 ) h = 1.e-8
      ea = e
      ea(ib) = ea(ib) + h
      yfa = delta_analytical( x, ea, V, nbath, m )
      dyda(k) = ( yfa - yf ) / h
    end do   ! ib1, nbahh

!  d chi^2/ d V(i,m)
    do ib = 1, nbath
      do m1 = 1, m
        k = k + 1
        h = 1.e-8 * V(ib,j)
        if( h == 0.e0 ) h = 1.e-8
        Va = V
        Va(ib,j) = Va(ib,j) + h
        yfa = delta_analytical( x, e, Va, nbath, m )
        dyda(k) = ( yfa - yf ) / h
      end do
    end do   ! ib1, nbahh
    
    deallocate( e, ea, V, Va )
    
  end subroutine f_delta_num

  subroutine f_delta( x, a, yf, dyda, p, m, i, j )
    implicit none

    integer, intent(in   ) :: i, j   
    integer, intent(in   ) :: m     
    integer, intent(in   ) :: p
    real(8), intent(in   ) :: a(p)
    complex(8), intent(in   ) :: x
    complex(8), intent(  out) :: yf
    complex(8), intent(  out) :: dyda(*)
    

    integer :: nbath, ib, k, m1
    real(8), allocatable :: e(:), V(:,:)

    if( mod(p,m+1) /= 0 ) stop ' Error in f_delta'
    
    nbath = p / ( m + 1 )
    
!   Define array of bath energies and hybridization parameters

    allocate( e(nbath), V(nbath,m) )
  
    do ib = 1, nbath
      e(ib)   = a(ib)   
      V(ib,:) = a(nbath+1+(ib-1)*m:nbath+ib*m)
    end do

!  y(x,a)
    yf = delta_analytical( x, e, V, nbath, m )
    
    k = 0
        
!  d chi^2/ d e(i)
    do ib = 1, nbath
      k = k + 1
      dyda(k) = V(k,i) * V(k,j) / ( x - e(k) )**2
    end do   ! ib1, nbahh
    
!  d chi^2/ d V(i,m)
    do ib = 1, nbath
      do m1 = 1, m
        k = k + 1
        dyda(k) = V(ib,j) / ( x - e(ib) )
        if( i == j ) dyda(k) = 2 * dyda(k)
      end do
    end do   ! ib1, nbahh
    
    deallocate( e, V )
    
  end subroutine f_delta

  subroutine chi2( x, y, n, m, a, p, chi_square, mode, sigma, alpha, beta )
!
!   Calculate merit function, chi^2, derivative of it with respect
!  to fitting parameters and Hessian matrix 
!
!                                V_{km}*V_{km'}
!    Delta_{m,m'}(x_n) = sum_k  ---------------
!                                  x_n - e_k
!
    implicit none

    integer, intent(in   ) :: p              !  # of fitting parameters
    integer, intent(in   ) :: mode           !  mode of calculation
    integer, intent(in   ) :: n              !  # of points in function
    integer, intent(in   ) :: m              !  dimension of y
    real(8), intent(in   ) :: a(p)           !  vector of fitting parameters 
    real(8), intent(in   ) :: sigma(m,m,n)   !  standard deviation for y
    complex(8), intent(in   ) :: x(n)           !  x-mesh
    complex(8), intent(in   ) :: y(m,m,n)       !  data points on the x-mesh

    real(8), intent(  out) :: chi_square     !  merit function, chi^2
    real(8), intent(  out) :: alpha(p,p)     !  Hessian matrix
    real(8), intent(  out) :: beta(p)        !  gradient of chi^2
 
    integer :: nbath
  
    real(8), allocatable :: e(:), V(:,:)
    complex(8) :: Delta(m,m,n)
  
    integer :: i, j, k, iw, m1, m2, ib1, ib2
    real(8) :: dtmp1, dtmp2
    complex(8) :: ztmp1
  

    if( mod(p,m+1) /= 0 ) stop ' Error in chi2'
    
    nbath = p / ( m + 1 )

!   Define array of bath energies and hybridization parameters

    allocate( e(nbath), V(nbath,m) )
  
    do i = 1, nbath
      e(i)   = a(i)   
      V(i,:) = a(nbath+1+(i-1)*m:nbath+i*m)
!     write(outf,"(100(1x,f8.4))") e(i), ( V(i,m1), m1 = 1,m )
    end do

!   Calculate difference between the numerical result and fitting function
    Delta = y
    do m1 = 1, m
     do m2 = 1, m
      do iw = 1, n
       do k = 1, nbath
         Delta(m1,m2,iw) = Delta(m1,m2,iw) - V(k,m1) * V(k,m2) / ( x(iw) - e(k) )
       end do 
      end do
     end do
    end do
    Delta = conjg( Delta )

    select case (mode)
      case(1)
!
  !   Calculate chi square
!    
        chi_square = 0
        do m1 = 1, m
         do m2 = 1, m
          do iw = 1, n
            chi_square = chi_square + abs( Delta(m1,m2,iw) )**2 / sigma(m1,m2,iw) / sigma(m1,m2,iw)
          end do 
         end do
        end do 
        chi_square = chi_square / n
 
!       Uncomment for debug
!       write(outf,*) ' Chi square ', chi_square

      case(2)
!
!     Calculate gradient of chi^2 and Hessian matrix
!
!       Gradient of chi^2
        i     = 0
        beta  = 0
        do ib1 = 1, nbath
          i = i + 1
        
        !  d chi^2/ d e(i)
          do m1 = 1, m
            do m2 = 1, m
              dtmp1 = V(ib1,m1) * V(ib1,m2)
              do iw = 1, n
                ztmp1 = dtmp1 * Delta(m1,m2,iw) / ( x(iw) - e(ib1) )**2
                beta(i) = beta(i) + real( ztmp1, 8 ) / sigma(m1,m2,iw) / sigma(m1,m2,iw)
              end do
            end do
          end do
        end do   ! ib1, nbahh

        !  d chi^2/ d V(i,m)
        do ib1 = 1, nbath
          do m1 = 1, m
            i = i + 1
          
            do m2 = 1, m
              dtmp1 = V(ib1,m2)
              do iw = 1, n
                ztmp1 = dtmp1 * ( Delta(m1,m2,iw) + Delta(m2,m1,iw) ) / ( x(iw) - e(ib1) )
                beta(i) = beta(i) + real( ztmp1, 8 ) / sigma(m1,m2,iw) / sigma(m1,m2,iw)
              end do
            end do
          end do
        end do   ! ib1, nbahh
      
        beta = beta / n
 
!       Uncomment for debug
!       write(outf,*) ' Beta'
!       do ib1 = 1, p
!         write(outf,*) beta(ib1)
!       end do

!       Hessian matrix
        i     = 0
        alpha = 0
        do ib1 = 1, nbath
          i = i + 1
          j = 0  

        !  d^2 chi^2/ d e(i) d e(j)
          do ib2 = 1, nbath
            j = j + 1
            do m1 = 1, m
              do m2 = 1, m
                dtmp1 = V(ib1,m1) * V(ib1,m2)
                dtmp2 = dtmp1 * V(ib2,m1) * V(ib2,m2)
                do iw = 1, n
                  ztmp1 = dtmp2 / ( x(iw) - e(ib1) )**2 / ( conjg(x(iw)) - e(ib2) )**2
                  alpha(i,j) = alpha(i,j) + real( ztmp1, 8 ) / sigma(m1,m2,iw) / sigma(m1,m2,iw)
        !         if( ib1 == ib2 )then
        !          ztmp2 = dtmp1 * Delta(m1,m2,iw) / ( x(iw) - e(ib1) )**3
        !          alpha(i,j) = alpha(i,j) - real(ztmp2,8) / sigma(m1,m2,iw) / sigma(m1,m2,iw)
        !        end if
                end do
              end do
            end do
          end do ! ib2, nbath
 
        !  d^2 chi^2/ d V(i,m) d e(j)
          do ib2 = 1, nbath
            do m1 = 1, m
              j = j + 1
          
              do m2 = 1, m
                dtmp1 = 2 * V(ib1,m2) * V(ib2,m1) * V(ib2,m2)
                do iw = 1, n
                  ztmp1 = dtmp1 / ( x(iw) - e(ib1) ) / ( conjg(x(iw)) - e(ib2) )**2
                  alpha(i,j) = alpha(i,j) + real( ztmp1, 8 ) / sigma(m1,m2,iw) / sigma(m1,m2,iw)
        !        if( ib1 == ib2 )then
        !          ztmp2 = V(ib1,m2) * ( Delta(m1,m2,iw) + Delta(m2,m1,iw) ) / ( x(iw) - e(ib1) )**2
        !          alpha(i,j) = alpha(i,j) - real(ztmp2,8) / sigma(m1,m2,iw) / sigma(m1,m2,iw)
        !        end if
                end do
              end do
            end do

          end do ! ib2, nbath
        end do   ! ib1

!       Uncomment for debug
!       write(outf,*)  ' OK', i, j, nbath, n
!       do ib1 = 1, nbath
!         write(outf,"(100(1x,f8.4))") e(ib1), ( V(ib1,m1), m1 = 1,m )
!       end do

        !  d^2 chi^2/ d V(i,m) d V(j,m')
        i = nbath 
        do ib1 = 1, nbath
          do m1 = 1, m
            i = i + 1
            j = nbath 
           
            do ib2 = 1, nbath
              do m2 = 1, m
                j = j + 1
              
                dtmp1 = 2 * V(ib1,m1) * V(ib2,m2)
                do iw = 1, n
                  ztmp1 = dtmp1 / ( x(iw) - e(ib1) ) / ( conjg(x(iw)) - e(ib2) ) 
                  alpha(i,j) = alpha(i,j) + real(ztmp1,8) / sigma(m1,m2,iw) / sigma(m1,m2,iw)

       !          if( ib1 == ib2 )then
       !           ztmp2 = ( Delta(m1,m2,iw) + Delta(m2,m1,iw) ) / ( x(iw) - e(ib1) )
       !           alpha(i,j) = alpha(i,j) + real(ztmp2,8) / sigma(m1,m2,iw) / sigma(m1,m2,iw)
       !         end if
                end do   ! n
              
              end do  ! m2
            end do !  ib2
          end do   !  m1
        end do   ! ib1, nbahh
      
        do i = 1, p-1
          do j = i+1, p
            alpha(j,i) =  alpha(i,j)
          end do  
        end do 
    
        alpha = alpha / n

!       Uncomment for debug
!       write(outf,*) ' Hessian'
!       do i = 1, p
!          write(outf,"(100f15.8)") ( alpha(i,j), j = 1, p )
!       end do
  
    end select
  
    deallocate( e, V )
  
  end subroutine chi2

end module fit_module
