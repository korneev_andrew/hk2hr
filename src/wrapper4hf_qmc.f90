subroutine wrapper4hf_qmc( itype )
!
!    This is calling sequence for HF-QMC solver
!
  use parameter_module
  use impurity_module
  use iolib_module
  use energy_module
  use moments_module
  use mpi_module
  use symmetry_module
  use hf_qmc_module
  use math_module
  use tools_module

  implicit none
  
  integer, intent(in   ) :: itype

!   Local variables

  integer :: n_orb, nfield
  integer :: i, ierr

  real(8) :: e_d0, average_sign, acceptance_rate, m_z2

  logical :: lmom(0:3)
  real(8), allocatable :: occup(:,:,:), ninj(:,:,:,:), nn(:,:), deltaG(:,:,:,:)

  n_orb = impurity(itype)%nlm

!   Fourier transform of G0 to tau domain
  lmom(0:3) = (/ .false., .false., .true., .true. /)
  impurity(itype)%moments_g0 = 0
  forall( i = 1:n_orb ) impurity(itype)%moments_g0(1,i,i,:) = 1.d0

  call get_moments( impurity(itype)%Green0(:,:,n_energy-ntail:n_energy,:), fermionic(n_energy-ntail:n_energy),         &
                    impurity(itype)%moments_g0, lmom )

  call fourier( impurity(itype)%Green0, impurity(itype)%G0tau, fermionic, impurity(itype)%moments_g0, 1 )

  call set_symmetry( impurity(itype)%G0tau, impurity(itype)%smask )

!     Print out
  if( verbos >= 10 )  call write_function( mkfn('G0tau',itype), impurity(itype)%G0tau, tau_mesh, ierr, 'a' )

!     Change double-counting value according to HF-QMC
  e_d0 = sum( impurity(itype)%dcc ) / nspin
  e_d0 = e_d0 + impurity(itype)%u_value*( n_orb - 0.5 ) - 0.5*impurity(itype)%j_value*( n_orb - 1 )
  e_d0 = -e_d0

  nfield = count( impurity(itype)%ummss > 0 ) / 2
  call vns_ini( Ltau-1, nfield )

  call rw_vns( itype, ierr, 'r', Vns, nfield, Ltau-1 )

  allocate( occup(n_orb,n_orb,2), ninj(n_orb,n_orb,Ltau,4), nn(2*n_orb,2*n_orb), deltaG(n_orb,n_orb,Ltau,nspin) )

!     Call Hirsh-Fye QMC
  call qmc( impurity(itype)%G0tau(:,:,1:Ltau-1,:), impurity(itype)%Gtau(:,:,1:Ltau-1,:), ninj(:,:,1:Ltau-1,:),         &
            beta, e_d0, impurity(itype)%ummss, acceptance_rate, average_sign, impurity(itype)%lninj,                   &
            impurity(itype)%nqmc, impurity(itype)%nqmcwarm )

  call rw_vns( itype, ierr, 'w', Vns, nfield, Ltau-1 )

!     Collect data from processors and print out
  call hf_data_reduce( impurity(itype)%Gtau, deltaG, ninj, n_orb, Ltau, nspin, acceptance_rate, average_sign )

  if( verbos > 0 )then
    call write_function( mkfn('Gtau',itype), impurity(itype)%Gtau, tau_mesh, ierr, 'a' )
    call write_function( mkfn('Gtau_var',itype), deltaG, tau_mesh, ierr, 'a' )

    if( impurity(itype)%lninj )  call write_function( mkfn('ninj',itype), ninj, tau_mesh, ierr, 'a' )
  end if

!     Print out
  if( verbos > 0 )then
!     Occupancy
    occup(:,:,1:nspin) = impurity(itype)%Gtau(:,:,1,:)
    forall( i = 1:n_orb ) occup(i,i,:) = occup(i,i,:) + 1.d0
    if( nspin == 1 ) occup(:,:,2) = occup(:,:,1)

    call write_matrix( 14, occup, 'G(tau=0)$', 'Spin$', 'Number of electrons in spin channel$', ierr )

    write(14,"(' Number of impurity electrons',12x,f12.8,/,' Magnetization',27x,f12.8)")      &
                spur( occup(:,:,2), n_orb ) + spur( occup(:,:,1), n_orb ),                    &
                spur( occup(:,:,2), n_orb ) - spur( occup(:,:,1), n_orb )

!     Correlator
    if( impurity(itype)%lninj )then
      nn(1:n_orb,        1:n_orb        ) = ninj(:,:,1,1)
      nn(n_orb+1:2*n_orb,1:n_orb        ) = ninj(:,:,1,2)
      nn(1:n_orb,        n_orb+1:2*n_orb) = ninj(:,:,1,3)
      nn(n_orb+1:2*n_orb,n_orb+1:2*n_orb) = ninj(:,:,1,4)

      call write_matrix( 14, nn, 'Correlator < n_i,n_j >$', ierr )

!     Instant squared magnetic moment
      m_z2 = sum( nn(1:n_orb,1:n_orb) )                    +                   &
             sum( nn(n_orb+1:2*n_orb,n_orb+1:2*n_orb) )    -                   &
             sum( nn(1:n_orb,n_orb+1:2*n_orb) )            -                   &
             sum( nn(n_orb+1:2*n_orb,1:n_orb) )

      write(14,"(' Instant squared magnetic moment, < m_z^2 >, ',f12.8)") m_z2
    end if

!     Calculate Hartree-Fock energy, etc
    call hartree_fock_energy( impurity(itype)%umtrx, impurity(itype)%ummss, occup, nn, n_orb, nspin )
  end if

  deallocate( occup, ninj, nn, deltaG )

!     Fourier transform Green function to tau domain  
  call fourier( impurity(itype)%Gtau(:,:,1:Ltau-1,:), impurity(itype)%Green, fermionic )

end subroutine wrapper4hf_qmc