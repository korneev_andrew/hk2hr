subroutine rw_sigma( ierr, rw, reim )
!
!    This subroutine reads/writes self-energy to disk
!
  use impurity_module
  use parameter_module
  use energy_module
  use iolib_module

  implicit none

  integer      :: ierr         !   Error status value
  character(1) :: rw           !   Read/write switch
  character(4) :: reim         !   Real/imaginary energy axis switch

  integer :: iunit, itype
  character(16) :: fname, stat, act

  iunit = 22

  do itype = 1, n_imp_type

    write(fname,*) itype
    if( index(reim,'real') == 1 )then
      fname = mkfn( 'sigma', '_re', itype )
    else
      fname = mkfn( 'sigma', itype )
    end if

    if( index(rw,'r') == 1 )then
      stat = 'old'
      act  = 'read'
    else
      stat = 'unknown'
      act  = 'write'
    end if

    open( iunit, file=fname, form='formatted', status=stat, iostat=ierr, action=act )

    if( ierr == 0 )then
!    Start towards self-consistency
      call io_sigma( rw, iunit, ierr, n_energy, impurity(itype)%nlm, nspin, impurity(itype)%Sigma, beta )
    else
!    Start from scratch
      ierr = 0
      impurity(itype)%Sigma = cmplx(0,0,8)
    end if

    close(iunit)
  end do

end subroutine rw_sigma

subroutine io_sigma( ifi, iunit, ierr, Iwmax, nlm, nsp, sigma, beta )
!
!   Read/write impurity related quantities and self-energy from/to file sigma
!
  implicit none

  character(1), intent (in ) :: ifi                        !  Read/write key
  integer,    intent (in   ) :: iunit                      !  Unit specifier
  integer,    intent (  out) :: ierr                       !  Error status value
  integer,    intent (in   ) :: Iwmax                      !  Number of energy points
  integer,    intent (in   ) :: nlm                        !  Degeneracy of sigma
  integer,    intent (in   ) :: nsp                        !  Number of spins
  real(8),    intent (in   ) :: beta                       !  Inverse temperature
  complex(8), intent (inout) :: sigma(nlm,nlm,Iwmax,nsp)   !  Self-energy

!  Local variables

  integer :: i, j, is, iom, nvers, Iwmax0, nspin0, nlm0, nsp0
  character(4),  parameter :: vernum = 'v5.0'
  logical :: tag_exist
  real(8) :: ddum, beta0
  complex(8), allocatable :: zdum(:,:,:,:)

  ierr = 0

  if( ifi == 'w' )then

    write(iunit,"('& Beta',/,f12.7,/)")  beta

    write(iunit,"('& nspin',/,i3,/)") nsp

    write(iunit,"('& nlm',/,i3,/)")   nlm

    write(iunit,"('& Iwmax',/,i0,/)") Iwmax

    write(iunit,"('& Sigma')")
    do is = 1, nsp
      do i = 1, nlm
        do j = i, nlm
          write(iunit,*) ( sigma(i,j,iom,is), iom=1,Iwmax )
        end do
      end do
    end do

   else
!
!     Read self-energy
!
    sigma = cmplx(0,0,8)

!   Attempts to read tagged format

    if( tag_exist( iunit, 'beta' ) )then
      read(iunit,*) beta0

      ierr = -1

      if( tag_exist( iunit, 'nspin' ) )then
        read(iunit,*)  nsp0
      else
        return
      end if

      if( tag_exist( iunit, 'nlm' ) )then
        read(iunit,*)  nlm0
      else
        return
      end if

      if( nlm0 /= nlm )then
        ierr = 888
        return
      end if

      if( tag_exist( iunit, 'Iwmax' ) )then
        read(iunit,*) Iwmax0
      else
        return
      end if
!
!    Read self-energy
!
      if( tag_exist( iunit, 'sigma' ) )then
        allocate( zdum(nlm0,nlm0,Iwmax0,nsp0) )

        do is = 1, nsp0
          do i = 1, nlm0
            do j = i, nlm0
              read(iunit,*,iostat=ierr) ( zdum(i,j,iom,is), iom=1,Iwmax0 )
              if( ierr /= 0 ) return
            end do
          end do
        end do 
      end if

    else
!
!   Old untagged format
!
      rewind(iunit)

      read(iunit,"(2x,i1)",iostat=ierr) nvers
      if( ierr /= 0 ) return

      read(iunit,*,iostat=ierr) Iwmax0, nlm0, nsp0
      if( ierr /= 0 ) return
      if( nlm0 /= nlm )then
        ierr = 888
        return
      end if

      read(iunit,*,iostat=ierr) beta0
      if( ierr /= 0 ) return

      do is = 1, nsp0
        do i = 1, nlm0
          read(iunit,*,iostat=ierr) ( ddum, j = 1,nlm0 )
          if( ierr /= 0 ) return
        end do
      end do   ! is
!
!    Read self-energy
!
      allocate( zdum(nlm0,nlm0,Iwmax0,nsp0) )

      do is = 1, nsp0
        do i = 1, nlm0
          do j = i, nlm0
            read(iunit,*,iostat=ierr) ( zdum(i,j,iom,is), iom=1,Iwmax0 )
            if( ierr /= 0 ) return
          end do
        end do
      end do

    end if

!    Make self-energy paramagnetic if nspin different in input files

    if( nsp == 1 .and. nsp0 == 2 ) zdum(:,:,:,1) = ( zdum(:,:,:,1) + zdum(:,:,:,2) ) / 2

    nspin0 = min(nsp,nsp0)

    if( Iwmax0 >= Iwmax )then
      sigma(:,:,1:Iwmax,1:nspin0)  = zdum(:,:,1:Iwmax,1:nspin0)
    else
      sigma(:,:,1:Iwmax0,1:nspin0) = zdum(:,:,1:Iwmax0,1:nspin0)
      forall( i = Iwmax0+1:Iwmax )   sigma(:,:,i,1:nspin0) = zdum(:,:,Iwmax0,1:nspin0)
    end if

    deallocate( zdum )
!
!    Make sigma symmetric and complete second spin
!
    do i = 1, nlm0-1
      do j = i+1, nlm0
        sigma(j,i,:,:) = sigma(i,j,:,:)
      end do
    end do

    if( nsp == 2 .and. nsp0 == 1 ) sigma(:,:,:,2) = sigma(:,:,:,1)

  end if  !  ifi

end subroutine io_sigma
