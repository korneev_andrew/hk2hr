#!/bin/bash
#

for arg in "$@"
do
  if [[ $arg == "-h" ]]
  then
    echo ' Usage:  scf_data.sh [-h] [filename]'
    echo '   scf_data.sh prints out different information from LDA+DMFT self-consistent run.'
    echo '   Options:'
    echo '     -h       - print this message and exit.'
    echo '     filename - filename from which collect data (default - amulet.out).'
    echo '   Example:'
    echo '     Collect data from file amulet.out_1 located in the_dir_where/I/calculate directory'
    echo '     scf_data.sh the_dir_where/I/calculate/amulet.out_1'
    exit
  fi
done

[ -n "$1" ] && FN=$1 || FN='amulet.out'

if [ ! -e $FN ]
then
  echo ' File ' $FN ' does not exist'
  exit
fi

FNAME=`mktemp $0.XXXXXXX`
cp $FN $FNAME

FILE_1_TMP=`mktemp $0.XXXXXXX`
FILE_2_TMP=`mktemp $0.XXXXXXX`
FILE_3_TMP=`mktemp $0.XXXXXXX`

mu=`awk '/DFT Fermi/,/=DFT====/ {print}' $FNAME | grep Fermi | grep -v DFT | awk '{print $3}'`

qlm=`awk '/DFT Fermi/,/=DFT====/ {print}' $FNAME | grep 'ns per imp' | awk '{print $6}'`

magmom=`awk '/DFT Fermi/,/=DFT====/ {print}' $FNAME | grep Magnetization | awk '{print $2}'`

solver=`grep solver $FNAME | awk '{if ( FNR==1 ) print}'`

nimptype=`grep 'Impurity #' $FNAME | awk 'BEGIN{nimp=1} {if($3>nimp) nimp=$3} END{print nimp}'`

echo '#################################################################'
echo '#'
echo '#                            DFT data'
echo '#'
echo '#  Fermi level , mu = ' $mu
echo '#'
echo '#################################################################'
if [ "$nimptype" != 1 ]; then
  echo '#'
  echo '#      There are ' $nimptype ' types of impurities'
  echo '#'
  echo '#################################################################'
fi

echo '#'
echo '#  Occupation,  qlm = ' $qlm
echo '#  Magnetization, m = ' $magmom
echo '#'

cp $FN $FNAME
awk '/Iteration #   1/,0' $FNAME > $FILE_1_TMP
mv $FILE_1_TMP $FNAME

echo '#################################################################'
echo '#'
echo '#                          LDA+DMFT data'
echo '#'
echo '#      ' $solver
echo '#'
echo '#      Fermi  Occupancy  Magnetic  Double  <m_z^2>'
echo '#      level              moment  counting'
echo '#'


awk '/Iteration #/,/-----/ {if($1~/Fermi/)  {i++; printf "%3i  % 8.5f \n", i, $3}}' $FNAME > $FILE_1_TMP

for (( itype=1; itype<=$nimptype; itype++ ))
do
#     Occupancy
  awk '/Impurity #  '$itype'/,/Magnet/ {if($4~/electrons/) printf "| % 7.4f\n", $5}' $FNAME > $FILE_2_TMP
  paste -d ' ' $FILE_1_TMP $FILE_2_TMP > $FILE_3_TMP
  mv $FILE_3_TMP $FILE_1_TMP

#     Magnetic moment
  awk '/Occupation matrix for impurity #  '$itype'/,/Magnet/ {if($1~/Magnet/) printf "% 7.4f\n", $2}' $FNAME > $FILE_2_TMP
  paste -d ' ' $FILE_1_TMP $FILE_2_TMP > $FILE_3_TMP
  mv $FILE_3_TMP $FILE_1_TMP

#     Double counting value
  awk '/Impurity #  '$itype'/,/Double counting value/ {if($3~/value/) printf "% 7.4f\n", $4}' $FNAME > $FILE_2_TMP
  paste -d ' ' $FILE_1_TMP $FILE_2_TMP > $FILE_3_TMP
  mv $FILE_3_TMP $FILE_1_TMP

#     < m_z^2 >
  awk '/Impurity #  '$itype'/,/Instant squared/ {if($1~/Instant/) printf "% 7.4f \n", $8}' $FNAME > $FILE_2_TMP
  paste -d ' ' $FILE_1_TMP $FILE_2_TMP > $FILE_3_TMP
  mv $FILE_3_TMP $FILE_1_TMP
done

cat -T $FILE_1_TMP

rm $FNAME $FILE_1_TMP $FILE_2_TMP
