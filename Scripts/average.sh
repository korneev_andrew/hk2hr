#!/bin/bash
#
#   This script average NITER number of iterations from end of file (mask *.dat)
#   

if [[ $1 = *h ]]
then
  echo "Usage: average.sh NITER NW"
  echo "  NITER - number of iterations to average from end of file (default=1)"
  echo "  NW    - number of energy points in one iteration (default=self-defined)"
  echo "  Files must have an *.dat extension"
  echo "  WARNING: ALL *.dat file will be overwritten"
  exit
fi

TMPFILE=`mktemp $0.XXXXXXX`
TMPFILE1=`mktemp $0.XXXXXXX`
TMPFILE2=`mktemp $0.XXXXXXX`

#   Set number of iterations to average
[ -n "$1" ] && NITER=$1 || NITER=1

FILELIST=`ls *.dat`

for ifile in $FILELIST
  do

#   Exist and regular file
  [ ! -e $ifile -o ! -f $ifile ] && continue

  if [[ $ifile = *Moments.dat ]]
  then
#    echo " Tail the last iteration of the Moment file (omega)", $ifile
    NDIM=`awk '{if ( FNR==1 ) print ($1+1)*7 }' $ifile`
    tail -n$NDIM $ifile > $TMPFILE
    cat $TMPFILE > $ifile
  elif [[ $ifile = *M?.dat ]]
  then
#    echo " Tail the last iteration of the Moment file (tau)", $ifile
    NDIM=`awk '{if ( FNR==1 ) print ($1+1) }' $ifile`
    tail -n$NDIM $ifile > $TMPFILE
    cat $TMPFILE > $ifile
  else
#    Define number of Matsubara frequencies
    [ -n "$2" ] && NW=$2 || NW=`awk '{if( NF == 0 ) n+=1} END{print 2*NR/n-2}' $ifile`

#    Define number of lines and columns in the file
    NLINES=`awk 'END { print NR }' $ifile`
    NCOLS=`awk '{if ( FNR==3 ) print NF}' $ifile`

    let "NDIM=$NW + 2"
    if [ "$NLINES" -lt "$NDIM" ]
    then
      echo "Problem with length of file"
      continue
    fi

#    Tail last iteration
    tail -n$NDIM $ifile > $TMPFILE

    let "AA=$NITER*$NDIM"
    [ "$NLINES" -lt "$AA" ] && NITER=1

#    echo $NLINES $NDIM $NITER $NW $NCOLS

#    Sum iterations if NITER>1
    for (( i=1; i < NITER ; i++)) 
    do
      let "istart=$NLINES - ($i+1)*$NDIM+1"
      let "iend=$NLINES   - ($i-0)*$NDIM"
    #  awk -v istart="$istart" iend="$iend" 'FNR==$istart, FNR==$iend {print}' $ifile # > $TMPFILE3
      awk 'FNR=='$istart', FNR=='$iend' {print}' $ifile > $TMPFILE1
      paste $TMPFILE $TMPFILE1 > $TMPFILE2
      if   [ "$NCOLS" -eq 2 ]; then
        awk '{OFMT="%.20g"; if (NF==0) print; else print $1, $2+$4 }' $TMPFILE2 > $TMPFILE
      elif [ "$NCOLS" -eq 3 ]; then
        awk '{OFMT="%.20g"; if (NF==0) print; else print $1, $2+$5, $3+$6 }' $TMPFILE2 > $TMPFILE
      elif [ "$NCOLS" -eq 5 ]; then
        awk '{OFMT="%.20g"; if (NF==0) print; else print $1, $2+$7, $3+$8, $4+$9, $5+$10 }' $TMPFILE2 > $TMPFILE
      else
	echo "Not implemented"
      fi
    done

    if   [ "$NCOLS" -eq 2 ]; then
      awk '{OFMT="%.20g"; if (NF==0) print; else print $1, $2/'$NITER' }' $TMPFILE > $ifile
    elif [ "$NCOLS" -eq 3 ]; then
      awk '{OFMT="%.20g"; if (NF==0) print; else print $1, $2/'$NITER', $3/'$NITER' }' $TMPFILE > $ifile
    elif [ "$NCOLS" -eq 5 ]; then
      awk '{OFMT="%.20g"; if (NF==0) print; else print $1, $2/'$NITER', $3/'$NITER', $4/'$NITER', $5/'$NITER' }' $TMPFILE > $ifile
    else
      echo "Not implemented"
    fi

  fi 

done

rm -f $TMPFILE $TMPFILE1 $TMPFILE2 
