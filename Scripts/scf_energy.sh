#!/bin/bash
#
FN=$*
NSTAT=0
shopt -s extglob
#set -xv

MFMT1="%3i  % 8.5f \n"
MFMT2="% 8.5f \n"
MFMT3="| % 7.4f\n"
MFMT4="% 7.4f\n"

for arg in "$@"
do
  if [[ $arg == "-h" ]]
  then
    echo ' Usage:  scf_energy.sh [-h] [-s[=N]] [-u] [filename]'
    echo '   scf_energy.sh prints out different information from LDA+DMFT self-consistent run.'
    echo '   Options:'
    echo '     -h       - print this message and exit.'
    echo '     -s[=N]   - evaluate mean values and variances over last N iterations.'
    echo '                It uses all iterations if N is absent.'
    echo '     -u       - print unformated data (no pretty-printing but more accurate).'
    echo '     filename - filename to parse (default - amulet.out).'
    echo '   Example:'
    echo '     Collect data from file amulet.out_1 located in the_dir_where/I/calculate directory'
    echo '     scf_energy.sh the_dir_where/I/calculate/amulet.out_1'
    exit
  fi

  if [[ "$arg" == -s ]]
  then
    NSTAT=1000
    FN=${FN//$arg}
  fi

  if [[ "$arg" == -s=* ]]
  then
    NSTAT=${arg#-s=}
    FN=${FN//$arg}
  fi

  if [[ "$arg" == -u ]]
  then
    MFMT1="%3i  % s \n"
    MFMT2="% s \n"
    MFMT3="| % s\n"
    MFMT4="% s\n"
    FN=${FN//$arg}
  fi

done

FN=${FN//+([[:space:]])/}

if [[ -z "$FN" ]]
then
  FN='amulet.out'
fi

#echo '111'$FN'111'
#echo $MFMT1
#echo '222'$NSTAT'222'

if [ ! -e "$FN" ]
then
  echo ' File '$FN' does not exist. Run scf_energy.sh -h for help.'
  exit
fi

#exit

#===============================================================================
#  Functions

function statistics ()
{
  FNM=$1
  COL=$2

  MEAN=`awk '{ mean += $'$COL'; n++ }; END{ printf "%.12g", mean/n }' $FNM`
  ABSMEAN=${MEAN#-}

  if [ "$MEAN" == "$ABSMEAN" ]; then
    SDEV=`awk '{ sd += ($'$COL'-'$MEAN')*($'$COL'-'$MEAN'); n++ }; END{ printf "% .12g\n", sqrt(sd/n) }' $FNM`
  else
    SDEV=`awk '{ sd += ($'$COL'+'$ABSMEAN')*($'$COL'+'$ABSMEAN'); n++ }; END{ printf "% .12g\n", sqrt(sd/n) }' $FNM`
  fi

  echo "# Mean value " $MEAN " Standart deviation " $SDEV
}

#===============================================================================


FNAME=`mktemp $0.XXXXXXX`
cp $FN $FNAME

FILE_1_TMP=`mktemp $0.XXXXXXX`
FILE_2_TMP=`mktemp $0.XXXXXXX`
FILE_3_TMP=`mktemp $0.XXXXXXX`

mu=`awk '/DFT Fermi/,/=DFT====/ {print}' $FNAME | grep Fermi | grep -v DFT | awk '{print $3}'`

beta=`awk '/Inverse temperature/ {print $3}' $FNAME`

t1=`awk '/Temperature    / {print $2}' $FNAME`

t2=`awk '/Temperature \(in K\)/ {print $4}' $FNAME`

kinen=`awk '/DFT contributions to total energy/,/=DFT====/ {print}' $FNAME | grep Kinetic | awk '{print $3}'`

qlm=`awk '/DFT Fermi/,/=DFT====/ {print}' $FNAME | grep 'ns per imp' | awk '{print $6}'`

magmom=`awk '/DFT Fermi/,/=DFT====/ {print}' $FNAME | grep Magnetization | awk '{print $2}'`

solver=`grep solver $FNAME | awk '{if ( FNR==1 ) print}'`

nimptype=`grep 'Impurity #' $FNAME | awk 'BEGIN{nimp=1} {if($3>nimp) nimp=$3} END{print nimp}'`

echo '####################################################################'
echo '#'
echo '#                            DFT data'
echo '#'
echo '#  Fermi level,           mu = ' $mu
echo '#  Inverse temperature, beta = ' $beta
echo '#  Temperature,        k_B T = ' $t1 
echo '#  Temperature (in K),     T = ' $t2
echo '#  Kinetic energy,         K = ' $kinen
echo '#'
echo '####################################################################'
if [ "$nimptype" != 1 ]; then
  echo '#'
  echo '#      There are ' $nimptype ' types of impurities'
  echo '#'
  echo '####################################################################'
fi

echo '#'
echo '#  Occupation,   qlm = ' $qlm
echo '#  Magnetization,  m = ' $magmom
echo '#'

cp $FN $FNAME
awk '/Iteration #   1/,0' $FNAME > $FILE_1_TMP
mv $FILE_1_TMP $FNAME

echo '####################################################################'
echo '#'
echo '#                          LDA+DMFT data'
echo '#'
echo '#      ' $solver
echo '#'
echo '#      Fermi    Kinetic   Occupancy   DC     TrSG    U<nn>  U<n><n>'
echo '#      level    energy              energy'
echo '#'

#  Info about Fermi level
awk -v mfrmt="$MFMT1" '/Iteration #/,/-----/ {if($1~/Fermi/)  {i++; printf mfrmt, i, $3}}' $FNAME > $FILE_1_TMP

#  Kinetic energy
awk -v mfrmt="$MFMT2" '/Iteration #/,/Kinetic/ {if($1~/Kinetic/)  {printf mfrmt, $3}}' $FNAME > $FILE_2_TMP
paste -d ' ' $FILE_1_TMP $FILE_2_TMP > $FILE_3_TMP
mv $FILE_3_TMP $FILE_1_TMP


for (( itype=1; itype<=$nimptype; itype++ ))
do
#     Occupancy from impurity solver
  awk -v mfrmt="$MFMT3" '/Impurity #  '$itype'/,/Magnet/ {if($4~/electrons/) printf mfrmt, $5}' $FNAME > $FILE_2_TMP
  paste -d ' ' $FILE_1_TMP $FILE_2_TMP > $FILE_3_TMP
  mv $FILE_3_TMP $FILE_1_TMP

#     Double counting energy
  awk -v mfrmt="$MFMT4" '/Impurity type   '$itype'/,/Double-counting energy/ {if($1~/Double-counting/) printf mfrmt, $3}' $FNAME > $FILE_2_TMP
  paste -d ' ' $FILE_1_TMP $FILE_2_TMP > $FILE_3_TMP
  mv $FILE_3_TMP $FILE_1_TMP

#     Tr Sigma G
  awk -v mfrmt="$MFMT4" '/Impurity type   '$itype'/,/Double-counting energy/ {if($1~/Tr/) printf mfrmt, $4}' $FNAME > $FILE_2_TMP
  paste -d ' ' $FILE_1_TMP $FILE_2_TMP > $FILE_3_TMP
  mv $FILE_3_TMP $FILE_1_TMP

#     U <nn>
  awk -v mfrmt="$MFMT4" '/Impurity #  '$itype'/,/U<nn>/ {if($1~/Coulomb/) printf mfrmt, $5}' $FNAME > $FILE_2_TMP
  paste -d ' ' $FILE_1_TMP $FILE_2_TMP > $FILE_3_TMP
  mv $FILE_3_TMP $FILE_1_TMP

#     U <n><n>
  awk -v mfrmt="$MFMT4" '/Impurity #  '$itype'/,/U<n>/ {if($1~/Hartree-Fock/) printf mfrmt, $4}' $FNAME > $FILE_2_TMP
  paste -d ' ' $FILE_1_TMP $FILE_2_TMP > $FILE_3_TMP
  mv $FILE_3_TMP $FILE_1_TMP

done

cat -T $FILE_1_TMP

if [[ $NSTAT > 0 ]]
then
  NFULL=`awk '{ if( FNR == 1 ) NSTART=NF }; END{ if( NSTART == NF ) print NR; else print NR-1 }' $FILE_1_TMP`

  if (( "$NSTAT" > "$NFULL" ))
  then
    NSTAT=$NFULL
  fi
  head -n$NFULL $FILE_1_TMP | tail -n$NSTAT > $FILE_2_TMP

  echo
  echo
  echo '####################################################################'
  echo '#'
  echo '#    Statistics over ' $NSTAT ' last iterations'
  echo '#'
  echo '####################################################################'

  echo '#'
  echo '#   Fermi energy'
  statistics $FILE_2_TMP 2

  echo '#'
  echo '#   Kinetic energy'
  statistics $FILE_2_TMP 3

  kinen=${kinen/e/*10^}
  MEAN=${MEAN/e/*10^}
  SDEV=${SDEV/e/*10^}

  etot1=`echo "($MEAN)-($kinen)" | bc -l`
  meto1=`echo "($SDEV)^2" | bc -l`

  etot2=$etot1
  meto2=$meto1

#   Info about impurity quantities
#

  echo '####################################################################'

  for (( itype=1; itype<=$nimptype; itype++ ))
  do
    echo '#'
    echo '#    Impurity number ' $itype
    echo '#'
    echo '#   Number of particles'
    #statistics $FILE_2_TMP 5
    statistics $FILE_2_TMP `echo "5+($itype-1)*6" | bc -l`

    echo '#'
    echo '#   Double-counting energy'
    statistics $FILE_2_TMP `echo "6+($itype-1)*6" | bc -l`

    MEAN=${MEAN/e/*10^}
    SDEV=${SDEV/e/*10^}

    etot1=`echo "($etot1)-($MEAN)" | bc -l`
    meto1=`echo "($meto1)+($SDEV)^2" | bc -l`

    echo '#'
    echo '#   Trace Sigma G'
    statistics $FILE_2_TMP `echo "7+($itype-1)*6" | bc -l`

    MEAN=${MEAN/e/*10^}
    SDEV=${SDEV/e/*10^}

    etot2=`echo "($etot2)+($MEAN)" | bc -l`
    meto2=`echo "($meto2)+($SDEV)^2" | bc -l`

    echo '#'
    echo '#   U<nn>'
    statistics $FILE_2_TMP `echo "8+($itype-1)*6" | bc -l`

    MEAN=${MEAN/e/*10^}
    SDEV=${SDEV/e/*10^}

    etot1=`echo "($etot1)+($MEAN)" | bc -l`
    meto1=`echo "($meto1)+($SDEV)^2" | bc -l`

    echo '#'
    echo '#   U<n><n>'
    statistics $FILE_2_TMP `echo "9+($itype-1)*6" | bc -l`

    echo '#-------------------------------------------------------------------'
  done

  meto1=`echo "sqrt($meto1)" | bc -l`
  meto2=`echo "sqrt($meto2)" | bc -l`

  echo '####################################################################'
  echo '#'
  echo '#         DMFT correction to total energy (I)'
  echo '#     E(DMFT) = Kin(DMFT)-Kin(DFT)+U<nn>-Edc'
  echo '#  E(DMFT) = ' $etot1 '  Standart deviation ' $meto1
  echo '#'
  echo '#         DMFT correction to total energy (II)'
  echo '#     E(DMFT) = Kin(DMFT)-Kin(DFT)+TrSigmaG'
  echo '#  E(DMFT) = ' $etot2 '  Standart deviation ' $meto2

fi

rm $FNAME $FILE_1_TMP $FILE_2_TMP
