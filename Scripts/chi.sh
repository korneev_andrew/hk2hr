#!/bin/bash
#

#   Default values
#  Calculate spin (yes, default) or charge (no) susceptibility
CHARGE=no
#  Default filename start. Files should be named FNAME_i,j.dat, where i and j belongs to NLM
FNAME=ninj_1
#  Orbital indexes
NLM=$*

#   Check positional parameters and set variables
for arg in "$@"
do
  if [[ $arg == "-h" ]]
  then
    echo ' Usage:  chi.sh [option] [orbital indices]'
    echo '  chi.sh calculates spin or charge susceptibility from fmask_i,j.dat files'
    echo '  Options:'
    echo '    -h         - print this help message and exit'
    echo '    -c         - calculate charge susceptibility (default - spin)'
    echo '    -fm=fmask  - mask of input files (default "ninj_1")'
    echo '  Orbital indices - set of orbital indices to use at calculating susceptibility.'
    echo '    By default all files with proper fmask will be used.'
    echo '  Examples:'
    echo '     Calculate charge susceptibility using ninj_1_i,j.dat files as input'
    echo '    chi.sh -c'
    echo '     Calculate spin susceptibility using nn_1,1.dat, nn_1,2.dat, nn_1,4.dat,'
    echo '     nn_2,2.dat, nn_2,4.dat and nn_4,4.dat. It is assumed that nn_i,j.dat = nn_j,i.dat'
    echo '    chi.sh  1 2 4 -fm=nn'
    echo
    exit
  fi

  if [[ $arg == "-c" ]]
  then
    CHARGE=yes
    NLM=${NLM//-c}
  fi

  if [[ "$arg" == -fm=* ]]
  then
    FNAME=${arg#-fm=}
    NLM=${NLM//$arg}
  fi
done 

NLM=${NLM## }
NLM=${NLM%% }

#echo ' CHARGE=' $CHARGE
#echo ' FNAME =' $FNAME
#echo ' NLM   =' $NLM ' lenght ' ${#NLM}

#   Set number of orbitals to use for susceptibility

if [ -z "$NLM" -o "$NLM" = " " ]
then
  for (( i=1; i <= 14; i++ ))
  do
    [ -e ${FNAME}_$i,$i.dat ] && NLM+=$i' '
  done
fi
unset i

TMP1=`mktemp $0.XXXXXXX`
TMP2=`mktemp $0.XXXXXXX`
TMPSUM=`mktemp $0.XXXXXXX`

#   Calculate spin (charge) susceptibility for given orbitals

for i in $NLM
do
  for j in $NLM
  do
    if [ -e ${FNAME}_$i,$j.dat ]
    then
      cp ${FNAME}_$i,$j.dat $TMP1
    else
      cp ${FNAME}_$j,$i.dat $TMP1
    fi

    if [ "$CHARGE" = "no" ]
    then
      awk '{ if( NF > 0 ) print $2-$3-$4+$5 }' $TMP1 > $TMP2
    else
      awk '{ if( NF > 0 ) print $2+$3+$4+$5 }' $TMP1 > $TMP2
    fi

    paste $TMPSUM $TMP2 | awk '{ print $1+$2 }' > $TMP1
    cat $TMP1 > $TMPSUM
  done
done

#    Create chi.dat

paste ${FNAME}_$i,$j.dat $TMPSUM | awk '{ if( NF > 0 ) print $1, $6 }' > chi.dat

rm -f $TMP1 $TMP2 $TMPSUM
