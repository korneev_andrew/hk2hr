include make.arch

DIRS = src Libs/ARPACK/src Libs/ARPACK/mpi Solvers/CTQMCHYB

LIBOBJ = 
LIB_DEPS = 

TAR_FLAGS := $(findstring --exclude-vcs,$(shell tar --help | grep "\-\-exclude\-vcs"))

#   Defines variable MAKE if it is not defined
ifeq ($(MAKE),)
  MAKE := make
endif

ifneq ($(USE_MPI),)
  LIB_DEPS += parpacklib
  LIBOBJ += $(PWD)/Libs/ARPACK/mpi/libparpack.a
endif

ifeq ($(ARPACK),)
  LIB_DEPS += arpacklib
  LIBOBJ += $(PWD)/Libs/ARPACK/src/libarpack.a
else
  LIBOBJ += $(ARPACK)
endif

ifeq ($(CTHYB),)
  LIB_DEPS += ctqmchyblib
  LIBOBJ += $(PWD)/Solvers/CTQMCHYB/libcthyb.a
else
  LIBOBJ += $(CTHYB)
endif

#LIBOBJ += $(LAPACK)

################################################################################
#
amulet: amuletlib $(LIB_DEPS)
	$(FC) -o $@ src/libamulet.a $(LIBOBJ) $(LFLAGS)
	mv $@ $(INSTALLDIR)/$@$(SUFF)
	- $(RM) *__genmod.*
#
all: amulet tools
#
clean:
	$(MAKE) -C src clean
#
distclean:
	for d in $(DIRS);            \
	do                           \
	   $(MAKE) -C $$d distclean; \
	done
	- $(RM) *.mod *.a *.so *.d work* core* */*.o *__genmod.f90 *.gz
#
pack: clean
	- $(RM) src.tar*
	tar $(TAR_FLAGS) -cf src.tar src Tools Makefile make.arch
	gzip -9 src.tar
#
#allpack: distclean rev_info
allpack: distclean 
	rm -f *.tar*
	tar $(TAR_FLAGS) --exclude=*.tex* --ignore-case -cf AMULET.tar *
	gzip -9 AMULET.tar
#
ctqmchyblib:
	$(MAKE) -C Solvers/CTQMCHYB
#
arpacklib:
	$(MAKE) -C Libs/ARPACK/src	
#
parpacklib:
	$(MAKE) -C Libs/ARPACK/mpi
#
amuletlib:
	$(MAKE) -C src
#
tools: amuletlib
	$(MAKE) -C Tools
#
akw: amuletlib
	$(MAKE) -C Tools $@
#
dmft_dos: amuletlib
	$(MAKE) -C Tools $@
#
green_function: amuletlib
	$(MAKE) -C Tools $@
#
hk2hr: amuletlib
	$(MAKE) -C Tools $@
